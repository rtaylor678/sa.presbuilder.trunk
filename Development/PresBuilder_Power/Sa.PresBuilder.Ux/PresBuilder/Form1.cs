﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Microsoft.Office.Core;
using ppt = Microsoft.Office.Interop.PowerPoint;

namespace PresBuilder
{
    public partial class Form1 : Form
	{

        Int32 workingSlide;
        String ErrorMsg;

        public Form1()
		{
			InitializeComponent();
			this.saveLocation.Text = Properties.Settings.Default.SaveTo;
			this.TemplateToUse.Text = Properties.Settings.Default.Template;
			this.Option1.Text = Properties.Settings.Default.Option1;
			this.Option2.Text = Properties.Settings.Default.Option2;
			this.Option3.Text = Properties.Settings.Default.Option3;
			this.Option4.Text = Properties.Settings.Default.Option4;
			this.Option5.Text = Properties.Settings.Default.Option5;
			this.currencyCode.Text = Properties.Settings.Default.Currency;
			if (Properties.Settings.Default.USUnits == true)
			{
				this.rdoUSUnits.Checked = true;
			}
			else
			{
				this.rdoMetricUnits.Checked = true;
			}
			this.server.Text = Properties.Settings.Default.ServerName;
			this.database.Text = Properties.Settings.Default.DBName;
            this.option1label.Text = Properties.Settings.Default.Option1Name;
            this.option2label.Text = Properties.Settings.Default.Option2Name;
            this.option3label.Text = Properties.Settings.Default.Option3Name;
            this.option4label.Text = Properties.Settings.Default.Option4Name;
            this.option5label.Text = Properties.Settings.Default.Option5Name;
			this.chkOpenPres.Checked = Properties.Settings.Default.OpenPres;

			this.lblVersion.Text = Application.ProductVersion;
        }

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void button2_Click(object sender, EventArgs e)
		{
			saveFileDialog1.Filter = "Powerpoint (*.pptx)|*.pptx|All files (*.*)|*.*";
			if (saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				saveLocation.Text = saveFileDialog1.FileName;
			}
			
		}

		private void changeTemplate_Click(object sender, EventArgs e)
		{
			openFileDialog1.Filter = "Powerpoint (*.pptx)|*.pptx|All files (*.*)|*.*";
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				TemplateToUse.Text = openFileDialog1.FileName;
			}

		}

		private void button1_Click(object sender, EventArgs e)
		{
			errorLabel.Text = "";//just reset it

			//check for a valid save location
			String filepath = saveLocation.Text;//where to save to
			String fileResult = CheckFileName(filepath);

			if (fileResult != "")
			{
				errorLabel.Text = fileResult;
				return;
			}

			//if the file path doesn't end with .pptx we append it here (after checking the rest for validity)
			//may end up with something like .ppt.pptx but that's okay, that's user error and we can't catch everything
			if (!filepath.EndsWith(".pptx"))
			{
				saveLocation.Text = saveLocation.Text + ".pptx";
				filepath = saveLocation.Text;
			}

			//check for a valid template file
			String template = TemplateToUse.Text;//the template to use

			fileResult = CheckFileExists(template, "Template");
			if (fileResult != "")
			{
				errorLabel.Text = fileResult;
				return;
			}

			if (server.Text == "")
			{
				errorLabel.Text = "Need server name";
				return;
			}

			if (database.Text == "")
			{
				errorLabel.Text = "Need database name";
				return;
			}

			errorLabel.Text = "Presentation started";

			btnRunPresentation.Enabled = false;

			//create the object and populate it with data from the form

			Sa.PresBuilder.PresBuilder instance = new Sa.PresBuilder.PresBuilder();
			instance.templateToUse = TemplateToUse.Text;
			instance.filenameToSaveAs = saveLocation.Text;
			instance.Option1 = Option1.Text;
			instance.Option2 = Option2.Text;
			instance.Option3 = Option3.Text;
			instance.Option4 = Option4.Text;
			instance.Option5 = Option5.Text;
			instance.CurrencyToUse = currencyCode.Text;
			instance.Metric = rdoMetricUnits.Checked;
			instance.server = server.Text;
			instance.database = database.Text;
            instance.slideid = txtSlideID.Text;
      

			//then run it as a background worker (so we can display the progress)
			backgroundWorker1.RunWorkerAsync(instance);


		}

		private String CheckFileName(String fileName, String source = "")
		{
			//just needs to be a name we can save to, whether it exists or not
			System.IO.FileInfo fi = null;
			try
			{
				fi = new System.IO.FileInfo(fileName);
			}
			catch (Exception) { return "ERROR: " + source + " filename is not valid."; }

			if (fi.Name == "")
			{
				return "ERROR: " + source + " filename is not valid.";
			}

			if (ReferenceEquals(fi, null))
			{
				// file name is not valid
				return "ERROR: " + source + " filename is not valid.";
			}
				
			// file name is valid
			return "";

		}

		private String CheckFileExists(String fileName, String source = "")
		{
			//must exist already
			System.IO.FileInfo fi = null;
			try
			{
				fi = new System.IO.FileInfo(fileName);
			}
			catch (Exception) { return "ERROR: " + source + " filename is not valid."; }

			if (fi.Exists == false)
			{
				return "ERROR: " + source + " file does not exist.";
			}

			//file exists
			return "";
		}

		private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			btnRunPresentation.Enabled = true;
            if (e.Error != null)
            {
                errorLabel.Text = "Slide " + (workingSlide + 1).ToString() + " error: " + e.Error.Message + e.Error.TargetSite + Environment.NewLine + ErrorMsg.Replace("|", "");
            }
            else
            {
                errorLabel.Text = "Complete";
				if (this.chkOpenPres.Checked == true)
				{
					ppt.Application app = new ppt.Application();
					ppt.Presentations pres = app.Presentations;
					try
					{
						pres.Open(this.saveLocation.Text, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoTrue);
					}
					catch (System.Runtime.InteropServices.COMException)
					{
						errorLabel.Text = "Program completed, but there was an error in the generated PPT.";
					}
				}
            }
		}

		private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{

			Sa.PresBuilder.PresBuilder.CurrentState state = (Sa.PresBuilder.PresBuilder.CurrentState)e.UserState;
            ErrorMsg = state.errMsg;

            if (state.status == "")
			{
				errorLabel.Text = (state.workingSlideNumber+1).ToString() + " of " + state.totalSlides.ToString();
				workingSlide = state.workingSlideNumber;
			}
			else
			{
				errorLabel.Text = state.status;
			}

		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
			System.ComponentModel.BackgroundWorker worker;
			worker = (System.ComponentModel.BackgroundWorker)sender;

			Sa.PresBuilder.PresBuilder wkg = (Sa.PresBuilder.PresBuilder)e.Argument;
			wkg.CreatePresFromTemplate(worker, e);

		}

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{

			//save all the settings
			Properties.Settings.Default.SaveTo = this.saveLocation.Text;
			Properties.Settings.Default.Template = this.TemplateToUse.Text;
			Properties.Settings.Default.Option1 = this.Option1.Text;
			Properties.Settings.Default.Option2 = this.Option2.Text;
			Properties.Settings.Default.Option3 = this.Option3.Text;
			Properties.Settings.Default.Option4 = this.Option4.Text;
			Properties.Settings.Default.Option5 = this.Option5.Text;
			Properties.Settings.Default.Currency = this.currencyCode.Text;
			Properties.Settings.Default.ServerName = this.server.Text;
			Properties.Settings.Default.DBName = this.database.Text;
			Properties.Settings.Default.USUnits = this.rdoUSUnits.Checked;//because we're tracking if the USUnits are checked or not

            Properties.Settings.Default.Option1Name = this.option1label.Text;
            Properties.Settings.Default.Option2Name = this.option2label.Text;
            Properties.Settings.Default.Option3Name = this.option3label.Text;
            Properties.Settings.Default.Option4Name = this.option4label.Text;
            Properties.Settings.Default.Option5Name = this.option5label.Text;
			Properties.Settings.Default.OpenPres = this.chkOpenPres.Checked;

			Properties.Settings.Default.Save();

		}

		private void chkOpenPres_CheckedChanged(object sender, EventArgs e)
		{

		}

	}
}
