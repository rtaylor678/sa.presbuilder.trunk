﻿namespace PresBuilder
	{
	partial class Form1
		{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
			{
			if (disposing && (components != null))
				{
				components.Dispose();
				}
			base.Dispose(disposing);
			}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
			{
            this.label1 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rdoUSUnits = new System.Windows.Forms.RadioButton();
            this.rdoMetricUnits = new System.Windows.Forms.RadioButton();
            this.btnRunPresentation = new System.Windows.Forms.Button();
            this.changeSaveLocation = new System.Windows.Forms.Button();
            this.saveLocation = new System.Windows.Forms.TextBox();
            this.changeTemplate = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TemplateToUse = new System.Windows.Forms.TextBox();
            this.Option1 = new System.Windows.Forms.TextBox();
            this.Option2 = new System.Windows.Forms.TextBox();
            this.Option3 = new System.Windows.Forms.TextBox();
            this.currencyCode = new System.Windows.Forms.TextBox();
            this.errorLabel = new System.Windows.Forms.Label();
            this.Option4 = new System.Windows.Forms.TextBox();
            this.Option5 = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.server = new System.Windows.Forms.TextBox();
            this.database = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.option1label = new System.Windows.Forms.TextBox();
            this.option2label = new System.Windows.Forms.TextBox();
            this.option3label = new System.Windows.Forms.TextBox();
            this.option4label = new System.Windows.Forms.TextBox();
            this.option5label = new System.Windows.Forms.TextBox();
            this.chkOpenPres = new System.Windows.Forms.CheckBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblSlideID = new System.Windows.Forms.Label();
            this.txtSlideID = new System.Windows.Forms.TextBox();
            this.pnlSlideID = new System.Windows.Forms.Panel();
            this.pnlSlideID.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Save To:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 73);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Template To Use:";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 373);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Currency:";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 424);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 25);
            this.label7.TabIndex = 6;
            this.label7.Text = "US or Metric:";
            // 
            // rdoUSUnits
            // 
            this.rdoUSUnits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rdoUSUnits.AutoSize = true;
            this.rdoUSUnits.Checked = true;
            this.rdoUSUnits.Location = new System.Drawing.Point(161, 421);
            this.rdoUSUnits.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoUSUnits.Name = "rdoUSUnits";
            this.rdoUSUnits.Size = new System.Drawing.Size(107, 29);
            this.rdoUSUnits.TabIndex = 11;
            this.rdoUSUnits.TabStop = true;
            this.rdoUSUnits.Text = "US units";
            this.rdoUSUnits.UseVisualStyleBackColor = true;
            // 
            // rdoMetricUnits
            // 
            this.rdoMetricUnits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rdoMetricUnits.AutoSize = true;
            this.rdoMetricUnits.Location = new System.Drawing.Point(261, 421);
            this.rdoMetricUnits.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoMetricUnits.Name = "rdoMetricUnits";
            this.rdoMetricUnits.Size = new System.Drawing.Size(132, 29);
            this.rdoMetricUnits.TabIndex = 12;
            this.rdoMetricUnits.TabStop = true;
            this.rdoMetricUnits.Text = "Metric units";
            this.rdoMetricUnits.UseVisualStyleBackColor = true;
            // 
            // btnRunPresentation
            // 
            this.btnRunPresentation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRunPresentation.Location = new System.Drawing.Point(795, 493);
            this.btnRunPresentation.Name = "btnRunPresentation";
            this.btnRunPresentation.Size = new System.Drawing.Size(200, 52);
            this.btnRunPresentation.TabIndex = 17;
            this.btnRunPresentation.Text = "Run Presentation";
            this.btnRunPresentation.UseVisualStyleBackColor = true;
            this.btnRunPresentation.Click += new System.EventHandler(this.button1_Click);
            // 
            // changeSaveLocation
            // 
            this.changeSaveLocation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.changeSaveLocation.Location = new System.Drawing.Point(815, 19);
            this.changeSaveLocation.Name = "changeSaveLocation";
            this.changeSaveLocation.Size = new System.Drawing.Size(180, 31);
            this.changeSaveLocation.TabIndex = 2;
            this.changeSaveLocation.Text = "Change Save Location";
            this.changeSaveLocation.UseVisualStyleBackColor = true;
            this.changeSaveLocation.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveLocation
            // 
            this.saveLocation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.saveLocation.Location = new System.Drawing.Point(161, 21);
            this.saveLocation.Name = "saveLocation";
            this.saveLocation.Size = new System.Drawing.Size(637, 30);
            this.saveLocation.TabIndex = 1;
            this.saveLocation.Text = "C:\\Users\\cm\\Desktop\\TestPres.pptx";
            // 
            // changeTemplate
            // 
            this.changeTemplate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.changeTemplate.Location = new System.Drawing.Point(815, 69);
            this.changeTemplate.Name = "changeTemplate";
            this.changeTemplate.Size = new System.Drawing.Size(180, 31);
            this.changeTemplate.TabIndex = 4;
            this.changeTemplate.Text = "Change Template";
            this.changeTemplate.UseVisualStyleBackColor = true;
            this.changeTemplate.Click += new System.EventHandler(this.changeTemplate_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TemplateToUse
            // 
            this.TemplateToUse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TemplateToUse.Location = new System.Drawing.Point(161, 70);
            this.TemplateToUse.Name = "TemplateToUse";
            this.TemplateToUse.Size = new System.Drawing.Size(637, 30);
            this.TemplateToUse.TabIndex = 3;
            this.TemplateToUse.Text = "K:\\STUDY\\Power\\2016\\Presentations\\Template\\16 Power Template.pptx";
            // 
            // Option1
            // 
            this.Option1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option1.Location = new System.Drawing.Point(161, 121);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(456, 30);
            this.Option1.TabIndex = 5;
            this.Option1.Text = "Power17: Saudi Aramco";
            // 
            // Option2
            // 
            this.Option2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option2.Location = new System.Drawing.Point(161, 172);
            this.Option2.Name = "Option2";
            this.Option2.Size = new System.Drawing.Size(456, 30);
            this.Option2.TabIndex = 6;
            this.Option2.Text = "Power 16";
            // 
            // Option3
            // 
            this.Option3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option3.Location = new System.Drawing.Point(161, 221);
            this.Option3.Name = "Option3";
            this.Option3.Size = new System.Drawing.Size(456, 30);
            this.Option3.TabIndex = 7;
            this.Option3.Text = "2017";
            // 
            // currencyCode
            // 
            this.currencyCode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.currencyCode.Location = new System.Drawing.Point(161, 372);
            this.currencyCode.Name = "currencyCode";
            this.currencyCode.Size = new System.Drawing.Size(164, 30);
            this.currencyCode.TabIndex = 10;
            this.currencyCode.Text = "USD";
            // 
            // errorLabel
            // 
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(24, 454);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(750, 90);
            this.errorLabel.TabIndex = 18;
            // 
            // Option4
            // 
            this.Option4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option4.Location = new System.Drawing.Point(161, 272);
            this.Option4.Name = "Option4";
            this.Option4.Size = new System.Drawing.Size(456, 30);
            this.Option4.TabIndex = 8;
            // 
            // Option5
            // 
            this.Option5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option5.Location = new System.Drawing.Point(161, 321);
            this.Option5.Name = "Option5";
            this.Option5.Size = new System.Drawing.Size(456, 30);
            this.Option5.TabIndex = 9;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // server
            // 
            this.server.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.server.Location = new System.Drawing.Point(883, 329);
            this.server.Name = "server";
            this.server.Size = new System.Drawing.Size(112, 30);
            this.server.TabIndex = 13;
            // 
            // database
            // 
            this.database.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.database.Location = new System.Drawing.Point(883, 361);
            this.database.Name = "database";
            this.database.Size = new System.Drawing.Size(112, 30);
            this.database.TabIndex = 14;
            // 
            // username
            // 
            this.username.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.username.Location = new System.Drawing.Point(883, 393);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(112, 30);
            this.username.TabIndex = 15;
            this.username.Visible = false;
            // 
            // password
            // 
            this.password.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.password.Location = new System.Drawing.Point(883, 427);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(112, 30);
            this.password.TabIndex = 16;
            this.password.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(791, 332);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 25);
            this.label3.TabIndex = 26;
            this.label3.Text = "Server:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(791, 364);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 25);
            this.label4.TabIndex = 27;
            this.label4.Text = "Database:";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(791, 396);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 25);
            this.label5.TabIndex = 28;
            this.label5.Text = "UserName:";
            this.label5.Visible = false;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(791, 430);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 25);
            this.label8.TabIndex = 29;
            this.label8.Text = "Password:";
            this.label8.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(791, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(201, 25);
            this.label9.TabIndex = 30;
            this.label9.Text = "Data Connection Info:";
            // 
            // option1label
            // 
            this.option1label.BackColor = System.Drawing.SystemColors.Control;
            this.option1label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option1label.Location = new System.Drawing.Point(20, 120);
            this.option1label.Name = "option1label";
            this.option1label.Size = new System.Drawing.Size(140, 23);
            this.option1label.TabIndex = 31;
            this.option1label.Text = "Option 1:";
            // 
            // option2label
            // 
            this.option2label.BackColor = System.Drawing.SystemColors.Control;
            this.option2label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option2label.Location = new System.Drawing.Point(20, 171);
            this.option2label.Name = "option2label";
            this.option2label.Size = new System.Drawing.Size(140, 23);
            this.option2label.TabIndex = 32;
            this.option2label.Text = "Option 2:";
            // 
            // option3label
            // 
            this.option3label.BackColor = System.Drawing.SystemColors.Control;
            this.option3label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option3label.Location = new System.Drawing.Point(20, 220);
            this.option3label.Name = "option3label";
            this.option3label.Size = new System.Drawing.Size(140, 23);
            this.option3label.TabIndex = 33;
            this.option3label.Text = "Option 3:";
            // 
            // option4label
            // 
            this.option4label.BackColor = System.Drawing.SystemColors.Control;
            this.option4label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option4label.Location = new System.Drawing.Point(20, 271);
            this.option4label.Name = "option4label";
            this.option4label.Size = new System.Drawing.Size(140, 23);
            this.option4label.TabIndex = 34;
            this.option4label.Text = "Option 4:";
            // 
            // option5label
            // 
            this.option5label.BackColor = System.Drawing.SystemColors.Control;
            this.option5label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option5label.Location = new System.Drawing.Point(20, 320);
            this.option5label.Name = "option5label";
            this.option5label.Size = new System.Drawing.Size(140, 23);
            this.option5label.TabIndex = 35;
            this.option5label.Text = "Option 5:";
            // 
            // chkOpenPres
            // 
            this.chkOpenPres.AutoSize = true;
            this.chkOpenPres.Location = new System.Drawing.Point(796, 466);
            this.chkOpenPres.Name = "chkOpenPres";
            this.chkOpenPres.Size = new System.Drawing.Size(269, 29);
            this.chkOpenPres.TabIndex = 36;
            this.chkOpenPres.Text = "Open Pres when Complete";
            this.chkOpenPres.UseVisualStyleBackColor = true;
            this.chkOpenPres.CheckedChanged += new System.EventHandler(this.chkOpenPres_CheckedChanged);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(13, 544);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(100, 13);
            this.lblVersion.TabIndex = 37;
            // 
            // lblSlideID
            // 
            this.lblSlideID.AutoSize = true;
            this.lblSlideID.Location = new System.Drawing.Point(4, 11);
            this.lblSlideID.Name = "lblSlideID";
            this.lblSlideID.Size = new System.Drawing.Size(86, 25);
            this.lblSlideID.TabIndex = 38;
            this.lblSlideID.Text = "Slide ID:";
            // 
            // txtSlideID
            // 
            this.txtSlideID.Location = new System.Drawing.Point(96, 11);
            this.txtSlideID.Name = "txtSlideID";
            this.txtSlideID.Size = new System.Drawing.Size(100, 30);
            this.txtSlideID.TabIndex = 39;
            // 
            // pnlSlideID
            // 
            this.pnlSlideID.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlSlideID.Controls.Add(this.txtSlideID);
            this.pnlSlideID.Controls.Add(this.lblSlideID);
            this.pnlSlideID.Location = new System.Drawing.Point(796, 220);
            this.pnlSlideID.Name = "pnlSlideID";
            this.pnlSlideID.Size = new System.Drawing.Size(199, 47);
            this.pnlSlideID.TabIndex = 40;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1032, 563);
            this.Controls.Add(this.pnlSlideID);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.chkOpenPres);
            this.Controls.Add(this.option5label);
            this.Controls.Add(this.option4label);
            this.Controls.Add(this.option3label);
            this.Controls.Add(this.option2label);
            this.Controls.Add(this.option1label);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.Controls.Add(this.database);
            this.Controls.Add(this.server);
            this.Controls.Add(this.Option5);
            this.Controls.Add(this.Option4);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.currencyCode);
            this.Controls.Add(this.Option3);
            this.Controls.Add(this.Option2);
            this.Controls.Add(this.Option1);
            this.Controls.Add(this.TemplateToUse);
            this.Controls.Add(this.changeTemplate);
            this.Controls.Add(this.saveLocation);
            this.Controls.Add(this.changeSaveLocation);
            this.Controls.Add(this.btnRunPresentation);
            this.Controls.Add(this.rdoMetricUnits);
            this.Controls.Add(this.rdoUSUnits);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "PresBuilder";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlSlideID.ResumeLayout(false);
            this.pnlSlideID.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

			}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.RadioButton rdoUSUnits;
		private System.Windows.Forms.RadioButton rdoMetricUnits;
		private System.Windows.Forms.Button btnRunPresentation;
		private System.Windows.Forms.Button changeSaveLocation;
		private System.Windows.Forms.TextBox saveLocation;
		private System.Windows.Forms.Button changeTemplate;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.TextBox TemplateToUse;
		private System.Windows.Forms.TextBox Option1;
		private System.Windows.Forms.TextBox Option2;
		private System.Windows.Forms.TextBox Option3;
		private System.Windows.Forms.TextBox currencyCode;
		private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.TextBox Option4;
        private System.Windows.Forms.TextBox Option5;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.TextBox server;
		private System.Windows.Forms.TextBox database;
		private System.Windows.Forms.TextBox username;
		private System.Windows.Forms.TextBox password;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox option1label;
        private System.Windows.Forms.TextBox option2label;
        private System.Windows.Forms.TextBox option3label;
        private System.Windows.Forms.TextBox option4label;
        private System.Windows.Forms.TextBox option5label;
		private System.Windows.Forms.CheckBox chkOpenPres;
		private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblSlideID;
        private System.Windows.Forms.TextBox txtSlideID;
        private System.Windows.Forms.Panel pnlSlideID;
    }
	}

