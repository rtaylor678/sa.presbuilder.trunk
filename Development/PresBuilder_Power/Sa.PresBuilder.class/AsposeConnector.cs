﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Aspose.Slides;
using Aspose.Slides.Charts;
using System.Drawing;
//using S = System.Data;
using System.Data;

namespace Sa.PresBuilder.AsposeConnector
{

	public class AsposeConnect
	{
		//this class acts as a connector between the data processing class and the Aspose library

		#region Public Variables
			//don't like adding these here but I'm not sure of a better way to do it
			public String Option1 = "";
			public String Option2 = "";
			public String Option3 = "";
			public String Option4 = "";
			public String Option5 = "";
			public String CurrencyToUse = "USD";
			public Boolean Metric = false;

			public System.Data.DataTable slideColorsTable = new System.Data.DataTable("SlideColors");
			public System.Data.DataTable slideLineFormatsTable = new System.Data.DataTable("SlideLineFormats");
			public System.Data.DataTable slideTableFormatsTable = new System.Data.DataTable("SlideTableFormats");

		#endregion

		#region Private Variables
			private Presentation presentation;
			private ISlide currentSlide;
			private String currentSlideName;//save this for use somewhere
			private IChart currentChart;
			private ITable currentTable;
			private IChartSeries currentSeries;
			private Color defaultColor = Color.FromArgb(41, 41, 41);
			private Color plotAreaBackgroundColor = Color.FromArgb(234, 234, 234);
			private Int32 plotAreaTransparency = 0;
			private float defaultFontHeight = 16;
			//private ChartSetup[] chartSetup;
			private Int32 xPos = 47;//these positions are default positions
			private Int32 yPos = 97;
			private Int32 width = 603;
			private Int32 height = 363;
			private Int32 chartPosRow = 0;
			private Int32 chartPosCol = 0;
			private IChartDataWorkbook fact;
			private float axisFontHeight = 16;//this may be reset as the chart is processed
			//private Double yMax = 0;//used to hold the maximum value stored on the Y axis
			//private Double y2Max = 0;//used to hold the maximum value stored on the Y secondary axis
			//private Double xMax = 0;//used to hold the maximum value stored on the X axis

			private IChart[] perfSumCharts;
			private Double[,] perfSumData;
			private Int32 perfSumCount = 0;

			private System.Data.DataTable chartFixes = new System.Data.DataTable("chartFixes");
			//private System.Data.DataTable dateFix = new System.Data.DataTable("dateFix");
			//private System.Data.DataTable chartPosition = new System.Data.DataTable("chartPosition");
			//private System.Data.DataTable bar7ChartPosition = new System.Data.DataTable("bar7ChartPosition");
			//private System.Data.DataTable multiLevelFix = new System.Data.DataTable("multiLevelFix");
			//private System.Data.DataTable chartYAxisFix = new System.Data.DataTable("chartYAxisFix");

			public System.Data.DataSet currentSlideData = new DataSet();

		#endregion


		#region Public Functions

			#region Presentation

				public AsposeConnect(String fileName, Boolean deleteAllSlides = false)
				{
					//The constructor, creates a new presentation from the supplied filename. Removes all slides if you just want to use the structure (vs keeping the usual blank first slide)
					//TODO: whatif: the file path doesn't exist. The filename won't exist (or if it does it will be overwritten?) but what about the path?
					presentation = new Presentation(fileName);
					if (deleteAllSlides == true)
					{
						while (presentation.Slides.Count() > 0)
							presentation.Slides[0].Remove();
					}

					//and a little bit of setup


					//chartFixes is used to add certain slides that need rows fixing by OpenXML after the process is complete
					
					chartFixes.Columns.Add("SlideNumber", typeof(Int32));
					chartFixes.Columns.Add("FixType", typeof(Int32));//will be a number for some reason

					//chart position tables are used to fix certain slides at the end
					//chartPosition.Columns.Add("SlideNumber", typeof(Int32));
					//bar7ChartPosition.Columns.Add("SlideNumber", typeof(Int32));
					//multiLevelFix.Columns.Add("SlideNumber", typeof(Int32));
					//chartYAxisFix.Columns.Add("SlideNumber", typeof(Int32));

				}

				public void Save(String filenameToSaveAs)
				{
					//System.Diagnostics.Debug.Print("Starting save");
					//System.Diagnostics.Stopwatch swall = System.Diagnostics.Stopwatch.StartNew();

					//System.Diagnostics.Stopwatch swall2 = System.Diagnostics.Stopwatch.StartNew();

					RemoveUnusedMasters();
					//swall2.Stop();
					//System.Diagnostics.Stopwatch swall3 = System.Diagnostics.Stopwatch.StartNew();
                  
                    presentation.Save(filenameToSaveAs, Aspose.Slides.Export.SaveFormat.Pptx);
					//swall3.Stop();
					//multiLevelFix = multiLevelFix.DefaultView.ToTable(true);
					OpenXML.PostPresCleanup(filenameToSaveAs, chartFixes);//, chartPosition, bar7ChartPosition, multiLevelFix, chartYAxisFix);

					//swall.Stop();
					//System.Diagnostics.Debug.Print("Save times");
					//System.Diagnostics.Debug.Print(swall2.ElapsedMilliseconds.ToString());
					//System.Diagnostics.Debug.Print(swall3.ElapsedMilliseconds.ToString());
					//System.Diagnostics.Debug.Print(swall.ElapsedMilliseconds.ToString());

				}

			#endregion

			#region Slides

				public void AddSlideFromAnotherPresentation(AsposeConnect sourcePres, String slideName)
				{
					//Clone the desired slide by slide name from the source presentation to the end of the collection of slides in this presentation
					//this assumes there is a textbox on the slide called SlideName
					//whatif: can't get path? path doesn't matter, we assume a valid source presentation is passed
					//whatif: slidename doesnt exist in that pres? we add a blank slide so that we don't get errors elsewhere
					ISlideCollection slides = sourcePres.presentation.Slides;
					Int32 slideNumberToAdd = GetSlideNameSlide(slides, slideName);

					if (slideNumberToAdd != -1)//assuming we found a slide
					{
						//ISlide newSlide = presentation.Slides.AddClone(sourcePres.presentation.Slides[slideNumberToAdd], presentation.Masters[0], true);//uses the existing master, copies master if they don't match
						currentSlide = presentation.Slides.AddClone(sourcePres.presentation.Slides[slideNumberToAdd]);//, presentation.Masters[0], true);//uses the existing master, copies master if they don't match
						ResetPerfSums();
						//and then remove the slide name box
						foreach (IShape shape in currentSlide.Shapes)
						{
							if (shape.Name == "SlideName")
							{
								shape.ParentGroup.Shapes.Remove(shape);
								break;
							}
						}
					}
					else //if we didnt find a slide we need to add something otherwise we get a bunch of errors, so we add a blank slide
					{
						currentSlide = presentation.Slides.AddEmptySlide(presentation.LayoutSlides[0]);
						ResetPerfSums();
					}
					currentSlideName = "Template";
				}

				public void AddSlide(System.Data.DataRow slideRow)
				{
					//add a blank slide and populate with data from the SlideRow
					String title = fieldReplace(slideRow["Title"].ToString());
					String subtitle = fieldReplace(slideRow["SubTitle"].ToString());
					currentSlideName = slideRow["Name"].ToString();
					//add an empty slide to the end of the presentation, and fill in the title and subtitle
					presentation.Slides.AddEmptySlide(presentation.LayoutSlides[0]);
					currentSlide = presentation.Slides[presentation.Slides.Count - 1];
					ResetPerfSums();//it's a new slide so we reset perfsum data from any old slides
					SetSlideTitle(title);
					if (subtitle != "")
					{
						SetSlideSubTitle(subtitle);
					}
					CheckForSlideFixes();
				}

				public void ResetSubTitle(String subtitle)
				{
					SetSlideSubTitle(subtitle);
				}

				//public void SetSlideMaster()
				//{

				//}

			#endregion

			#region Charts

				public void AddChart(System.Data.DataRow chartRow)
				{
					DoAddChart(chartRow);
				}

				public void AddDataToChart(System.Data.DataRow dataRow, System.Data.DataRow chartRow, System.Data.DataTable data)
				{
					//adds a table of data to a chart
					//dataRow comes from SlideData
					//chartRow comes from SlideCharts
					//data is the data being added
					DoAddData(dataRow, chartRow, data);
				}

				public void FormatChartAxes(System.Data.DataRow chartRow)
				{
					//called at the end of adding all the data to the chart
					DoFormatChartAxes(chartRow);
				}

				public void FixLegendPosition()
				{
					//called at the end of adding all the charts to the slide
					DoFixLegendPositions();
				}

				public void FixYAxisTitlePosition()
				{
					DoFixYAxisTitlePosition();
				}

			#endregion

			#region Annotations

				public void AddAnnotation(System.Data.DataRow annotation)
				{
					SetAnnotation(annotation);
				}

			#endregion

		#endregion


		#region Private Functions

			#region Presentation

				private void RemoveUnusedMasters()
				{
					presentation.Masters.RemoveUnused(false);
					presentation.LayoutSlides.RemoveUnused();
				}

				private void CheckForChartFixes(System.Data.DataRow chartRow)
				{
            //check for a date fix
            //if (chartRow["ChartDate"].ToString() != "")
            if (chartRow["ChartDate"] != DBNull.Value)
            {
                if ((chartRow["ChartDate"].ToString()=="1") || (chartRow["ChartDate"].ToString()=="2"))
                { 
                System.Data.DataRow dr = chartFixes.NewRow();
                dr["SlideNumber"] = presentation.Slides.Count();
                dr["FixType"] = chartRow["ChartDate"];//this will be a 1 or a 2, 1 being a date, 2 using the date as a proxy for the position on the horizontal axis
                chartFixes.Rows.Add(dr);
                }
            }
					//have added a chart, store the data if necessary so we can place it correctly
					if (chartRow["YPos"].ToString() == "" && chartRow["Height"].ToString() == "" && currentSlideName.Substring(0, 4) != "Bar7")
					{
						System.Data.DataRow dr = chartFixes.NewRow();
						dr["SlideNumber"] = presentation.Slides.Count();
						dr["FixType"] = 3;//3 being the chart position
						chartFixes.Rows.Add(dr);
					}

				}

				private void CheckForSlideFixes()
				{
					//added Bar7 chart, needs special formatting at the end, so store it here
					if (currentSlideName.Substring(0, 4) == "Bar7")
					{
						System.Data.DataRow dr = chartFixes.NewRow();
						dr["SlideNumber"] = presentation.Slides.Count();
						dr["FixType"] = 4;//4 being a Bar7 chart position
						chartFixes.Rows.Add(dr);
					}
				}

			#endregion

			#region Slides

				private Int32 GetSlideNameSlide(ISlideCollection slides, String slideName)
				{
					//given a collection of slides, finds the one with the right name and returns it
					foreach (ISlide slide in slides)
					{
						foreach (IShape shape in slide.Shapes)
						{
							if (shape.Name == "SlideName")
							{
								ITextFrame text = ((IAutoShape)shape).TextFrame;
								if (text.Text == slideName)
								{
									return slide.SlideNumber - 1;
								}
								break;//gets us out of this slide once we've found the name, don't need to check all the other shapes on the slide
							}
						}
					}
					return -1;//slide with this name wasn't found
				}

				private void SetSlideTitle(String title)
				{
					//TODO: is this correct? that it is shape 0? not sure it will always be so
					IShape shape = currentSlide.Shapes[0];
					IPortion port = ((IAutoShape)shape).TextFrame.Paragraphs[0].Portions[0];

					//String fontName = "Tahoma";// port.PortionFormat.LatinFont.FontName;
					//Color color = port.PortionFormat.FillFormat.SolidFillColor.Color;
					//NullableBool fontBold = port.PortionFormat.FontBold;
					//float? fontHeight = port.PortionFormat.FontHeight;


					SetPortion(port, title);
				}

				private void SetSlideSubTitle(String subtitle)
				{
					//TODO: is this correct? that it is shape 0? not sure it will always be so
					IShape shape = currentSlide.Shapes[0];
					ITextFrame text = ((IAutoShape)shape).TextFrame;
					if (text.Paragraphs.Count == 1)//if there is no subtitle, add one, otherwise change it
					{
						text.Paragraphs.AddFromHtml(subtitle);
						IParagraph para2 = text.Paragraphs[1];
						SetPortion(para2.Portions[0], fontHeight: 20);
					}
					else
					{
						text.Paragraphs[1].Portions[0].Text = subtitle;
					}
				}

			#endregion

			#region Charts

				private void DoAddChart(System.Data.DataRow chartRow)
				{
					//get the chart type and add the chart
					ChartType chartType = GetChartType(chartRow["ChartType"].ToString());

					//set the position of the chart - default positions attached
					xPos = Utilities.CheckForNull<Int32>(chartRow["XPos"], 0);//47
					yPos = Utilities.CheckForNull<Int32>(chartRow["YPos"], 97);
					width = Utilities.CheckForNull<Int32>(chartRow["Width"], 720);//603
					height = Utilities.CheckForNull<Int32>(chartRow["Height"], 363);
					plotAreaTransparency = Utilities.CheckForNull<Int32>(chartRow["PlotAreaTransparent"], 0);
                    
					currentChart = currentSlide.Shapes.AddChart(chartType, xPos, yPos, width, height);
                    currentChart.ValidateChartLayout();
					currentChart.AlternativeText = "Chart " + chartRow["SlideChartID"].ToString();

					//Delete default generated series and categories
					currentChart.ChartData.Series.Clear();
					currentChart.ChartData.Categories.Clear();
					currentChart.ChartData.ChartDataWorkbook.Clear(0);

					//delete legend and title 
					currentChart.HasLegend = false;
					currentChart.HasTitle = false;

					String chartTitle = fieldReplace(chartRow["ChartTitle"].ToString());
					String yAxisTitle = fieldReplace(chartRow["ChartYAxisTitle"].ToString());
					String xAxisTitle = fieldReplace(chartRow["ChartXAxisTitle"].ToString());

					if (chartTitle != "")
					{
						AddTitleToChart(chartTitle);
					}

					if (yAxisTitle != "")
					{
						Int32 titleHeight = height;
						if (titleHeight == 363)
						{
							titleHeight = (Int32)(titleHeight * 0.7);//because the default chart is 363, and the default plot area size is 0.7
						}
						AddYAxisTitle(yAxisTitle, titleHeight, chartRow["SlideChartID"].ToString());
					}

					if (xAxisTitle != "")
					{
						AddXAxisTitle(xAxisTitle);
					}

					if (chartRow["ChartType"].ToString() == "PerfSum" || chartRow["ChartType"].ToString() == "Pie")
					{
						FormatPlotArea();
					}
					else
					{
						FormatPlotArea(plotAreaBackgroundColor, defaultColor, plotAreaTransparency);
					}

					currentChart.Axes.VerticalAxis.IsVisible = true;
                    currentChart.Axes.HorizontalAxis.IsVisible = Boolean.Parse(chartRow["DisplayXAxis"].ToString());

                    //not using the chart axis titles
                    //currentChart.Axes.VerticalAxis.HasTitle = false;
                    currentChart.Axes.HorizontalAxis.HasTitle = false;

					switch (chartRow["ChartType"].ToString())
					{
						case "Bar":
						case "Bar100":
						case "BarCluster":
							axisFontHeight = 12;
							break;
						default:
							axisFontHeight = 16;
							break;
					}

					//reset some chart pointers
					chartPosRow = 0;
					chartPosCol = 0;
					fact = currentChart.ChartData.ChartDataWorkbook;

					currentChart.DisplayBlanksAs = DisplayBlanksAsType.Gap;

					CheckForChartFixes(chartRow);
				}

				private void RemoveCurrentChart()
				{
					//this is for the rare occasion where it isn't needed, particularly in the case of a table
					currentSlide.Shapes.Remove(currentChart);//getting rid of the placeholder
				}

				private void DoAddData(System.Data.DataRow dataRow, System.Data.DataRow chartRow, System.Data.DataTable queryDataTable)
				{
					//this is the main function for handling the data, figuring out what kind of data it is and what to do with it

					System.Data.DataTable shortColorsTable = Utilities.GetShortColorTable(slideColorsTable, (Int32)dataRow["SlideColorGroup"]);

					//do the actual data posting to the slide
					switch (dataRow["DataType"].ToString())
					{

						case "DistPoints":
							DoDistPoints(queryDataTable, shortColorsTable);
							break;
						case "ScatLine":
							DoScatLine(queryDataTable, dataRow);
							break;
						case "LineMarker":
							DoLineMarker(queryDataTable, dataRow, shortColorsTable, chartRow);
							break;
						case "Bar":
						case "Line":
						case "Bar100":
						case "BarCluster":
						case "BarHoriz":
						case "Area":
							AddBarsAndLines(queryDataTable, dataRow, shortColorsTable, chartRow);
							break;
						case "PerfSum": //PerfSum does not have a multi?
							DoPerfSum(queryDataTable, shortColorsTable);
							break;
						case "StackArea":
							//TODO: StackArea multis?
							DoStackArea(queryDataTable, shortColorsTable);
							break;
						case "Scatter":
							DoScatter(queryDataTable, dataRow);
							break;
						case "Table":
							DoTable(queryDataTable, dataRow, chartRow);
							break;
						case "Traffic":
							DoTrafficLight(queryDataTable, dataRow, chartRow);
							break;
						default:
							break;
					}

					if (dataRow["AxisTitle"].ToString() != "")
					{
						if ((Boolean)dataRow["AxisNumber"] == true)
						{
							AddYAxisTitle(dataRow["AxisTitle"].ToString(), 363, chartRow["SlideChartID"].ToString(), true);//363 is a default height
						}
					}

				}

				private void DoScatter(System.Data.DataTable queryDataTable, System.Data.DataRow dataRow)
				{
					//TODO: Scatter multis?
					LabelFormat lF = new LabelFormat();

					//TODO: fix this to work in the database?
					switch (Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]))
					{
						case 6://made this one up just for the Heat Rate labels
							lF.fontName = "Tahoma";
							lF.fontHeight = 10;
							lF.color = Color.FromArgb(77, 77, 77);
							break;
						case 7://made this one up just for the Analysis By Major Process labels
							lF.fontName = "Tahoma";
							lF.fontBold = false;
							lF.color = Color.FromArgb(77, 77, 77);
							break;
						default:
							break;
					}
					AddScatterPoints(queryDataTable, lF);

				}

				private void DoPerfSum(System.Data.DataTable queryDataTable, System.Data.DataTable shortColorsTable)
				{
					foreach (System.Data.DataRow queryRow in queryDataTable.Rows)
					{
						if (queryRow["Q12"].ToString() != "")//the first row, data for the bar
						{
							PerfSumAddTheBars(queryRow, shortColorsTable);
						}
						else//a unit data point
						{
							string unitName = queryRow["BarTitle"].ToString();//the unit name, bar title is just the column name
							Double unitValue = Double.Parse(queryRow["Minimum"].ToString());//the unit value, minimum is just the column name
							AddPerfSumSeriesWithPoint(unitName, unitValue, LegendDataLabelPosition.Center, false, queryRow["Minimum"].ToString());
						}
					}
					//end of the rows, now we recalc the slide to correct the perfsums
					RecalcPerfSums();
					
				}

				private void DoLineMarker(System.Data.DataTable queryDataTable, System.Data.DataRow dataRow, System.Data.DataTable shortColorsTable, System.Data.DataRow chartRow)
				{
					axisFontHeight = 16;//this is a reset just for the LineMarker type
					//first we flip the data, then we add it
					queryDataTable = ReformatLineMarkers(queryDataTable);
					//queryDataTable = AddNA(queryDataTable);
					AddBarsAndLines(queryDataTable, dataRow, shortColorsTable, chartRow);
				}

				private void DoDistPoints(System.Data.DataTable queryDataTable, System.Data.DataTable shortColorsTable)
				{
					LabelFormat labelFormat = new LabelFormat();
					try
					{
						//set the color of the labels here; it uses the first value in the color table for the SlideColorGroup
						System.Data.DataRow[] colorrow = shortColorsTable.Select("ColorOrder = 1");
						labelFormat.color = System.Drawing.ColorTranslator.FromHtml("#" + colorrow[0]["Color"].ToString());
					}
					catch (IndexOutOfRangeException)//if we get an exception that means there was nothing in shortColorsTable that matched, so we ignore the exception and just use the default color in a labelformat
					{
					}
					AddDistPoints(queryDataTable, labelFormat);

				}

				private LineFormat GetLineFormat(Int32 lineFormatNumber)
				{
					LineFormat lineFormat = new LineFormat();

					System.Data.DataRow[] shortLineFormat = slideLineFormatsTable.Select("SlideLineFormatID = " + lineFormatNumber.ToString());

					if (shortLineFormat.Length > 0)
					{
						System.Data.DataRow row = shortLineFormat[0];
						lineFormat.SetLineFormat(System.Drawing.ColorTranslator.FromHtml("#" + row["Color"].ToString()), Convert.ToDouble(row["LineWidth"]), Convert.ToInt32(row["LineDashStyle"]), Convert.ToInt32(row["LineBeginArrow"]), Convert.ToInt32(row["LineEndArrow"]), Convert.ToInt32(row["MarkerType"]), Convert.ToInt32(row["MarkerSize"]), Convert.ToBoolean(row["MarkerLastOnly"]));
					}

					return lineFormat;
				}

				private void DoScatLine(System.Data.DataTable queryDataTable, System.Data.DataRow dataRow)
				{
					//first get the format
					LineFormat lineFormat = GetLineFormat((Int32)dataRow["LineFormat"]);

					Int32 secondAxis = Utilities.CheckForNull(dataRow["AxisNumber"], 0);

					//then add the data
					//two different possibilities here: one line or multiple lines (but not a multi)

					if (queryDataTable.Columns.Count == 2)
					{
						//if it is a single line, it will only have two columns of data
						AddScatterLine(queryDataTable, lineFormat, secondAxis);
					}
					if (queryDataTable.Columns.Count == 3)
					{
						//if it is multiple lines, it should have three columns (first column being the splitter column

						//get the distinct values in the first column
						System.Data.DataView view = new System.Data.DataView(queryDataTable);
						String colname = queryDataTable.Columns[0].ColumnName;
						System.Data.DataTable distinctRows = view.ToTable(true, colname);

						string slideToMake = "";

						foreach (System.Data.DataRow row in distinctRows.Rows)
						{
							slideToMake = row.ItemArray[0].ToString();
							System.Data.DataTable outputData = queryDataTable.Copy();
							outputData = Utilities.SplitMultiData(outputData, slideToMake);

							AddScatterLine(outputData, lineFormat, secondAxis);
						}

					}
				}

				private void SetSeriesOverlap(ChartType chartType)
				{

					switch (chartType)
					{
						case ChartType.ClusteredColumn:
							currentSeries.ParentSeriesGroup.Overlap = Convert.ToSByte(0);
							break;
						default:
							currentSeries.ParentSeriesGroup.Overlap = Convert.ToSByte(100);
							break;
					}

				}

				private void SetSeriesColor(Color color)
				{
					currentSeries.Format.Fill.FillType = FillType.Solid;
					currentSeries.Format.Fill.SolidFillColor.Color = color;
					currentSeries.Format.Line.FillFormat.FillType = FillType.Solid;
					if (currentSeries.Type == ChartType.Line)
					{
						currentSeries.Format.Line.FillFormat.SolidFillColor.Color = color;
					}
					if (currentSeries.Type == ChartType.StackedColumn)
					{
						//this is the odd situation where we are plotting blank columns in a column chart (i.e. as spacers), if our fill color matches the background we remove the line around it
						if (color == currentChart.PlotArea.Format.Fill.SolidFillColor.Color)
						{
							currentSeries.Format.Line.FillFormat.FillType = FillType.NoFill;
						}
					}
				}

				private void SetBarWidth(System.Data.DataRow dataRow)
				{
					Int32 barGapWidth = Utilities.CheckForNull(dataRow["BarGapWidth"], 0);
					if (barGapWidth != 0) //0 is the null parameter here
					{
					//foreach (IChartSeries barSeries in currentChart.ChartData.Series)
					//{
						currentSeries.ParentSeriesGroup.GapWidth = (ushort)barGapWidth;
					}
					//}
				}

				private void AddSeries(String seriesName, ChartType chartType, Int32 axisNumber = 0)
				{
					currentSeries = currentChart.ChartData.Series.Add(fact.GetCell(0, chartPosRow, chartPosCol, seriesName), chartType);
					if (axisNumber == 1)
					{
						currentSeries.PlotOnSecondAxis = true;
					}

					chartPosRow++;
					//currentSeries.DataPoints.DataSourceTypeForValues = DataSourceType.DoubleLiterals;
				}

				private void AddBarsAndLines(System.Data.DataTable queryDataTable, System.Data.DataRow dataRow, System.Data.DataTable slideColors, System.Data.DataRow chartRow)
				{
                    //if(queryDataTable.Rows.Count == 0) { return;  }

					ChartType chartChartType = GetChartType(chartRow["ChartType"].ToString());
					ChartType dataChartType = GetChartType(dataRow["DataType"].ToString());

					Int32 secondAxis = Utilities.CheckForNull(dataRow["AxisNumber"], 0);

					for (int rownum = 0; rownum < queryDataTable.Rows.Count; rownum++)
					{
						AddCategory(queryDataTable.Rows[rownum].ItemArray[0], (chartRow["ChartType"].ToString() == "Bar100") ? true : false);
					}

					chartPosRow = 0;

                    if (queryDataTable.Rows.Count == 0)
                    {
                        chartPosCol++;
                    }
                    else
                    {
                        if (queryDataTable.Rows
                            [0].ItemArray[0].ToString().IndexOf("|") < 0)
                        {
                            chartPosCol++;
                        }
                        else
                        {
                            String[] pipeCount = queryDataTable.Rows[0].ItemArray[0].ToString().Split('|');
                            chartPosCol = chartPosCol + pipeCount.Count() + 1;
                        }
                    }

					for (int colnum = 1; colnum < queryDataTable.Columns.Count; colnum++) //skip the first column, it is the category names
					{
						//ChartType seriesChartType = GetChartType(dataRow["DataType"].ToString());

						AddSeries(queryDataTable.Columns[colnum].ColumnName, dataChartType, secondAxis);

						Color color = defaultColor;
						try
						{
							color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colnum - 1].ItemArray[0]);
						}
						catch (IndexOutOfRangeException)
						{
							//just catching the exception that it couldn't get the color
							//no need to do anything with it, it will use the defaultColor if it couldn't get one
						}

						SetSeriesColor(color);
						SetSeriesOverlap(dataChartType);
						SetBarWidth(dataRow);
						currentSeries.InvertIfNegative = false;//don't know why this is in here but we get rid of it

						for (int rownumber = 0; rownumber < queryDataTable.Rows.Count; rownumber++)
						{
							switch (dataChartType)
							{
								case ChartType.StackedColumn:
								case ChartType.StackedBar:
								case ChartType.ClusteredColumn:
									AddDataPoint(queryDataTable.Rows[rownumber].ItemArray[colnum], dataChartType);
									break;
								case ChartType.Line:
								case ChartType.LineWithMarkers:
									AddDataPoint(queryDataTable.Rows[rownumber].ItemArray[colnum], ChartType.Line);
									if (dataRow["LineFormat"] != DBNull.Value)
									{
										LineFormat lineFormat = GetLineFormat((Int32)dataRow["LineFormat"]);
										lineFormat.lineColor = color;//because the GetLineFormat sets the color to the default in the table, we need to reset it here
										FormatLine(lineFormat);
									}
									break;

								default:
									break;
							}
						}

						if ((Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"])) == true)
						{
							if (dataChartType == chartChartType)
							{
								AddManualLegend(dataChartType, queryDataTable.Columns[colnum].ColumnName, colnum, color, (chartRow["ChartType"].ToString() == "Bar100") ? true : false);
							}
						}

						chartPosRow = 0;
						chartPosCol++;
					}

				}

				private void AddManualLegend(ChartType dataChartType, String legendName, Int32 colnum, Color color, Boolean isBar100 = false)
				{

					switch (dataChartType)
					{
						case ChartType.StackedColumn:
						case ChartType.Line:
						case ChartType.ClusteredColumn:
							AddLegendBelowChart(legendName, color, colnum);
							break;
						case ChartType.StackedBar:
							if (isBar100 == true)
							{
								AddLegendBelowChart(legendName, color, colnum);
							}
							break;
						default:
							break;
					}
				}

				private void AddDataPoint<T>(T value, ChartType chartType)
				{
					switch (chartType)
					{
						case ChartType.StackedColumn:
						case ChartType.StackedBar:
						case ChartType.ClusteredColumn:
							if (value.GetType() != typeof(System.DBNull))
							{
								currentSeries.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, value));
							}
							else
							{
								currentSeries.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol));
							}
							chartPosRow++;
							break;

						case ChartType.Line:
							if (value.GetType() != typeof(System.DBNull))//because if it's null, the program pretends it is 0 without this
							{
								currentSeries.DataPoints.AddDataPointForLineSeries(fact.GetCell(0, chartPosRow, chartPosCol, value));
							}
							else
							{
								currentSeries.DataPoints.AddDataPointForLineSeries(fact.GetCell(0, chartPosRow, chartPosCol));
							}
							chartPosRow++;
							break;

						default:
							break;
					}

					//GetChartMax();
				}

				private void AddDataPoint<T>(T valueX, T valueY, ChartType chartType)
				{
					switch (chartType)
					{
						case ChartType.ScatterWithMarkers:
							currentSeries.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, valueX), fact.GetCell(0, chartPosRow, chartPosCol + 1, valueY));
							chartPosRow++;
							break;
						default:
							break;
					}
				}

				private void AddLegendBelowChart(String legendName, Color legendColor, Int32 groupnumber)
				{
					//we are using the text "GroupShape" + number in the alt text of the groupshape to identify them, per Aspose site direction
					//we can use this to iterate through them and find all of them to place them with

					float left = 0;//starting positions
					float top = 444;

					//Iterating through all shapes inside the slide
					for (int i = 0; i < currentSlide.Shapes.Count; i++)
					{
						//If the alternative text of the slide matches with the required one then
						if (currentSlide.Shapes[i].AlternativeText.Contains("GroupShape"))
						{
							if (currentSlide.Shapes[i].X + currentSlide.Shapes[i].Width > left)
							{
								left = currentSlide.Shapes[i].X + currentSlide.Shapes[i].Width;
							}
						}
					}

					left += 6;//a little space between labels

					IShapeCollection slideShapes = currentSlide.Shapes;
					IGroupShape groupShape = slideShapes.AddGroupShape();
					groupShape.AlternativeText = "GroupShape" + groupnumber;
					IAutoShape shp;
					shp = groupShape.Shapes.AddAutoShape(ShapeType.Rectangle, left, top, 9, 9);
					shp.FillFormat.FillType = FillType.Solid;
					shp.FillFormat.SolidFillColor.Color = legendColor;
					shp.ShapeStyle.LineColor.Color = Color.Black;
					shp.LineFormat.Width = 0.75;

					//how wide should the next shape be?
					Bitmap b = new Bitmap(720, 540);
					Graphics g = Graphics.FromImage(b);
					Font f1 = new Font("Tahoma", 10, FontStyle.Regular);
					//TODO: why do I have to do 10 here if the font is 14 tall? size doesn't calc right, I don't know why, it seems to work if the size measured here is 4 points less than the actual font (e.g. 14 to 10, 16 to 12)
					SizeF size1 = new SizeF(g.MeasureString(legendName, f1));

					shp = groupShape.Shapes.AddAutoShape(ShapeType.Rectangle, left + 12, top - 4, size1.Width, 16); // + 12 gives room for the box drawn above, -4 evens out to the box, 16 is a nice number
					shp.FillFormat.FillType = FillType.NoFill;
					shp.ShapeStyle.LineColor.Color = Color.Transparent;

					ITextFrame tf = ((IAutoShape)shp).TextFrame;

					//we want to add the text as html, because that gives more control to the user over how to display it
					//so we run it through this convert to html - if there is no html in the text, it won't do anything; if there is, it will fix it up appropriately
					//legendName = HttpUtility.HtmlEncode(legendName);
					////and if there's a >= or <= they get messed up, so this fixes them back again
					//legendName = legendName.Replace("&gt;=", "&ge;");
					//legendName = legendName.Replace("&lt;=", "&le;");
					legendName = legendName.Replace(">=", "&ge;");
					legendName = legendName.Replace("<=", "&le;");
					legendName = legendName.Replace(">", "&gt;");
					legendName = legendName.Replace("<", "&lt;");

					//tf.Paragraphs.RemoveAt(0);
					tf.Paragraphs.Clear();
					tf.Paragraphs.AddFromHtml(legendName);
		
					IParagraph para = tf.Paragraphs[0];

					para.ParagraphFormat.Alignment = TextAlignment.Left;
					tf.TextFrameFormat.WrapText = NullableBool.False;
					tf.TextFrameFormat.MarginLeft = 0;
					//SetPortion(para.Portions[0], fontHeight: 14, text: legendName);
					foreach (Portion p in tf.Paragraphs[0].Portions)
					{
						p.PortionFormat.LatinFont = new FontData("Tahoma");
						p.PortionFormat.FontHeight = 14;
						p.PortionFormat.FillFormat.FillType = FillType.Solid;
						p.PortionFormat.FillFormat.SolidFillColor.Color = defaultColor;
						p.PortionFormat.FontBold = NullableBool.False; //NullableBools are the bane of my existence

					}
					//TODO: logic to move the boxes around, to following rows etc.

				}

				private void AddDistPoints(System.Data.DataTable tableToAdd, LabelFormat labelFormat)
				{
					//the difference here is that we only have the Y point, we need to search along the line and get the X points as well
					//so we call a function to find those points, then call the Scatter function to add them

					//TODO: how to know what series to add it to? currentSeries is the easy answer, but users need to be able to identify a particular series 
					//so the db doesn't have to be set up in the right order to work

					//step 1 is figuring out where the data goes
					tableToAdd = GetDistX(tableToAdd);

					AddScatterPoints(tableToAdd, labelFormat);
					chartPosCol++;
				}

				private System.Data.DataTable ReformatLineMarkers(System.Data.DataTable queryDataTable)
				{
					//reformat line marker tables to be able to work with AddDataToChart (easier here than in SQL because of the variable number of columns)

					//first add our category field
					System.Data.DataView view = new System.Data.DataView(queryDataTable);
					String secondcolname = queryDataTable.Columns[1].ColumnName;//this should be the category
					System.Data.DataTable outputData = view.ToTable(true, secondcolname);

					String firstcolname = queryDataTable.Columns[0].ColumnName;
					System.Data.DataTable distinctRows = view.ToTable(true, firstcolname);

					//then add our columns
					foreach (System.Data.DataRow row in distinctRows.Rows)
					{
						outputData.Columns.Add(row.ItemArray[0].ToString(), queryDataTable.Columns[2].DataType);
					}

					//then insert the data
					foreach (System.Data.DataRow inputrow in queryDataTable.Rows)
					{
						foreach (System.Data.DataRow outrow in outputData.Rows)
						{
							if ((inputrow.ItemArray[1].ToString() == outrow.ItemArray[0].ToString()) && inputrow.ItemArray[0].ToString().Length > 0)
							{
								//matching row
								outrow[inputrow.ItemArray[0].ToString()] = inputrow.ItemArray[2];
							}
						}

					}
					return outputData;
				}

				//private void AddCategory<T>(Int32 xPos, Int32 yPos, T value)
				//{
				//	//				IChartDataWorkbook fact = currentChart.ChartData.ChartDataWorkbook;
				//	IChartDataCell cdc = fact.GetCell(0, xPos, yPos);

				//	if (typeof(T) == typeof(Int32)) // most of the time it is string, but Aspose can't deal with it being an actual date, so we pass an int and set the number format to date
				//	{
				//		cdc.PresetNumberFormat = 14;
				//	}

				//	cdc.Value = value;
				//	currentChart.ChartData.Categories.Add(cdc);
				//}

				private void AddCategoryDate(DateTime value)
				{
					Int32 intDate = DateTimeToInt(value);

					IChartDataCell cdc = fact.GetCell(0, chartPosRow, chartPosCol);
					cdc.PresetNumberFormat = 14;
					cdc.Value = intDate;
					currentChart.ChartData.Categories.Add(cdc);
				}

				private void AddCategory<T>(T value, Boolean isBar100 = false)
				{
					if (value.GetType() == typeof(System.DateTime))
					{
						AddCategoryDate((DateTime)(object)value);
					}
					else
					{
						String stringToAdd = value.ToString();

						if (isBar100 == true)
						{
							//if (stringToAdd.IndexOf("(") > 0)
							//{
								stringToAdd = stringToAdd.Replace("(", "\n(");
							//}
						}

						//adding option to put a pipe in the string, which will break it into different nodes if present
						if (stringToAdd.IndexOf("|") < 0) //if no pipe, just add the category
						{
							currentChart.ChartData.Categories.Add(fact.GetCell(0, chartPosRow + 1, chartPosCol, stringToAdd));
						}


						else //if there is a pipe
						{

							String[] splitted = stringToAdd.Split('|');

							IChartCategory workingCat;
							//the actual value
							workingCat = currentChart.ChartData.Categories.Add(fact.GetCell(0, chartPosRow + 1, chartPosCol + splitted.Count()-1, splitted[splitted.Count()-1].ToString().Trim()));

							for (int n = 0; n < splitted.Count() - 1 ; n++)
							{
								if (splitted[n].ToString().Trim() != "")
								{
									workingCat.GroupingLevels.SetGroupingItem(n + 1, fact.GetCell(0, chartPosRow + 1, chartPosCol + n, splitted[n].ToString().Trim()));
									//workingCat.GroupingLevels.SetGroupingItem(n + 1, splitted[n].ToString().Trim());
								}
							}

							System.Data.DataRow dr = chartFixes.NewRow();
							dr["SlideNumber"] = presentation.Slides.Count();
							dr["FixType"] = 5;//5 being a grouping level fix
							chartFixes.Rows.Add(dr);
							
						}

					}
					chartPosRow++;
				}

				private void AddTitleToChart(String title)
				{

					currentChart.HasTitle = true;
					currentChart.ChartTitle.AddTextFrameForOverriding(title);
					IPortion cTitle = currentChart.ChartTitle.TextFrameForOverriding.Paragraphs[0].Portions[0];

					if (currentChart.Type == ChartType.Pie)//why do we do this?
					{
						SetPortion(cTitle, fontHeight: 22);
					}
					else
					{
						SetPortion(cTitle, fontBold: NullableBool.True);
					}

					currentChart.ChartTitle.TextFrameForOverriding.TextFrameFormat.MarginTop = 0;
				}

				private void AddScatterPoints(System.Data.DataTable dataToAdd, LabelFormat labelFormat)
				{

					//IChartDataWorkbook fact3 = currentChart.ChartData.ChartDataWorkbook;

					////find the last used column and row of the data, and add below it
					//Tuple<int, int> tup = GetLastUsedCell(currentChart.ChartData);

					//int lastrow = tup.Item1;
					//int lastcol = tup.Item2;
					chartPosRow = 0;
					chartPosCol++;

					for (int i = 0; i < dataToAdd.Rows.Count; i++)
					{

                        //Double xVal = Convert.ToDouble((dataToAdd.Rows[i].ItemArray[1]==null? dataToAdd.Rows[i].ItemArray[1] : 0));
                        //Double yVal = Convert.ToDouble((dataToAdd.Rows[i].ItemArray[2]==null? dataToAdd.Rows[i].ItemArray[2] : 0));

                        Double xVal = Convert.ToDouble(dataToAdd.Rows[i].ItemArray[1]);
                        Double yVal = Convert.ToDouble(dataToAdd.Rows[i].ItemArray[2]);


                        IChartSeries newSeries = currentChart.ChartData.Series.Add(fact.GetCell(0, chartPosRow, chartPosCol, dataToAdd.Rows[i].ItemArray[0].ToString()), ChartType.ScatterWithMarkers);
						chartPosRow++;
						newSeries.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, xVal), fact.GetCell(0, chartPosRow, chartPosCol + 1, yVal));
						chartPosRow++;
						newSeries.Marker.Symbol = MarkerStyleType.None;

						IDataLabel label = newSeries.DataPoints[0].Label;

						NullableBool boldfont = NullableBool.NotDefined;//this is a WTF workaround because of freaking NullableBools
						if (labelFormat.fontBold == true)
						{
							boldfont = NullableBool.True;
						}
						if (labelFormat.fontBold == false)
						{
							boldfont = NullableBool.False;
						}
                        FormatLabel(label, labelFormat.fontName, boldfont, labelFormat.fontHeight, labelFormat.labelPosition, labelFormat.showSeriesName, labelFormat.color, labelFormat.fillType);

                        //int shapeCount = currentSlide.Shapes.Count;

                        //float plotX = ((currentChart.Frame.X + currentChart.PlotArea.ActualX) * 3.7f) + ((float)xVal * 3.7f);

                        ////float plotY = currentChart.PlotArea.ActualHeight * .9f;

                        //float plotY = ((currentChart.Frame.Y + currentChart.PlotArea.ActualY) * 3.3f) - ((float)yVal * 3.3f);

                        //IAutoShape textBoxShape = currentSlide.Shapes.AddAutoShape(ShapeType.Rectangle, plotX, plotY, 60, 20);


                        //textBoxShape.AddTextFrame(" ");

                        //textBoxShape.FillFormat.FillType = FillType.NoFill;

                        //textBoxShape.LineFormat.FillFormat.FillType = FillType.NoFill;

                        //ITextFrame textBoxFrame = textBoxShape.TextFrame;

                        //IParagraph textBoxPara = textBoxFrame.Paragraphs[0];

                        //IPortion textBoxPortion = textBoxPara.Portions[0];

                        //textBoxPortion.Text = fact.GetCell(0, chartPosRow, chartPosCol, dataToAdd.Rows[i].ItemArray[0].ToString()).Value.ToString();

                        //textBoxPortion.PortionFormat.LatinFont = new FontData(labelFormat.fontName);

                        //textBoxPortion.PortionFormat.FillFormat.FillType = FillType.Solid;

                        //textBoxPortion.PortionFormat.FillFormat.SolidFillColor.Color = labelFormat.color;

                        //textBoxPortion.PortionFormat.FontHeight = labelFormat.fontHeight;

                        //textBoxPortion.PortionFormat.FontBold = NullableBool.True;

                    }

				 }

				private void AddScatterLine(System.Data.DataTable dataToAdd, LineFormat lineFormat, Int32 axisNumber)
				{
					AddSeries("Line " + currentChart.ChartData.Series.Count().ToString(), ChartType.ScatterWithMarkers, axisNumber);

					for (int i = 0; i < dataToAdd.Rows.Count; i++)
					{
						AddDataPoint(dataToAdd.Rows[i].ItemArray[0], dataToAdd.Rows[i].ItemArray[1], ChartType.ScatterWithMarkers);
						currentSeries.Marker.Symbol = MarkerStyleType.None;
					}

					FormatLine(lineFormat);
					chartPosCol+= 2;

				}

				private void SetHorizontalAxisMinMax(double minValue, double maxValue)
				{
					currentChart.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
					currentChart.Axes.HorizontalAxis.MaxValue = maxValue;
					currentChart.Axes.HorizontalAxis.IsAutomaticMinValue = false;
					currentChart.Axes.HorizontalAxis.MinValue = minValue;
				}

				private void SetAxisNumberFormat(Int32 axisNumber)
				{
					//axisNumber: 0 = left (Y) axis, 1 = right (Y) axis, 2 = bottom (X) axis

					Double verticalMax = 0;

					switch (axisNumber)
					{
						case 0:
							if (currentChart.Axes.VerticalAxis.IsAutomaticMaxValue == false)
							{
								verticalMax = currentChart.Axes.VerticalAxis.MaxValue;
								AxisFormat(currentChart.Axes.VerticalAxis, verticalMax);
							}
							break;
						case 1:
							if (currentChart.Axes.SecondaryVerticalAxis != null)
							{
								if (currentChart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue == false)
								{
									verticalMax = currentChart.Axes.SecondaryVerticalAxis.MaxValue;
									AxisFormat(currentChart.Axes.SecondaryVerticalAxis, verticalMax);
								}
							}
							break;

						default:
							break;
					}

				}

				private void AxisFormat(IAxis axis, Double numberToFormat)
				{

					String numberFormat = "#,##0.000";//the default
					//y axis number formatting
					if (numberToFormat >= 4.8)//magic numbers from Microsoft
					{
						numberFormat = "#,##0";
					}
					else if (numberToFormat >= .48)//yeah, magic number time
					{
						numberFormat = "#,##0.0";//1 digit
					}
					else if (numberToFormat >= .048)//yeah, magic number time
					{
						numberFormat = "#,##0.00";
					}

					axis.IsNumberFormatLinkedToSource = false;
					axis.NumberFormat = numberFormat;
					//axis.TickLabelPosition = TickLabelPositionType.Low;
				}

				private void FormatPlotArea(Color? fillColor = null, Color? lineColor = null, Int32 transparency = 0)
				{
					if (fillColor != null)
					{
						//{
						currentChart.PlotArea.Format.Fill.FillType = FillType.Solid;
						currentChart.PlotArea.Format.Fill.SolidFillColor.Color = (Color)fillColor;
						currentChart.PlotArea.Format.Line.FillFormat.FillType = FillType.Solid;
						currentChart.PlotArea.Format.Line.FillFormat.SolidFillColor.Color = (Color)lineColor;
					}
					else
					{
						currentChart.PlotArea.Format.Fill.FillType = FillType.NoFill;
						currentChart.PlotArea.Format.Line.FillFormat.FillType = FillType.NoFill;
					}
					if (transparency == 1)
					{
						currentChart.PlotArea.Format.Fill.SolidFillColor.Color = Color.Transparent;
					}
				}

				private void FormatLabel(IDataLabel label, String fontName, NullableBool fontBold, float fontHeight, LegendDataLabelPosition labelPosition, Boolean showSeriesName, Color fontColor, FillType fillType = FillType.Solid)
				{
					label.TextFormat.PortionFormat.LatinFont = new FontData(fontName);
					label.TextFormat.PortionFormat.FontBold = fontBold;// == true ? NullableBool.True : NullableBool.False;//I hate NullableBools
					label.TextFormat.PortionFormat.FontHeight = fontHeight;
					label.DataLabelFormat.Position = labelPosition;
					label.DataLabelFormat.Position = labelPosition;
					label.DataLabelFormat.ShowSeriesName = showSeriesName;
					label.TextFormat.PortionFormat.FillFormat.FillType = fillType;
					label.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = fontColor;
				}

				private void FormatLine(LineFormat lineFormat)//, String forceColor = "")
				{

					currentSeries.Format.Line.FillFormat.FillType = FillType.Solid;
					currentSeries.Format.Line.FillFormat.SolidFillColor.Color = lineFormat.lineColor;
					currentSeries.Format.Line.Width = lineFormat.lineWidth;
					currentSeries.Format.Line.DashStyle = (LineDashStyle)lineFormat.lineDashStyle;
					currentSeries.Format.Line.BeginArrowheadStyle = (LineArrowheadStyle)lineFormat.lineBeginArrow;
					currentSeries.Format.Line.EndArrowheadStyle = (LineArrowheadStyle)lineFormat.lineEndArrow;

					if (lineFormat.lineMarkerType != -1)
					{
						currentSeries.Marker.Format.Fill.FillType = FillType.Solid;
						currentSeries.Marker.Format.Fill.SolidFillColor.Color = lineFormat.lineColor;
						currentSeries.Marker.Format.Line.FillFormat.FillType = FillType.Solid;
						currentSeries.Marker.Format.Line.FillFormat.SolidFillColor.Color = lineFormat.lineColor;
						currentSeries.Marker.Symbol = (MarkerStyleType)lineFormat.lineMarkerType;
						currentSeries.Marker.Size = lineFormat.lineMarkerSize;
					}

					if (lineFormat.lineLabelLastOnly == true)//really should be lineLabelLastOnly
					{
						try
						{
							currentSeries.DataPoints[currentSeries.DataPoints.Count() - 2].Label.DataLabelFormat.ShowSeriesName = false;
						}
						catch (ArgumentOutOfRangeException)
						{
							//just here to catch the very first record (when there is only one point in the series this will error)
						}
						IDataLabel label = currentSeries.DataPoints[currentSeries.DataPoints.Count() - 1].Label;

						LabelFormat lf = new LabelFormat();
						if (currentSeries.Type == ChartType.LineWithMarkers)
						{
							lf.labelPosition = LegendDataLabelPosition.Right;
						}
						lf.color = lineFormat.lineColor;

						label.TextFormat.PortionFormat.LatinFont = new FontData(lf.fontName);
						label.TextFormat.PortionFormat.FontBold = lf.fontBold == true ? NullableBool.True : NullableBool.False;
						label.TextFormat.PortionFormat.FontHeight = lf.fontHeight;
						label.DataLabelFormat.Position = lf.labelPosition;
						label.DataLabelFormat.ShowSeriesName = lf.showSeriesName;
						label.TextFormat.PortionFormat.FillFormat.FillType = lf.fillType;
						label.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = lf.color;
					}

				}

				private void DoStackArea(System.Data.DataTable dataToAdd, System.Data.DataTable slideColors)
				{

					Int32 stackedAreaCount = 0;
					foreach (IChartSeries s in currentChart.ChartData.Series)
					{
						if (s.Type == ChartType.StackedArea)
							stackedAreaCount++;
					}
					DateTime pDate = new DateTime(2000, 1, 1);

					chartPosRow = 0;
					chartPosCol++;
					if (stackedAreaCount == 0) //this has never been added before, so we need to leave room for it
					{
						chartPosCol++;
					}

					currentSeries = currentChart.ChartData.Series.Add(fact.GetCell(0, chartPosRow, chartPosCol, "Stack " + currentChart.ChartData.Series.Count().ToString()), ChartType.StackedArea);
					chartPosRow++;

					for (int i = 0; i < dataToAdd.Rows.Count; i++)
					{
						currentSeries.DataPoints.AddDataPointForAreaSeries(fact.GetCell(0, chartPosRow, chartPosCol, dataToAdd.Rows[i].ItemArray[1]));
						chartPosRow++;
						if (stackedAreaCount == 0)//never been added before, so we put in the field names
						{
							chartPosRow--;//because the categories are shifted down if we don't
							chartPosRow--;

							chartPosCol--;//to shift it back to the correct column
							AddCategory(dataToAdd.Rows[i].ItemArray[0], false);
							chartPosCol++;//to shift the correct column forward again

							chartPosRow++;//this is done because of an issue with the AddCategory automatically adding a row, need to take it off here, and we need to do the double shift up, so it nets out to add 1
						}
					}
					currentSeries.PlotOnSecondAxis = true;

					if (slideColors.Rows[0].ItemArray[0].ToString() == "000000")
					{
						currentSeries.Format.Fill.FillType = FillType.NoFill;
					}
					else
					{
						if (slideColors.Rows.Count == 1)
						{//one color means color the box this color
							Color color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[0].ItemArray[0]);
							currentSeries.Format.Fill.FillType = FillType.Solid;
							currentSeries.Format.Fill.SolidFillColor.Color = color;
						}
						else
						{//two colors means we're doing a gradient
							Color color1 = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[0].ItemArray[0]);
							Color color2 = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[1].ItemArray[0]);
							currentSeries.Format.Fill.FillType = FillType.Gradient;
							currentSeries.Format.Fill.GradientFormat.GradientShape = GradientShape.Linear;
							currentSeries.Format.Fill.GradientFormat.GradientDirection = GradientDirection.FromCorner1;
							//newSeries.Format.Fill.GradientFormat.LinearGradientAngle = 135;
							currentSeries.Format.Fill.GradientFormat.GradientStops.Add(0.5f, color1);
							currentSeries.Format.Fill.GradientFormat.GradientStops.Add(1f, color2);
						}
					}

					currentChart.Axes.SecondaryHorizontalAxis.IsAutomaticMaxValue = false;
					currentChart.Axes.SecondaryHorizontalAxis.IsAutomaticMinValue = false;
					currentChart.Axes.SecondaryHorizontalAxis.MinValue = 2;// chart.Axes.HorizontalAxis.MinValue;
					currentChart.Axes.SecondaryHorizontalAxis.MaxValue = 100;// chart.Axes.HorizontalAxis.MaxValue;
					currentChart.Axes.SecondaryHorizontalAxis.IsVisible = false;

					currentChart.Axes.SecondaryVerticalAxis.IsVisible = false;
					currentChart.Axes.SecondaryVerticalAxis.MinValue = currentChart.Axes.VerticalAxis.MinValue;
					currentChart.Axes.SecondaryVerticalAxis.MaxValue = currentChart.Axes.VerticalAxis.MaxValue;

				}

				private void DoFixLegendPositions()
				{
					//we labeled all the legend shapes with "GroupShape" when we put them on the page, we now find those shapes and center them on the page
					Int32 shapeCount = 0;
					float shapeWidth = 0;
					float[] shapeSizes = new float[100];

					//Iterating through all shapes inside the slide
					for (int i = 0; i < currentSlide.Shapes.Count; i++)
					{
						if (currentSlide.Shapes[i].AlternativeText.Contains("GroupShape"))
						{
							shapeCount++;
							shapeWidth += currentSlide.Shapes[i].Width;
							shapeSizes[shapeCount - 1] = currentSlide.Shapes[i].Width;
						}
					}

					shapeSizes = shapeSizes.Take(shapeCount).ToArray();

					if (shapeCount > 0)
					{
						if (shapeWidth < 576)
						{
							//center it by moving every shape right the calculated amount
							float moveRight = (720 - shapeWidth) / 2;
							for (int i = 0; i < currentSlide.Shapes.Count; i++)
							{
								//If the alternative text of the slide matches with the required one then
								if (currentSlide.Shapes[i].AlternativeText.Contains("GroupShape"))
									currentSlide.Shapes[i].X += moveRight;
							}
						}
						else
						{
							//too much for one row, we need to split it to two
							Int32 shapeNum = 0;
							Int32[] shapeRow = new Int32[shapeCount];

							float shapeWidth1 = 0;
							float shapeWidth2 = 0;

							//this figures out which row the text should go on (half on the first row, half on the second, odd one out goes to the second row)
							for (int i = 0; i < currentSlide.Shapes.Count; i++)
							{
								if (currentSlide.Shapes[i].AlternativeText.Contains("GroupShape"))
								{
									shapeNum++;
									if (shapeNum <= shapeCount / 2)
									{
										shapeRow[shapeNum - 1] = 1;
										shapeWidth1 += currentSlide.Shapes[i].Width;
									}
									else
									{
										shapeRow[shapeNum - 1] = 2;
										shapeWidth2 += currentSlide.Shapes[i].Width;
									}
								}
							}


							float moveRight1 = (720 - shapeWidth1) / 2;
							float moveRight2 = (720 - shapeWidth2) / 2;

							float boxGap = 8;
							shapeNum = 0;
							////adjust for the gap between the labels
							for (int i = 0; i < currentSlide.Shapes.Count; i++)
							{
								if (currentSlide.Shapes[i].AlternativeText.Contains("GroupShape"))
								{
									shapeNum++;
									if (shapeRow[shapeNum - 1] == 1)
									{
										moveRight1 -= boxGap / 2;
									}
									else
									{
										moveRight2 -= boxGap / 2;
									}
								}
							}

							//readjust because the first item on each row shouldnt have moved
							moveRight1 += boxGap / 2;
							moveRight2 += boxGap / 2;

							//then we actually do the moving, pushing the labels to the right and moving them to the next row if necessary
							shapeNum = 0;
							for (int i = 0; i < currentSlide.Shapes.Count; i++)
							{
								if (currentSlide.Shapes[i].AlternativeText.Contains("GroupShape"))
								{
									shapeNum++;
									if (shapeRow[shapeNum - 1] == 1)
									{
										currentSlide.Shapes[i].X = moveRight1;
										moveRight1 += currentSlide.Shapes[i].Width;
										moveRight1 += boxGap;
									}
									else
									{
										currentSlide.Shapes[i].X = moveRight2;
										moveRight2 += currentSlide.Shapes[i].Width;
										moveRight2 += boxGap;
										currentSlide.Shapes[i].Y += 20;
									}
								}
							}
						}
					}
				}

				private void DoFixYAxisTitlePosition()
				{
					foreach (Shape s in currentSlide.Shapes)
					{
						if (s.AlternativeText == "YAxisLabelLeft" || s.AlternativeText == "YaxisLabelRight")
						{
							//check the position
							Int32 plotAreaTop = Convert.ToInt32(currentChart.Y + (currentChart.Height * currentChart.PlotArea.Y));
							Int32 plotAreaHeight = Convert.ToInt32(currentChart.Height * currentChart.PlotArea.Height);
							Int32 chartAreaHeight = Convert.ToInt32(currentChart.Height);

							if (chartAreaHeight != 363)//non-standard sized height
							{
								s.Height = chartAreaHeight;
							}
							else
							{
								if (s.Height < plotAreaHeight)
								{
									s.Height = plotAreaHeight;
									s.Y = s.Y - 10;//seem to be about 1/8 of an inch low every time
								}
							}
							//reset the label
							s.AlternativeText = "";
						}
					}
				}

				private Tuple<Double, Double> GetChartMax(Double chartMax)
				{
					if (chartMax == 0)//get rid of the bad input
					{
						return Tuple.Create(1.0, 1.0);
					}

					//this code adapted from http://peltiertech.com/calculate-nice-axis-scales-in-excel-vba/
					//We are assuming the minimum is 0

					Double dMax = chartMax * 1.02;
					Double dPower = System.Math.Log(dMax) / System.Math.Log(10);
					Double truncDPower = dPower;
					if (truncDPower < 0) { truncDPower--; }
					truncDPower = System.Math.Truncate(truncDPower);
					Double dScale = System.Math.Pow(10, (dPower - truncDPower));
					Double dSmall = 0;

					if (dScale <= 2.5)
					{
						dScale = 0.2;
						dSmall = 0.05;
					}
					else if (dScale <= 5)
					{
						dScale = 0.5;
						dSmall = 0.1;
					}
					else if (dScale < 7.5)
					{
						dScale = 1;
						dSmall = 0.2;
					}
					else
					{
						dScale = 2;
						dSmall = 0.5;
					}

					dScale = dScale * System.Math.Pow(10, truncDPower);
					dSmall = dSmall * System.Math.Pow(10, truncDPower);

					Double axisMax = dScale * (System.Math.Truncate(dMax / dScale) + 1);
					Double axisMajor = dScale;

					return Tuple.Create(axisMax, axisMajor);

				}


				private Tuple<Double, Double> GetChartMin(Double chartMin)
				{
					if (chartMin >= 0)//get rid of the bad input, which is just if it's >= 0, set it to 0, we're only concerned about negative mins here
					{
						return Tuple.Create(0.0, 0.0);
					}

					//convert the number to positive, treat it as a max value for the calculation, then revert it to negative for the output
					Tuple<Double, Double> newMin = GetChartMax(chartMin * -1);
					
					Double minValue = newMin.Item1 * -1;
					Double major = 0;//we don't need this value, so just set it to 0 for the return

					return Tuple.Create(minValue, major);

				}

				private ChartType GetChartType(String dataChartType)
				{
					//TODO: add other chart types as we need them or know them
					ChartType chartType = new ChartType();

					switch (dataChartType)
					{
						case "Area":
							chartType = ChartType.Area;
							break;
						case "StackArea":
							chartType = ChartType.StackedArea;
							break;
						case "Bar100":
						case "BarHoriz":
						case "PerfSum":
							chartType = ChartType.StackedBar;
							break;
						case "BarCluster":
							chartType = ChartType.ClusteredColumn;
							break;
						case "Line":
						case "LineMarker":
							chartType = ChartType.Line;//.ScatterWithStraightLines;//or just Line?
							break;
						case "Pie":
							chartType = ChartType.Pie;
							break;
						case "Scatter":
							chartType = ChartType.ScatterWithStraightLines;
							break;
						case "Bar":
						default:
							chartType = ChartType.StackedColumn;
							break;
					}

					return chartType;
				}

				private void FormatAxes(IChart chart, double primaryAxisMax, double secondaryAxisMax)
				{
					CheckAxisMax(chart.Axes.VerticalAxis, primaryAxisMax);

					if (chart.Axes.SecondaryVerticalAxis != null)
					{
						CheckAxisMax(chart.Axes.SecondaryVerticalAxis, secondaryAxisMax);
					}
				}

				private void AddVerticalAxisLabelLeft(String yAxisTitle, Int32 height, String chartID)
				{
					//AddVerticalAxisLabel(yAxisTitle, chartID, 45, 105, height: height);
					AddVerticalAxisLabel(yAxisTitle, currentChart.Axes.VerticalAxis);
				}

				private void AddVerticalAxisLabelRight(String yAxisTitle, Int32 height, String chartID)
				{
					//AddVerticalAxisLabel(yAxisTitle, chartID, 629, 105, height: height);
					AddVerticalAxisLabel(yAxisTitle, currentChart.Axes.SecondaryVerticalAxis);
				}

				private void AddVerticalAxisLabel(String yAxisTitle, IAxis axis)
				{

					axis.HasTitle = true;
					axis.Title.AddTextFrameForOverriding(yAxisTitle);
					IPortionFormat pf = axis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat;
					pf.FontHeight = 16;
					pf.FontBold = NullableBool.False;
					pf.LatinFont = new FontData("Tahoma");
					pf.FillFormat.FillType = FillType.Solid;
					pf.FillFormat.SolidFillColor.Color = defaultColor;

					System.Data.DataRow dr = chartFixes.NewRow();
					dr["SlideNumber"] = presentation.Slides.Count();
					dr["FixType"] = 6;//6 is a Y axis fix type
					chartFixes.Rows.Add(dr);

				}
				//private void AddVerticalAxisLabel(String yAxisTitle, String chartID, Int32 chartX, Int32 chartY, Int32 width = 30, Int32 height = 274)
				//{

				//	currentChart.Axes.VerticalAxis.HasTitle = true;
				//	currentChart.Axes.VerticalAxis.Title.AddTextFrameForOverriding(yAxisTitle);
				//	currentChart.Axes.VerticalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 16;
				//	currentChart.Axes.VerticalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.FontBold = NullableBool.False;
				//	currentChart.Axes.VerticalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.LatinFont = new FontData("Tahoma");
				//	currentChart.Axes.VerticalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.FillFormat.FillType = FillType.Solid;
				//	currentChart.Axes.VerticalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.FillFormat.SolidFillColor.Color = defaultColor;

				//	currentChart.Axes.SecondaryVerticalAxis
				//	//currentChart.Axes.VerticalAxis.LabelOffset = 1000;

				//	//////////width and height are standard values for a standard sized chart
				//	////////SizeF stringSize = Utilities.GetStringLengthInPixels(yAxisTitle);
				//	////////height = Convert.ToInt32(stringSize.Width);
				//	////////width = Convert.ToInt32(stringSize.Height);

				//	////////IAutoShape ashpVATitle = currentSlide.Shapes.AddAutoShape(ShapeType.Rectangle, chartX, chartY, width, height);
				//	////////if (chartX < 360)
				//	////////{
				//	////////	ashpVATitle.AlternativeText = "YAxisLabelLeft " + chartID;
				//	////////}
				//	////////else
				//	////////{
				//	////////	ashpVATitle.AlternativeText = "YAxisLabelRight " + chartID;
				//	////////}

				//	////////System.Data.DataRow dr = chartYAxisFix.NewRow();
				//	////////dr["SlideNumber"] = presentation.Slides.Count();
				//	////////chartYAxisFix.Rows.Add(dr);

				//	////////ashpVATitle.FillFormat.FillType = FillType.Solid;
				//	////////ashpVATitle.FillFormat.SolidFillColor.Color = Color.Transparent;
				//	////////ashpVATitle.ShapeStyle.LineColor.Color = Color.Transparent;
				//	////////ashpVATitle.AddTextFrame(" ");
				//	////////ITextFrame txtFrame = ashpVATitle.TextFrame;
				//	////////txtFrame.TextFrameFormat.TextVerticalType = TextVerticalType.Vertical270;
				//	////////IParagraph para = txtFrame.Paragraphs[0];
				//	//////////IPortion portion = para.Portions[0];
				//	////////Int32 fontHeight = 16;
				//	////////if (height < 250) fontHeight = 15;//small boxes //note, number was 274 before, I have to figure out how to get this to work dynamically
				//	////////if (yAxisTitle.Length > 35) fontHeight = 15;//long titles
				//	////////SetPortion(para.Portions[0], fontHeight: fontHeight, text: yAxisTitle);
				//}

				private void AddHorizontalAxisLabel(String yAxisTitle)
				{
					IAutoShape ashpVATitle = currentSlide.Shapes.AddAutoShape(ShapeType.Rectangle, 125, 404, 494, 30);//233,404,254,30
					ashpVATitle.FillFormat.FillType = FillType.Solid;
					ashpVATitle.FillFormat.SolidFillColor.Color = Color.Transparent;
					ashpVATitle.ShapeStyle.LineColor.Color = Color.Transparent;
					ashpVATitle.AddTextFrame(" ");
					ITextFrame txtFrame = ashpVATitle.TextFrame;
					IParagraph para = txtFrame.Paragraphs[0];
					//IPortion portion = para.Portions[0];
					SetPortion(para.Portions[0], fontHeight: 16, text: yAxisTitle);
				}

				private void CheckAxisMax(IAxis axis, double max)
				{

					if (axis.IsAutomaticMaxValue == false)
					{
						if (axis.MaxValue > max)
						{
							max = axis.MaxValue;
						}
					}

					AxisFormat(axis, max);
				}

				private void AddYAxisTitle(String yAxisTitle, Int32 height, String chartID, Boolean secondary = false)
				{
					if (secondary == false)
					{
						AddVerticalAxisLabelLeft(yAxisTitle, height, chartID);
					}
					else
					{
						AddVerticalAxisLabelRight(yAxisTitle, height, chartID);
					}
				}

				private void AddXAxisTitle(String xAxisTitle)
				{
					AddHorizontalAxisLabel(xAxisTitle);
				}

				private Double GetYAxisMax(Boolean rightAxis = false)
				{
					Double axisMax = 0;
					Boolean hasStack = false;
					Int32 numCols = 0;

					foreach (IChartSeries s in currentChart.ChartData.Series)
					{
						if (s.PlotOnSecondAxis == rightAxis)
						{
							switch (s.Type)
							{
								case ChartType.Line:
								case ChartType.LineWithMarkers:
								case ChartType.ClusteredColumn:
									foreach (IChartDataPoint dp in s.DataPoints)
									{
										if (dp.Value.ToDouble() > axisMax)
										{
											axisMax = dp.Value.ToDouble();
										}
									}
									break;
								case ChartType.ScatterWithStraightLines:
									foreach (IChartDataPoint dp in s.DataPoints)
									{
										if (Convert.ToDouble(dp.YValue.AsISingleCellChartValue.AsCell.Value) > axisMax)
										{
											axisMax = Convert.ToDouble(dp.YValue.AsISingleCellChartValue.AsCell.Value);
										}
									}
									break;
								case ChartType.StackedColumn:
									hasStack = true;
									if (s.DataPoints.Count() > numCols)
									{
										numCols = s.DataPoints.Count();
									}
									break;
								default:
									break;
							}
						}
					}

					if (hasStack == true)
					{
						Double[] stackCols = new Double[numCols];
						foreach (IChartSeries s in currentChart.ChartData.Series)
						{
							if (s.PlotOnSecondAxis == rightAxis)
							{
								if (s.Type == ChartType.StackedColumn)
								{
									for (Int32 n = 0; n < numCols; n++)
									{
										if (s.DataPoints[n].Value.AsISingleCellChartValue.AsCell != null)
										{
											if (s.DataPoints[n].Value.AsISingleCellChartValue.AsCell.Value != DBNull.Value)
											{
												stackCols[n] += Convert.ToDouble(s.DataPoints[n].Value.AsISingleCellChartValue.AsCell.Value);
											}
										}
									}
								}
							}
						}
						for (Int32 o = 0; o < numCols; o++)
						{
							if (stackCols[o] > axisMax)
							{
								axisMax = Convert.ToDouble(stackCols[o]);
							}
						}

					}

					return axisMax;
				}

				private Double GetYAxisMin(Boolean rightAxis = false)
				{
					Double axisMin = 999;
					Boolean hasStack = false;
					Int32 numCols = 0;

					foreach (IChartSeries s in currentChart.ChartData.Series)
					{
						if (s.PlotOnSecondAxis == rightAxis)
						{

							switch (s.Type)
							{
								case ChartType.Line:
								case ChartType.LineWithMarkers:
								case ChartType.ClusteredColumn:
									foreach (IChartDataPoint dp in s.DataPoints)
									{
										if (dp.Value.ToDouble() < axisMin)
										{
											axisMin = dp.Value.ToDouble();
										}
									}
									break;
								case ChartType.ScatterWithStraightLines:
									foreach (IChartDataPoint dp in s.DataPoints)
									{
										if (Convert.ToDouble(dp.YValue.AsISingleCellChartValue.AsCell.Value) < axisMin)
										{
											axisMin = Convert.ToDouble(dp.YValue.AsISingleCellChartValue.AsCell.Value);
										}
									}
									break;
								case ChartType.StackedColumn:
									hasStack = true;
									if (s.DataPoints.Count() > numCols)
									{
										numCols = s.DataPoints.Count();
									}
									break;
								default:
									break;
							}
						}
					}

					if (hasStack == true)
					{
						Double[] stackCols = new Double[numCols];
						foreach (IChartSeries s in currentChart.ChartData.Series)
						{
							if (s.PlotOnSecondAxis == rightAxis)
							{
								if (s.Type == ChartType.StackedColumn)
								{
									for (Int32 n = 0; n < numCols; n++)
									{
										if (s.DataPoints[n].Value.AsISingleCellChartValue.AsCell != null)
										{
											if (s.DataPoints[n].Value.AsISingleCellChartValue.AsCell.Value != DBNull.Value)
											{
												if (Convert.ToDouble(s.DataPoints[n].Value.AsISingleCellChartValue.AsCell.Value) < 0)
												{
													stackCols[n] += Convert.ToDouble(s.DataPoints[n].Value.AsISingleCellChartValue.AsCell.Value);
												}
											}
										}
									}
								}
							}
						}
						for (Int32 o = 0; o < numCols; o++)
						{
							if (stackCols[o] < axisMin)
							{
								axisMin = Convert.ToDouble(stackCols[o]);
							}
						}

					}

					return axisMin;
				}

				private void SetXAxisMaxToData()
				{

					Double xMax = 0;
					//TODO: Use AsposeConnector
					foreach (IChartSeries series in currentChart.ChartData.Series)
					{
						foreach (ChartDataPoint dp in series.DataPoints)
						{
							if (Convert.ToDouble(dp.XValue.Data) > xMax)
								xMax = Convert.ToDouble(dp.XValue.Data);
						}
					}

					if (xMax > 0)
					{
						currentChart.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
						currentChart.Axes.HorizontalAxis.MaxValue = xMax;
					}

				}

				private void DoFormatChartAxes(System.Data.DataRow chartRow)
				{
					//TODO: this whole function
					Double minValue;
					Double maxValue ;
					Double majorValue;
					Int32 setToData;
					Int32 squeezeData;

					//setting the axis maximums
					if (chartRow["ChartType"].ToString() != "PerfSum")
					{
						//start with the left Y axis
						if (chartRow["ChartType"].ToString() != "Bar100")
						{
							minValue = Utilities.CheckForNull(chartRow["YAxisMin"], 0.0);
							maxValue = Utilities.CheckForNull(chartRow["YAxisMax"], 0.0);
							majorValue = Utilities.CheckForNull(chartRow["YAxisMajorUnit"], 0.0);
							setToData = Utilities.CheckForNull(chartRow["SetYAxisMaxToData"], 0);
							squeezeData = Utilities.CheckForNull(chartRow["SqueezeYAxis"], 0);

							SetAxis(currentChart.Axes.VerticalAxis, false, minValue, maxValue, majorValue, setToData, squeezeData);
						
							//then the secondary Y axis
							if (currentChart.Axes.SecondaryVerticalAxis != null)
							{
								//then the right Y axis
								minValue = Utilities.CheckForNull(chartRow["SecondYAxisMin"], 0.0);
								maxValue = Utilities.CheckForNull(chartRow["SecondYAxisMax"], 0.0);
								majorValue = Utilities.CheckForNull(chartRow["SecondYAxisMajorUnit"], 0.0);
								//setToData = Utilities.CheckForNull(chartRow["SetYAxisMaxToData"], 0);

								SetAxis(currentChart.Axes.SecondaryVerticalAxis, true, minValue, maxValue, majorValue, setToData, squeezeData);
							}
						}
						
						//then the horizontal axis

						minValue = Utilities.CheckForNull(chartRow["XAxisMin"], 0.0);
						maxValue = Utilities.CheckForNull(chartRow["XAxisMax"], 0.0);
						majorValue = Utilities.CheckForNull(chartRow["XAxisMajorUnit"], 0.0);
						setToData = Utilities.CheckForNull(chartRow["SetXAxisMaxToData"], 0);

						if ((minValue + maxValue + majorValue) > 0)
						{
							SetAxis(currentChart.Axes.HorizontalAxis, false, minValue, maxValue, majorValue, setToData, 0);
						}

						if (setToData == 1)
						{
							SetXAxisMaxToData();
						}

					}

					//gridlines, almost always turned off
					currentChart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					currentChart.Axes.VerticalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;

					if (chartRow["ChartType"].ToString() != "Bar100")
					{
						currentChart.Axes.HorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					}
					else
					{
						currentChart.Axes.HorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.Solid;
						currentChart.Axes.HorizontalAxis.MajorGridLinesFormat.Line.FillFormat.SolidFillColor.Color = defaultColor;
					}
					currentChart.Axes.HorizontalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;

					if (currentChart.Axes.SecondaryHorizontalAxis != null)
					{
						currentChart.Axes.SecondaryHorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
						currentChart.Axes.SecondaryHorizontalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					}

					if (currentChart.Axes.SecondaryVerticalAxis != null)
					{
						currentChart.Axes.SecondaryVerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
						currentChart.Axes.SecondaryVerticalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
						currentChart.Axes.SecondaryHorizontalAxis.IsVisible = false;
					}

					SetPortionFormat(currentChart.Axes.HorizontalAxis.TextFormat.PortionFormat);

					ChartType chartType = GetChartType(chartRow["ChartType"].ToString());

					currentChart.Axes.HorizontalAxis.TextFormat.PortionFormat.FontHeight = axisFontHeight;

					SetPortionFormat(currentChart.Axes.VerticalAxis.TextFormat.PortionFormat);

					if (chartRow["ChartType"].ToString() == "Bar100")
					{
						currentChart.Axes.VerticalAxis.TextFormat.PortionFormat.FontHeight = 12;
						//currentChart.Axes.HorizontalAxis.TextFormat.PortionFormat.FontHeight = 12;
						currentChart.Axes.HorizontalAxis.NumberFormat = "0";
					}

					if (currentChart.Axes.SecondaryVerticalAxis != null)
					{
						SetPortionFormat(currentChart.Axes.SecondaryVerticalAxis.TextFormat.PortionFormat);
					}

					//Axis line color
					currentChart.Axes.HorizontalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
					currentChart.Axes.HorizontalAxis.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
					currentChart.Axes.VerticalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
					currentChart.Axes.VerticalAxis.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;

					if (currentChart.Axes.SecondaryVerticalAxis != null)
					{
						currentChart.Axes.SecondaryVerticalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
						currentChart.Axes.SecondaryVerticalAxis.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
					}

					SetPlotAreaPosition();
				}

				private void SetPlotAreaPosition()
				{
					//need to know:
					//is there a Y axis visible?
					Boolean yAxisVisible = currentChart.Axes.VerticalAxis.IsVisible;
					//is there a secondary y axis
					Boolean hasSecyAxis = currentChart.Axes.SecondaryVerticalAxis == null ? false : true;
					//and is it visible?
					Boolean secyAxisVisible = false;
					if (hasSecyAxis)
					{
						secyAxisVisible = currentChart.Axes.SecondaryVerticalAxis.IsVisible;
					}
					//is there an X axis visible?
					Boolean xAxisVisible = currentChart.Axes.HorizontalAxis.IsVisible;
					//scale of Y axis

					//values in a plot area are a range from 0-1, 1 being the full range. so a width of 0.5 will make the plot area be half of the entire chart area
					//we start with some default positions
					float width = 0.8735f;
					float height = 0.8135f;
					float x = 0.071f;
					float y = 0.023f;

					////these are magic numbers, but somehow they work. i got them by trial and error.
					currentChart.PlotArea.Width = width;
					currentChart.PlotArea.Height = height;//.8
					currentChart.PlotArea.X = x;//0f
					currentChart.PlotArea.Y = y;//0

				}

				private void SetAxis(IAxis axis, Boolean rightAxis, Double minValue, Double maxValue, Double majorValue, Int32 setToData, Int32 squeezeYAxis)
				{

					//calculate the max for the left Y axis
					Double yMax;
					Double yMin;

					if (axis == currentChart.Axes.HorizontalAxis)
					{
						yMax = 0;
						yMin = 0;
						//axis.TickLabelPosition = TickLabelPositionType.Low;
					}
					else
					{
						yMax = GetYAxisMax(rightAxis);
						yMin = GetYAxisMin(rightAxis);
						currentChart.Axes.HorizontalAxis.TickLabelPosition = TickLabelPositionType.Low;
					}

					Double savedYMax = yMax;//keeping this for later just in case

					if (setToData == 1)// && rightAxis == false)
					{
						axis.IsAutomaticMaxValue = false;
						axis.MaxValue = (double)yMax;
						Tuple<Double, Double> tup = GetChartMax(yMax);
						majorValue = tup.Item2;//just getting the major
						axis.IsAutomaticMajorUnit = false;
						axis.MajorUnit = (double)majorValue;
						axis.IsAutomaticMinValue = false;
					}
					else
					{
						if ((minValue + majorValue + maxValue) == 0)
						{
							//round it up
							Tuple<Double, Double> tup = GetChartMax(yMax);
							maxValue = tup.Item1;
							majorValue = tup.Item2;

							Tuple<Double, Double> tupmin = GetChartMin(yMin);
							minValue = tupmin.Item1;
							minValue = Math.Floor(minValue / majorValue) * majorValue;

						}
						else
						{
							//one of the values is not 0, we need to calc them (this came up when I was setting just the min value in the database
							Tuple<Double, Double> tup = GetChartMax(yMax);
							if (maxValue == 0)
							{
								maxValue = tup.Item1;
							}
							if (majorValue == 0)
							{
								majorValue = tup.Item2;
							}

						}

						//round it up
						if (maxValue != 0)
						{
							axis.IsAutomaticMaxValue = false;
							axis.MaxValue = (Double)maxValue;
							yMax = (Double)maxValue;
						}
						if (majorValue == 0 && maxValue != 0)
						{
							Tuple<Double, Double> tup = GetChartMax(yMax);
							majorValue = tup.Item2;//just getting the major
						}
						if (majorValue != 0)
						{
							axis.IsAutomaticMajorUnit = false;
							axis.MajorUnit = (Double)majorValue;
						}
						axis.IsAutomaticMinValue = false;
						axis.MinValue = (Double)minValue;

						//but some additional work here for barcharts
						//if the starting points are really big but the rest of the bars are really small, e.g. a waterfall
						//we may want to adjust the minimum a little so that we see more of the values
						//the idea for this is taken from a calculation in a spreadsheet that Dave created
						if (squeezeYAxis == 1)// && maxValue > 500)//500 just for effect, probably ought to look at this
						{
							SqueezeYAxis(axis, yMax, majorValue, savedYMax);
						}
					}

					CheckAxisMax(axis, yMax);
				}

				private void SqueezeYAxis(IAxis axis, Double yMax, Double majorValue, Double savedYMax)
				{
					//say you have a bar chart, where the lowest bar is at 15,000 and the highest bar is at 18,000
					//you may not want to show 0 to 15,000 on the chart, you might want to emphasize the changes between 15,000 and 18,000, e.g. in a waterfall
					//this function squeezes the chart to find those numbers
					//it finds the actual min and max in the chart data (GetActualMaxMin)
					//then it calculates good values for what should be the min, max and major values on the Y axis (CalcNewMaxMinMajor)
					//assuming it finds something good to use, it assigns those numbers to the axis

					if (currentChart.ChartData.Series.Count() > 0)
					{
						IChartSeries s = currentChart.ChartData.Series[0];
						if (s.Type == ChartType.StackedColumn || s.Type == ChartType.LineWithMarkers)
						{

							Tuple<Double, Double> maxMinResult = GetActualMaxMin(s);

							Tuple<Double, Double, Double> newMaxMin = CalcNewMaxMinMajor(maxMinResult.Item1, maxMinResult.Item2);

							if ((newMaxMin.Item1 + newMaxMin.Item2 + newMaxMin.Item3) > 0)//if they're all 0 do nothing, something went wrong somewhere and hopefully it will be caught in Graphics
							{
								axis.MinValue = newMaxMin.Item1;
								axis.MajorUnit = newMaxMin.Item2;
								axis.MaxValue = newMaxMin.Item3;
							}
						}
					}
				}

				private Tuple<Double, Double> GetActualMaxMin(IChartSeries s)
				{
					//check through each column in the chart, trying to get the smallest value and the largest
					//note that smallest value means the smallest of the bottom bars, in the stacked bar chart
					//note that largest value means the sum of all values in the bar, i.e. the highest point in the chart

					Int32 numberOfPoints = s.DataPoints.Count;
					Int32 numberOfSeries = currentChart.ChartData.Series.Count();

					Double firstColMin = 10000000;//really high number
					Double firstColMax = 0;

					for (Int32 n = 0; n < numberOfPoints; n++)//for each column
					{

						Double numberHit = 0;//have you hit any kind of non-zero yet? this will be the lowest bar
						Double thisColTotal = 0;//this sums up all the bars in the column
						Double thisColValue;

						for (Int32 o = 0; o < numberOfSeries; o++)
						{
							IChartSeries thisSeries = currentChart.ChartData.Series[o];
							if (thisSeries.Type == ChartType.StackedColumn || thisSeries.Type == ChartType.LineWithMarkers)
							{
								IChartDataPoint dp = thisSeries.DataPoints[n];
								try
								{
									thisColValue = Convert.ToDouble(dp.Value.AsISingleCellChartValue.AsCell.Value);

									if (thisSeries.Type == ChartType.StackedColumn)
									{
										thisColTotal += thisColValue;
									}
									else
									{
										thisColTotal = thisColValue;
									}

									if (thisColTotal > firstColMax)
									{
										firstColMax = thisColTotal;
									}

									if (thisColValue > 0)
									{
										if (thisSeries.Type == ChartType.StackedColumn)
										{
											if (numberHit == 0)
											{
												numberHit = thisColValue;//yes, we hit a number, put it in here to signal we actually got something and don't need to check it again
												if (numberHit < firstColMin)
												{
													firstColMin = numberHit;
												}
											}
										}
										else
										{
											//for a line, it doesn't matter which order it is, we just want the lowest value
											if (thisColValue < firstColMin)
											{
												firstColMin = thisColValue;
											}
										}
									}
								}
								catch (NullReferenceException)
								{
									//just means that the cell didnt have a value that could be read
									firstColMin = 0;
								}
							}
						}
					}

					return Tuple.Create(firstColMin, firstColMax);
				}

				private Tuple<Double, Double, Double> CalcNewMaxMinMajor(Double actualColMin, Double actualColMax)
				{
					//checks which of the following possible major values works best with the given maximum and minimum
					Double[] possibles = { 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000 };

					foreach (Double checkValue in possibles)
					{
						Tuple<Double, Double, Double, Boolean> getMinMax = minMaxCalcs(actualColMin, actualColMax, checkValue);

						Boolean isGoodResult = getMinMax.Item4;

						if (isGoodResult == true)
						{
							return Tuple.Create(getMinMax.Item1, getMinMax.Item2, getMinMax.Item3);
						}
					}

					//if it gets to the end we need to do something, so we return 0s all the way
					return Tuple.Create(0.0, 0.0, 0.0);
				}


				private Tuple<Double, Double, Double, Boolean> minMaxCalcs(Double actualColMin, Double actualColMax, Double valToTest)
				{
					//round the minimum down to the next value level (e.g. if min is 36 and valToTest is 10, it will round down to 30)
					//round the maximum up to the next value level
					//if these values work (they don't produce too many major values between them), return them with a Good flag set to true, otherwise return them false
					Boolean isGoodValue = false;

					Double lowVal = Math.Floor(actualColMin / valToTest) * valToTest;
					if (((actualColMin - lowVal) / valToTest) < 0.2)//make sure it's not too close to the break point
					{
						lowVal -= valToTest;
					}

					if (lowVal < valToTest)
					{
						lowVal = 0;
					}

					Double highVal = Math.Ceiling(actualColMax / valToTest) * valToTest;
					if (((highVal - actualColMax) / valToTest) < 0.2)
					{
						highVal += valToTest;
					}

					Double diffVal = highVal - lowVal;

					if ((diffVal / valToTest) < 10)//potentially up to 10 major values on the Y axis line
					{
						isGoodValue = true;
					}

					return Tuple.Create(lowVal, valToTest, highVal, isGoodValue);

				}



			#endregion

			#region Tables

				private void DoTable(System.Data.DataTable dataToAdd, System.Data.DataRow dataRow, System.Data.DataRow chartRow)
				{
					if (dataToAdd.Rows.Count > 0)
					{
						//RemoveCurrentChart();//getting rid of the placeholder, is this really necessary or should we just not add it in the first place?
						//it is added to make that code a little cleaner (don't have to add a whole lot of if statements at that point)

						//		//TODO: fix table settings

						Int32 tableRotate = Convert.IsDBNull(dataRow["LineFormat"]) ? 0 : Convert.ToInt32(dataRow["LineFormat"]);

						if (tableRotate == 1)
						{
							dataToAdd = RotateTable(dataToAdd);
						}

						 TableSetup table = new TableSetup();

						//add position from the charts table (add it here, it may be overridden below by the default in the table settings) (should it be the other way round?)
						Int32 tableX = Convert.IsDBNull(chartRow["XPos"]) ? -1 : Convert.ToInt32(chartRow["XPos"]);
						Int32 tableY = Convert.IsDBNull(chartRow["YPos"]) ? -1 : Convert.ToInt32(chartRow["YPos"]);

						if (tableX != -1)
						{
							table.tableX = tableX;
						}

						if (tableY != -1)
						{
							table.tableY = tableY;
						}

						//now change defaults from the table
						Int32 tableSettings = Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]);
						if (tableSettings != 0)
						{
							table = GetTableSettings(table, tableSettings);
						}


						Tuple<double[], double[]> colResult = TableRowColWidths(dataToAdd, table.defaultColWidth, table.defaultRowHeight);
						double[] dblCols = colResult.Item1;
						double[] dblRows = colResult.Item2;

						table.cols = dblCols;
						table.rows = dblRows;
						AddTable(table);

						FillTable(dataToAdd, table);
					}

				}

				private void AddTable(TableSetup table)
				{
					currentTable = currentSlide.Shapes.AddTable(table.tableX, table.tableY, table.cols, table.rows);
					currentTable.StylePreset = TableStylePreset.None;

					for (Int32 i = 0; i < currentTable.Rows.Count; i++)
					{
						for (Int32 j = 0; j < currentTable.Columns.Count; j++)
						{
							currentTable[j, i].MarginBottom = 0;
							currentTable[j, i].MarginLeft = 0;
							currentTable[j, i].MarginRight = 0;
							currentTable[j, i].MarginTop = 0;

							if (table.cellFill == FillType.Solid)
							{
								currentTable[j, i].FillFormat.FillType = FillType.Solid;
								currentTable[j, i].FillFormat.SolidFillColor.Color = table.cellColor;
							}

							if (table.cellBorderFill == FillType.Solid)
							{
								currentTable[j, i].BorderLeft.FillFormat.FillType = FillType.Solid;
								currentTable[j, i].BorderRight.FillFormat.FillType = FillType.Solid;
								currentTable[j, i].BorderTop.FillFormat.FillType = FillType.Solid;
								currentTable[j, i].BorderBottom.FillFormat.FillType = FillType.Solid;

								currentTable[j, i].BorderLeft.FillFormat.SolidFillColor.Color = table.cellBorderColor;
								currentTable[j, i].BorderRight.FillFormat.SolidFillColor.Color = table.cellBorderColor;
								currentTable[j, i].BorderTop.FillFormat.SolidFillColor.Color = table.cellBorderColor;
								currentTable[j, i].BorderBottom.FillFormat.SolidFillColor.Color = table.cellBorderColor;
							}
							else
							{
								currentTable[j, i].BorderLeft.FillFormat.FillType = FillType.NoFill;
								currentTable[j, i].BorderRight.FillFormat.FillType = FillType.NoFill;
								currentTable[j, i].BorderTop.FillFormat.FillType = FillType.NoFill;
								currentTable[j, i].BorderBottom.FillFormat.FillType = FillType.NoFill;

							}

						}
					}

				}

				private void FillTable(System.Data.DataTable dataToAdd, TableSetup table)
				{

					for (int i = dataToAdd.Rows.Count - 1; i >= 0; i--) //why go backwards here? because otherwise cells following a merge overwrite the merge settings
					{
						for (int j = dataToAdd.Columns.Count - 1; j >= 0; j--)
						{

							//TODO: Use AsposeConnector
							//ICell cell = tbl[i, j];
							TableCell cell = new TableCell(table);
							cell.cellX = i;
							cell.cellY = j;

							String output = dataToAdd.Rows[i].ItemArray[j].ToString();
							String format = "";

							if (output.Contains("<FMT "))
							{
								format = output.Substring(0, output.IndexOf(">"));//save the FMT string
								output = output.Substring(output.IndexOf(">") + 1);//get rid of the FMT string from the output
							}

							if (format != "")
							{
								String newFormat;
								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "Merge=");
								if (newFormat != "")
								{
									Int32 mergeWidth = Convert.ToInt32(newFormat.Substring(0, newFormat.IndexOf(",")));
									Int32 mergeHeight = Convert.ToInt32(newFormat.Substring(newFormat.IndexOf(",") + 1));
									//tbl.MergeCells(tbl[i, j], tbl[i + mergeWidth - 1, j + mergeHeight - 1], false);
									cell.mergeWidth = mergeWidth;
									cell.mergeHeight = mergeHeight;
								}

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "Align=");
								if (newFormat != "")
								{
									switch (newFormat)
									{
										case "Center":
											//TODO: Use AsposeConnector
											cell.alignment = Aspose.Slides.TextAlignment.Center;
											break;
										case "Right":
											//TODO: Use AsposeConnector
											cell.alignment = Aspose.Slides.TextAlignment.Right;
											break;
										case "Left":
										default:
											//TODO: Use AsposeConnector
											cell.alignment = Aspose.Slides.TextAlignment.Left;
											break;
									}
								}

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "fontBold=");
								if (newFormat != "")
									cell.fontBold = true;

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "fontHeight=");
								if (newFormat != "")
									cell.fontHeight = (float)Convert.ToDouble(newFormat);

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "Color=");
								if (newFormat != "")
								{
									cell.cellColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
									//TODO: Use AsposeConnector
									cell.cellFillType = Aspose.Slides.FillType.Solid;//because if they defined a color it must be solid, regardless of the overriding cellFill value
								}

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "Transparent=");
								if (newFormat != "")
									cell.cellColor = Color.Transparent;

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "ColorFont=");
								if (newFormat != "")
									cell.fontColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "RightBorder=");
								if (newFormat != "")
								{
									//rightBorder = true;
									//TODO: Use AsposeConnector
									cell.borderRightFillType = Aspose.Slides.FillType.Solid;
									cell.borderRightColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
								}

                                newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "LeftBorder=");
                                if (newFormat != "")
                                {
                                    //rightBorder = true;
                                    //TODO: Use AsposeConnector
                                    cell.borderLeftFillType = Aspose.Slides.FillType.Solid;
                                    cell.borderLeftColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
                                }

                                newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "TopBorder=");
                                if (newFormat != "")
                                {
                                    //bottomBorder = true;
                                    //TODO: Use AsposeConnector
                                    cell.borderBottomFillType = Aspose.Slides.FillType.Solid;
                                    cell.borderBottomColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
                                }

                                newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "BottomBorder=");
								if (newFormat != "")
								{
									//bottomBorder = true;
									//TODO: Use AsposeConnector
									cell.borderBottomFillType = Aspose.Slides.FillType.Solid;
									cell.borderBottomColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
								}

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "AllBorders=");
								if (newFormat != "")
								{
									//bottomBorder = true;
									//TODO: Use AsposeConnector
									cell.borderBottomFillType = Aspose.Slides.FillType.Solid;
									//cell.borderBottomColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
                                    cell.borderBottomColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
                                    cell.borderTopFillType = Aspose.Slides.FillType.Solid;
									//cell.borderTopColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
                                    cell.borderTopColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
                                    cell.borderLeftFillType = Aspose.Slides.FillType.Solid;
									//cell.borderLeftColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
                                    cell.borderLeftColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
                                    cell.borderRightFillType = Aspose.Slides.FillType.Solid;
									//cell.borderRightColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
                                    cell.borderRightColor = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);

                                }

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "ThickWhiteBorder=");
								if (newFormat != "")
								{
									//bottomBorder = true;
									//TODO: Use AsposeConnector
									cell.borderBottomFillType = Aspose.Slides.FillType.Solid;
									cell.borderBottomColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
									cell.borderTopFillType = Aspose.Slides.FillType.Solid;
									cell.borderTopColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
									cell.borderLeftFillType = Aspose.Slides.FillType.Solid;
									cell.borderLeftColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
									cell.borderRightFillType = Aspose.Slides.FillType.Solid;
									cell.borderRightColor = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
									cell.borderThickness = 3;
								}

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "RightMargin=");
								if (newFormat != "")
									cell.marginRight = Convert.ToDouble(newFormat);

								newFormat = Sa.PresBuilder.Utilities.GetFormatString(format, "LeftMargin=");
								if (newFormat != "")
									cell.marginLeft = Convert.ToDouble(newFormat);

							}

							//if (output.Contains("<b>") && output.Contains("</b>"))
							//{
							//	cell.fontBold = true;
							//}


							if (output != "")
							{
								cell.cellText = output;
							}

							FillTableCell(cell);
							//cell.marginLeft = 10;
						}
					}
				}

				private void FillTableCell(TableCell cell)
				{
					ICell tableCell = currentTable[cell.cellX, cell.cellY];

					if (cell.mergeWidth > 0 || cell.mergeHeight > 0)
					{
						currentTable.MergeCells(tableCell, currentTable[cell.cellX + cell.mergeWidth - 1, cell.cellY + cell.mergeHeight - 1], false);
					}

					//if (cell.cellText.Contains("</") || cell.cellText.Contains("/>"))//if it has some html with a close tag
					//{

					if (cell.cellText != "")
					{
					tableCell.TextFrame.Paragraphs.Clear();

						tableCell.TextFrame.Paragraphs.AddFromHtml(cell.cellText);//adds it as an additional paragraph
					}
						//if (tableCell.TextFrame.Paragraphs.Count > 1)//now there should be more than one, but there may be the rare case where there is not, such as the rare <b></b> data point
						//	tableCell.TextFrame.Paragraphs.RemoveAt(0);//so we have to delete the original paragraph
					//}
					//else
					//{
					//	tableCell.TextFrame.Text = cell.cellText;//just put it in as text
					//}

					//String legendName = HttpUtility.HtmlEncode(cell.cellText);
					////and if there's a >= or <= they get messed up, so this fixes them back again
					//legendName = legendName.Replace("&gt;=", "&ge;");
					//legendName = legendName.Replace("&lt;=", "&le;");

					//tableCell.TextFrame.Paragraphs.RemoveAt(0);
					//tableCell.TextFrame.Paragraphs.AddFromHtml(cell.cellText);


					tableCell.MarginBottom = cell.marginBottom;
					tableCell.MarginLeft = cell.marginLeft;
					tableCell.MarginRight = cell.marginRight;
					tableCell.MarginTop = cell.marginTop;

					tableCell.TextAnchorType = cell.textAnchor;
					tableCell.FillFormat.FillType = cell.cellFillType;
					tableCell.FillFormat.SolidFillColor.Color = cell.cellColor;

					if (tableCell.TextFrame.Paragraphs.Count > 0)
					{
						tableCell.TextFrame.Paragraphs[0].ParagraphFormat.Alignment = cell.alignment;

						foreach (Portion p in tableCell.TextFrame.Paragraphs[0].Portions)
						{
							p.PortionFormat.FontHeight = cell.fontHeight;
							p.PortionFormat.FillFormat.FillType = FillType.Solid;
							p.PortionFormat.FillFormat.SolidFillColor.Color = cell.fontColor;
							//p.PortionFormat.FontBold = cell.fontBold ? NullableBool.True : NullableBool.False; //NullableBools are the bane of my existence
						}
					}


					if (cell.borderBottomFillType != FillType.NoFill)
					{
						tableCell.BorderBottom.FillFormat.FillType = cell.borderBottomFillType;
						tableCell.BorderBottom.FillFormat.SolidFillColor.Color = cell.borderBottomColor;
						if (cell.borderThickness != 0)
						{
							tableCell.BorderBottom.Width = cell.borderThickness;
						}
					}

					if (cell.borderRightFillType != FillType.NoFill)
					{
						tableCell.BorderRight.FillFormat.FillType = cell.borderRightFillType;
						tableCell.BorderRight.FillFormat.SolidFillColor.Color = cell.borderRightColor;
						if (cell.borderThickness != 0)
						{
							tableCell.BorderRight.Width = cell.borderThickness;
						}
					}

					if (cell.borderLeftFillType != FillType.NoFill)
					{
						tableCell.BorderLeft.FillFormat.FillType = cell.borderLeftFillType;
						tableCell.BorderLeft.FillFormat.SolidFillColor.Color = cell.borderLeftColor;
						if (cell.borderThickness != 0)
						{
							tableCell.BorderLeft.Width = cell.borderThickness;
						}
						//if i change the left i have to change the right of the one to the left (otherwise it overwrites for some reason in the aspose library)
						if (cell.cellX > 0)
						{
							currentTable[cell.cellX - 1, cell.cellY].BorderRight.FillFormat.FillType = cell.borderLeftFillType;
							currentTable[cell.cellX - 1, cell.cellY].BorderRight.FillFormat.SolidFillColor.Color = cell.borderLeftColor;
						}

					}

					if (cell.borderTopFillType != FillType.NoFill)
					{
						tableCell.BorderTop.FillFormat.FillType = cell.borderTopFillType;
						tableCell.BorderTop.FillFormat.SolidFillColor.Color = cell.borderTopColor;
						if (cell.borderThickness != 0)
						{
							tableCell.BorderTop.Width = cell.borderThickness;
						}
						//if i change the top i have to change the bottom of the one above (otherwise it overwrites for some reason in the aspose library)
						if (cell.cellY > 0)
						{
							currentTable[cell.cellX, cell.cellY - 1].BorderBottom.FillFormat.FillType = cell.borderTopFillType;
							currentTable[cell.cellX, cell.cellY - 1].BorderBottom.FillFormat.SolidFillColor.Color = cell.borderTopColor;
						}

					}
					//tableCell.MarginLeft = 7;

				}

				private System.Data.DataTable RotateTable(System.Data.DataTable tableToRotate)
				{
					//takes a table and flips it XY, does nothing else
					System.Data.DataTable rotatedTable = new System.Data.DataTable();
					Int32 column = 1;
					foreach (System.Data.DataRow row in tableToRotate.Rows)
					{
						rotatedTable.Columns.Add(column.ToString(), typeof(String));
						column++;
					}

					for (Int32 n = 0; n < tableToRotate.Columns.Count; n++)
					{
						System.Data.DataRow row = rotatedTable.NewRow();//rotatedTable.Rows.Add();
						for (Int32 o = 0; o < tableToRotate.Rows.Count; o++)
						{
							row[o] = tableToRotate.Rows[o].ItemArray[n].ToString();
						}
						rotatedTable.Rows.Add(row);
					}

					return rotatedTable;
				}

				private TableSetup GetTableSettings(TableSetup table, Int32 tableSettings)
				{

					//looks up each value in the TableFormats and changes the value if it is populated
					foreach (System.Data.DataRow row in slideTableFormatsTable.Rows)
					{
						if ((Int32)row["SlideTableFormatID"] == tableSettings) //they match
						{
							if (row["TableX"] != DBNull.Value) table.tableX = (Int32)row["TableX"];
							if (row["TableY"] != DBNull.Value) table.tableY = (Int32)row["TableY"];
							if (row["FontHeight"] != DBNull.Value) table.fontHeight = (float)row["FontHeight"];
							if (row["RowHeight"] != DBNull.Value) table.defaultRowHeight = (Int32)row["RowHeight"];
							if (row["ColWidth"] != DBNull.Value) table.defaultColWidth = Convert.ToDouble(row["ColWidth"]);
							if (row["CellFill"] != DBNull.Value)
							{
								if (row["CellFill"].ToString() == "0")
									//TODO: Use AsposeConnector
									table.cellFill = Aspose.Slides.FillType.NoFill;
								else
									//TODO: Use AsposeConnector
									table.cellFill = Aspose.Slides.FillType.Solid;
							}
							//TODO: Use AsposeConnector
							if (row["BorderFill"] != DBNull.Value) table.cellBorderFill = Aspose.Slides.FillType.Solid;
							if (row["BorderColorGroup"] != DBNull.Value)
							{
								//get the first color in this group
								System.Data.DataTable shortColorsTable = Utilities.GetShortColorTable(slideColorsTable, (Int32)row["BorderColorGroup"]);
								foreach (System.Data.DataRow colorRow in shortColorsTable.Rows)
								{
									if (Convert.ToInt32(colorRow["ColorOrder"]) == 1)
										table.cellBorderColor = System.Drawing.ColorTranslator.FromHtml("#" + colorRow["Color"].ToString());
								}
							}
						}
					}

					return table;

				}

				private Tuple<double[], double[]> TableRowColWidths(System.Data.DataTable dataToAdd, Double defaultColWidth, Int32 defaultRowHeight)
				{

					int q = 0;
					Double colWidth;
					double[] dblCols = new double[dataToAdd.Rows.Count];
					double[] dblRows = new double[dataToAdd.Columns.Count];

					//must read the data to see if there are format instructions in it
					foreach (System.Data.DataRow row in dataToAdd.Rows)
					{
						colWidth = defaultColWidth;//default value
						if (row.ItemArray[0].ToString().Contains("colWidth="))
						{
							String newWidth = row.ItemArray[0].ToString();
							newWidth = newWidth.Substring(newWidth.IndexOf("colWidth="));
							newWidth = newWidth.Substring(0, newWidth.IndexOf(">"));
							if (newWidth.IndexOf(" ") > 0)
								newWidth = newWidth.Substring(0, newWidth.IndexOf(" "));
							newWidth = newWidth.Substring(9);
							colWidth = Convert.ToInt32(newWidth);
						}
						dblCols[q] = colWidth;
						q++;
					}

					Int32 rowHeight;
					for (int c = 0; c < dataToAdd.Columns.Count; c++)
					{
						rowHeight = defaultRowHeight;//default value
						if (dataToAdd.Rows[0].ItemArray[c].ToString().Contains("rowHeight="))
						{
							String newHeight = dataToAdd.Rows[0].ItemArray[c].ToString();
							newHeight = newHeight.Substring(newHeight.IndexOf("rowHeight="));
							newHeight = newHeight.Substring(0, newHeight.IndexOf(">"));
							if (newHeight.IndexOf(" ") > 0)
								newHeight = newHeight.Substring(0, newHeight.IndexOf(" "));
							newHeight = newHeight.Substring(10);
							rowHeight = Convert.ToInt32(newHeight);
						}
						dblRows[c] = rowHeight;
					}

					return Tuple.Create(dblCols, dblRows);
				}

				private void DoTrafficLight(System.Data.DataTable dataToAdd, System.Data.DataRow dataRow, System.Data.DataRow chartRow)
				{
					Int32 rowCount = dataToAdd.Rows.Count;
					String firstColWidth = "216";
					String regColWidth = "50";
					String thinColWidth = "10";

					if (rowCount > 8)
					{
						thinColWidth = "5";
					}
					if (rowCount > 9) // has 9 or more boxes
					{
						firstColWidth = "180";
					}

					if (rowCount > 10) //has 10 or more boxes
					{
						regColWidth = "42";
					}

					if (rowCount > 0)
					{
						dataToAdd.Rows[0].SetField(0, "<FMT colWidth=" + firstColWidth + ">" + dataToAdd.Rows[0].ItemArray[0].ToString());

						//set row heights 
						for (int o = 1; o < dataToAdd.Columns.Count; o++)
						{
							if (dataToAdd.Rows[0].ItemArray[o].ToString() == "")
							{
								dataToAdd.Rows[0].SetField(o, "<FMT rowHeight=10>");
							}
							else
							{
								dataToAdd.Rows[0].SetField(o, "<FMT rowHeight=50 fontHeight=16>" + dataToAdd.Rows[0].ItemArray[o].ToString());
							}
						}

						if (rowCount > 1)
						{
							for (int n = 1; n < rowCount; n++)
							{
								dataToAdd.Rows[n].SetField(0, "<FMT colWidth=" + regColWidth + " Align=Center fontHeight=16 fontBold=True>" + dataToAdd.Rows[n].ItemArray[0].ToString());
								for (int o = 1; o < dataToAdd.Columns.Count; o++)
								{
									//TODO: fix this so it reads from color table
									//boxcolor
									String boxColor = "";
									String fontColor = " ColorFont=292929";
									if (dataToAdd.Rows[n].ItemArray[o].ToString() != "")
									{
										Int32 boxValue = Convert.ToInt32(dataToAdd.Rows[n].ItemArray[o].ToString());
										if (boxValue < 25)
										{
											boxColor = "C00000";// "F94545";//249 69 69
											fontColor = " ColorFont=FFFFFF";
										}
										if (boxValue >= 25 && boxValue < 50)
										{
											boxColor = "F79646";// "FF9933";//255 153 51
										}
										if (boxValue >= 50 && boxValue < 75)
										{
											boxColor = "FFD966";// "FFE000";//255 224 0
										}
										if (boxValue >= 75)
										{
											boxColor = "00B050";// "33CC33";//51 204 51
											fontColor = " ColorFont=FFFFFF";
										}
										boxColor = " AllBorders=true Color=" + boxColor;
									}
									dataToAdd.Rows[n].SetField(o, "<FMT Align=Center" + fontColor + boxColor + ">" + dataToAdd.Rows[n].ItemArray[o].ToString());
								}
							}

							for (int p = dataToAdd.Rows.Count - 1; p > 1; p--)
							{
								DataRow toInsert = dataToAdd.NewRow();
								toInsert.SetField(0, "<FMT colWidth=" + thinColWidth + ">");
								dataToAdd.Rows.InsertAt(toInsert, p);
							}
						}
						DoTable(dataToAdd, dataRow, chartRow);

					}
				}

			#endregion

			#region Annotations

				private void SetAnnotation(System.Data.DataRow annotation)
				{
					//get variables from the datarow
					String text = fieldReplace(annotation["AnnotationText"].ToString());
					float annotationLeft = (float)annotation["Left"];
					float annotationTop = (float)annotation["Top"];
					float annotationWidth = (float)annotation["Width"];
					float annotationHeight = (float)annotation["Height"];
					Int32 fontHeight = (Int32)annotation["FontHeight"];
					Boolean wrap = Convert.IsDBNull(annotation["Wrap"]) ? false : Convert.ToBoolean(annotation["Wrap"]);
					Boolean backColor = Convert.IsDBNull(annotation["BackColor"]) ? false : Convert.ToBoolean(annotation["BackColor"]);
					Color color = Convert.IsDBNull(annotation["Color"]) ? defaultColor : System.Drawing.ColorTranslator.FromHtml("#" + annotation["Color"].ToString());
					Int32 alignment = (Int32)annotation["Alignment"];

					TextAlignment align;
					switch (alignment)
					{
						case 2:
							align = TextAlignment.Center;
							break;
						case 3:
							align = TextAlignment.Right;
							break;
						case 1:
						default:
							align = TextAlignment.Left;
							break;
					}

					//add the shape
					IAutoShape shp = currentSlide.Shapes.AddAutoShape(ShapeType.Rectangle, annotationLeft, annotationTop, annotationWidth, annotationHeight);

					if (backColor == false) //color the background appropriately
					{
						shp.FillFormat.FillType = FillType.NoFill;
					}
					else
					{
						shp.FillFormat.FillType = FillType.Solid;
						//TODO: why these colors? they are the standard plot area color, but maybe this should just be transparent?
						shp.FillFormat.SolidFillColor.Color = Color.FromArgb(234, 234, 234);
					}
					shp.ShapeStyle.LineColor.Color = Color.Transparent;

					//add the text to the shape
					ITextFrame tf = ((IAutoShape)shp).TextFrame;
					tf.TextFrameFormat.WrapText = wrap ? NullableBool.True : NullableBool.False;//how else to convert from Bool to NullableBool?;
					SetPortion(tf.Paragraphs[0].Portions[0], color: color, fontHeight: fontHeight, text: text);
					tf.Paragraphs[0].ParagraphFormat.Alignment = align;
				}

			#endregion

			#region PerfSum

				private void ResetPerfSums()
				{
					perfSumCharts = new IChart[10];
					perfSumData = new Double[10, 20];
					perfSumCount = 0;
				 }

				private void RecalcPerfSums()
				{
					//calculate the new position numbers for the bars
					CalcPerfSumPositions();

					//reset the charts by pushing the new position numbers into them
					if (perfSumCount > 1)
					{
						for (int i = 0; i < perfSumCount; i++)
						{
							IChart chart2 = perfSumCharts[i];

							foreach (IChartSeries series in chart2.ChartData.Series)
							{
								switch (series.Type)
								{
									case ChartType.StackedBar:
										//these are the bars, we further identify them by their series name
										switch (series.Name.ToString())
										{
											case "Minimum":
												AddPerfSumSeriesDataPoint(series, ChartType.StackedBar, perfSumData[i, 14]);
												break;
											case "Q12":
												AddPerfSumSeriesDataPoint(series, ChartType.StackedBar, perfSumData[i, 15] - perfSumData[i, 14]);
												break;
											case "Q23":
												AddPerfSumSeriesDataPoint(series, ChartType.StackedBar, perfSumData[i, 16] - perfSumData[i, 15]);
												break;
											case "Q34":
												AddPerfSumSeriesDataPoint(series, ChartType.StackedBar, perfSumData[i, 17] - perfSumData[i, 16]);
												break;
											case "Maximum":
												AddPerfSumSeriesDataPoint(series, ChartType.StackedBar, perfSumData[i, 18] - perfSumData[i, 17]);
												break;
											default:
												break;
										}
										break;
									case ChartType.ScatterWithMarkers:
										//the pacesetter
										AddPerfSumSeriesDataPoint(series, ChartType.ScatterWithMarkers, perfSumData[i, 19]);
										break;
									case ChartType.ScatterWithStraightLines:
										//bar labels and unit data
										if (series.Labels[0].AsIOverridableText.TextFrameForOverriding.Text == "")
										{
											//if it has no label, it is one of the bar numbers
											for (Int32 j = 0; j < 5; j++)
											{
												//get the data amount, convert to a label so we can compare
												String labelvalue = String.Format(((perfSumData[i, j] > 2000) ? "{0:n0}" : "{0:n2}"), perfSumData[i, j]);

												if (series.Name.ToString() == labelvalue)
												{
													Double valueY = (Double)series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value;
													AddPerfSumSeriesDataPoint(series, ChartType.ScatterWithStraightLines, perfSumData[i, j + 14], valueY);

													LegendDataLabelPosition labelPos = LegendDataLabelPosition.Bottom;
													Int32 fontHeight = 9;

													if (valueY == 1)
													{
														fontHeight = 12;
														if (j == 0)
														{
															labelPos = LegendDataLabelPosition.Left;
														}
														if (j == 4)
														{
															labelPos = LegendDataLabelPosition.Right;
														}
													}
													FormatLabel(series.DataPoints[0].Label, "Tahoma", NullableBool.False, fontHeight, labelPos, true, defaultColor, FillType.Solid);
												}
											}
										}
										else
										{
											//if it has a label, it is a unit
											Double unitValue = ((perfSumData[i, 18] - perfSumData[i, 14]) * (Convert.ToDouble(series.Name.ToString()) - perfSumData[i, 0]) / (perfSumData[i, 4] - perfSumData[i, 0])) + perfSumData[i, 14];
											String unitLabel = series.Labels[0].AsIOverridableText.TextFrameForOverriding.Text;
											Double valueY = (Double)series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value;
											AddPerfSumSeriesDataPoint(series, ChartType.ScatterWithStraightLines, unitValue, valueY);

											IDataLabel lbl = series.DataPoints[0].Label;
											lbl.DataLabelFormat.ShowSeriesName = false;
											lbl.DataLabelFormat.Position = LegendDataLabelPosition.Center;
											lbl.TextFrameForOverriding.Text = unitLabel.Trim();

											SetPortion(lbl.TextFrameForOverriding.Paragraphs[0].Portions[0], "Arial", fontBold: NullableBool.True, fontHeight: 13);
										}
										break;
									default:
										break;
								}
							}
							Spread(chart2, true);
						}
					}
				}

				private void PerfSumAddOneBar(String barName, Double barValue, Color barColor)//Int32 rownum, Double barValue, String barColor)
				{
					////adds one segment of the bar to a perfsum
					//IChartData cd = currentChart.ChartData;
					//IChartDataWorkbook fact = currentChart.ChartData.ChartDataWorkbook;
					//IChartSeries series;
					//String barText = barValue.ToString();


					this.AddSeries(barName, ChartType.StackedBar);
					this.AddDataPoint(barValue, ChartType.StackedBar);


					//series = cd.Series.Add(fact.GetCell(0, rownum, 0, barText), currentChart.Type);//series name is the original value, so we can use it later when recalculating
					//series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, rownum, 1, barValue));
					currentSeries.Format.Fill.FillType = FillType.Solid;
					currentSeries.Format.Fill.SolidFillColor.Color = barColor;
					//and add the border
					if (barName == "Minimum")//first bar is the spacer, which has no color and should have no line
					{
						currentSeries.Format.Line.FillFormat.FillType = FillType.NoFill;
					}
					else
					{
						currentSeries.Format.Line.FillFormat.FillType = FillType.Solid;
						currentSeries.Format.Line.FillFormat.SolidFillColor.Color = Color.Black;
					}

					currentSeries.ParentSeriesGroup.Overlap = 100;
				}

				private void CalcPerfSumPositions()
				{
					if (perfSumCount > 1)
					{
						//okay, stealing all this from the PerfSum calculator Excel file, just for ease
						//TODO: may redo this once done (haha) to get it more efficient

						Double minVal = 0;
						Double maxVal = 0;
						//cols 0-5 are the values from the db: min, Q12, Q23, Q34, max, ps
						//col 6 is range, col 7 is midpoint, col 8-13 are calced positions
						for (int n = 0; n < perfSumCount; n++)
						{
							perfSumData[n, 6] = perfSumData[n, 4] - perfSumData[n, 0];

							perfSumData[n, 7] = perfSumData[n, 2];
							if (perfSumData[n, 7] == 0) //need to check this, could be null?
								perfSumData[n, 7] = perfSumData[n, 1];

							for (int o = 8; o <= 13; o++)
								perfSumData[n, o] = (perfSumData[n, (o - 8)] - perfSumData[n, 7]) / perfSumData[n, 6];

							for (int o = 8; o <= 12; o++)
							{
								if (perfSumData[n, o] < minVal) { minVal = perfSumData[n, o]; }
								if (perfSumData[n, o] > maxVal) { maxVal = perfSumData[n, o]; }
							}
						}

						Double maxLength = maxVal - minVal;
						Double centerPosition = (1 - maxVal / maxLength) * 100;

						//cols 14-19 are the final positions for the bars
						for (int n = 0; n < perfSumCount; n++)
						{
							for (int o = 14; o <= 19; o++)
								perfSumData[n, o] = perfSumData[n, (o - 6)] / maxLength * 100 + centerPosition;
						}

						//return bars;
					}
				}

				private void PerfSumAddTheBars(System.Data.DataRow dataRow, System.Data.DataTable slideColors)
				{
					//this adds the bar in a perfsum chart, a blank white filler on the left followed by the four quartile bars, with labels and the pacesetter
					//TODO: do we always use labels? do we always have a pacesetter? maybe need flags for those
					//TODO: figure out which parts of this should go into the database, if any
					//what other parts can be variable?

					//format the chart
					currentChart.Axes.VerticalAxis.IsVisible = false;
					//currentChart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					currentChart.Axes.HorizontalAxis.IsVisible = false;
					//currentChart.Axes.HorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					SetHorizontalAxisMinMax(-10, 110);//this allows us to have some space out to the sides after we plot the data from 0-100
					currentChart.PlotArea.Width = 1f;
					currentChart.PlotArea.X = 0f;
					currentChart.PlotArea.Height = 0.75f;
					currentChart.PlotArea.Y = 0f;

					perfSumCount++;
					perfSumCharts[perfSumCount - 1] = currentChart;

					////add each of the bars
					PerfSumAddOneBar("Minimum", Double.Parse(dataRow["Minimum"].ToString()), System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[0].ItemArray[0].ToString()));
					perfSumData[perfSumCount - 1, 0] = Double.Parse(dataRow["Minimum"].ToString());
					PerfSumAddOneBar("Q12", Double.Parse(dataRow["Q12"].ToString()), System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[1].ItemArray[0].ToString()));
					perfSumData[perfSumCount - 1, 1] = Double.Parse(dataRow["Q12"].ToString());
					PerfSumAddOneBar("Q23", Double.Parse(dataRow["Q23"].ToString()), System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[2].ItemArray[0].ToString()));
					perfSumData[perfSumCount - 1, 2] = Double.Parse(dataRow["Q23"].ToString());
					if (dataRow["Q34"] != System.DBNull.Value)
					{
						PerfSumAddOneBar("Q34", Double.Parse(dataRow["Q34"].ToString()), System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[3].ItemArray[0].ToString()));
						perfSumData[perfSumCount - 1, 3] = Double.Parse(dataRow["Q34"].ToString());
					}
					PerfSumAddOneBar("Maximum", Double.Parse(dataRow["Maximum"].ToString()), System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[4].ItemArray[0].ToString()));
					perfSumData[perfSumCount - 1, 4] = Double.Parse(dataRow["Maximum"].ToString());

					////plot each of the text labels
					AddPerfSumSeriesWithPoint(dataRow["Minimum"].ToString(), Double.Parse(dataRow["Minimum"].ToString()), LegendDataLabelPosition.Left);//bar left value
					AddPerfSumSeriesWithPoint(dataRow["Maximum"].ToString(), Double.Parse(dataRow["Maximum"].ToString()), LegendDataLabelPosition.Right);//bar right value
					AddPerfSumSeriesWithPoint(dataRow["Q12"].ToString(), Double.Parse(dataRow["Q12"].ToString()), LegendDataLabelPosition.Bottom);
					AddPerfSumSeriesWithPoint(dataRow["Q23"].ToString(), Double.Parse(dataRow["Q23"].ToString()), LegendDataLabelPosition.Bottom);
					if (dataRow["Q34"] != System.DBNull.Value)
					{
						if (dataRow["Q34"].ToString() != dataRow["Q23"].ToString())
							AddPerfSumSeriesWithPoint(dataRow["Q34"].ToString(), Double.Parse(dataRow["Q34"].ToString()), LegendDataLabelPosition.Bottom);
						else
							AddPerfSumSeriesWithPoint(dataRow["Q34"].ToString(), Double.Parse(dataRow["Q34"].ToString()), LegendDataLabelPosition.Bottom, false, "", false);//we don't want to show both when they match, which is when there are only three quartiles
					}

					//plot the pacesetter
					AddPerfSumSeriesWithPoint(dataRow["Pacesetter"].ToString(), Double.Parse(dataRow["Pacesetter"].ToString()), LegendDataLabelPosition.Center, true);//pacesetter box
					perfSumData[perfSumCount - 1, 5] = Double.Parse(dataRow["Pacesetter"].ToString());
				}

				private void AddPerfSumSeriesWithPoint(String seriesName, Double seriesX, LegendDataLabelPosition labelPos, Boolean showPSBox = false, String originalValue = "", Boolean showLabel = true)
				{
					//adds a single label to a perfsumbar
					//TODO: several hardcoded items in here, including font name and size, label color etc which should be made variable
					//TODO: also sets the secondary axis every time, might be able to clean that up by setting it when the initial perfsum is built
					//IChartData cd = chart.ChartData;
					//IChartDataWorkbook fact = currentChart.ChartData.ChartDataWorkbook;
					//IChartSeries series;

					Double seriesY;
					Int32 fontHeight;
					String fontName = "Tahoma";
					NullableBool fontBold = NullableBool.False;

					if (labelPos == LegendDataLabelPosition.Bottom)//the quartile labels below the chart
					{
						seriesY = 0.8;
						fontHeight = 9;
					}
					else
					{//the minimum and maximum labels at each end of the chart
						seriesY = 1;
						if (originalValue == "")
							fontHeight = 12;
						else
						{
							fontHeight = 16;// labelFontSize;
							fontName = "Arial";
							fontBold = NullableBool.True;
						}
					}

					//find the first blank row and column
					//Int32 seriesRow = 1;
					//Int32 seriesCol = 1;
					//while (fact.GetCell(0, seriesRow, 0).Value != null) seriesRow += 1;
					//while (fact.GetCell(0, 0, seriesCol).Value != null) seriesCol += 1;

					if (originalValue == "")
					{
						Double seriesVal = Double.Parse(seriesName.Trim());//the following few lines format the number for the label, with the simple rule that if <2000 it will format like 0.00, if >2000 it will format like 2,000

						String barText;
						if (seriesVal > 2000)
							barText = String.Format("{0:n0}", seriesVal);
						else
							barText = String.Format("{0:n2}", seriesVal);

						//series = currentChart.ChartData.Series.Add(fact.GetCell(0, 0, seriesCol, barText), ChartType.ScatterWithMarkers);
						this.AddSeries(barText, ChartType.ScatterWithMarkers);
					}
					else
					{
						//series = currentChart.ChartData.Series.Add(fact.GetCell(0, 0, seriesCol, originalValue.Trim()), ChartType.ScatterWithMarkers);
						this.AddSeries(originalValue.Trim(), ChartType.ScatterWithMarkers);
					}
					currentSeries.PlotOnSecondAxis = true;


					currentChart.Axes.SecondaryHorizontalAxis.IsAutomaticMaxValue = false;
					currentChart.Axes.SecondaryHorizontalAxis.IsAutomaticMinValue = false;
					currentChart.Axes.SecondaryHorizontalAxis.MinValue = -10;
					currentChart.Axes.SecondaryHorizontalAxis.MaxValue = 110;
					currentChart.Axes.SecondaryHorizontalAxis.IsVisible = false;
					currentChart.Axes.SecondaryVerticalAxis.IsVisible = false;
					//currentChart.Axes.SecondaryVerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					//currentChart.Axes.SecondaryHorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					//currentChart.Axes.SecondaryVerticalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					//currentChart.Axes.SecondaryHorizontalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;

					//currentSeries.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, seriesRow, 0, seriesX), fact.GetCell(0, seriesRow, seriesCol, seriesY));
					AddDataPoint(seriesX, seriesY, ChartType.ScatterWithMarkers);

					if (showPSBox == false)
					{
						currentSeries.Marker.Symbol = MarkerStyleType.None;
						IDataLabel lbl = currentSeries.DataPoints[0].Label;
						if (originalValue == "")
						{
							FillType fillType = FillType.Solid;
							if (showLabel == false)
								fillType = FillType.NoFill;
							FormatLabel(lbl, fontName, fontBold, fontHeight, labelPos, true, defaultColor, fillType);
						}
						else
						{
							lbl.DataLabelFormat.ShowSeriesName = false;
							lbl.DataLabelFormat.Position = labelPos;
							lbl.TextFrameForOverriding.Text = seriesName.Trim();
							if (showLabel == false)
								lbl.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.FillFormat.FillType = FillType.NoFill;
							else
								SetPortion(lbl.TextFrameForOverriding.Paragraphs[0].Portions[0], fontName, fontBold: fontBold, fontHeight: fontHeight);
						}
					}
					else
					{//formatting the pacesetter box
						currentSeries.Marker.Symbol = MarkerStyleType.Square;
						currentSeries.Marker.Format.Fill.FillType = FillType.Solid;
						currentSeries.Marker.Format.Fill.SolidFillColor.Color = Color.White;
						currentSeries.Marker.Format.Line.FillFormat.FillType = FillType.Solid;
						currentSeries.Marker.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
						currentSeries.Marker.Size = 8;
					}

				}

				private void AddPerfSumSeriesDataPoint(IChartSeries series, ChartType chartType, Double value, Double? valueY = null)
				{
					series.DataPoints[0].Remove();

					switch (chartType)
					{
						case ChartType.StackedBar:
							series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, value));
							break;
						case ChartType.ScatterWithMarkers:
							series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, value), fact.GetCell(0, chartPosRow, chartPosCol + 1, 1));
							chartPosRow++;
							break;
						case ChartType.ScatterWithStraightLines:
							series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, value), fact.GetCell(0, chartPosRow, chartPosCol + 1, valueY));
							break;
						default:
							break;
					}

					chartPosRow++;

				}

			#endregion

			#region Other
		
				private void Spread(IChart chart, Boolean upDown)
				{
					//if upDown = true then we're going to move them up and down, if false then left and right
					//the assumption with startRow and startCol is that anything from here down/right is something that needs to move, and anything above/left is a fixed position (like a perfsumbar)

					//System.Diagnostics.Debug.Print("Start Spread");
					//Boolean isCollision = true;
					//Int32 endRow = startRow;
					//while (chart.ChartData.ChartDataWorkbook.GetCell(0, endRow, 0).Value != null)
					//{
					//	endRow++;
					//}
					//endRow--;//adjust for the last row

					Bitmap b = new Bitmap(612, 60);

					IDataLabel[] labels = new IDataLabel[100];//how many labels on a chart? it'd be ridiculous to have 100, right?
					IChartDataPoint[] points = new IChartDataPoint[100];
					Int32 labelcounter = 0;

					foreach (IChartSeries series in chart.ChartData.Series)
					{
						//	
						switch (series.Type)
						{
							case ChartType.ScatterWithStraightLines:
								if (series.Labels[0].AsIOverridableText.TextFrameForOverriding.Text != "")
								{
									//we only care if it has a label
									labels[labelcounter] = series.Labels[0];
									points[labelcounter] = series.DataPoints[0];
									labelcounter++;
								}

								break;
							default:
								break;

						}

					}
					if (labelcounter > 1)//if it's 0 or 1 we don't need to spread it
					{
						labels = labels.Take(labelcounter).ToArray();//to get to just the labels we need
						points = points.Take(labelcounter).ToArray();

						Boolean isCollision = true;
						while (isCollision == true)
						{
						//	//fix any collisions we find
							isCollision = false;//set it to false, if we find any as we go through we reset it to true so that it will make another pass
							for (int n = 0; n < labelcounter; n++)
							{
								for (int o = 0; o < labelcounter; o++)
								{
									if (o > n) //checking each label above the one we're looking at
									{
										//	ITextFrame frame1 = chart.ChartData.Series[n - 1].DataPoints[0].Label.TextFrameForOverriding;
										//	ITextFrame frame2 = chart.ChartData.Series[n].DataPoints[0].Label.TextFrameForOverriding;
										ITextFrame frame1 = labels[n].TextFrameForOverriding;
										ITextFrame frame2 = labels[o].TextFrameForOverriding;

										//	Double frame1X = Double.Parse(chart.ChartData.Series[n - 1].DataPoints[0].XValue.Data.ToString());
										//	Double frame1Y = Double.Parse(chart.ChartData.Series[n - 1].DataPoints[0].YValue.Data.ToString());
										//	Double frame2X = Double.Parse(chart.ChartData.Series[n].DataPoints[0].XValue.Data.ToString());
										//	Double frame2Y = Double.Parse(chart.ChartData.Series[n].DataPoints[0].YValue.Data.ToString());

										Double frame1X = Double.Parse(points[n].XValue.Data.ToString());
										Double frame1Y = Double.Parse(points[n].YValue.Data.ToString());
										Double frame2X = Double.Parse(points[o].XValue.Data.ToString());
										Double frame2Y = Double.Parse(points[o].YValue.Data.ToString());


											if (LabelOverlap(frame1, frame2, frame1X, frame1Y, frame2X, frame2Y, b) == true)
											{
										//		System.Diagnostics.Debug.Print(chart.ChartData.Series[n - 1].DataPoints[0].Label.TextFrameForOverriding.Text + " collides with " + chart.ChartData.Series[n].DataPoints[0].Label.TextFrameForOverriding.Text);

												if (upDown == true)
												{
													isCollision = true;

										//			while (isCollision)
										//			{
										//				//frame1Y = Double.Parse(chart.ChartData.Series[n - 1].DataPoints[0].YValue.Data.ToString());
										//				frame1Y = Double.Parse(chart.ChartData.ChartDataWorkbook.GetCell(0, n, n - (startRow - startCol)).Value.ToString());
													frame1Y -= 0.01;
										//				frame2Y = Double.Parse(chart.ChartData.ChartDataWorkbook.GetCell(0, n + 1, n + 1 - (startRow - startCol)).Value.ToString());
													frame2Y += 0.01;
													if (frame1Y < 0.75)
													{
														//frame1Y = 0.75;
														//frame2Y += 0.01;
													}
													points[n].YValue.Data = frame1Y;
													points[o].YValue.Data = frame2Y;
										//				chart.ChartData.ChartDataWorkbook.GetCell(0, n, n - (startRow - startCol), frame1Y);//60 because the plot area is 60 points tall
										//				chart.ChartData.ChartDataWorkbook.GetCell(0, n + 1, n + 1 - (startRow - startCol), frame2Y);
										//				isCollision = LabelOverlap(frame1, frame2, frame1X, frame1Y, frame2X, frame2Y, b);
										//			}
												}
											}
									}
								}
							}

							////check for any leftover collisions
							//isCollision = true;
						}
					}
				}

				private Double[] CheckLabelDist(Double[] orig, string[] names)
				{

					Double[,] newVals = new Double[3, 2];
					Int32 diffmin = 6;
					newVals[0, 0] = orig[0];
					newVals[1, 0] = orig[1];
					newVals[2, 0] = orig[2];

					Double[] widths = new Double[3];

					//set font, size & style
					Font f = new Font("Tahoma", 9, FontStyle.Regular);
					//create a bmp / graphic to use MeasureString on
					Bitmap b = new Bitmap(720, 540);
					Graphics g = Graphics.FromImage(b);
					//measure the string

					widths[0] = g.MeasureString(names[0], f).Width;
					widths[1] = g.MeasureString(names[1], f).Width;
					widths[2] = g.MeasureString(names[2], f).Width;

					Double mult = 606.0 / 120.0;//504 because the chart is 504 pixels wide, 120 because the chart goes from -10 to 110

					newVals[0, 1] = newVals[0, 0] * mult;
					newVals[1, 1] = newVals[1, 0] * mult;
					newVals[2, 1] = newVals[2, 0] * mult;

					Double diff12 = (newVals[1, 1] - (widths[1] / 2) + 7.2) - (newVals[0, 1] + (widths[0] / 2) - 7.2);
					Double diff23 = (newVals[2, 1] - (widths[2] / 2) + 7.2) - (newVals[1, 1] + (widths[1] / 2) - 7.2);

					while (diff12 < diffmin || diff23 < diffmin)
					{
                        if (diff12 < diffmin)
						{
                            newVals[0, 0] -= 0.1;
                        }
						if (diff23 < diffmin)
						{
                            newVals[2, 0] += 0.1;
                        }

						newVals[0, 1] = newVals[0, 0] * mult;
						newVals[1, 1] = newVals[1, 0] * mult;
						newVals[2, 1] = newVals[2, 0] * mult;

						diff12 = (newVals[1, 1] - (widths[1] / 2) + 7.2) - (newVals[0, 1] + (widths[0] / 2) - 7.2);//7.2 because the standard label internal margin is 0.1" left/right
						diff23 = (newVals[2, 1] - (widths[2] / 2) + 7.2) - (newVals[1, 1] + (widths[1] / 2) - 7.2);
					}

					Double[] outVals = new Double[3];
					outVals[0] = newVals[0, 0];
					outVals[1] = newVals[1, 0];
					outVals[2] = newVals[2, 0];

					return outVals;

				}

				private Boolean LabelOverlap(ITextFrame label1, ITextFrame label2, Double label1X, Double label1Y, Double label2X, Double label2Y, Bitmap b)
				{
					Boolean overlap = false;

					Graphics g = Graphics.FromImage(b);

					IPortion p1 = label1.Paragraphs[0].Portions[0];
					IPortion por2 = label2.Paragraphs[0].Portions[0];
					Font f1 = new Font(p1.PortionFormat.LatinFont.FontName, p1.PortionFormat.FontHeight, FontStyle.Regular);
					Font f2 = new Font(por2.PortionFormat.LatinFont.FontName, por2.PortionFormat.FontHeight, FontStyle.Regular);
					SizeF size1 = new SizeF(g.MeasureString(label1.Text, f1));
					SizeF size2 = new SizeF(g.MeasureString(label2.Text, f2));
					float width1 = size1.Width - 14.4f;//padding 2/10 of inch (0.1 left and right)
					float height1 = size1.Height - 14.4f;//vertical padding 1/10 of inch (0.05 bot and top)
					float width2 = size2.Width - 14.4f;
					float height2 = size2.Height - 14.4f;

					Double mult = b.Width / 120;//need to figure out what the 120 should be, it is 120 because the chart goes from -10 to 110 but should be variable
					Double heightmult = b.Height / 2;//need to figure out what the 2 should be, it is 2 because the chart goes from 0 to 2 but should be variable

					Double x1left = (label1X * mult) - width1 / 2;
					Double x1right = (label1X * mult) + width1 / 2;
					Double x1bot = (label1Y * heightmult) - height1 / 2;
					Double x1top = (label1Y * heightmult) + height1 / 2;


					Double x2left = (label2X * mult) - width2 / 2;
					Double x2right = (label2X * mult) + width2 / 2;
					Double x2bot = (label2Y * heightmult) - height2 / 2;
					Double x2top = (label2Y * heightmult) + height2 / 2;

					if (x2left < x1right && x2right >= x1left)
					{
						if (x2bot < x1top && x2top >= x1bot)
						{
							overlap = true;
						}
					}

					return overlap;
				}

				private System.Data.DataTable GetDistX(System.Data.DataTable inputData)
				{
					inputData.Columns.Add("XValue", typeof(Double)).SetOrdinal(1);

					foreach (System.Data.DataRow row in inputData.Rows)
					{
						//row["XValue"] = GetDistX(currentSeries, Convert.ToDouble((row.ItemArray[2]==null? row.ItemArray[2] : 0)));
                        row["XValue"] = GetDistX(currentSeries, Convert.ToDouble(row.ItemArray[2]));
                    }
					inputData.AcceptChanges();

					return inputData;
				}

				private Double GetDistX(IChartSeries series, Double y)
				{
					//Give me the range and the Y value and I will give you the X value
					Double returnValue;

                    if (series == null)
                    {
                        returnValue = 0.00;
                        return returnValue;
                    }
                    else
                    {
                        Double minX = Convert.ToDouble(series.DataPoints[0].XValue.AsISingleCellChartValue.AsCell.Value);
                        Double minY = Convert.ToDouble(series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value);
                        Double maxX = Convert.ToDouble(series.DataPoints[series.DataPoints.Count - 1].XValue.AsISingleCellChartValue.AsCell.Value);
                        Double maxY = Convert.ToDouble(series.DataPoints[series.DataPoints.Count - 1].YValue.AsISingleCellChartValue.AsCell.Value);
                        Double cellX;
                        Double cellY;

                        if (minY > maxY) //this is a reverse order chart where higher = better, e.g. Thermal Efficiency
                        {
                            returnValue = GetDistXReverse(series, y);
                        }

                        else
                        {
                            if (y < minY)
                                returnValue = minX;
                            else
                                if (y > maxY)
                                returnValue = maxX;
                            else
                            {
                                for (int i = 0; i < series.DataPoints.Count; i++)
                                {
                                    cellX = Convert.ToDouble(series.DataPoints[i].XValue.AsISingleCellChartValue.AsCell.Value);
                                    cellY = Convert.ToDouble(series.DataPoints[i].YValue.AsISingleCellChartValue.AsCell.Value);

                                    if (cellY <= y)
                                    {
                                        minY = cellY;
                                        minX = cellX;
                                    }
                                }

                                for (int i = series.DataPoints.Count - 1; i > 0; i--)
                                {
                                    cellX = Convert.ToDouble(series.DataPoints[i].XValue.AsISingleCellChartValue.AsCell.Value);
                                    cellY = Convert.ToDouble(series.DataPoints[i].YValue.AsISingleCellChartValue.AsCell.Value);

                                    if (cellY >= y)
                                    {
                                        maxY = cellY;
                                        maxX = cellX;
                                    }
                                }
                                if (minY == maxY)//the rare case where there are multiple values in a distribution that match, this will catch them
                                {
                                    returnValue = Math.Max(minX, maxX);
                                }
                                else
                                {
                                    returnValue = minX + ((maxX - minX) * ((y - minY) / (maxY - minY)));//pct of diff
                                }
                            }
                        }
                        return returnValue;
                    }
				}

				private Double GetDistXReverse(IChartSeries series, Double y)
				{
					//Give me the range and the Y value and I will give you the X value

					Double returnValue;

					//String x = workbook.GetCell(0, 1, 0).Value.ToString();
					Double minX = Convert.ToDouble(series.DataPoints[0].XValue.AsISingleCellChartValue.AsCell.Value); //0;//assuming they're not going to be below 0? probably dangerous
					Double minY = Convert.ToDouble(series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value);
					Double maxX = Convert.ToDouble(series.DataPoints[series.DataPoints.Count - 1].XValue.AsISingleCellChartValue.AsCell.Value);
					Double maxY = Convert.ToDouble(series.DataPoints[series.DataPoints.Count - 1].YValue.AsISingleCellChartValue.AsCell.Value);
					Double cellX;
					Double cellY;

					if (y > minY)
						returnValue = minX;
					else
						if (y < maxY)
							returnValue = maxX;
						else
						{
							for (int i = 0; i < series.DataPoints.Count; i++)
							{
								cellX = Convert.ToDouble(series.DataPoints[i].XValue.AsISingleCellChartValue.AsCell.Value);
								cellY = Convert.ToDouble(series.DataPoints[i].YValue.AsISingleCellChartValue.AsCell.Value);

								if (cellY >= y)
								{
									minY = cellY;
									minX = cellX;
								}
							}

							for (int i = series.DataPoints.Count - 1; i >= 0; i--)
							{
								cellX = Convert.ToDouble(series.DataPoints[i].XValue.AsISingleCellChartValue.AsCell.Value);
								cellY = Convert.ToDouble(series.DataPoints[i].YValue.AsISingleCellChartValue.AsCell.Value);

								if (cellY <= y)
								{
									maxY = cellY;
									maxX = cellX;
								}
							}

							if (minY == maxY)//the rare case where there are multiple values in a distribution that match, this will catch them
							{
								returnValue = Math.Min(minX, maxX);
							}
							else
							{
								returnValue = minX + ((maxX - minX) * ((y - minY) / (maxY - minY)));//pct of diff
							}

						}

					return returnValue;
				}

				private void SetPortion(IPortion portion, String fontName = "Tahoma", Color? color = null, NullableBool fontBold = NullableBool.NotDefined, float? fontHeight = null, String text = "")
				{
					//passes to the formatting function, then sets the text
					IPortionFormat pfmt = portion.PortionFormat;
					SetPortionFormat(pfmt, fontName, color, fontBold, fontHeight);

					if (text != "")
					{
						portion.Text = text;
					}
				}

				private void SetPortion(IPortion portion, String text = "")
				{
					//this one just fills in the text without any formatting
					if (text != "")
					{
						portion.Text = text;
					}
				}

				private void SetPortionFormat(IBasePortionFormat pfmt, String fontName = "Tahoma", Color? color = null, NullableBool fontBold = NullableBool.False, float? fontHeight = null)
				{
					//does the actual formatting
					//why all this complication? because Aspose created two different PortionFormats, one a PortionFormat and one a ChartPortionFormat
					//they are essentially the same thing in different parts of the chart, but they are just different enough to be a PITA to handle
					//so I wrote code to bring them both into here and format them, rather than do a whole lot of duplication

					//the next two because they give compile time error if specified in the method call
					color = color ?? defaultColor;
					fontHeight = fontHeight ?? defaultFontHeight;

					//set text portion values depending on what is passed to the function
					if (fontName != "")
					{
						pfmt.LatinFont = new FontData(fontName);
					}

					pfmt.FillFormat.FillType = FillType.Solid;
					pfmt.FillFormat.SolidFillColor.Color = (System.Drawing.Color)color;

					pfmt.FontBold = fontBold;
					pfmt.FontHeight = (float)fontHeight;
				}

				private void SetPortionFormat(IPortionFormat pfmt, String fontName = "Tahoma", Color? color = null, NullableBool fontBold = NullableBool.False, float? fontHeight = null)
				{
					//converts an IPortionFormat to an IBasePortionFormat, then passes it to the next function
					SetPortionFormat(pfmt.AsIBasePortionFormat, fontName, color, fontBold, fontHeight);
				}

				private void SetPortionFormat(IChartPortionFormat pfmt, String fontName = "Tahoma", Color? color = null, NullableBool fontBold = NullableBool.False, float? fontHeight = null)
				{
					//converts an IChartPortionFormat to an IBasePortionFormat, then passes it to the next function
					SetPortionFormat(pfmt.AsIBasePortionFormat, fontName, color, fontBold, fontHeight);
				}

				//private void SetPortionFormat(IChartPortionFormat format)
				//{
				//	format.FillFormat.FillType = FillType.Solid;
				//	format.FillFormat.SolidFillColor.Color = defaultColor;
				//	format.LatinFont = new FontData("Tahoma");
				//	format.FontHeight = defaultFontHeight;
				//}

				private int DateTimeToInt(DateTime theDate)
				{
					//converts a datetime to an integer, which is used in certain charts to get the x axis to float properly (because Aspose doesnt support it)
					return (int)(theDate.Date - new DateTime(1900, 1, 1)).TotalDays + 2;
				}

				private String fieldReplace(String input)
				{

					input = input.Replace("@CurrencyCode", CurrencyToUse);
					input = input.Replace("@Option1", Option1);
					input = input.Replace("@Option2", Option2);
					input = input.Replace("@Option3", Option3);
					input = input.Replace("@Option4", Option4);
					input = input.Replace("@Option5", Option5);

					if (Metric == true)
					{
						input = input.Replace("@Btu", "MJ");
						input = input.Replace("@MBtu", "GJ");
					}
					else
					{
						input = input.Replace("@Btu", "Btu");
						input = input.Replace("@MBtu", "MBtu");
					}

					return input;
				}

			#endregion

		#endregion









		#region TestOnlyDoNotUse

		//public void RecalcPerfSums()
			//{
			//	//calculate the new position numbers for the bars
			//	CalcPerfSumPositions();

			//	//reset the charts by pushing the new position numbers into them
			//	if (perfSumCount > 1)
			//	{
			//		for (int i = 0; i < perfSumCount; i++)
			//		{
			//			IChart chart2 = perfSumCharts[i];

			//			foreach (IChartSeries series in chart2.ChartData.Series)
			//			{
			//				switch (series.Type)
			//				{
			//					case ChartType.StackedBar:
			//						//these are the bars, we further identify them by their series name
			//						switch (series.Name.ToString())
			//						{
			//							case "Minimum":
			//								series.DataPoints[0].Remove();
			//								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, 14]));
			//								chartPosRow++;
			//								break;
			//							case "Q12":
			//								series.DataPoints[0].Remove();
			//								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, 15] - perfSumData[i, 14]));
			//								chartPosRow++;
			//								break;
			//							case "Q23":
			//								series.DataPoints[0].Remove();
			//								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, 16] - perfSumData[i, 15]));
			//								chartPosRow++;
			//								break;
			//							case "Q34":
			//								series.DataPoints[0].Remove();
			//								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, 17] - perfSumData[i, 16]));
			//								chartPosRow++;
			//								break;
			//							case "Maximum":
			//								series.DataPoints[0].Remove();
			//								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, 18] - perfSumData[i, 17]));
			//								chartPosRow++;
			//								break;
			//							default:
			//								break;
			//						}
			//						break;
			//					case ChartType.ScatterWithMarkers:
			//						//the pacesetter
			//						series.DataPoints[0].Remove();
			//						series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, 19]), fact.GetCell(0, chartPosRow, chartPosCol + 1, 1));
			//						chartPosRow++;
			//						break;
			//					case ChartType.ScatterWithStraightLines:
			//						//bar labels and unit data
			//						if (series.Labels[0].AsIOverridableText.TextFrameForOverriding.Text == "")
			//						{
			//							//if it has no label, it is one of the bar numbers
			//							String labelvalue = "";
			//							for (Int32 j = 0; j < 5; j++)
			//							{
			//								//get the data amount, convert to a label so we can compare
			//								if (perfSumData[i, j] > 2000)
			//								{
			//									labelvalue = String.Format("{0:n0}", perfSumData[i, j]);
			//								}
			//								else
			//								{
			//									labelvalue = String.Format("{0:n2}", perfSumData[i, j]);
			//								}

			//								if (series.Name.ToString() == labelvalue)
			//								{
			//									Double valueY = (Double)series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value;
			//									series.DataPoints[0].Remove();
			//									series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, perfSumData[i, j + 14]), fact.GetCell(0, chartPosRow, chartPosCol + 1, valueY));
			//									chartPosRow++;

			//									LegendDataLabelPosition labelPos = LegendDataLabelPosition.Left;
			//									Int32 fontHeight;
			//									String fontName = "Tahoma";
			//									NullableBool fontBold = NullableBool.False;

			//									if (valueY == 1)
			//									{
			//										fontHeight = 12;
			//										if (j == 0)
			//										{
			//											labelPos = LegendDataLabelPosition.Left;
			//										}
			//										if (j == 4)
			//										{
			//											labelPos = LegendDataLabelPosition.Right;
			//										}
			//									}
			//									else
			//									{
			//										fontHeight = 9;
			//										labelPos = LegendDataLabelPosition.Bottom;
			//									}

			//									IDataLabel lbl = series.DataPoints[0].Label;
			//									FillType fillType = FillType.Solid;
			//									FormatLabel(lbl, fontName, fontBold, fontHeight, labelPos, true, defaultColor, fillType);

			//								}
			//							}
			//						}
			//						else
			//						{
			//							//if it has a label, it is a unit
			//							Double unitValue = Convert.ToDouble(series.Name.ToString());
			//							unitValue = ((perfSumData[i, 18] - perfSumData[i, 14]) * (unitValue - perfSumData[i, 0]) / (perfSumData[i, 4] - perfSumData[i, 0])) + perfSumData[i, 14];
			//							String unitLabel = series.Labels[0].AsIOverridableText.TextFrameForOverriding.Text;
			//							Double valueY = (Double)series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value;
			//							series.DataPoints[0].Remove();
			//							series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, chartPosRow, chartPosCol, unitValue), fact.GetCell(0, chartPosRow, chartPosCol + 1, valueY));
			//							chartPosRow++;

			//							IDataLabel lbl = series.DataPoints[0].Label;
			//							lbl.DataLabelFormat.ShowSeriesName = false;
			//							lbl.DataLabelFormat.Position = LegendDataLabelPosition.Center;
			//							lbl.TextFrameForOverriding.Text = unitLabel.Trim();

			//							Int32 fontHeight = 13;
			//							String fontName = "Arial";
			//							NullableBool fontBold = NullableBool.True;

			//							SetPortion(lbl.TextFrameForOverriding.Paragraphs[0].Portions[0], fontName, defaultColor, fontBold, fontHeight);

			//						}

			//						break;

			//					default:
			//						break;
			//				}
			//			}

			//			Spread(chart2, true);
			//		}
			//	}
		//}

		#endregion


	}
}
