﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

using Aspose.Slides;
using Aspose.Slides.Charts;
using System.Drawing;
////using S = System.Data;
//using System.Data;

class DataSeries
{
	//used to pass data from the controlling object to the chart
	//public ChartType chartType;
	//public System.Data.DataTable dataTable;
	//public Int32 barGapWidth;
	//public Color color;
	//public Boolean addManualLegend;
	//public String lineFormat;
	//public Boolean axis;
	public Double? secondYAxisMax = null;
	public Double? secondYAxisMajorUnit = null;
	public Double? secondYAxisMin = null;

}

class LabelFormat
{
	public String fontName;
	public Boolean fontBold;
	public float fontHeight;
	public LegendDataLabelPosition labelPosition;
	public Boolean showSeriesName;
	public FillType fillType;
	public Color color;

	public LabelFormat()
	{
		//constructor, default values
		fontName = "Arial";
		fontBold = true;
		fontHeight = 13;
		labelPosition = LegendDataLabelPosition.Center;
		showSeriesName = true;
		fillType = FillType.Solid;
		color = Color.FromArgb(41, 41, 41);
	}

}

class LineFormat
{
	public Color lineColor;
	public Double lineWidth;
	public Int32 lineDashStyle;
	public Int32 lineBeginArrow;
	public Int32 lineEndArrow;
	public Int32 lineMarkerType;
	public Int32 lineMarkerSize;
	public Boolean lineLabelLastOnly;

	public LineFormat()
	{
		//constructor, default values
		lineColor = Color.FromArgb(41, 41, 41);
		lineWidth = 3;
		lineDashStyle = -1;//Not Defined
		lineBeginArrow = 0;//None
		lineEndArrow = 0;//None
		lineMarkerType = -1;//Not Defined
		lineMarkerSize = 0;
		lineLabelLastOnly = false;
	}

	public void SetLineFormat(Color color, Double width, Int32 style, Int32 begin, Int32 end, Int32 type, Int32 size, Boolean last)
	{
		//get the settings for a lineformat from the data
		lineColor = color;
		lineWidth = width;
		lineDashStyle = style;
		lineBeginArrow = begin;
		lineEndArrow = end;
		lineMarkerType = type;
		lineMarkerSize = size;
		lineLabelLastOnly = last;
	}
}

//class ChartSetup
//{
//	//this is used for passing certain parameters to set up the charts
//	public String chartType = "";
//	public String xAxisTitle = "";
//	public String yAxisTitle = "";
//	public String chartTitle = "";
//	public Int32 xPos = 47;//these positions are default positions
//	public Int32 yPos = 97;
//	public Int32 width = 603;
//	public Int32 height = 363;
//	public Double? YAxisMax = null;
//	public Double? YAxisMajorUnit = null;
//	public Double? YAxisMin = null;
//	public Int32? XAxisMax = null;
//	public Decimal? XAxisMajorUnit = null;
//	public Int32? XAxisMin = null;

//}

class TableSetup
{
	public Int32 tableX = 40;
	public Int32 tableY = 200;
	public Int32 defaultRowHeight = 50;
	public Double defaultColWidth = 50;
	public FillType cellFill = FillType.Solid;
	public Color cellColor = Color.White;
	public FillType cellBorderFill = FillType.NoFill;
	public Color cellBorderColor = Color.Empty;
	public Color fontColor = Color.FromArgb(41, 41, 41);
	public Double[] cols;
	public Double[] rows;
	public float fontHeight = 18;
}

class TableCell
{
	public Int32 cellX;
	public Int32 cellY;
	public float fontHeight = 18;
	public TextAlignment alignment = TextAlignment.Left;
	public FillType cellFillType = FillType.Solid;
	public Color cellColor = Color.White;
	public Color fontColor = Color.FromArgb(41, 41, 41);
	public Color borderBottomColor = Color.Empty;
	public Color borderTopColor = Color.Empty;
	public Color borderLeftColor = Color.Empty;
	public Color borderRightColor = Color.Empty;
	public Double marginBottom = 0;
	public Double marginLeft = 0;
	public Double marginRight = 0;
	public Double marginTop = 0;
	public TextAnchorType textAnchor = TextAnchorType.Center;
	public FillType borderBottomFillType = FillType.NoFill;
	public FillType borderTopFillType = FillType.NoFill;
	public FillType borderLeftFillType = FillType.NoFill;
	public FillType borderRightFillType = FillType.NoFill;
	public Int32 mergeWidth = 0;
	public Int32 mergeHeight = 0;
	public String cellText = "";
	public Boolean fontBold = false;
	public float borderThickness = 0;

	public TableCell(TableSetup table)
	{
		//we pass the table setup it belongs to so we can set the default values to what the table is, where they have matching values
		fontHeight = table.fontHeight;
		cellFillType = table.cellFill;
		cellColor = table.cellColor;
		fontColor = table.fontColor;
		//borderBottomColor = table.cellBorderColor;
		//borderLeftColor = table.cellBorderColor;
		//borderRightColor = table.cellBorderColor;
		//borderTopColor = table.cellBorderColor;
		//borderBottomFillType = table.cellBorderFill;
		//borderLeftFillType = table.cellBorderFill;
		//borderRightFillType = table.cellBorderFill;
		//borderTopFillType = table.cellBorderFill;

	}
}

class PerfSumData
{
    public IChart perfSumChart;
    public Double[] points = new Double[20];

    public PerfSumData(IChart chart)
    {
        perfSumChart = chart;//set the chart 

    }
}


