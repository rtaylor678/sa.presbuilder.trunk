﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Aspose.Slides;
using Aspose.Slides.Charts;
using Aspose.Slides.Export;
using SQL = System.Data.SqlClient;
using S = System.Data;
using System.Data;

using DOX = DocumentFormat.OpenXml;
using DOXP = DocumentFormat.OpenXml.Packaging;
using DOXC = DocumentFormat.OpenXml.Drawing.Charts;
using DOXA = DocumentFormat.OpenXml.Drawing;
using DOXPr = DocumentFormat.OpenXml.Presentation;

using Sa.PresBuilder.AsposeConnector;

//using DocumentFormat.OpenXml.Drawing.Charts;
//using DocumentFormat.OpenXml.Packaging;

namespace Sa.AsposePresBuilder
{
	using ce = CustomExtensions.ExtentionMethods;

	public class AsposeTest
	{
		#region variables

		public class CurrentState
		{
            public String status;
			public Int32 workingSlideNumber;
			public Int32 totalSlides;
		}
		//need to pull at least some of these out of here and not have them be global

		//public:
		public String Option1 = "";
		public String Option2 = "";
		public String Option3 = "";
		public String Option4 = "";
		public String Option5 = "";
		public String CurrencyToUse = "USD";
		public Boolean Metric = false;
		public String templateToUse;
		public String filenameToSaveAs;
		public String server = "";
		public String database = "";
		public String username = "";
		public String password = "";

		//public Int32 numberOfSlides = 0;

		//private:
		Int32 wsIndex = 0; //used throughout, just sets the index of the workbook to the first worksheet
		Color defaultColor = Color.FromArgb(41, 41, 41);
		Font defaultFont = new Font("Tahoma", 16, FontStyle.Regular);
		Double primaryVerticalMax = 0;
		Double secondaryVerticalMax = 0;
		//Boolean hasRightVerticalAxis = false;
		//Boolean isBarChart = false;
		//String CompanyList = "";
		//String StudyList = "";
		//String StudyYear = "";

		Int32 slideNumber = 0;
		Int32 slideTableRow = 0;
		//Int32 slidesRemaining;

		S.DataTable dateFix = new S.DataTable("dateFix");
		S.DataTable chartPosition = new S.DataTable("chartPosition");
		S.DataTable bar7ChartPosition = new S.DataTable("bar7ChartPosition");
//		Boolean ConvertToMetric = false;
		S.DataTable slideColorsTable = new S.DataTable("SlideColors");
		S.DataTable slideAnnotationsTable = new S.DataTable("SlideAnnotations");
		S.DataTable slideLineFormatsTable = new S.DataTable("SlideLineFormats");
		S.DataTable slideTableFormatsTable = new S.DataTable("SlideTableFormats");
		//String masterTemplate = @"K:\STUDY\Power\Templates\Presentation Master Template.pptx";
        String masterTemplate = @"K:\Development\Common\PresBuilder\Presentation Master Template.pptx";
		Presentation pres;
		//Presentation template;
		//Boolean testP2 = false;
		//AsposeConnect p2;
		//AsposeConnect othertemplate;
		SQL.SqlConnection conn;
		S.DataTable slidesTable;
		S.DataTable slideChartsTable;
		S.DataTable slideDataTable;
		CurrentState state;
		//System.ComponentModel.BackgroundWorker bgWorker;
		Int32 labelFontSize = 13;


		#endregion

		#region Presentation Creation

		//public void CreatePresFromTemplate(String templateToUse, String filenameToSaveAs, String option1, String option2, String option3, String option4, String option5, String currencyCode, Boolean metric)
		public void CreatePresFromTemplate()
		{
			//this version of the CreatePresFromTemplate is for the Console version of the app
			//the Console version doesn't handle the BackgroundWorker like the Form version does
			//so, here we create one, and pass it through to the other entry point, it is just so that method has the right items called
			//nothing else is done with it, it just gets thrown away
			System.ComponentModel.BackgroundWorker worker = new System.ComponentModel.BackgroundWorker();
			worker.WorkerReportsProgress = true;
			System.ComponentModel.DoWorkEventArgs e = new System.ComponentModel.DoWorkEventArgs(this);
			
			CreatePresFromTemplate(worker, e);
		}
		
		public void CreatePresFromTemplate(System.ComponentModel.BackgroundWorker worker, System.ComponentModel.DoWorkEventArgs e)
		//String companyList, String studyList, String studyYear, String currency, Boolean Metric)
		{
			if(templateToUse == "" || filenameToSaveAs == "")//gotta have a template and a save as
			{
				return;
			}

			//Instantiate the License class
			Aspose.Slides.License license = new Aspose.Slides.License();
			license.SetLicense("Aspose.Slides.lic");

			conn = new SQL.SqlConnection("Server=" + server + ";Database=" + database + ";Trusted_Connection=True; ");
			conn.Open(); // opens the database connection

			LoadStandardTables(conn);

			//loading the data tables
			slidesTable = LoadDataTable(conn, "SELECT s.* FROM Slides s WHERE Active = 1 AND SortOrder IS NOT NULL ORDER BY ISNULL(SortOrder,99999)", "Slides");
			slideChartsTable = LoadDataTable(conn, "SELECT * FROM SlideCharts WHERE SlideID IN (SELECT SlideID FROM Slides WHERE Active = 1)", "SlideCharts");
			slideDataTable = LoadDataTable(conn, "SELECT * FROM SlideData WHERE SlideChartID IN (SELECT SlideChartID FROM SlideCharts WHERE SlideID IN (SELECT SlideID FROM Slides WHERE Active = 1)) ORDER BY SlideDataID", "SlideData");

			state = new CurrentState();
			state.totalSlides = slidesTable.Rows.Count;
			state.workingSlideNumber = slideTableRow;
            state.status = "";
			worker.ReportProgress(0, state);

			//if (testP2 == false)
			//{
				//create our new blank presentation from a master template
				pres = new Presentation(masterTemplate);
				//TODO: this is clunky but it works. get all slides in the template (which also gets the master), then delete them all. must be a better way?
				while (pres.Slides.Count() > 0)
				{
					pres.Slides[0].Remove();
				}

				SlideLoop(conn, slidesTable, slideChartsTable, slideDataTable, worker, state);

				conn.Close();

				state.status = "Saving";
				worker.ReportProgress(0, state);
				pres.Masters.RemoveUnused(false);
				pres.LayoutSlides.RemoveUnused();
				pres.Save(filenameToSaveAs, SaveFormat.Pptx);
				Sa.PresBuilder.Utilities.PostPresCleanup(filenameToSaveAs, dateFix, chartPosition, bar7ChartPosition);
			//}

			//else
			//{
			//	p2 = new AsposeConnect(masterTemplate, true);
			//	bgWorker = worker;
			//	//SlideLoopP2();
			//	conn.Close();

			//	state.status = "Saving";
			//	bgWorker.ReportProgress(0, state);
			//	//TODO: remove unused masters
			//	//TODO: remove unused layouts	
			//	p2.RemoveUnusedMasters();
			//	p2.Save(filenameToSaveAs);

			//	//	PostPresCleanup(@"C:\Development\AsposeSWFromTemplateP2test.pptx");
			//}
		}

		//TODO: Use AsposeConnector
		//private void SlideLoopP2()
		//{
		//	//loop through every slide, processing as appropriate
		//	foreach (S.DataRow slideRow in slidesTable.Rows)
		//	{
		//		slideTableRow++;
		//		//if the title of the slide is "Template" we get a slide from the template and put it into the presentation, and do nothing else to it
		//		if (slideRow["Title"].ToString() == "Template")
		//		{
		//			////if we haven't already loaded the template, load it now (this saves a little time for presentations that don't use the template, they never have to load it)
		//			//if (othertemplate == null)
		//			//{
		//			//	othertemplate = new AsposeConnect(templateToUse);
		//			//}
		//			//p2.AddSlideFromAnotherPresentation(othertemplate, slideRow["SubTitle"].ToString());
		//			//slideNumber++;
		//		}

		//		else
		//		{
		//			//multi slides are slides where the user provides a block of data, and we split it into a slide for each section, based on the first column
		//			//for example if you want individual slides for each unit, provide all the data at once and you'll get each unit individually output on a slide
		//			//note that for multi slides, a 1 means you want the slide to be multi with the first column used as the subtitle on the page, while a 2 means
		//			//you want the slide to be multi, but leave the existing subtitle alone
		//			slidesRemaining = 1;
		//			Int32 multiSlides = Convert.IsDBNull(slideRow["Multi"]) ? 0 : Convert.ToInt32(slideRow["Multi"]);

		//			if (multiSlides > 0)
		//			{
		//				slidesRemaining = 99;//really high number that we'll never reach, this will be reset the first time we pass through a multi slide
		//			}

		//			for (Int32 slideCount = 0; slideCount < slidesRemaining; slideCount++)
		//			{
		//				//we add a blank slide with some header info
		//				p2.AddSlide(slideRow["Title"].ToString(), fieldReplace(slideRow["SubTitle"].ToString()));
		//				slideNumber++;

		//				//we look to find any matching charts to draw
		//				foreach (S.DataRow chartRow in slideChartsTable.Rows)
		//				{
		//					if (Convert.ToInt32(chartRow["SlideID"]) == Convert.ToInt32(slideRow["SlideID"]))
		//					{
		//						//matching data, we add a chart to the slide
		//						//TODO: Use AsposeConnector
		//						WorkWithChartsP2(chartRow);//chartRow, currentSlide);
		//						//isBarChart = false;
		//						//hasRightVerticalAxis = false;
		//						//primaryVerticalMax = 0;
		//						//secondaryVerticalMax = 0;

		//						CheckForChartFixes(chartRow, slideRow);

		//						//we look to find any matching data rows so we can populate their data
		//						foreach (S.DataRow dataRow in slideDataTable.Rows)
		//						{
		//							if (Convert.ToInt32(dataRow["SlideChartID"]) == Convert.ToInt32(chartRow["SlideChartID"]))
		//							{
		//								//slidesRemaining = WorkWithData(conn, dataRow, currentSlide, chart, chartRow, slidesRemaining, multiSlides, slideCount);
		//								WorkWithDataP2(dataRow, slideCount, chartRow);
		//								//if (dataRow["DataType"].ToString() == "Bar" || chartRow["ChartType"].ToString() == "Bar")
		//								//	isBarChart = true;
		//							}
		//						}

		//						//AxisFormatting(chart);
		//						//p2.
		//						p2.FixLegendPosition();
		//						FormatChartAxes(chartRow);

		//						//////if the chart position is not defined in the db, i.e. if it is the standard position
		//						//Int32 XPos = Convert.IsDBNull(chartRow["XPos"]) ? -1 : Convert.ToInt32(chartRow["XPos"]);
		//						//Int32 YPos = Convert.IsDBNull(chartRow["YPos"]) ? -1 : Convert.ToInt32(chartRow["YPos"]);
		//						//Int32 Width = Convert.IsDBNull(chartRow["Width"]) ? -1 : Convert.ToInt32(chartRow["Width"]);
		//						//Int32 Height = Convert.IsDBNull(chartRow["Height"]) ? -1 : Convert.ToInt32(chartRow["Height"]);

		//						//if ((XPos + YPos + Width + Height) == -4) //if any of them are not null it will not add to -4
		//						//{

		//						//	//	//TODO: secondary Y axis
		//						//	//	if (hasRightVerticalAxis == false)
		//						//	//		{
		//						//	//System.Diagnostics.Debug.WriteLine("PVM Value = " + primaryVerticalMax.ToString());
		//						//	//System.Diagnostics.Debug.WriteLine("SVM Value = " + secondaryVerticalMax.ToString());

		//						//	//calculate what the max and min for the bar should be
		//						//	//if they're already set to a value, do nothing
		//						//	if (primaryVerticalMax > 0)
		//						//	{
		//						//		if (chart.Axes.VerticalAxis.IsAutomaticMaxValue == true && chart.Axes.VerticalAxis.IsAutomaticMinValue == true)
		//						//		{
		//						//			//i am assuming the minimum is 0 for ease of programming here
		//						//			Tuple<Double, Double> tup = GetChartMax(primaryVerticalMax);//returns Max as Item1, Major as Item2

		//						//			chart.Axes.VerticalAxis.IsAutomaticMinValue = false;
		//						//			chart.Axes.VerticalAxis.IsAutomaticMaxValue = false;
		//						//			chart.Axes.VerticalAxis.IsAutomaticMajorUnit = false;
		//						//			chart.Axes.VerticalAxis.MaxValue = tup.Item1;
		//						//			chart.Axes.VerticalAxis.MinValue = 0;
		//						//			chart.Axes.VerticalAxis.MajorUnit = tup.Item2;

		//						//		}
		//						//	}

		//						//	if (secondaryVerticalMax > 0)
		//						//	{
		//						//		if (chart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue == true && chart.Axes.SecondaryVerticalAxis.IsAutomaticMinValue == true)
		//						//		{
		//						//			Tuple<Double, Double> tup = GetChartMax(secondaryVerticalMax);//returns Max as Item1, Major as Item2

		//						//			chart.Axes.SecondaryVerticalAxis.IsAutomaticMinValue = false;
		//						//			chart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue = false;
		//						//			chart.Axes.SecondaryVerticalAxis.IsAutomaticMajorUnit = false;
		//						//			chart.Axes.SecondaryVerticalAxis.MaxValue = tup.Item1;
		//						//			chart.Axes.SecondaryVerticalAxis.MinValue = 0;
		//						//			chart.Axes.SecondaryVerticalAxis.MajorUnit = tup.Item2;

		//						//		}
		//						//	}
		//						//}

		//						//if (Convert.IsDBNull(chartRow["SetXAxisMaxToData"]) ? false : Convert.ToBoolean(chartRow["SetXAxisMaxToData"]) == true)
		//						//{
		//						//	SetXAxisMaxToData(chart);
		//						//}

		//						//if (Convert.IsDBNull(chartRow["SetYAxisMaxToData"]) ? false : Convert.ToBoolean(chartRow["SetYAxisMaxToData"]) == true)
		//						//{
		//						//	SetYAxisMaxToData(chart);
		//						//}

		//					}
		//				}

		//				//and a little adjustment for the legend at the bottom
		//				//FixLegendPosition(currentSlide);
		//				p2.FixLegendPosition();

		//				//and look for annotations
		//				foreach (S.DataRow annotationRow in slideAnnotationsTable.Rows)
		//				{
		//					if (annotationRow["SlideID"].ToString() == slideRow["SlideID"].ToString())
		//					{
		//						//AddAnnotations(currentSlide, annotationRow);
		//						p2.AddAnnotation();
		//					}
		//				}
		//			}

		//		}

		//		//and check for layout changes on all slides including masters
		//		if (slideRow["SlideLayout"].ToString() != "")
		//		{
		//			foreach (IMasterSlide masterSlide in pres.Masters)
		//			{
		//				//foreach (IMasterLayoutSlideCollection layoutSlides in masterSlides.
		//				IMasterLayoutSlideCollection layoutSlides = masterSlide.LayoutSlides;
		//				foreach (ILayoutSlide layoutSlide in layoutSlides)
		//				{
		//					if (layoutSlide.Name == slideRow["SlideLayout"].ToString())
		//					{
		//						//pres.Slides[slideNumber - 1].LayoutSlide = layoutSlide;
		//						p2.SetSlideMaster();
		//					}
		//				}
		//			}
		//		}

		//		state.workingSlideNumber = slideTableRow;
		//		bgWorker.ReportProgress(0, state);

		//	}

		//}

		//private void FormatChartAxes(S.DataRow chartRow)
		//{
		//	//is there data for Y left?

		//	Double axisMax = ce.CheckForNull<Double>(chartRow["YAxisMax"], 0);
		//	Double axisMajor = ce.CheckForNull<Double>(chartRow["YAxisMajorUnit"], 0);
		//	Double axisMin = ce.CheckForNull<Double>(chartRow["YAxisMin"], 0);

		//	if ((axisMax + axisMin + axisMajor) != 0)
		//	{
		//		p2.FormatChartAxes(0, true, axisMin, axisMax, axisMajor);
		//	}
		//	else
		//	{
		//		p2.FormatChartAxes(0, false);
		//	}
		//	//is there data for Y right?


		//	//is there data for X?

		//}

		private void CheckForChartFixes(S.DataRow chartRow, S.DataRow slideRow)
		{
			//check for a date fix
			if (chartRow["ChartDate"].ToString() != "")
			{
				DataRow dr = dateFix.NewRow();
				dr["SlideNumber"] = slideNumber;
				dr["DateType"] = chartRow["ChartDate"];
				dateFix.Rows.Add(dr);
			}

			//have added a chart, store the data if necessary so we can place it correctly
			if (chartRow["YPos"].ToString() == "" && chartRow["Height"].ToString() == "" && slideRow["Name"].ToString().Substring(0, 4) != "Bar7")
			{
				DataRow dr = chartPosition.NewRow();
				dr["SlideNumber"] = slideNumber;
				chartPosition.Rows.Add(dr);
			}

			//added Bar7 chart, needs special formatting at the end, so store it here
			if (slideRow["Name"].ToString().Substring(0, 4) == "Bar7")
			{
				DataRow dr = bar7ChartPosition.NewRow();
				dr["SlideNumber"] = slideNumber;
				bar7ChartPosition.Rows.Add(dr);
			}

		}

        private void SlideLoop(SQL.SqlConnection conn, S.DataTable slidesTable, S.DataTable slideChartsTable, S.DataTable slideDataTable, System.ComponentModel.BackgroundWorker worker, CurrentState state)
        {
			Presentation template = new Presentation();

            foreach (S.DataRow slideRow in slidesTable.Rows)
            {
                slideTableRow++;
                //if the title of the slide is "Template" we get a slide from the template and put it into the presentation, and do nothing else to it
                if (slideRow["Title"].ToString() == "Template")
                {
                    //if we haven't already loaded the template, load it now
                    //this saves a little time for presentations that don't use the template, they never have to load it
                    if (template == null)
                    {
                        template = new Presentation(templateToUse);
                        //the following is a poor way to handle this, but Aspose gives little choice
                        //basically we copy every master from the template into our presentation, to make sure they're all there
                        //later we will remove any unused masters, so this won't have any effect on the final presentation)
                        //previously this was done on an individual slide basis, but that caused a whole lot of duplication
                        foreach (IMasterSlide templateMaster in template.Masters)
                        {
                            pres.Masters.AddClone(templateMaster);
                        }

						//if (testP2 == true)
						//{
						//	othertemplate = new AsposeConnect(templateToUse);
						//	//TODO: do we need to clone the master as well when we do this?
						//}
                    }
                    //AddTemplateSlide(pres, template, Convert.ToInt32(slideRow["SubTitle"].ToString()) - 1);
                    AddTemplateSlide(pres, template, slideRow["SubTitle"].ToString());
					//if (testP2 == true)
					//{
					//	//p2.AddSlideFromAnotherPresentation(othertemplate, Convert.ToInt32(slideRow["SubTitle"].ToString()) - 1);
					//	p2.AddSlideFromAnotherPresentation(othertemplate, slideRow["SubTitle"].ToString());
					//}
                    slideNumber++;
                }

                else
                {
                    //multi slides are slides where the user provides a block of data, and we split it into a slide for each section, based on the first column
                    //for example if you want individual slides for each unit, provide all the data at once and you'll get each unit individually output on a slide
                    //note that for multi slides, a 1 means you want the slide to be multi with the first column used as the subtitle on the page, while a 2 means
                    //you want the slide to be multi, but leave the existing subtitle alone
                    Int32 slidesRemaining = 1;
                    Int32 multiSlides = Convert.IsDBNull(slideRow["Multi"]) ? 0 : Convert.ToInt32(slideRow["Multi"]);

                    if (multiSlides > 0)
                    {
                        slidesRemaining = 99;//really high number that we'll never reach, this will be reset the first time we pass through a multi slide
                    }

                    for (Int32 slideCount = 0; slideCount < slidesRemaining; slideCount++)
                    {
                        //we add a blank slide with some header info
                        String subtitle = slideRow["SubTitle"].ToString();
                        subtitle = fieldReplace(subtitle);
                        ISlide currentSlide = MakeSlide(pres, slideRow["Title"].ToString(), subtitle);
						//if (testP2 == true)
						//{
						//	p2.AddSlide(slideRow["Title"].ToString(), fieldReplace(slideRow["SubTitle"].ToString()));
						//}
                        slideNumber++;

                        //we look to find any matching charts to draw
                        foreach (S.DataRow chartRow in slideChartsTable.Rows)
                        {

                            if (chartRow["SlideID"].ToString() == slideRow["SlideID"].ToString())
                            {
                                //matching data, we add a chart to the slide
								//TODO: Use AsposeConnector
								IChart chart = WorkWithCharts(chartRow, currentSlide);
								//isBarChart = false;
								//hasRightVerticalAxis = false;
                                primaryVerticalMax = 0;
                                secondaryVerticalMax = 0;

                                //check for a date fix
                                if (chartRow["ChartDate"].ToString() != "")
                                {
                                    DataRow dr = dateFix.NewRow();
                                    dr["SlideNumber"] = slideNumber;
                                    dr["DateType"] = chartRow["ChartDate"];
                                    dateFix.Rows.Add(dr);
                                }

                                //have added a chart, store the data if necessary so we can place it correctly
                                if (chartRow["YPos"].ToString() == "" && chartRow["Height"].ToString() == "" && slideRow["Name"].ToString().Substring(0, 4) != "Bar7")
                                {
                                    DataRow dr = chartPosition.NewRow();
                                    dr["SlideNumber"] = slideNumber;
                                    chartPosition.Rows.Add(dr);
                                }

                                //added Bar7 chart, needs special formatting at the end, so store it here
                                if (slideRow["Name"].ToString().Substring(0, 4) == "Bar7")
                                {
                                    DataRow dr = bar7ChartPosition.NewRow();
                                    dr["SlideNumber"] = slideNumber;
                                    bar7ChartPosition.Rows.Add(dr);
                                }

                                //we look to find any matching data rows so we can populate their data
                                foreach (S.DataRow dataRow in slideDataTable.Rows)
                                {
                                    if (dataRow["SlideChartID"].ToString() == chartRow["SlideChartID"].ToString())
                                    {

                                        slidesRemaining = WorkWithData(conn, dataRow, currentSlide, chart, chartRow, slidesRemaining, multiSlides, slideCount);

										//if (dataRow["DataType"].ToString() == "Bar" || chartRow["ChartType"].ToString() == "Bar")
											//isBarChart = true;
                                    }
                                }

                                AxisFormatting(chart);

                                ////if the chart position is not defined in the db, i.e. if it is the standard position
                                Int32 XPos = Convert.IsDBNull(chartRow["XPos"]) ? -1 : Convert.ToInt32(chartRow["XPos"]);
                                Int32 YPos = Convert.IsDBNull(chartRow["YPos"]) ? -1 : Convert.ToInt32(chartRow["YPos"]);
                                Int32 Width = Convert.IsDBNull(chartRow["Width"]) ? -1 : Convert.ToInt32(chartRow["Width"]);
                                Int32 Height = Convert.IsDBNull(chartRow["Height"]) ? -1 : Convert.ToInt32(chartRow["Height"]);

                                if ((XPos + YPos + Width + Height) == -4) //if any of them are not null it will not add to -4
                                {

                                    //	//TODO: secondary Y axis
                                    //	if (hasRightVerticalAxis == false)
                                    //		{
                                    //System.Diagnostics.Debug.WriteLine("PVM Value = " + primaryVerticalMax.ToString());
                                    //System.Diagnostics.Debug.WriteLine("SVM Value = " + secondaryVerticalMax.ToString());

                                    //calculate what the max and min for the bar should be
                                    //if they're already set to a value, do nothing
                                    if (primaryVerticalMax > 0)
                                    {
                                        if (chart.Axes.VerticalAxis.IsAutomaticMaxValue == true && chart.Axes.VerticalAxis.IsAutomaticMinValue == true)
                                        {
                                            //i am assuming the minimum is 0 for ease of programming here
                                            Tuple<Double, Double> tup = GetChartMax(primaryVerticalMax);//returns Max as Item1, Major as Item2

                                            chart.Axes.VerticalAxis.IsAutomaticMinValue = false;
                                            chart.Axes.VerticalAxis.IsAutomaticMaxValue = false;
                                            chart.Axes.VerticalAxis.IsAutomaticMajorUnit = false;
                                            chart.Axes.VerticalAxis.MaxValue = tup.Item1;
                                            chart.Axes.VerticalAxis.MinValue = 0;
                                            chart.Axes.VerticalAxis.MajorUnit = tup.Item2;

                                        }
                                    }

                                    if (secondaryVerticalMax > 0)
                                    {
                                        if (chart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue == true && chart.Axes.SecondaryVerticalAxis.IsAutomaticMinValue == true)
                                        {
                                            Tuple<Double, Double> tup = GetChartMax(secondaryVerticalMax);//returns Max as Item1, Major as Item2

                                            chart.Axes.SecondaryVerticalAxis.IsAutomaticMinValue = false;
                                            chart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue = false;
                                            chart.Axes.SecondaryVerticalAxis.IsAutomaticMajorUnit = false;
                                            chart.Axes.SecondaryVerticalAxis.MaxValue = tup.Item1;
                                            chart.Axes.SecondaryVerticalAxis.MinValue = 0;
                                            chart.Axes.SecondaryVerticalAxis.MajorUnit = tup.Item2;

                                        }
                                    }
                                }

                                if (Convert.IsDBNull(chartRow["SetXAxisMaxToData"]) ? false : Convert.ToBoolean(chartRow["SetXAxisMaxToData"]) == true)
                                {
                                    SetXAxisMaxToData(chart);
                                }

                                if (Convert.IsDBNull(chartRow["SetYAxisMaxToData"]) ? false : Convert.ToBoolean(chartRow["SetYAxisMaxToData"]) == true)
                                {
                                    SetYAxisMaxToData(chart);
                                }

                            }
                        }

                        //and a little adjustment for the legend at the bottom
                        FixLegendPosition(currentSlide);

                        //and look for annotations
                        foreach (S.DataRow annotationRow in slideAnnotationsTable.Rows)
                        {

                            if (annotationRow["SlideID"].ToString() == slideRow["SlideID"].ToString())
                            {
                                AddAnnotations(currentSlide, annotationRow);
                            }
                        }
                    }

                }

                //and check for layout changes on all slides including masters
                if (slideRow["SlideLayout"].ToString() != "")
                {
                    foreach (IMasterSlide masterSlide in pres.Masters)
                    {
                        //foreach (IMasterLayoutSlideCollection layoutSlides in masterSlides.
                        IMasterLayoutSlideCollection layoutSlides = masterSlide.LayoutSlides;
                        foreach (ILayoutSlide layoutSlide in layoutSlides)
                        {
                            if (layoutSlide.Name == slideRow["SlideLayout"].ToString())
                            {
                                pres.Slides[slideNumber - 1].LayoutSlide = layoutSlide;
                            }
                        }
                    }
                    //			IMasterLayoutSlideCollection layoutSlides = pres.Masters.LayoutSlides;
                    //ILayoutSlide layoutSlide =
                    //	layoutSlides.GetByType(SlideLayoutType.TitleAndObject) ??
                    //	layoutSlides.GetByType(SlideLayoutType.Title);
                    //		LayoutSlide layoutSlide = LayoutSlideCollection.
                    //		pres.Slides[slideNumber].LayoutSlide = LayoutSlide.

                }

                state.workingSlideNumber = slideTableRow;
                worker.ReportProgress(0, state);

            }

        }

        private void AdjustChartMaxMin<T>(Double valueToCheck, T axis)
        {
            if (valueToCheck > 0)
            {

            }

        }



        //public void AddCategory<T>(Int32 xPos, Int32 yPos, T value)
        //{
        //    IChartDataWorkbook fact = currentChart.ChartData.ChartDataWorkbook;
        //    IChartDataCell cdc = fact.GetCell(0, xPos, yPos);

        //    if (typeof(T) == typeof(Int32)) // most of the time it is string, but Aspose can't deal with it being an actual date, so we pass an int and set the number format to date
        //    {
        //        cdc.PresetNumberFormat = 14;
        //    }

        //    cdc.Value = value;
        //    currentChart.ChartData.Categories.Add(cdc);
        //}



		//private void PostPresCleanup(String filenameToSaveAs)
		//{
		//	//these are things that the Aspose library cannot handle properly, so here we switch out to using the OpenXML stuff from Microsoft
		//	//painful but necessary unless/until Aspose makes changes in their library (which doesn't seem likely)

		//	if (dateFix.Rows.Count > 0 || chartPosition.Rows.Count > 0 || bar7ChartPosition.Rows.Count > 0)
		//	{
		//		using (DOXP.PresentationDocument doc = DOXP.PresentationDocument.Open(filenameToSaveAs, true))
		//		{
		//			foreach (S.DataRow dateRow in dateFix.Rows)
		//			{
		//				switch (Convert.ToInt32(dateRow["DateType"]))
		//				{
		//					case 1:
		//						FixCategoryDate(doc, Convert.ToInt32(dateRow["SlideNumber"]));
		//						break;
		//					case 2:
		//						FixCategoryDateVersion2(doc, Convert.ToInt32(dateRow["SlideNumber"]));
		//						break;
		//					default:
		//						break;
		//				}
		//			}

		//			foreach (S.DataRow chartRow in chartPosition.Rows)
		//			{
		//				AdjustLayoutTarget(doc, Convert.ToInt32(chartRow["SlideNumber"]));
		//			}

		//			foreach (S.DataRow chartRow in bar7ChartPosition.Rows)
		//			{
		//				Bar7AdjustLayoutTarget(doc, Convert.ToInt32(chartRow["SlideNumber"]));
		//			}
		//		}
		//	}
		//}

		private void LoadStandardTables(SQL.SqlConnection conn)
		{
			//Load some standard tables

            slideColorsTable = LoadDataTable(conn, "SELECT * FROM vwSlideColors ORDER BY SlideColorGroup, ColorOrder", "slideColorsTable");
            slideAnnotationsTable = LoadDataTable(conn, "SELECT * FROM vwSlideAnnotations ORDER BY SlideID", "slideAnnotationsTable");
            slideLineFormatsTable = LoadDataTable(conn, "SELECT * FROM vwSlideLineFormats ORDER BY SlideLineFormatID", "slideLineFormatsTable");
            slideTableFormatsTable = LoadDataTable(conn, "SELECT * FROM SlideTableFormats", "slideTableFormatsTable");

			//dateFix is used to add certain slides that need rows fixing by OpenXML after the process is complete
			dateFix.Columns.Add("SlideNumber", typeof(Int32));
			dateFix.Columns.Add("DateType", typeof(Int32));//will be a number for some reason

			//chart position tables are used to fix certain slides at the end
			chartPosition.Columns.Add("SlideNumber", typeof(Int32));
			bar7ChartPosition.Columns.Add("SlideNumber", typeof(Int32));

		}

		private S.DataTable LoadDataTable(String SQLCommand, String tableName)
		{

			SQL.SqlCommand slidesCommand = new SQL.SqlCommand(SQLCommand, conn);
			S.DataTable slidesTable = new S.DataTable(tableName);
			SQL.SqlDataAdapter dapSlides = new SQL.SqlDataAdapter(slidesCommand);
			slidesTable.Clear();
			dapSlides.Fill(slidesTable);

			return slidesTable;
		}

		private  S.DataTable LoadDataTable(SQL.SqlConnection conn, String SQLCommand, String tableName)
		{

			SQL.SqlCommand slidesCommand = new SQL.SqlCommand(SQLCommand, conn);
			S.DataTable slidesTable = new S.DataTable(tableName);
			SQL.SqlDataAdapter dapSlides = new SQL.SqlDataAdapter(slidesCommand);
			slidesTable.Clear();
			dapSlides.Fill(slidesTable);

			return slidesTable;
		}
		//TODO: Use AsposeConnector
		private void AxisFormatting(IChart chart)
		{
			if (chart.Axes.VerticalAxis.IsAutomaticMaxValue == false)
				{
				if (chart.Axes.VerticalAxis.MaxValue > primaryVerticalMax)
					{
					primaryVerticalMax = chart.Axes.VerticalAxis.MaxValue;
					}
				}

			//y axis number formatting
			if (primaryVerticalMax >= 4.8)//magic numbers from Microsoft
				{
				SetAxisFormat(chart.Axes.VerticalAxis, "#,##0");
				}
			else if (primaryVerticalMax >= .48)//yeah, magic number time
				{
				SetAxisFormat(chart.Axes.VerticalAxis, "#,##0.0");//1 digit
				}
			else if (primaryVerticalMax >= .048)//yeah, magic number time
				{
				SetAxisFormat(chart.Axes.VerticalAxis, "#,##0.00");
				}
			else
				{
				SetAxisFormat(chart.Axes.VerticalAxis, "#,##0.000");
				}

			if (chart.Axes.SecondaryVerticalAxis != null)
				{
				if (chart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue == false)
					{
					if (chart.Axes.SecondaryVerticalAxis.MaxValue > secondaryVerticalMax)
						{
						secondaryVerticalMax = chart.Axes.SecondaryVerticalAxis.MaxValue;
						}
					}

			//y axis number formatting
			if (secondaryVerticalMax >= 4.8)//magic numbers from Microsoft
				{
				SetAxisFormat(chart.Axes.SecondaryVerticalAxis, "#,##0");
				}
			else if (secondaryVerticalMax >= .48)//yeah, magic number time
				{
				SetAxisFormat(chart.Axes.SecondaryVerticalAxis, "#,##0.0");
				}
			else if (secondaryVerticalMax >= .048)//yeah, magic number time
				{
				SetAxisFormat(chart.Axes.SecondaryVerticalAxis, "#,##0.00");
				}
			else
				{
				SetAxisFormat(chart.Axes.SecondaryVerticalAxis, "#,##0.000");
				}
			}

			//if (testP2 == true)
			//{
			//	p2.FormatChartAxes(chart, primaryVerticalMax, secondaryVerticalMax);
			//}
		}

		private void SetAxisFormat(IAxis axis, String numberFormat)
		{
			axis.IsNumberFormatLinkedToSource = false;
			axis.NumberFormat = numberFormat;
		}

		private Tuple<Double, Double> GetChartMax(Double chartMax)
			{
			//this code adapted from http://peltiertech.com/calculate-nice-axis-scales-in-excel-vba/
			//We are assuming the minimum is 0
			Double dMax = chartMax * 1.02;
			Double dPower = System.Math.Log(dMax) / System.Math.Log(10);
			Double truncDPower = dPower;
			if (truncDPower < 0) { truncDPower--; }
			truncDPower = System.Math.Truncate(truncDPower);
			Double dScale = System.Math.Pow(10, (dPower - truncDPower));
			Double dSmall = 0;

			if (dScale <= 2.5)
				{
				dScale = 0.2;
				dSmall = 0.05;
				}
			else if (dScale <= 5)
				{
				dScale = 0.5;
				dSmall = 0.1;
				}
			else if (dScale < 7.5)
				{
				dScale = 1;
				dSmall = 0.2;
				}
			else
				{
				dScale = 2;
				dSmall = 0.5;
				}

			dScale = dScale * System.Math.Pow(10, truncDPower);
			dSmall = dSmall * System.Math.Pow(10, truncDPower);

			Double axisMax = dScale * (System.Math.Truncate(dMax / dScale) + 1);
			Double axisMajor = dScale;

			return Tuple.Create(axisMax, axisMajor);
			
			}
		//TODO: Use AsposeConnector
		private void SetXAxisMaxToData(IChart chart)
			{

			Double xMax = 0;
			//TODO: Use AsposeConnector
			foreach (IChartSeries series in chart.ChartData.Series)
				{
				foreach (ChartDataPoint dp in series.DataPoints)
					{
					if (Convert.ToDouble(dp.XValue.Data) > xMax )
						xMax = Convert.ToDouble(dp.XValue.Data);
					}
				}

			if (xMax > 0)
				{
				chart.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
				chart.Axes.HorizontalAxis.MaxValue = xMax;
				}

			}
		//TODO: Use AsposeConnector
		private void SetYAxisMaxToData(IChart chart)
			{

			Double yMax = 0;
			//TODO: Use AsposeConnector
			foreach (IChartSeries series in chart.ChartData.Series)
				{
				foreach (ChartDataPoint dp in series.DataPoints)
					{
					if (Convert.ToDouble(dp.YValue.Data) > yMax)
						yMax = Convert.ToDouble(dp.YValue.Data);
					}
				}

			if (yMax > 0)
				{
				chart.Axes.VerticalAxis.IsAutomaticMaxValue = false;
				chart.Axes.VerticalAxis.MaxValue = yMax;
				}

			}

		private void AdjustPlotArea(IChart chart, float X, float Y, float Width, float Height)
		{
			chart.PlotArea.X = X;
			chart.PlotArea.Y = Y;
			chart.PlotArea.Width = Width;
			chart.PlotArea.Height = Height;
		}

		//private void WorkWithChartsP2(S.DataRow chartRow)
		//{
		//	//get the chart type and add the chart
		//	ChartType chartType = GetChartType(chartRow["ChartType"].ToString());
		//	p2.AddChart(chartType);
		//	//set the position of the chart
		//	Int32 xPos = ce.CheckForNull<Int32>(chartRow["XPos"], -1);
		//	Int32 yPos = ce.CheckForNull<Int32>(chartRow["YPos"], -1);
		//	Int32 width = ce.CheckForNull<Int32>(chartRow["Width"], -1);
		//	Int32 height = ce.CheckForNull<Int32>(chartRow["Height"], -1);
		//	if ((xPos + yPos + width + height) > -4)
		//	{
		//		p2.SetChartPosition(xPos, yPos, width, height);
		//	}


		//	if (fieldReplace(chartRow["ChartTitle"].ToString()) != "")
		//	{
		//		p2.AddTitleToChart(fieldReplace(chartRow["ChartTitle"].ToString()));
		//	}

		//	if (chartRow["ChartType"].ToString() == "PerfSum" | chartRow["ChartType"].ToString() == "Pie")
		//	{
		//		p2.BlankPlotArea();
		//	}

		//}

		private IChart WorkWithCharts(S.DataRow chartRow, ISlide currentSlide)
		{
			IChart chart;

			//do some data conversions
			//the Y axis values may contain both US units and Metric units, divided by a # in the string
			Double YAxisMax = Sa.PresBuilder.Utilities.GetUSorMetricValue(chartRow["YAxisMaxText"].ToString());
			Double YAxisMajorUnit = Sa.PresBuilder.Utilities.GetUSorMetricValue(chartRow["YAxisMajorUnitText"].ToString());
			Double YAxisMin = Sa.PresBuilder.Utilities.GetUSorMetricValue(chartRow["YAxisMinText"].ToString());

			Int32 XAxisMax = ce.CheckForNull<Int32>(chartRow["XAxisMax"], 0);
			Decimal XAxisMajorUnit = ce.CheckForNull<Decimal>(chartRow["XAxisMajorUnit"], 0);
			Int32 XAxisMin = ce.CheckForNull<Int32>(chartRow["XAxisMin"], 0);

			Int32 xPos = ce.CheckForNull<Int32>(chartRow["XPos"], -1);
			Int32 yPos = ce.CheckForNull<Int32>(chartRow["YPos"], -1);
			Int32 width = ce.CheckForNull<Int32>(chartRow["Width"], -1);
			Int32 height = ce.CheckForNull<Int32>(chartRow["Height"], -1);

			String chartType = chartRow["ChartType"].ToString();

            String chartXAxisTitle = fieldReplace(chartRow["ChartXAxisTitle"].ToString());
            String chartYAxisTitle = fieldReplace(chartRow["ChartYAxisTitle"].ToString());
            String chartTitle = fieldReplace(chartRow["ChartTitle"].ToString());

			//then create the actual chart
			switch (chartType)
				{
				case "Bar":
				case "Line":
				case "LineMarker":
				case "Bar100":
				case "BarCluster":
				case "BarHoriz":
				case "Area":
					chart = AddChart(currentSlide, chartType, chartXAxisTitle, chartYAxisTitle, chartTitle, xPos, yPos, width, height, YAxisMax, YAxisMajorUnit, YAxisMin, XAxisMax, XAxisMajorUnit, XAxisMin);
					break;
				case "PerfSum":
				case "Pie":
					chart = AddChart(currentSlide, chartType, chartXAxisTitle, chartYAxisTitle, chartTitle, xPos, yPos, width, height);
					break;
				case "Scatter":
					chart = AddChart(currentSlide, chartType, chartXAxisTitle, chartYAxisTitle, chartTitle, -1, -1, -1, -1, YAxisMax, YAxisMajorUnit, YAxisMin, XAxisMax, XAxisMajorUnit, XAxisMin);
					break;
				case "Table"://this is just a placeholder, it will be changed later
				default:
					chart = currentSlide.Shapes.AddChart(ChartType.StackedColumn, 0, 54, 720, 432);//if in doubt, bar it out
					break;
				}


			//if (testP2 == true)
			//{
			//	//p3.ChartSetup cs = new p3.ChartSetup();
			//	ChartSetup cs = new ChartSetup();
			//	if (xPos != -1) cs.xPos = xPos;
			//	if (yPos != -1) cs.yPos = yPos;
			//	if (width != -1) cs.width = width;
			//	if (height != -1) cs.height = height;
			//	cs.YAxisMax = YAxisMax;
			//	cs.YAxisMin = YAxisMin;
			//	cs.YAxisMajorUnit = YAxisMajorUnit;
			//	cs.XAxisMax = XAxisMax;
			//	cs.XAxisMin = XAxisMin;
			//	cs.XAxisMajorUnit = XAxisMajorUnit;
			//	cs.chartType = chartType;
			//	cs.xAxisTitle = chartXAxisTitle;
			//	cs.yAxisTitle = chartYAxisTitle;
			//	cs.chartTitle = chartTitle;

			//	p2.AddChart(cs);
			//}


			return chart;
		}

		//TODO: Use AsposeConnector

		//private void WorkWithDataP2(S.DataRow dataRow, Int32 slideCount, S.DataRow chartRow)
		//{

		//	////slidesRemaining is the number of slides to build, which may in rare cases vary (usually one, some slides need to be built for each unit, for example)
		//	////slidecount is the slide we're working on, which will be >0 only for multi slides (and is used to parse the correct data)
		//	////we only check it if it is marked as a multi slide, and if slidesRemaining = 99 (the default value for multi slides)

		//	////get the data table
		//	String sqlString = MakeDataSQLString(dataRow);
		//	S.DataTable queryDataTable = LoadDataTable(sqlString, "QueryData");

		//	////create a temporary color table, copy the main color table, delete rows that don't match, and use it
		//	S.DataTable shortColorsTable = GetShortColorTable(slideColorsTable, dataRow["SlideColorGroup"].ToString());

		//	//Int32 SecondYAxisMax = ce.CheckForNull<Int32>(chartRow["SecondYAxisMax"], 0);
		//	//Int32 SecondYAxisMajorUnit = ce.CheckForNull<Int32>(chartRow["SecondYAxisMajorUnit"], 0);
		//	//Int32 SecondYAxisMin = ce.CheckForNull<Int32>(chartRow["SecondYAxisMin"], 0);

		//	////get some values from the chart data

		//	Int32 MultiData = ce.CheckForNull<Int32>(dataRow["Multi"], 0);

		//	if (MultiData > 0)
		//	{
		//		//break out the appropriate multi data

		//		//get the distinct values in the first column
		//		DataView view = new DataView(queryDataTable);
		//		String colname = queryDataTable.Columns[0].ColumnName;
		//		S.DataTable distinctRows = view.ToTable(true, colname);

		//		if (slidesRemaining == 99) //we haven't reset the value yet, so change it here
		//		{
		//			slidesRemaining = distinctRows.Rows.Count;
		//		}
		//		string slideToMake = distinctRows.Rows[slideCount].ItemArray[0].ToString();//the unit name

		//		switch (dataRow["DataType"].ToString())
		//		{

		//			case "DistPoints":
		//			case "ScatLine":
		//			case "LineMarker":
		//			case "Bar":
		//			case "Line":
		//			case "Bar100":
		//			case "BarCluster":
		//			case "BarHoriz":
		//			case "Area":
		//			case "Scatter":
		//				queryDataTable = SplitMultiData(queryDataTable, slideToMake);
		//				break;
		//			default:
		//				break;

		//			//TODO: Multis for Pie?
		//		}

		//		if (MultiData == 1)
		//		{
		//			//if it is 1, we want to use the unit name for the subtitle of the slide
		//			if (slideToMake != "")
		//			{
		//				//SetSlideSubTitle(currentSlide, slideToMake);
		//				//if (testP2 == true)
		//				//{
		//				p2.ResetSubTitle(slideToMake);
		//				//}
		//			}
		//		}
		//	}

		//	////and do the actual data posting to the slide
		//	switch (dataRow["DataType"].ToString())
		//	{

		//	//	case "DistPoints":
		//	//		AddDistPoints(currentSlide, queryDataTable, ce.CheckForNull<String>(dataRow["SlideColorGroup"].ToString(), ""));
		//	//		break;
		//	//	case "ScatLine":
		//	//		AddScatterLine(chart, queryDataTable, shortColorsTable, dataRow["LineFormat"].ToString());
		//	//		//if (testP2 == true)
		//	//		//{
		//	//		//	//LabelFormat labelFormat = new LabelFormat();
		//	//		//	Sa.PresBuilder.AsposeConnector.LineFormat lineFormat = new Sa.PresBuilder.AsposeConnector.LineFormat();
		//	//		//	foreach (DataRow row in slideLineFormatsTable.Rows)
		//	//		//	{
		//	//		//		if (row["SlideLineFormatID"].ToString() == dataRow["LineFormat"].ToString())
		//	//		//		{
		//	//		//			lineFormat.SetLineFormat(System.Drawing.ColorTranslator.FromHtml("#" + row["Color"].ToString()), Convert.ToDouble(row["LineWidth"]), Convert.ToInt32(row["LineDashStyle"]), Convert.ToInt32(row["LineBeginArrow"]), Convert.ToInt32(row["LineEndArrow"]), Convert.ToInt32(row["MarkerType"]), Convert.ToInt32(row["MarkerSize"]), Convert.ToBoolean(row["MarkerLastOnly"]));
		//	//		//		}
		//	//		//	}
		//	//		//	//GetLineFormat(lineFormat, dataRow["LineFormat"].ToString());
		//	//		//	p2.AddScatterLine(queryDataTable, lineFormat);
		//	//		//}
		//	//		break;
		//		case "LineMarker":
		//		case "Bar":
		//		case "Line":
		//		case "Bar100":
		//		case "BarCluster":
		//		case "BarHoriz":
		//		case "Area":
		//			AddDataToChartP2(queryDataTable, dataRow, shortColorsTable, chartRow);
		//			//AddDataToChart(currentSlide, chart, queryDataTable, dataRow["DataType"].ToString(), shortColorsTable, Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]), Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]), Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]), SecondYAxisMax, SecondYAxisMajorUnit, SecondYAxisMin, dataRow["LineFormat"].ToString());
		//			break;
		//	//	case "Pie":
		//	//		AddDataToChart(currentSlide, chart, queryDataTable, dataRow["DataType"].ToString(), shortColorsTable, Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]), Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]), Convert.ToBoolean(0));
		//	//		break;
		//	//	case "PerfSum": //PerfSum does not have a multi?
		//	//		foreach (S.DataRow queryRow in queryDataTable.Rows)
		//	//		{
		//	//			if (queryRow["Q12"].ToString() != "")//the first row, data for the bar
		//	//			{
		//	//				PerfSumAddBar(chart, queryRow, shortColorsTable);
		//	//			}
		//	//			else//a unit data point
		//	//			{
		//	//				string unitName = queryRow["BarTitle"].ToString();//the unit name, bar title is just the column name
		//	//				Double unitValue = Double.Parse(queryRow["Minimum"].ToString());//the unit value, minimum is just the column name
		//	//				//TODO: when the final presentation is output, the labels are positioned as right, even though the code is putting them at center. why?
		//	//				//TODO: Use AsposeConnector
		//	//				AddPerfSumSeriesWithPoint(chart, unitName, unitValue, LegendDataLabelPosition.Center, false, queryRow["Minimum"].ToString());
		//	//			}
		//	//		}
		//	//		//end of the rows, now we recalc the slide to correct the perfsums
		//	//		RecalcPerfSums(currentSlide);
		//	//		break;
		//	//	case "StackArea":
		//	//		//TODO: StackArea multis?
		//	//		AddStackArea(chart, queryDataTable, shortColorsTable);
		//	//		break;
		//	//	case "Scatter":
		//	//		//TODO: Scatter multis?
		//	//		AddScatterPoints(chart, queryDataTable, Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]));
		//	//		//if (testP2 == true)
		//	//		//{
		//	//		//	LabelFormat labelFormat = new LabelFormat();
		//	//		//	p2.AddScatterPoints(queryDataTable, labelFormat);
		//	//		//}
		//	//		break;
		//	//	case "Table":
		//	//		currentSlide.Shapes.Remove(chart);//getting rid of the placeholder
		//	//		//TODO: fix table settings

		//	//		//TODO: multi slide table, remove the first column before sending it to AddTable
		//	//		if (multiSlides > 0)
		//	//		{

		//	//			//count the number of distinct values in the first column
		//	//			DataView view = new DataView(queryDataTable);
		//	//			String colname = queryDataTable.Columns[0].ColumnName;
		//	//			S.DataTable distinctRows = view.ToTable(true, colname);

		//	//			if (slidesRemaining == 99)
		//	//			{
		//	//				////count the number of distinct values in the first column
		//	//				slidesRemaining = distinctRows.Rows.Count;
		//	//			}

		//	//			String slideToMake = "";
		//	//			//we pull based on the slideCount and distinctRows, get each unit one at a time
		//	//			if ((slideCount + 1) < distinctRows.Rows.Count)
		//	//			{
		//	//				slideToMake = distinctRows.Rows[slideCount + 1].ItemArray[0].ToString();//the unit name, we add 1 because the first row is the titles
		//	//			}

		//	//			for (int r = 1; r < queryDataTable.Rows.Count; r++)
		//	//			{
		//	//				if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
		//	//					queryDataTable.Rows[r].Delete();
		//	//			}

		//	//			queryDataTable.AcceptChanges();
		//	//			queryDataTable.Columns.RemoveAt(0);//remove the first column, should this be named or is index okay? index is probably the better solution, right?

		//	//		}

		//	//		AddTable(currentSlide, queryDataTable, Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]));//BarGapWidth used as proxy for table settings
		//	//		break;
		//		default:
		//			break;
		//	}

		//	////and if we need to add a second axis title
		//	//if (Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]) == true)
		//	//{
		//	//	if (dataRow["AxisTitle"].ToString() != "")
		//	//	{
		//	//		//add an axis title on the right hand side
		//	//		String newTitle = fieldReplace(dataRow["AxisTitle"].ToString());
		//	//		AddVerticalAxisLabelRight(currentSlide, newTitle);
		//	//	}
		//	//}

		//	////slidesRemaining = DoLiveData(queryDataTable, conn, dataRow, currentSlide, chart, chartRow, slidesRemaining, multiSlides, slideCount);
		//	//CalculateDataMaximums(queryDataTable, dataRow);
		//	//return slidesRemaining;


		//}

		private Int32 WorkWithData(SQL.SqlConnection conn, S.DataRow dataRow, ISlide currentSlide, IChart chart, S.DataRow chartRow, Int32 slidesRemaining, Int32 multiSlides, Int32 slideCount)
		{
			//slidesRemaining is the number of slides to build, which may in rare cases vary (usually one, some slides need to be built for each unit, for example)
			//slidecount is the slide we're working on, which will be >0 only for multi slides (and is used to parse the correct data)
			//we only check it if it is marked as a multi slide, and if slidesRemaining = 99 (the default value for multi slides)

			//get the data table
			String sqlString = MakeDataSQLString(dataRow);
			S.DataTable queryDataTable = LoadDataTable(conn, sqlString, "QueryData");

			//create a temporary color table, copy the main color table, delete rows that don't match, and use it
			S.DataTable shortColorsTable = GetShortColorTable(slideColorsTable, dataRow["SlideColorGroup"].ToString());

			Int32 SecondYAxisMax = ce.CheckForNull<Int32>(chartRow["SecondYAxisMax"], 0);
			Int32 SecondYAxisMajorUnit = ce.CheckForNull<Int32>(chartRow["SecondYAxisMajorUnit"], 0);
			Int32 SecondYAxisMin = ce.CheckForNull<Int32>(chartRow["SecondYAxisMin"], 0);

			//get some values from the chart data

			Int32 MultiData = ce.CheckForNull<Int32>(dataRow["Multi"], 0);

			if (MultiData > 0)
			{
				//break out the appropriate multi data

				//get the distinct values in the first column
				DataView view = new DataView(queryDataTable);
				String colname = queryDataTable.Columns[0].ColumnName;
				S.DataTable distinctRows = view.ToTable(true, colname);

				if (slidesRemaining == 99) //we haven't reset the value yet, so change it here
				{
					slidesRemaining = distinctRows.Rows.Count;
				}

				if (distinctRows.Rows.Count > 0)
				{
					string slideToMake = distinctRows.Rows[slideCount].ItemArray[0].ToString();//the unit name

					switch (dataRow["DataType"].ToString())
					{

						case "DistPoints":
						case "ScatLine":
						case "LineMarker":
						case "Bar":
						case "Line":
						case "Bar100":
						case "BarCluster":
						case "Area":
						case "Scatter":
						case "Table":
							queryDataTable = SplitMultiData(queryDataTable, slideToMake);
							break;
						case "BarHoriz":
							//deletes any rows where the first column does not equal the slideToMake string, then deletes the first column
							for (int r = 0; r < queryDataTable.Rows.Count; r++)
							{
								if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
									queryDataTable.Rows[r].Delete();
							}
							queryDataTable.AcceptChanges();
							//barhoriz needs the first column intact?
							break;
						default:
							break;

						//TODO: Multis for Pie?
					}

					if (MultiData == 1)
					{
						//if it is 1, we want to use the unit name for the subtitle of the slide
						if (slideToMake != "")
						{
							SetSlideSubTitle(currentSlide, slideToMake);
							//if (testP2 == true)
							//{
							//	p2.ResetSubTitle(slideToMake);
							//}
						}
					}
				}
			}

			//and do the actual data posting to the slide
			switch (dataRow["DataType"].ToString())
			{

				case "DistPoints":
					AddDistPoints(currentSlide, queryDataTable, ce.CheckForNull<String>(dataRow["SlideColorGroup"].ToString(), ""));
					break;
				case "ScatLine":
					//		case "ScatLine":
					if (queryDataTable.Columns.Count > 2)
						{
						//count the number of distinct values in the first column
						DataView view = new DataView(queryDataTable);
						String colname = queryDataTable.Columns[0].ColumnName;
						S.DataTable distinctRows = view.ToTable(true, colname);

						String slideToMake = "";
						for (int r = 0; r < distinctRows.Rows.Count; r++)
							{
							slideToMake = distinctRows.Rows[r].ItemArray[0].ToString();

							S.DataTable scatData = queryDataTable.Copy();
							for (int s = 0; s < scatData.Rows.Count; s++)
								{
								if (scatData.Rows[s].ItemArray[0].ToString() != slideToMake)
										scatData.Rows[s].Delete();
								}
							scatData.AcceptChanges();
							scatData.Columns.RemoveAt(0);

							AddScatterLine(chart, scatData, shortColorsTable, dataRow["LineFormat"].ToString());
							CalculateDataMaximums(scatData, dataRow);//we do this here because we broke out this data
							}

						}
					else
						{
						AddScatterLine(chart, queryDataTable, shortColorsTable, dataRow["LineFormat"].ToString());
						CalculateDataMaximums(queryDataTable, dataRow);
						}
					//break;
					//AddScatterLine(chart, queryDataTable, shortColorsTable, dataRow["LineFormat"].ToString());
					//if (testP2 == true)
					//{
					//	//LabelFormat labelFormat = new LabelFormat();
					//	Sa.PresBuilder.AsposeConnector.LineFormat lineFormat = new Sa.PresBuilder.AsposeConnector.LineFormat();
					//	foreach (DataRow row in slideLineFormatsTable.Rows)
					//	{
					//		if (row["SlideLineFormatID"].ToString() == dataRow["LineFormat"].ToString())
					//		{
					//			lineFormat.SetLineFormat(System.Drawing.ColorTranslator.FromHtml("#" + row["Color"].ToString()), Convert.ToDouble(row["LineWidth"]), Convert.ToInt32(row["LineDashStyle"]), Convert.ToInt32(row["LineBeginArrow"]), Convert.ToInt32(row["LineEndArrow"]), Convert.ToInt32(row["MarkerType"]), Convert.ToInt32(row["MarkerSize"]), Convert.ToBoolean(row["MarkerLastOnly"]));
					//		}
					//	}
					//	//GetLineFormat(lineFormat, dataRow["LineFormat"].ToString());
					//	p2.AddScatterLine(queryDataTable, lineFormat);
					//}
					break;
				case "LineMarker":
				case "Bar":
				case "Line":
				case "Bar100":
				case "BarCluster":
				case "BarHoriz":
				case "Area":
					AddDataToChart(currentSlide, chart, queryDataTable, dataRow["DataType"].ToString(), shortColorsTable, Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]), Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]), Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]), SecondYAxisMax, SecondYAxisMajorUnit, SecondYAxisMin, dataRow["LineFormat"].ToString());
					break;
				case "Pie":
					AddDataToChart(currentSlide, chart, queryDataTable, dataRow["DataType"].ToString(), shortColorsTable, Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]), Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]), Convert.ToBoolean(0));
					break;
				case "PerfSum": //PerfSum does not have a multi?
					foreach (S.DataRow queryRow in queryDataTable.Rows)
					{
						if (queryRow["Q12"].ToString() != "")//the first row, data for the bar
						{
							PerfSumAddBar(chart, queryRow, shortColorsTable);
						}
						else//a unit data point
						{
							string unitName = queryRow["BarTitle"].ToString();//the unit name, bar title is just the column name
							Double unitValue = Double.Parse(queryRow["Minimum"].ToString());//the unit value, minimum is just the column name
							//TODO: when the final presentation is output, the labels are positioned as right, even though the code is putting them at center. why?
							//TODO: Use AsposeConnector
							AddPerfSumSeriesWithPoint(chart, unitName, unitValue, LegendDataLabelPosition.Center, false, queryRow["Minimum"].ToString());
						}
					}
					//end of the rows, now we recalc the slide to correct the perfsums
					RecalcPerfSums(currentSlide);
					break;
				case "StackArea":
					//TODO: StackArea multis?
					AddStackArea(chart, queryDataTable, shortColorsTable);
					break;
				case "Scatter":
					//TODO: Scatter multis?
					AddScatterPoints(chart, queryDataTable, Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]));
					//if (testP2 == true)
					//{
					//	LabelFormat labelFormat = new LabelFormat();
					//	p2.AddScatterPoints(queryDataTable, labelFormat);
					//}
					break;
				case "Table":
					currentSlide.Shapes.Remove(chart);//getting rid of the placeholder
					//TODO: fix table settings

					//TODO: multi slide table, remove the first column before sending it to AddTable
					//if (multiSlides > 0)
					//{

					//	//count the number of distinct values in the first column
					//	DataView view = new DataView(queryDataTable);
					//	String colname = queryDataTable.Columns[0].ColumnName;
					//	S.DataTable distinctRows = view.ToTable(true, colname);

					//	if (slidesRemaining == 99)
					//	{
					//		////count the number of distinct values in the first column
					//		slidesRemaining = distinctRows.Rows.Count;
					//	}

					//	String slideToMake = "";
					//	//we pull based on the slideCount and distinctRows, get each unit one at a time
					//	if ((slideCount + 1) < distinctRows.Rows.Count)
					//	{
					//		slideToMake = distinctRows.Rows[slideCount + 1].ItemArray[0].ToString();//the unit name, we add 1 because the first row is the titles
					//	}

					//	for (int r = 1; r < queryDataTable.Rows.Count; r++)
					//	{
					//		if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
					//			queryDataTable.Rows[r].Delete();
					//	}

					//	queryDataTable.AcceptChanges();
					//	queryDataTable.Columns.RemoveAt(0);//remove the first column, should this be named or is index okay? index is probably the better solution, right?

					//}

					AddTable(currentSlide, queryDataTable, Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]));//BarGapWidth used as proxy for table settings
					break;
				default:
					break;
			}

			//and if we need to add a second axis title
			if (Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]) == true)
			{
				if (dataRow["AxisTitle"].ToString() != "")
				{
					//add an axis title on the right hand side
					String newTitle = fieldReplace(dataRow["AxisTitle"].ToString());
					AddVerticalAxisLabelRight(currentSlide, newTitle);
				}
			}

			//slidesRemaining = DoLiveData(queryDataTable, conn, dataRow, currentSlide, chart, chartRow, slidesRemaining, multiSlides, slideCount);
			CalculateDataMaximums(queryDataTable, dataRow);

			return slidesRemaining;
		}

		private S.DataTable SplitMultiData(S.DataTable queryDataTable, String slideToMake)
		{
			//deletes any rows where the first column does not equal the slideToMake string, then deletes the first column
			for (int r = 0; r < queryDataTable.Rows.Count; r++)
			{
				if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
					queryDataTable.Rows[r].Delete();
			}

			queryDataTable.AcceptChanges();
			queryDataTable.Columns.RemoveAt(0);

			return queryDataTable;
		}

		//private Int32 DoLiveData(S.DataTable queryDataTable, SQL.SqlConnection conn, S.DataRow dataRow, ISlide currentSlide, IChart chart, S.DataRow chartRow, Int32 slidesRemaining, Int32 multiSlides, Int32 slideCount)
		//{
		//	Int32 SecondYAxisMax = ce.CheckForNull<Int32>(chartRow["SecondYAxisMax"], 0);
		//	Int32 SecondYAxisMajorUnit = ce.CheckForNull<Int32>(chartRow["SecondYAxisMajorUnit"], 0);
		//	Int32 SecondYAxisMin = ce.CheckForNull<Int32>(chartRow["SecondYAxisMin"], 0);

		//	//create a temporary color table, copy the main color table, delete rows that don't match, and use it
		//	S.DataTable shortColorsTable = GetShortColorTable(slideColorsTable, dataRow["SlideColorGroup"].ToString());

		//	switch (dataRow["DataType"].ToString())
		//	{
		//		case "Bar":
		//		case "Line":
		//		case "LineMarker":
		//		case "Bar100":
		//		case "BarCluster":
		//		case "BarHoriz":
		//		case "Area":

		//			//TODO: multi slide table, remove the first column before sending it to AddData
		//			if (multiSlides > 0)
		//			{
		//				//get the distinct values in the first column
		//				DataView view = new DataView(queryDataTable);
		//				String colname = queryDataTable.Columns[0].ColumnName;
		//				S.DataTable distinctRows = view.ToTable(true, colname);

		//				if (slidesRemaining == 99) //we haven't reset the value yet, so change it here
		//				{
		//					slidesRemaining = distinctRows.Rows.Count;
		//				}

		//				//we pull based on the slideCount and distinctRows, get each unit one at a time
		//				string slideToMake = distinctRows.Rows[slideCount].ItemArray[0].ToString();//the unit name

		//				for (int r = 0; r < queryDataTable.Rows.Count; r++)
		//				{
		//					if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
		//						queryDataTable.Rows[r].Delete();
		//				}

		//				queryDataTable.AcceptChanges();

		//				if (multiSlides == 1)
		//				{
		//					//then we're going to take the value in first row first column and make it the subtitle of the slide, then blank out the first column
		//					String subtitle = queryDataTable.Rows[0].ItemArray[0].ToString();

		//					if (subtitle != "")
		//					{
		//						SetSlideSubTitle(currentSlide, subtitle);
		//						if (testP2 == true)
		//						{
		//							p2.ResetSubTitle(subtitle);
		//						}
		//					}
		//				}

		//				//blank out the first column
		//				DataColumn col = queryDataTable.Columns[0];
		//				foreach (DataRow row in queryDataTable.Rows)
		//				{
		//					row[col] = "";
		//				}
		//				if (queryDataTable.Columns[1].ColumnName == "Label")
		//				//TODO: special case, this ought to be fixed for production
		//				{
		//					queryDataTable.Columns.RemoveAt(0);
		//				}
		//				if (dataRow["DataType"].ToString() == "Line")
		//				{
		//					if (multiSlides > 0)//special case for Line and multi, remove the first column so it can recognize the date field
		//					{
		//						queryDataTable.Columns.RemoveAt(0);
		//					}
		//				}
		//			}
		//			AddDataToChart(currentSlide, chart, queryDataTable, dataRow["DataType"].ToString(), shortColorsTable, Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]), Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]), Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]), SecondYAxisMax, SecondYAxisMajorUnit, SecondYAxisMin, dataRow["LineFormat"].ToString());
		//			break;
		//		case "Pie":
		//			AddDataToChart(currentSlide, chart, queryDataTable, dataRow["DataType"].ToString(), shortColorsTable, Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]), Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]), Convert.ToBoolean(0));
		//			break;
		//		case "PerfSum":
		//			foreach (S.DataRow queryRow in queryDataTable.Rows)
		//			{
		//				if (queryRow["Q12"].ToString() != "")//the first row, data for the bar
		//				{
		//					PerfSumAddBar(chart, queryRow, shortColorsTable);
		//				}
		//				else//a unit data point
		//				{
		//					string unitName = queryRow["BarTitle"].ToString();//the unit name, bar title is just the column name
		//					Double unitValue = Double.Parse(queryRow["Minimum"].ToString());//the unit value, minimum is just the column name
		//					//TODO: when the final presentation is output, the labels are positioned as right, even though the code is putting them at center. why?
		//					AddPerfSumSeriesWithPoint(chart, unitName, unitValue, LegendDataLabelPosition.Center, false, queryRow["Minimum"].ToString());
		//				}
		//			}
		//			//end of the rows, now we recalc the slide to correct the perfsums
		//			RecalcPerfSums(currentSlide);

		//			break;
		//		case "Table":
		//			currentSlide.Shapes.Remove(chart);//getting rid of the placeholder
		//			//TODO: fix table settings

		//			//TODO: multi slide table, remove the first column before sending it to AddTable
		//			if (multiSlides > 0)
		//			{

		//				//count the number of distinct values in the first column
		//				DataView view = new DataView(queryDataTable);
		//				String colname = queryDataTable.Columns[0].ColumnName;
		//				S.DataTable distinctRows = view.ToTable(true, colname);

		//				if (slidesRemaining == 99)
		//				{
		//					////count the number of distinct values in the first column
		//					slidesRemaining = distinctRows.Rows.Count;
		//				}

		//				String slideToMake = "";
		//				//we pull based on the slideCount and distinctRows, get each unit one at a time
		//				if ((slideCount + 1) < distinctRows.Rows.Count)
		//				{
		//					slideToMake = distinctRows.Rows[slideCount + 1].ItemArray[0].ToString();//the unit name, we add 1 because the first row is the titles
		//				}

		//				for (int r = 1; r < queryDataTable.Rows.Count; r++)
		//				{
		//					if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
		//						queryDataTable.Rows[r].Delete();
		//				}

		//				queryDataTable.AcceptChanges();
		//				queryDataTable.Columns.RemoveAt(0);//remove the first column, should this be named or is index okay? index is probably the better solution, right?

		//			}

		//			AddTable(currentSlide, queryDataTable, Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]));//BarGapWidth used as proxy for table settings
		//			break;

		//		case "DistPoints":
		//			if (multiSlides > 0)
		//			{
		//				//get the distinct values in the first column
		//				DataView view = new DataView(queryDataTable);
		//				String colname = queryDataTable.Columns[0].ColumnName;
		//				S.DataTable distinctRows = view.ToTable(true, colname);

		//				if (slidesRemaining == 99) //we haven't reset the value yet, so change it here
		//				{
		//					slidesRemaining = distinctRows.Rows.Count;
		//				}

		//				//we pull based on the slideCount and distinctRows, get each unit one at a time
		//				string slideToMake = distinctRows.Rows[slideCount].ItemArray[0].ToString();//the unit name

		//				for (int r = 0; r < queryDataTable.Rows.Count; r++)
		//				{
		//					if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
		//						queryDataTable.Rows[r].Delete();
		//				}

		//				queryDataTable.AcceptChanges();

		//				if (multiSlides == 1)
		//				{
		//					//then we're going to take the value in first row first column and make it the subtitle of the slide, then blank out the first column
		//					String subtitle = queryDataTable.Rows[0].ItemArray[0].ToString();

		//					if (subtitle != "")
		//					{
		//						SetSlideSubTitle(currentSlide, subtitle);
		//						if (testP2 == true)
		//						{
		//							p2.ResetSubTitle(subtitle);
		//						}
		//					}
		//				}

		//				queryDataTable.Columns.RemoveAt(0);
		//			}

		//			AddDistPoints(currentSlide, queryDataTable);
		//			break;

		//		case "ScatLine":
		//			if (queryDataTable.Columns.Count > 2)
		//				{
		//				//count the number of distinct values in the first column
		//				DataView view = new DataView(queryDataTable);
		//				String colname = queryDataTable.Columns[0].ColumnName;
		//				S.DataTable distinctRows = view.ToTable(true, colname);

		//				String slideToMake = "";
		//				for (int r = 0; r < distinctRows.Rows.Count; r++)
		//					{
		//					slideToMake = distinctRows.Rows[r].ItemArray[0].ToString();

		//					S.DataTable scatData = queryDataTable.Copy();
		//					for (int s = 0; s < scatData.Rows.Count; s++)
		//						{
		//						if (scatData.Rows[s].ItemArray[0].ToString() != slideToMake)
		//								scatData.Rows[s].Delete();
		//						}
		//					scatData.AcceptChanges();
		//					scatData.Columns.RemoveAt(0);

		//					AddScatterLine(chart, scatData, shortColorsTable, dataRow["LineFormat"].ToString());
		//					CalculateDataMaximums(scatData, dataRow);//we do this here because we broke out this data
		//					}

		//				}
		//			else
		//				{
		//				AddScatterLine(chart, queryDataTable, shortColorsTable, dataRow["LineFormat"].ToString());
		//				CalculateDataMaximums(queryDataTable, dataRow);
		//				}
		//			break;

		//		case "StackArea":
		//			AddStackArea(chart, queryDataTable, shortColorsTable);
		//			break;

		//		case "Scatter":
		//			AddScatterPoints(chart, queryDataTable, Convert.IsDBNull(dataRow["BarGapWidth"]) ? 0 : Convert.ToInt32(dataRow["BarGapWidth"]));
		//			break;

		//		default:
		//			break;
		//	}

		//	//and if we need to add a second axis title
		//	if (Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]) == true)
		//	{
		//		if (dataRow["AxisTitle"].ToString() != "")
		//		{
		//			//add an axis title on the right hand side
		//			String newTitle = fieldReplace(dataRow["AxisTitle"].ToString());
		//			AddVerticalAxisLabelRight(currentSlide, newTitle);
		//		}
		//	}

		//	//check for maximums
		//	//we do this at the end in case queryDataTable has been adjusted for multis, so that every multi page doesnt have the same maximum, which may be wildly wrong
		//	if (dataRow["DataType"].ToString() != "ScatLine")//because that is done inline
		//	{
		//		CalculateDataMaximums(queryDataTable, dataRow);
		//	}


		//	return slidesRemaining;
		//}

		private  String MakeDataSQLString(S.DataRow dataRow)
		{

            if (dataRow["DataSource"].ToString().Trim() == "")
            {
                throw new ArgumentException("DataSource cannot be blank or Null");
            }

			//get the data
			String sqlString = "SELECT * FROM " + dataRow["DataSource"].ToString() + "(";
			if (dataRow["Filter1"] != DBNull.Value) { sqlString += "'" + dataRow["Filter1"].ToString() + "'"; }//we assume that filter1 is populated first
			if (dataRow["Filter2"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter2"].ToString() + "'"; }
			if (dataRow["Filter3"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter3"].ToString() + "'"; }
			if (dataRow["Filter4"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter4"].ToString() + "'"; }
			if (dataRow["Filter5"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter5"].ToString() + "'"; }
			if (dataRow["Filter6"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter6"].ToString() + "'"; }
			if (dataRow["Filter7"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter7"].ToString() + "'"; }

			sqlString += ")";

            sqlString = fieldReplace(sqlString);
			if (Metric == true)
			{
				sqlString = sqlString.Replace("'@Metric'", "1");
			}
			else
			{
				sqlString = sqlString.Replace("'@Metric'", "0");
			}


			return sqlString;
		}

		private void CalculateDataMaximums(S.DataTable queryDataTable, S.DataRow dataRow)
		{

			//checks the data for the maximum values, puts them into primaryVerticalMax or secondaryVerticalMax for later use
			for (int r = 0; r < queryDataTable.Rows.Count; r++)
			{
				Double rowValue = 0;
				for (int s = 1; s < queryDataTable.Columns.Count; s++)
				{
					switch (dataRow["DataType"].ToString())
					{
						case "Bar":
							if (queryDataTable.Rows[r].ItemArray[s] != DBNull.Value)
								rowValue += Convert.ToDouble(queryDataTable.Rows[r].ItemArray[s]);
							break;
						case "Line":
						case "ScatLine":
						case "Area":
						case "BarCluster":
							if (queryDataTable.Rows[r].ItemArray[s] != DBNull.Value)
							{
								if (queryDataTable.Rows[r].ItemArray[s].GetType() != typeof(DateTime))
								{
									if (Convert.ToDouble(queryDataTable.Rows[r].ItemArray[s]) > rowValue)
										rowValue = Convert.ToDouble(queryDataTable.Rows[r].ItemArray[s]);
								}
							}
							break;
						case "LineMarker":
							if (queryDataTable.Rows[r].ItemArray[s] != DBNull.Value)
							{
								if (s > 1)//because this is for Trends, and column 1 has the year in it which we dont want to compare to
								{
									if (Convert.ToDouble(queryDataTable.Rows[r].ItemArray[s]) > rowValue)
										rowValue = Convert.ToDouble(queryDataTable.Rows[r].ItemArray[s]);
								}
							}
							break;
						default:
							break;
					}
				}

                if (Convert.IsDBNull(dataRow["AxisNumber"]) ? false : Convert.ToBoolean(dataRow["AxisNumber"]) == true)
				{
					if (rowValue > secondaryVerticalMax)
						secondaryVerticalMax = rowValue;
				}
				else
				{
					if (rowValue > primaryVerticalMax)
						primaryVerticalMax = rowValue;
				}
			}
		}

		private  S.DataTable GetShortColorTable(S.DataTable slideColorsTable, String colorToUse)
		{

			//get a temporary color table with only the colors that match the appropriate SlideColorGroup
			S.DataTable shortColorsTable = slideColorsTable.Copy();

			for (int r = 0; r < shortColorsTable.Rows.Count; r++)
			{
				if (shortColorsTable.Rows[r].ItemArray[0].ToString() != colorToUse)//remove any record not in this unit
					shortColorsTable.Rows[r].Delete();
			}

			shortColorsTable.Columns.RemoveAt(0);
			shortColorsTable.AcceptChanges();

			return shortColorsTable;
		}

		#endregion

		#region Working with a Table on a slide
		//TODO: Use AsposeConnector
		private void FormatTable(ITable tbl, Double columnWidth, Double firstColumnWidth = -1)
		{
			Double width = 0;

			for (int r = 0; r < tbl.Columns.Count; r++)
			{
				tbl.Columns[r].Width = columnWidth;
				width += columnWidth;
			}

			if (firstColumnWidth != -1)
			{
				tbl.Columns[0].Width = firstColumnWidth;
				width += firstColumnWidth;
			}

			tbl.Width = (float)width;
		}
		//TODO: Use AsposeConnector
		private void TableColors(ITable tbl, FillType cellFill, Color cellColor, FillType cellBorder, Color cellBorderColor)
		{
			//TODO: Use AsposeConnector
			foreach (IRow row in tbl.Rows)
			{
				foreach (ICell cell in row)
				{
				cell.FillFormat.FillType = cellFill;
				cell.FillFormat.SolidFillColor.Color = cellColor;

				//cell.BorderBottom.FillFormat.FillType = cellBorder;
				//cell.BorderBottom.FillFormat.SolidFillColor.Color = cellBorderColor;
				//cell.BorderLeft.FillFormat.FillType = cellBorder;
				//cell.BorderLeft.FillFormat.SolidFillColor.Color = cellBorderColor;
				//cell.BorderRight.FillFormat.FillType = cellBorder;
				//cell.BorderRight.FillFormat.SolidFillColor.Color = cellBorderColor;
				//cell.BorderTop.FillFormat.FillType = cellBorder;
				//cell.BorderTop.FillFormat.SolidFillColor.Color = cellBorderColor;

				foreach (IParagraph para in cell.TextFrame.Paragraphs)
					{
					foreach (IPortion por in para.Portions)
						{
						SetPortion(por, color: defaultColor);
						}
					}
				}
			}
		}
		//TODO: Use AsposeConnector
		private void GetTableSettings(Int32 tableSettings, ref Int32 tableX, ref Int32 tableY, ref float fontHeight, ref Int32 defaultRowHeight, ref Double defaultColWidth, ref FillType cellFill, ref FillType cellBorder, ref Color cellBorderColor)
			{

			//looks up each value in the TableFormats and changes the value if it is populated
			foreach (S.DataRow row in slideTableFormatsTable.Rows)
				{
				if ((Int32)row["SlideTableFormatID"] == tableSettings) //they match
					{
					if (row["TableX"] != DBNull.Value) tableX = (Int32)row["TableX"];
					if (row["TableY"] != DBNull.Value) tableY = (Int32)row["TableY"];
					if (row["FontHeight"] != DBNull.Value) fontHeight = (float)row["FontHeight"];
					if (row["RowHeight"] != DBNull.Value) defaultRowHeight = (Int32)row["RowHeight"];
					if (row["ColWidth"] != DBNull.Value) defaultColWidth = Convert.ToDouble(row["ColWidth"]);
					if (row["CellFill"] != DBNull.Value)
						{
						if (row["CellFill"].ToString() == "0")
							//TODO: Use AsposeConnector
							cellFill = FillType.NoFill;
						else
							//TODO: Use AsposeConnector
							cellFill = FillType.Solid;
						}
					//TODO: Use AsposeConnector
					if (row["BorderFill"] != DBNull.Value) cellBorder = FillType.Solid;
					if (row["BorderColorGroup"] != DBNull.Value)
						{
						//get the first color in this group
						S.DataTable shortColorsTable = GetShortColorTable(slideColorsTable, row["BorderColorGroup"].ToString());
						foreach (S.DataRow colorRow in shortColorsTable.Rows)
							{
							if (Convert.ToInt32(colorRow["ColorOrder"]) == 1)
								cellBorderColor = System.Drawing.ColorTranslator.FromHtml("#" + colorRow["Color"].ToString());
							}
						}
					}
				}
			}

		private Tuple<double[], double[]> TableRowColWidths(S.DataTable dataToAdd, Double defaultColWidth, Int32 defaultRowHeight)
		{

			int q = 0;
			Double colWidth;
			double[] dblCols = new double[dataToAdd.Rows.Count];
			double[] dblRows = new double[dataToAdd.Columns.Count];

			//must read the data to see if there are format instructions in it
			foreach (S.DataRow row in dataToAdd.Rows)
			{
			colWidth = defaultColWidth;//default value
			if (row.ItemArray[0].ToString().Contains("colWidth="))
				{
				String newWidth = row.ItemArray[0].ToString();
				newWidth = newWidth.Substring(newWidth.IndexOf("colWidth="));
				newWidth = newWidth.Substring(0, newWidth.IndexOf(">"));
				if (newWidth.IndexOf(" ") > 0)
					newWidth = newWidth.Substring(0, newWidth.IndexOf(" "));
				newWidth = newWidth.Substring(9);
				colWidth = Convert.ToInt32(newWidth);
				}
			dblCols[q] = colWidth;
			q++;
			}

			Int32 rowHeight;
			for (int c = 0; c < dataToAdd.Columns.Count; c++)
			{
				rowHeight = defaultRowHeight;//default value
				if (dataToAdd.Rows[0].ItemArray[c].ToString().Contains("rowHeight="))
				{
					String newHeight = dataToAdd.Rows[0].ItemArray[c].ToString();
					newHeight = newHeight.Substring(newHeight.IndexOf("rowHeight="));
					newHeight = newHeight.Substring(0, newHeight.IndexOf(">"));
					if (newHeight.IndexOf(" ") > 0)
						newHeight = newHeight.Substring(0, newHeight.IndexOf(" "));
					newHeight = newHeight.Substring(10);
					rowHeight = Convert.ToInt32(newHeight);
				}
				dblRows[c] = rowHeight;
			}

			return Tuple.Create(dblCols, dblRows);
		}
		//TODO: Use AsposeConnector
		private void AddTable(ISlide slide, S.DataTable dataToAdd, Int32 tableSettings)
		{
			if (dataToAdd.Rows.Count > 0)
			{
				//default values
				Int32 tableX = 40;
				Int32 tableY = 200;
				float fontHeight = 18;
				Int32 defaultRowHeight = 50;
				Double defaultColWidth = 50;
				//TODO: Use AsposeConnector
				FillType cellFill = FillType.Solid;
				Color cellColor = Color.White;
				//TODO: Use AsposeConnector
				FillType cellBorder = FillType.NoFill;
				Color cellBorderColor = Color.White;
				Color fontColor = defaultColor;

				//TODO: need to lookup some values for non-standards
				if (tableSettings != 0)
				{
					GetTableSettings(tableSettings, ref tableX, ref tableY, ref fontHeight, ref defaultRowHeight, ref defaultColWidth, ref cellFill, ref cellBorder, ref cellBorderColor);
				}

				Tuple<double[], double[]> colResult = TableRowColWidths(dataToAdd, defaultColWidth, defaultRowHeight);
				double[] dblCols = colResult.Item1;
				double[] dblRows = colResult.Item2;

				//Add table shape to slide
				//TODO: Use AsposeConnector
				ITable tbl = slide.Shapes.AddTable(100, 50, dblCols, dblRows);
				tbl.X = tableX;//left edge
				tbl.Y = tableY;//top edge
				//TODO: Use AsposeConnector
				tbl.StylePreset = TableStylePreset.None;

				//TableColors(tbl, cellFill, cellColor, cellBorder, cellBorderColor);

				FillTable(dataToAdd, tbl, fontHeight, cellFill, cellColor, fontColor, cellBorder, cellBorderColor);
			}

		}
		//TODO: Use AsposeConnector
		private void FillTable(S.DataTable dataToAdd, ITable tbl, float fontHeight, FillType cellFill, Color cellColor, Color fontColor, FillType cellBorder, Color cellBorderColor)
		{

			//some default values
			Double marginBottom = 0;
			Double marginLeft = 0;
			Double marginRight = 0;
			Double marginTop = 0;
			//TODO: Use AsposeConnector
			TextAnchorType textAnchor = TextAnchorType.Center;

			for (int i = dataToAdd.Rows.Count - 1; i >= 0; i--) //why go backwards here? because otherwise cells following a merge overwrite the merge settings
			{
				for (int j = dataToAdd.Columns.Count - 1; j >= 0; j--)
				{

					//TODO: Use AsposeConnector
					ICell cell = tbl[i, j];

					String output = dataToAdd.Rows[i].ItemArray[j].ToString();
					String format = "";

					if (output.Contains("<FMT "))
						{
						format = output.Substring(0, output.IndexOf(">"));//save the FMT string
						output = output.Substring(output.IndexOf(">") + 1);//get rid of the FMT string from the output
						}

					//reset variables to default values (need to be set for every cell specifically by the user)
					float fontHeightToUse = fontHeight;
					//TODO: Use AsposeConnector
					TextAlignment align = TextAlignment.Left;//default
					//TODO: Use AsposeConnector
					FillType cellFillToUse = cellFill;
					Color cellColorToUse = cellColor;
					Color fontColorToUse = fontColor;

					Color borderBottomColor = cellBorderColor;
					Color borderTopColor = cellBorderColor;
					Color borderLeftColor = cellBorderColor;
					Color borderRightColor = cellBorderColor;

					//TODO: Use AsposeConnector
					FillType borderBottomFillType = cellBorder;
					//TODO: Use AsposeConnector
					FillType borderTopFillType = cellBorder;
					//TODO: Use AsposeConnector
					FillType borderLeftFillType = cellBorder;
					//TODO: Use AsposeConnector
					FillType borderRightFillType = cellBorder;


					//Boolean rightBorder = false;
					//Boolean bottomBorder = false;
					marginBottom = 0;
					marginLeft = 0;
					marginRight = 0;
					marginTop = 0;
					//TODO: Use AsposeConnector
					textAnchor = TextAnchorType.Center;

					if (format != "")
					{
						String newFormat;
						newFormat = GetFormatString(format, "Merge=");
						if (newFormat != "")
						{
							Int32 mergeWidth = Convert.ToInt32(newFormat.Substring(0, newFormat.IndexOf(",")));
							Int32 mergeHeight = Convert.ToInt32(newFormat.Substring(newFormat.IndexOf(",") + 1));
							tbl.MergeCells(tbl[i, j], tbl[i + mergeWidth - 1, j + mergeHeight - 1], false);
						}

						newFormat = GetFormatString(format, "Align=");
						if (newFormat != "")
						{
							switch (newFormat)
							{
								case "Left":
									//TODO: Use AsposeConnector
									align = TextAlignment.Left;
									break;
								case "Center":
									//TODO: Use AsposeConnector
									align = TextAlignment.Center;
									break;
								case "Right":
									//TODO: Use AsposeConnector
									align = TextAlignment.Right;
									break;
								default:
									//TODO: Use AsposeConnector
									align = TextAlignment.Left;
									break;
							}
						}

						newFormat = GetFormatString(format, "fontHeight=");
						if (newFormat != "")
							fontHeightToUse = (float)Convert.ToDouble(newFormat);

						newFormat = GetFormatString(format, "Color=");
						if (newFormat != "")
						{
							cellColorToUse = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);
							//TODO: Use AsposeConnector
							cellFillToUse = FillType.Solid;//because if they defined a color it must be solid, regardless of the overriding cellFill value
						}

						newFormat = GetFormatString(format, "ColorFont=");
						if (newFormat != "")
							fontColorToUse = System.Drawing.ColorTranslator.FromHtml("#" + newFormat);

						newFormat = GetFormatString(format, "RightBorder=");
						if (newFormat != "")
							{
					//rightBorder = true;
								//TODO: Use AsposeConnector
							borderRightFillType = FillType.Solid;
							borderRightColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
							}

						newFormat = GetFormatString(format, "BottomBorder=");
						if (newFormat != "")
							{
					//bottomBorder = true;
								//TODO: Use AsposeConnector
							borderBottomFillType = FillType.Solid;
							borderBottomColor = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
							}


						newFormat = GetFormatString(format, "RightMargin=");
						if (newFormat != "")
							marginRight = Convert.ToDouble(newFormat);

						newFormat = GetFormatString(format, "LeftMargin=");
						if (newFormat != "")
							marginLeft = Convert.ToDouble(newFormat);
					}

					cell.MarginBottom = marginBottom;
					cell.MarginLeft = marginLeft;
					cell.MarginRight = marginRight;
					cell.MarginTop = marginTop;
					cell.TextAnchorType = textAnchor;

					if (output != "")
						{
						if (output.Contains("</") || output.Contains("/>"))//if it has some html with a close tag
							{
							tbl[i, j].TextFrame.Paragraphs.AddFromHtml(output);//adds it as an additional paragraph
							if (tbl[i, j].TextFrame.Paragraphs.Count > 1)//now there should be more than one, but there may be the rare case where there is not, such as the rare <b></b> data point
								tbl[i, j].TextFrame.Paragraphs.RemoveAt(0);//so we have to delete the original paragraph
							}
						else
							{
							tbl[i, j].TextFrame.Text = output;//just put it in as text
							}
						}

					tbl[i, j].TextFrame.Paragraphs[0].ParagraphFormat.Alignment = align;

					tbl[i, j].FillFormat.FillType = cellFillToUse;
					tbl[i, j].FillFormat.SolidFillColor.Color = cellColorToUse;


					foreach (Portion p in tbl[i, j].TextFrame.Paragraphs[0].Portions)
					{
						////tableCell.TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = cell.fontHeight;
						////tableCell.TextFrame.Paragraphs[0].Portions[0].PortionFormat.FillFormat.FillType = FillType.Solid;
						////tableCell.TextFrame.Paragraphs[0].Portions[0].PortionFormat.FillFormat.SolidFillColor.Color = cell.fontColor;
						////p.ParagraphFormat.Alignment = cell.alignment;
						//p.PortionFormat.FontHeight = cell.fontHeight;
						//p.PortionFormat.FillFormat.FillType = FillType.Solid;
						//p.PortionFormat.FillFormat.SolidFillColor.Color = cell.fontColor;
						SetPortion(p, color: fontColorToUse, fontHeight: fontHeightToUse);

					}


					//SetPortion(tbl[i, j].TextFrame.Paragraphs[0].Portions[0], color: fontColorToUse, fontHeight: fontHeightToUse);
					//tbl[i, j].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = fontHeightToUse;
					//tbl[i, j].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FillFormat.FillType = FillType.Solid;
					//tbl[i, j].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FillFormat.SolidFillColor.Color = fontColorToUse;

					//if (rightBorder == true)
					//	{
						//TableRightBorder(tbl, j, i, rightBorder);
						//}
					//if (bottomBorder == true)
						//{
						//TableBottomBorder(tbl, j, i, bottomBorder);
						//}
					tbl[i, j].BorderBottom.FillFormat.FillType = borderBottomFillType;
					tbl[i, j].BorderBottom.FillFormat.SolidFillColor.Color = borderBottomColor;
					tbl[i, j].BorderLeft.FillFormat.FillType = borderLeftFillType;
					tbl[i, j].BorderLeft.FillFormat.SolidFillColor.Color = borderLeftColor;
					tbl[i, j].BorderRight.FillFormat.FillType = borderRightFillType;
					tbl[i, j].BorderRight.FillFormat.SolidFillColor.Color = borderRightColor;
					tbl[i, j].BorderTop.FillFormat.FillType = borderTopFillType;
					tbl[i, j].BorderTop.FillFormat.SolidFillColor.Color = borderTopColor;



				}
			}

		}

		private String GetFormatString(String format, String formatToFind)
		{

			String output = "";
			if (format.Contains(formatToFind))
			{
				output = format.Substring(format.IndexOf(formatToFind));
				if (output.IndexOf(" ") > 0)
					output = output.Substring(0, output.IndexOf(" "));
					output = output.Substring(formatToFind.Length);
			}

			return output;
		}

		[Obsolete]
		private void OriginalAddTable(ISlide slide, S.DataTable dataToAdd, Int32 tableSettings)
			{
			//THIS HAS BEEN SUPERCEDED (AKA REWRITTEN)
			double[] dblCols;// = { 90, 90, 90, 90, 90, 90 };//these are default values that should be overwritten, just here so the code works
			double[] dblRows;// = { 50, 50, 50, 50, 50, 50 };//these are default values that should be overwritten, just here so the code works
			//Int32 width = 0;
			//Int32 height = 0;
			Int32 tableX = 40;
			Int32 tableY = 200;
			Int32 fontHeight = 18;
			//TODO: Use AsposeConnector
			FillType cellFill = FillType.Solid;
			Color cellColor = Color.White;
			//TODO: Use AsposeConnector
			FillType cellBorder = FillType.Solid;
			Color cellBorderColor = Color.White;

			//for some reason the Aspose library has the row height as Get only (columns are GetSet) so we have to set their values before creating them
			Int32 rowHeight = 50;
			switch (tableSettings)
				{
				case 1:
				case 5:
					rowHeight = 50;
					break;
				case 2:
					rowHeight = 25;
					break;
				case 3:
				case 4:
					rowHeight = 24;
					break;
				default:
					rowHeight = 50;
					break;
				}

			//set up the table based on the data, these are reversed (Rows = dblCols) because that's the way the data should come in 
			dblCols = new double[dataToAdd.Rows.Count];
			for (int q = 0; q < dataToAdd.Rows.Count; q++)
				dblCols[q] = 50;

			dblRows = new double[dataToAdd.Columns.Count];
			for (int q = 0; q < dataToAdd.Columns.Count; q++)
				dblRows[q] = rowHeight;

			//certain slides will have other sizes
			switch (tableSettings)
				{
				case 3:
				case 4:
					dblRows[3] = 10;
					dblRows[4] = 10;
					dblRows[7] = 10;
					dblRows[10] = 10;
					dblRows[13] = 10;
					dblRows[16] = 10;
					break;
				default:
					break;
				}





			//Add table shape to slide
			//TODO: Use AsposeConnector
			ITable tbl = slide.Shapes.AddTable(100, 50, dblCols, dblRows);


			//TODO: how to know how wide the columns should be?
			switch (tableSettings)
				{
				case 1:
					FormatTable(tbl, 90, 200);
					break;
				case 2:
					FormatTable(tbl, 78.5, 106);//78,112
					tableX = 21;//20
					tableY = 307;//316
					fontHeight = 12;
					//TODO: Use AsposeConnector
					cellFill = FillType.NoFill;
					cellBorderColor = Color.FromArgb(41, 41, 41);
					break;
				case 3:
				case 4:
					FormatTable(tbl, 11);
					//TODO: Use AsposeConnector
					cellBorder = FillType.NoFill;
					//height = 372;
					tableY = 100;
					fontHeight = 11;

					break;
				case 5:
					FormatTable(tbl, 90, 200);
					break;
				default:

					break;
				}

			tbl.X = tableX;//left edge
			tbl.Y = tableY;//top edge

			for (int i = 0; i < dataToAdd.Rows.Count; i++)
				{
				for (int j = 0; j < dataToAdd.Columns.Count; j++)
					{
					String output;
					output = dataToAdd.Rows[i].ItemArray[j].ToString();
					if (output.Contains("</"))//if it has some html
						{
						tbl[i, j].TextFrame.Paragraphs.AddFromHtml(output);//adds it as an additional paragraph
						tbl[i, j].TextFrame.Paragraphs.RemoveAt(0);//so we have to delete the original paragraph
						}
					else
						{
						tbl[i, j].TextFrame.Text = output;//just put it in as text
						}

					tbl[i, j].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = fontHeight;//and set the font height
					}
				}

			TableColors(tbl, cellFill, cellColor, cellBorder, cellBorderColor);


			if (tableSettings == 3 || tableSettings == 4)//special case for LRO tree
				{
					//TODO: Use AsposeConnector
				tbl.StylePreset = TableStylePreset.None;



				for (int i = tbl.Columns.Count - 1; i >= 0; i--)
					{
					for (int j = tbl.Rows.Count - 1; j >= 0; j--)
						{
							//TODO: Use AsposeConnector
						ICell cell = tbl[i, j];
						int textlength = tbl[i, j].TextFrame.Text.Length;
						if (textlength > 0)
							{
							int colstouse = 5;
							if (j == 1) colstouse = 9;
							if (tableSettings == 4 && j == 5) colstouse = 7;


							tbl.MergeCells(tbl[i, j], tbl[i + colstouse, j + 1], false);

							//color the boxes
							//find out the color it should be based on the number
							string numToFind = cell.TextFrame.Text;
							string colorToUse = "660066";

							if (numToFind.IndexOf("%") > 0)
								{
								//find first percent sign, remove everything from there to end
								numToFind = numToFind.Remove(numToFind.IndexOf("%"));
								//find last space, remove everything from there to beginning
								numToFind = numToFind.Substring(numToFind.LastIndexOf(" "));
								//what's left should be a number

								double numFound = Convert.ToDouble(numToFind);
								if (numFound < 10)
									{
									colorToUse = "0380CD";
									}
								}

							//TODO: Use AsposeConnector
							tbl[i, j].FillFormat.FillType = FillType.Solid;
							tbl[i, j].FillFormat.SolidFillColor.Color = System.Drawing.ColorTranslator.FromHtml("#" + colorToUse);

							//TODO: Use AsposeConnector
							tbl[i, j].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FillFormat.FillType = FillType.Solid;
							tbl[i, j].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FillFormat.SolidFillColor.Color = Color.White;

							}
						cell.MarginBottom = 0;
						cell.MarginLeft = 0;
						cell.MarginRight = 0;
						cell.MarginTop = 0;
						//TODO: Use AsposeConnector
						cell.TextAnchorType = TextAnchorType.Center;

						//cell.TextFrame.TextFrameFormat.AutofitType = TextAutofitType.None;
						//cell.TextFrame.TextFrameFormat.CenterText = NullableBool.True;
							//TODO: Use AsposeConnector
						cell.TextFrame.Paragraphs[0].ParagraphFormat.Alignment = TextAlignment.Center;
						//cell.TextFrame.TextFrameFormat.AnchoringType = TextAnchorType.Center;
						}
					}

				//for the LRO tree certain rows need to be adjusted
				for (int i = 0; i < tbl.Columns.Count; i++)
					{
					tbl[i, 3].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 1;
					tbl[i, 4].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 1;
					tbl[i, 7].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 1;
					tbl[i, 10].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 1;
					tbl[i, 13].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 1;
					tbl[i, 16].TextFrame.Paragraphs[0].Portions[0].PortionFormat.FontHeight = 1;
					}

				for (int i = 0; i < tbl.Rows.Count; i++)
					{
					tbl.Rows[i].MinimalHeight = dblRows[i];
					}

				//TODO: fix how lines are drawn in Unplanned LRO Trees
				//and we need to do some coloring, this really needs to have a better way to do it but i'm pressed for time now so will try and fix later
				if (tableSettings == 3)
					{
					for (int p = 3; p < 51; p++)
						{
						TableBottomBorder(tbl, 3, p);
						}

					TableBottomBorder(tbl, 8, 3);
					TableBottomBorder(tbl, 11, 3);
					TableBottomBorder(tbl, 14, 3);
					TableBottomBorder(tbl, 17, 3);
					TableBottomBorder(tbl, 8, 15);
					TableBottomBorder(tbl, 11, 15);
					TableBottomBorder(tbl, 14, 15);
					TableBottomBorder(tbl, 17, 15);
					TableBottomBorder(tbl, 8, 38);
					TableBottomBorder(tbl, 11, 38);
					TableBottomBorder(tbl, 14, 38);
					TableBottomBorder(tbl, 17, 38);
					TableBottomBorder(tbl, 8, 50);
					TableBottomBorder(tbl, 11, 50);
					TableBottomBorder(tbl, 14, 50);
					TableBottomBorder(tbl, 8, 51);
					TableBottomBorder(tbl, 11, 51);

					TableRightBorder(tbl, 4, 2);
					TableRightBorder(tbl, 4, 14);
					TableRightBorder(tbl, 3, 26);
					TableRightBorder(tbl, 4, 26);
					TableRightBorder(tbl, 4, 38);
					TableRightBorder(tbl, 4, 50);

					for (int p = 7; p < 18; p++)
						{
						TableRightBorder(tbl, p, 2);
						TableRightBorder(tbl, p, 14);
						TableRightBorder(tbl, p, 38);
						if (p < 15) TableRightBorder(tbl, p, 50);
						}
					}

				if (tableSettings == 4)
					{
					for (int p = 9; p < 45; p++)
						{
						TableBottomBorder(tbl, 3, p);
						}

					TableRightBorder(tbl, 4, 8);
					TableRightBorder(tbl, 4, 20);
					TableRightBorder(tbl, 3, 26);
					TableRightBorder(tbl, 4, 32);
					TableRightBorder(tbl, 4, 44);





					}
				}
			else
				{
					//TODO: Use AsposeConnector
				IRow firstrow = tbl.Rows[0];
				foreach (ICell cell in firstrow)
					{
					foreach (IParagraph para in cell.TextFrame.Paragraphs)
						{
						foreach (IPortion por in para.Portions)
							{
							por.PortionFormat.FillFormat.FillType = FillType.Solid;
							por.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(152, 0, 46);
							}
						}
					}

				}



			//Add text to the merged cell
			//tbl[0, 0].TextFrame.Text = "Merged Cells";

			}
		//TODO: Use AsposeConnector
		private void TableBottomBorder(ITable tbl, int cellX, int cellY, Boolean useColor = false)
			{

			//tbl[cellY, cellX].BorderBottom.Style = LineStyle.Single;
				//TODO: Use AsposeConnector
			tbl[cellY, cellX].BorderBottom.FillFormat.FillType = FillType.Solid;
			//tbl[cellY, cellX].BorderBottom.Width = 1;
			Color colorToUse; 
			if (useColor == true)
				{
				colorToUse = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
				}
			else
				{
				colorToUse = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
				}
			tbl[cellY, cellX].BorderBottom.FillFormat.SolidFillColor.Color = colorToUse;//777779

			}
		//TODO: Use AsposeConnector
		private void TableRightBorder(ITable tbl, int cellX, int cellY, Boolean useColor = false)
			{

				//tbl[cellY, cellX].BorderRight.Style = LineStyle.Single;
				//TODO: Use AsposeConnector
			tbl[cellY, cellX].BorderRight.FillFormat.FillType = FillType.Solid;
				Color colorToUse;
				if (useColor == true)
				{
					colorToUse = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");
				}
				else
				{
					colorToUse = System.Drawing.ColorTranslator.FromHtml("#FFFFFF");
				}
				tbl[cellY, cellX].BorderRight.FillFormat.SolidFillColor.Color = colorToUse;


			}

		#endregion

		#region Make New Slides

		/// <summary>Add a blank slide to the presentation with the supplied title and subtitle</summary>
		//TODO: Use AsposeConnector
		private  ISlide MakeSlide(Presentation pres, String title, String subtitle)
			{
			//TODO: clean up fixed code (e.g. RGB)
				//TODO: Use AsposeConnector
			ISlideCollection slds = pres.Slides;

			//add the empty slide
			//TODO: Use AsposeConnector
			ISlide slide = slds.AddEmptySlide(pres.LayoutSlides[0]);

			SetSlideTitle(slide, title);
			if (subtitle != "")
				SetSlideSubTitle(slide, subtitle);

			return slide;
			}

		/// <summary>Add a slide from a template to the presentation</summary>
        [Obsolete("Call it with the slide name instead of the slide number")]
		//TODO: Use AsposeConnector
			private void AddTemplateSlide(Presentation currentPres, Presentation template, int slideNumber)
			{
			//TODO: whatifs: can't get path? slidenumber doesnt exist in that pres?
			//Clone the desired slide from the source presentation to the end of the collection of slides in destination presentation
			currentPres.Slides.AddClone(template.Slides[slideNumber], currentPres.Masters[0], true);//uses the existing master, copies master if they don't match
			}

		//TODO: Use AsposeConnector
		private void AddTemplateSlide(Presentation currentPres, Presentation template, String slideName)
		{
			//TODO: whatifs: can't get path? slidenumber doesnt exist in that pres?
			//Clone the desired slide from the source presentation to the end of the collection of slides in destination presentation
			//TODO: Use AsposeConnector
			ISlideCollection slides = template.Slides;

			Int32 slideNumberToAdd = GetSlideNameSlide(slides, slideName);

			if (slideNumberToAdd != -1)//assuming we found a slide
			{
				//TODO: Use AsposeConnector
                ISlide newSlide = currentPres.Slides.AddClone(template.Slides[slideNumberToAdd]);

                //and then remove the slide name box
				foreach(IShape shape in newSlide.Shapes)
				{
					if(shape.Name == "SlideName")
					{
						shape.ParentGroup.Shapes.Remove(shape);
						break;
					}
				}

			}
			else //if we didnt find a slide we need to add something otherwise we get a bunch of errors
			{
				currentPres.Slides.AddEmptySlide(currentPres.LayoutSlides[0]);
			}
		}

		//TODO: Use AsposeConnector
		private Int32 GetSlideNameSlide (ISlideCollection slides, String slideName)
		{
			//given a collection of slides, finds the one with the right name and returns it
			//TODO: Use AsposeConnector
			foreach (ISlide slide in slides)
				{
				foreach (IShape shape in slide.Shapes)
					{
					if (shape.Name == "SlideName")
						{
						ITextFrame text = ((IAutoShape)shape).TextFrame;
						if (text.Text == slideName)
							{
							return slide.SlideNumber - 1;
							}
						break;//gets us out of this slide once we've checked the name, don't need to check all the other shapes on the slide
						}
					}
				}

			return -1;//slide with this name wasn't found
		}

		#endregion

		#region Slide Titles

		//TODO: Use AsposeConnector
		private void SetSlideTitle(ISlide slide, String title)
		{
			//TODO: Use AsposeConnector
			IShape shape = slide.Shapes[0];
			//TODO: Use AsposeConnector
			ITextFrame text = ((IAutoShape)shape).TextFrame;
			//TODO: Use AsposeConnector
			IParagraph para = text.Paragraphs[0];
			//TODO: Use AsposeConnector
			IPortion port = para.Portions[0];
			//port.Text = title;
			SetPortion(port, text: title);
		}

		//TODO: Use AsposeConnector
		private void SetSlideSubTitle(ISlide slide, String subtitle)
		{
			//TODO: Use AsposeConnector
			IShape shape = slide.Shapes[0];
			//TODO: Use AsposeConnector
			ITextFrame text = ((IAutoShape)shape).TextFrame;
			if (text.Paragraphs.Count == 1)//if there is no subtitle, add one, otherwise change it
			{
				text.Paragraphs.AddFromHtml(subtitle);
				IParagraph para2 = text.Paragraphs[1];
				//IPortion port2 = para2.Portions[0];
				//port2.PortionFormat.FontHeight = 20;
				//port2.PortionFormat.FillFormat.FillType = FillType.Solid;
				//port2.PortionFormat.FillFormat.SolidFillColor.Color = defaultColor;
				SetPortion(para2.Portions[0], fontHeight: 20, color: defaultColor);
			}
			else
			{
				text.Paragraphs[1].Portions[0].Text = subtitle;
			}
		}

		#endregion

		#region Add a Chart to a Slide

		private  IChart AddChart(ISlide slide, String chartType, String xAxisTitle, String yAxisTitle, String chartTitle = "", Int32 xPos = -1, Int32 yPos = -1, Int32 width = -1, Int32 height = -1, Double? YAxisMax = null, Double? YAxisMajorUnit = null, Double? YAxisMin = null, Int32? XAxisMax = null, Decimal? XAxisMajorUnit = null, Int32? XAxisMin = null)//, Int32? SecondYAxisMax = null, Int32? SecondYAxisMajorUnit = null, Int32? SecondYAxisMin = null)
			{
			//this always adds a chart, using default values if none are supplied
			//TODO: much cleanup needed, removal of hardcoded values

			IChart chart;
			//these are default values, which are changed if the user sent new ones
			Int32 chartX = 47;// 62;//90
			Int32 chartY = 97;// 60;// 83;//90
			Int32 chartWidth = 603;// 584;//526
			Int32 chartHeight = 363;// 318;//336

			if (xPos != -1){chartX = xPos;}
			if (yPos != -1) { chartY = yPos; }
			if (width != -1) { chartWidth = width; }
			if (height != -1) { chartHeight = height; }

			//what kind of chart are we supposed to be building? comes from the data
			switch (chartType)
				{
				case "Bar":
					chart = slide.Shapes.AddChart(ChartType.StackedColumn, chartX, chartY, chartWidth, chartHeight);
					break;
				case "Bar100":
					//chart = slide.Shapes.AddChart(ChartType.PercentsStackedBar, chartX, chartY, chartWidth, chartHeight);
					chart = slide.Shapes.AddChart(ChartType.StackedBar, chartX, chartY, chartWidth, chartHeight);
					//chart.Axes.VerticalAxis.IsPlotOrderReversed = true;
					break;
				case "BarCluster":
					chart = slide.Shapes.AddChart(ChartType.ClusteredColumn, chartX, chartY, chartWidth, chartHeight);
					break;
				case "BarHoriz":
					chart = slide.Shapes.AddChart(ChartType.StackedBar, chartX, chartY, chartWidth, chartHeight);
					chart.Axes.VerticalAxis.IsPlotOrderReversed = true;
					break;
				case "Line":
					chart = slide.Shapes.AddChart(ChartType.ScatterWithStraightLines, chartX, chartY, chartWidth, chartHeight);
					break;
				case "LineMarker":
					chart = slide.Shapes.AddChart(ChartType.ScatterWithStraightLinesAndMarkers, chartX, chartY, chartWidth, chartHeight);
					chart.DisplayBlanksAs = DisplayBlanksAsType.Gap;
					break;
				case "PerfSum":
					chart = slide.Shapes.AddChart(ChartType.StackedBar, chartX, chartY, chartWidth, chartHeight);
					break;
				case "Pie":
					chart = slide.Shapes.AddChart(ChartType.Pie, chartX, chartY, chartWidth, chartHeight);
					break;
				case "Scatter":
					chart = slide.Shapes.AddChart(ChartType.ScatterWithStraightLines, chartX, chartY, chartWidth, chartHeight);
					break;
				default:
					chart = slide.Shapes.AddChart(ChartType.StackedColumn, chartX, chartY, chartWidth, chartHeight);//if in doubt, bar it out
					break;
				}

			//Delete default generated series and categories
			chart.ChartData.Series.Clear();
			chart.ChartData.Categories.Clear();
			chart.ChartData.ChartDataWorkbook.Clear(0);

			//delete legend
			chart.HasLegend = false;

			if (chartTitle != "")
				{
				chart.HasTitle = true;
				chart.ChartTitle.AddTextFrameForOverriding(chartTitle);
				IPortion cTitle = chart.ChartTitle.TextFrameForOverriding.Paragraphs[0].Portions[0];
				//cTitle.PortionFormat.FillFormat.FillType = FillType.Solid;
				//cTitle.PortionFormat.FillFormat.SolidFillColor.Color = defaultColor;
				if (chartType == "Pie")
					{
					//cTitle.PortionFormat.FontHeight = 22;
					//cTitle.PortionFormat.FontBold = NullableBool.False;
					SetPortion(cTitle, color: defaultColor, fontHeight: 22, fontBold: NullableBool.False);
					}
				else
					{
					//cTitle.PortionFormat.FontHeight = 16;
					//cTitle.PortionFormat.FontBold = NullableBool.True;
					SetPortion(cTitle, color: defaultColor, fontHeight: 16, fontBold: NullableBool.True);
					}
				
				chart.ChartTitle.TextFrameForOverriding.TextFrameFormat.MarginTop = 0;
				
				}

			switch (chartType)
				{
				case "PerfSum":
				case "Pie":
					chart.PlotArea.Format.Fill.FillType = FillType.NoFill;
					chart.PlotArea.Format.Line.FillFormat.FillType = FillType.NoFill;
					break;
				default:
					chart.PlotArea.Format.Fill.FillType = FillType.Solid;
					chart.PlotArea.Format.Fill.SolidFillColor.Color = Color.FromArgb(234, 234, 234);
					chart.PlotArea.Format.Line.FillFormat.FillType = FillType.Solid;
					chart.PlotArea.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
				break;
				}

			//plot area
			//pie plot area is centered on the middle of the pie, which is really annoying that it is different to everything else

			//plot area size is based on the plot plus the axes, so the numbers in the axis count
			//this is really irritating but i don't think we can do anything about it right now
			//1 simply means fill the entire plot area, 0 means start at the left/top edge

			switch (chartType)
				{
				case "Pie":
					chart.PlotArea.Width = 0.75f;
					chart.PlotArea.Height = 0.75f;
					chart.PlotArea.X = 0f;
					chart.PlotArea.Y = 0.05f;//slight offset to handle the title, not sure if i should do this or not, what it will affect elsewhere
					break;

				case "Bar100":
					chart.PlotArea.Height = 0.9f;
					break;

				default:
					chart.PlotArea.Width = 0.9f;//.99
					chart.PlotArea.X = 0.0f;//0f
					chart.PlotArea.Height = 0.8f;//.99
					chart.PlotArea.Y = 0f;
					break;
				}

			if (chartType != "Pie")
				{

				chart.Axes.VerticalAxis.IsVisible = true;
				chart.Axes.HorizontalAxis.IsVisible = true;

				chart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.Solid;
				chart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.SolidFillColor.Color = Color.Transparent;

				if (YAxisMax != 0 || YAxisMajorUnit != 0 || YAxisMin != 0)//if they're both zero we didnt get anything so don't do anything
					{
					if (YAxisMax != null)
						{
						chart.Axes.VerticalAxis.IsAutomaticMaxValue = false;
						chart.Axes.VerticalAxis.MaxValue = (Double)YAxisMax;
						}
					if (YAxisMajorUnit != null)
						{
						chart.Axes.VerticalAxis.IsAutomaticMajorUnit = false;
						chart.Axes.VerticalAxis.MajorUnit = (Double)YAxisMajorUnit;
						}
					if (YAxisMin != null)
						{
						chart.Axes.VerticalAxis.IsAutomaticMinValue = false;
						chart.Axes.VerticalAxis.MinValue = (Double)YAxisMin;
						}
					}

				if (XAxisMax != 0 || XAxisMajorUnit != 0 || XAxisMin != 0)//if they're both zero we didnt get anything so don't do anything
					{
					if (XAxisMax != null)
						{
						chart.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
						chart.Axes.HorizontalAxis.MaxValue = (Double)XAxisMax;
						}
					if (XAxisMajorUnit != null)
						{
						chart.Axes.HorizontalAxis.IsAutomaticMajorUnit = false;
						chart.Axes.HorizontalAxis.MajorUnit = (Double)XAxisMajorUnit;
						}
					if (XAxisMin != null)
						{
						chart.Axes.HorizontalAxis.IsAutomaticMinValue = false;
						chart.Axes.HorizontalAxis.MinValue = (Double)XAxisMin;
						}
					}



				FontData fd3 = new FontData("Tahoma");
				
				IChartPortionFormat txtCat = chart.Axes.HorizontalAxis.TextFormat.PortionFormat;
				txtCat.FillFormat.FillType = FillType.Solid;
				txtCat.FillFormat.SolidFillColor.Color = defaultColor;
				txtCat.LatinFont = fd3;
				chart.Axes.HorizontalAxis.TextFormat.PortionFormat.FontHeight = 16;

				IChartPortionFormat txtCat2 = chart.Axes.VerticalAxis.TextFormat.PortionFormat;
				txtCat2.FillFormat.FillType = FillType.Solid;
				txtCat2.FillFormat.SolidFillColor.Color = defaultColor;
				txtCat2.LatinFont = fd3;
				chart.Axes.VerticalAxis.TextFormat.PortionFormat.FontHeight = 16;


				if (chartType == "Bar100")
					{
					chart.Axes.VerticalAxis.TextFormat.PortionFormat.FontHeight = 12;
					chart.Axes.HorizontalAxis.TextFormat.PortionFormat.FontHeight = 12;
					chart.Axes.HorizontalAxis.NumberFormat = "0";
					}

				//Axis line color
				chart.Axes.HorizontalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
				chart.Axes.HorizontalAxis.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
				chart.Axes.VerticalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
				chart.Axes.VerticalAxis.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;


				//not using the chart axis titles
				chart.Axes.VerticalAxis.HasTitle = false;
				chart.Axes.HorizontalAxis.HasTitle = false;
				

				//TODO: add vertical axis label (when necessary)
				if (yAxisTitle != "")
					{
					if (chartType == "Bar" && yPos != -1 && height != -1)
						{
						AddVerticalAxisLabel(slide, yAxisTitle, 25, yPos, 30, height);
						}
					else
						{
						AddVerticalAxisLabelLeft(slide, yAxisTitle);//, chartY, chartHeight, fd3);
						}
					}

				if (xAxisTitle != "")
					{
					AddHorizontalAxisLabel(slide, xAxisTitle);
					}
				
				}

			//TODO: add horizontal axis label (when necessary)

			chart.DisplayBlanksAs = DisplayBlanksAsType.Gap;

			return chart;
			}

		#endregion

		#region Chart Data

		//private void AddDataToChartP2(S.DataTable queryDataTable, S.DataRow dataRow, S.DataTable slideColors, S.DataRow chartRow)
		//{

		//	for (int rownum = 0; rownum < queryDataTable.Rows.Count; rownum++)
		//	{
		//		p2.AddCategory(queryDataTable.Rows[rownum].ItemArray[0]);
		//	}

		//	for (int colnum = 1; colnum < queryDataTable.Columns.Count; colnum++) //skip the first column, it is the category names
		//	{
		//		ChartType seriesChartType = GetChartType(dataRow["DataType"].ToString());
		//		p2.AddSeries(queryDataTable.Columns[colnum].ColumnName, seriesChartType);

		//		Color color = defaultColor;
		//		try
		//		{
		//			color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colnum - 1].ItemArray[0]);
		//		}
		//		catch (IndexOutOfRangeException)
		//		{
		//			color = defaultColor;
		//		}

		//		p2.SetSeriesColor(color);
		//		p2.SetSeriesOverlap(seriesChartType);

		//		for (int rownumber = 0; rownumber < queryDataTable.Rows.Count; rownumber++)
		//		{
		//			switch (seriesChartType)
		//			{
		//				case ChartType.StackedColumn:
		//					p2.AddDataPoint((Double)queryDataTable.Rows[rownumber].ItemArray[colnum], ChartType.StackedColumn);
		//					break;
		//				default:
		//					break;
		//			}
		//		}

		//		Boolean addManualLegend = Convert.IsDBNull(chartRow["ManualLegend"]) ? false : Convert.ToBoolean(chartRow["ManualLegend"]);
		//		if (addManualLegend)
		//		{
		//			switch(seriesChartType)
		//			{
		//				case ChartType.StackedColumn:
		//					//AddLegendBelowChart(slide, dataToAdd.Columns[colnum].ColumnName, color, colnum);
		//					p2.AddLegendBelowChart(queryDataTable.Columns[colnum].ColumnName, color, colnum);
		//					break;
		//				default:
		//					break;
		//			}
		//		}

		//	}

		//}

		private void AddDataToChart(ISlide slide, IChart chart, S.DataTable dataToAdd, String dataChartType, S.DataTable slideColors, Boolean addManualLegend, Int32 barGapWidth, Boolean axis, Int32? SecondYAxisMax = null, Int32? SecondYAxisMajorUnit = null, Int32? SecondYAxisMin = null, String lineFormat = null)
		{

			//first figure out what charttype this particular data needs
			ChartType chartType = GetChartType(dataChartType);

			//get some references to the existing chart data
			IChartDataWorkbook fact = chart.ChartData.ChartDataWorkbook;
			Int32 catCount = chart.ChartData.Categories.Count;
			Int32 serCount = chart.ChartData.Series.Count;

			if (dataChartType == "LineMarker")
				{
				//DoLineMarker(chart, dataToAdd, fact, catCount, serCount, chartType, slideColors);
				DoLineMarker(slide, chart, dataToAdd, fact, catCount, serCount, chartType, slideColors, dataChartType, barGapWidth, addManualLegend, axis, SecondYAxisMax, SecondYAxisMajorUnit, SecondYAxisMin, lineFormat);
				}
			else
				{
				DoNonLineMarker(slide, chart, dataToAdd, fact, catCount, serCount, chartType, slideColors, dataChartType, barGapWidth, addManualLegend, axis, SecondYAxisMax, SecondYAxisMajorUnit, SecondYAxisMin, lineFormat);
				}

			//additional formatting of the charts
			switch (chartType)
				{
				case ChartType.StackedColumn:
				case ChartType.ClusteredColumn:
					chart.Axes.HorizontalAxis.TextFormat.PortionFormat.FontHeight = 12;
					break;
				case ChartType.ScatterWithStraightLines:
					SetHorizontalAxisMinMax(chart, 0, dataToAdd.Rows.Count - 1);
					break;
				default:
					//add more if necessary
					break;
				}

			if (dataChartType == "BarHoriz")
				{
				HorizontalBarFormatting(fact, chart);
				}

			if (dataChartType == "Bar")
				{
				chart.Axes.HorizontalAxis.AxisBetweenCategories = true;
				}

			if (dataChartType == "LineMarker")
				{
				chart.DisplayBlanksAs = DisplayBlanksAsType.Gap;
				}

			if (dataChartType == "BarCluster")
				{
				chart.Type = ChartType.ClusteredColumn;
				}
		}

		private void HorizontalBarFormatting(IChartDataWorkbook fact, IChart chart)
			{

			for (int i = 1; i < 9; i++)
				{
				fact.GetCell(0, i, 0).Value = fact.GetCell(0, i, 1).Value;
				}

			//series 1 is the top bar
			FormatWFLabel(chart.ChartData.Series[1].Labels, LegendDataLabelPosition.Center, Color.White);
			
			//series 2 is blank filler
			chart.ChartData.Series[2].Format.Fill.FillType = FillType.NoFill;
			chart.ChartData.Series[2].Format.Line.FillFormat.FillType = FillType.NoFill;

			//series 3 is the number label
			chart.ChartData.Series[3].Format.Fill.FillType = FillType.NoFill;
			chart.ChartData.Series[3].Format.Line.FillFormat.FillType = FillType.NoFill;
			FormatWFLabel(chart.ChartData.Series[3].Labels, LegendDataLabelPosition.InsideEnd);

			//series 6 is the bottom bar
			FormatWFLabel(chart.ChartData.Series[6].Labels, LegendDataLabelPosition.Center, Color.White);

			chart.Axes.HorizontalAxis.IsVisible = false;
			chart.Axes.HorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.VerticalAxis.TextFormat.PortionFormat.FontHeight = 12;
			//series.Marker.Format.Line.FillFormat.FillType = FillType.Solid;

			}

		private void FormatWFLabel(IDataLabelCollection labels, LegendDataLabelPosition labelPos, Color? labelTextColor = null)
			{

			if (labelTextColor == null) labelTextColor = defaultColor;
			labels.DefaultDataLabelFormat.ShowValue = true;
			labels.DefaultDataLabelFormat.Position = labelPos;
			labels.DefaultDataLabelFormat.TextFormat.PortionFormat.FontHeight = 12;
			labels.DefaultDataLabelFormat.TextFormat.PortionFormat.FillFormat.FillType = FillType.Solid;
			labels.DefaultDataLabelFormat.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = (System.Drawing.Color)labelTextColor;
			labels.DefaultDataLabelFormat.IsNumberFormatLinkedToSource = false;
			labels.DefaultDataLabelFormat.NumberFormat = "#0.00";

			}

//		private void DoLineMarker(IChart chart, S.DataTable dataToAdd, IChartDataWorkbook fact, Int32 catCount, Int32 serCount, ChartType chartType, S.DataTable slideColors)
		private void DoLineMarker(ISlide slide, IChart chart, S.DataTable dataToAdd, IChartDataWorkbook fact, Int32 catCount, Int32 serCount, ChartType chartType, S.DataTable slideColors, String dataChartType, Int32 barGapWidth, Boolean addManualLegend, Boolean axis, Int32? SecondYAxisMax = null, Int32? SecondYAxisMajorUnit = null, Int32? SecondYAxisMin = null, String lineFormat = null)
		{
			//certain tables need some manipulation before we can process them
			//the manipulation can't be done in SQL Server because it needs to be dynamic, so we just get the data then modify it here, and pass it on to DoNonLineMarker

			S.DataTable lineMarkers = new S.DataTable("lineMarkers");
			lineMarkers.Columns.Add("cat", typeof(Double));

			DataView view = new DataView(dataToAdd);
			String colname = dataToAdd.Columns[0].ColumnName;
			S.DataTable distinctRows = view.ToTable(true, colname);

			foreach(S.DataRow row in distinctRows.Rows)
			{
				lineMarkers.Columns.Add(row.ItemArray[0].ToString(), typeof(Double));
			}

			colname = dataToAdd.Columns[1].ColumnName;
			distinctRows = view.ToTable(true, colname);

			foreach(S.DataRow row in distinctRows.Rows)
			{
				lineMarkers.Rows.Add(row.ItemArray[0]);
			}

			foreach(S.DataRow dataToAddRow in dataToAdd.Rows)
			{
				foreach(S.DataRow lineMarkerRow in lineMarkers.Rows)
				{
					if (lineMarkerRow.ItemArray[0].ToString() == dataToAddRow.ItemArray[1].ToString())
					{
						lineMarkerRow[dataToAddRow.ItemArray[0].ToString()] = dataToAddRow.ItemArray[2];
					}
				}
			}

			DoNonLineMarker(slide, chart, lineMarkers, fact, catCount, serCount, chartType, slideColors, dataChartType, barGapWidth, addManualLegend, axis, SecondYAxisMax, SecondYAxisMajorUnit, SecondYAxisMin, lineFormat);


			lineFormat = ce.CheckForNull<String>(lineFormat, "");
			String color;
			Int32 colornumber = 0;

			if (lineFormat != "")
			{
				foreach(IChartSeries series in chart.ChartData.Series)
				{
					color = slideColors.Rows[colornumber].ItemArray[0].ToString();
					FormatLine(series, lineFormat, color);
					colornumber++;
				}
			}

			//chart.DisplayBlanksAs = DisplayBlanksAsType.Gap;

		}

		private void DoNonLineMarker(ISlide slide, IChart chart, S.DataTable dataToAdd, IChartDataWorkbook fact, Int32 catCount, Int32 serCount, ChartType chartType, S.DataTable slideColors, String dataChartType, Int32 barGapWidth, Boolean addManualLegend, Boolean axis, Int32? SecondYAxisMax = null, Int32? SecondYAxisMajorUnit = null, Int32? SecondYAxisMin = null, String lineFormat = null)
		{

			//add a category to the workbook for each row in the DataTable (first column in the DataTable must be the category name)
			for (int rownum = 0; rownum < dataToAdd.Rows.Count; rownum++)
			{
				if (dataToAdd.Rows[rownum].ItemArray[0].GetType() == typeof(System.DateTime))
				{
					DateTime dt = (DateTime)dataToAdd.Rows[rownum].ItemArray[0];
					Int32 intDate = DateTimeToInt(dt);

					IChartDataCell cdc = fact.GetCell(wsIndex, rownum + catCount + 1, 0);
					cdc.PresetNumberFormat = 14;
					cdc.Value = intDate;
					chart.ChartData.Categories.Add(cdc);

					//if (testP2 == true)
					//{
					//	p2.AddCategory(rownum + catCount + 1, 0, intDate);
					//}
				}
				else
				{
					if (dataChartType == "Bar100")
					{
						String stringToadd = dataToAdd.Rows[rownum].ItemArray[0].ToString();

						if (stringToadd.IndexOf("(") > 0)
						{
							stringToadd = stringToadd.Replace("(", "\n(");
						}

						chart.ChartData.Categories.Add(fact.GetCell(wsIndex, rownum + catCount + 1, 0, stringToadd));
						//if (testP2 == true)
						//{
						//	p2.AddCategory(rownum + catCount + 1, 0, stringToadd);
						//}
					}
					else
					{
						chart.ChartData.Categories.Add(fact.GetCell(wsIndex, rownum + catCount + 1, 0, dataToAdd.Rows[rownum].ItemArray[0]));
						//if (testP2 == true)
						//{
						//	p2.AddCategory(rownum + catCount + 1, 0, dataToAdd.Rows[rownum].ItemArray[0]);
						//}
					}
				}
			}



			//add a series to the workbook and plug in the data for it
			for (int colnum = 1; colnum < dataToAdd.Columns.Count; colnum++) //skip the first column, it is the category names
			{
				IChartSeries series = chart.ChartData.Series.Add(fact.GetCell(wsIndex, 0, colnum + serCount, dataToAdd.Columns[colnum].ColumnName), chartType);
				//if (testP2 == true)
				//{
				//	p2.AddSeries(0, colnum + serCount, dataToAdd.Columns[colnum].ColumnName, chartType);
				//}

				for (int rownumber = 0; rownumber < dataToAdd.Rows.Count; rownumber++)
				{
					switch (dataChartType)
					{
						case "Line":
						case "LineMarker":
							if (dataToAdd.Rows[rownumber].ItemArray[colnum] != DBNull.Value)
							{
								series.DataPoints.AddDataPointForLineSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount, dataToAdd.Rows[rownumber].ItemArray[colnum]));
							}
							else
							{
								series.DataPoints.AddDataPointForLineSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount));
							}
							break;
						case "Area":
							series.DataPoints.AddDataPointForAreaSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount, dataToAdd.Rows[rownumber].ItemArray[colnum]));
							break;
						case "Bar":
						case "Bar100":
						case "BarCluster":
						case "BarHoriz":
							if (dataToAdd.Rows[rownumber].ItemArray[colnum] != DBNull.Value)
							{
								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount, dataToAdd.Rows[rownumber].ItemArray[colnum]));
								//if (testP2 == true)
								//{
								//	p2.AddSeriesPoint(dataChartType, rownumber + catCount + 1, colnum + serCount, Convert.ToDouble(dataToAdd.Rows[rownumber].ItemArray[colnum]));
								//}
							}
							else
							{
								series.DataPoints.AddDataPointForBarSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount));
							}
							break;
						case "Pie":
							series.DataPoints.AddDataPointForPieSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum, dataToAdd.Rows[rownumber].ItemArray[colnum]));
							break;
						case "Scatter":
							series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount, dataToAdd.Rows[rownumber].ItemArray[colnum]), fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount + 1, dataToAdd.Rows[rownumber].ItemArray[colnum + 1]));
							break;
						default:
							break;
					}


					////and for p2
					//switch (dataChartType)
					//	{
					//	case "Line":
					//	case "LineMarker":
					//	case "Bar":
					//	case "Bar100":
					//	case "BarCluster":
					//	case "BarHoriz":
					//	case "Area":
					//		if (dataToAdd.Rows[rownumber].ItemArray[colnum] != DBNull.Value)
					//			{
					//			//								series.DataPoints.AddDataPointForLineSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount, dataToAdd.Rows[rownumber].ItemArray[colnum]));
					//			p2.AddSeriesPoint(dataChartType, rownumber + catCount + 1, colnum + serCount, Convert.ToDouble(dataToAdd.Rows[rownumber].ItemArray[colnum]));
					//			}
					//		else
					//			{
					//			//series.DataPoints.AddDataPointForLineSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount));
					//			p2.AddSeriesPoint(dataChartType, rownumber + catCount + 1, colnum + serCount);
					//			}
					//		break;
					//	case "Pie":
					//		//series.DataPoints.AddDataPointForPieSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum, dataToAdd.Rows[rownumber].ItemArray[colnum]));
					//		p2.AddSeriesPoint(dataChartType, rownumber + catCount + 1, colnum, Convert.ToDouble(dataToAdd.Rows[rownumber].ItemArray[colnum]));
					//		break;
					//	case "Scatter":
					//		//series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount, dataToAdd.Rows[rownumber].ItemArray[colnum]), fact.GetCell(wsIndex, rownumber + catCount + 1, colnum + serCount + 1, dataToAdd.Rows[rownumber].ItemArray[colnum + 1]));
					//		p2.AddSeriesPoint(dataChartType, rownumber + catCount + 1, colnum + serCount, Convert.ToDouble(dataToAdd.Rows[rownumber].ItemArray[colnum]), Convert.ToDouble(dataToAdd.Rows[rownumber].ItemArray[colnum + 1]));
					//		break;
					//	default:
					//		break;
					//	}


				}

				foreach (IChartSeries barSeries in chart.ChartData.Series)
				{
					if (dataChartType == "BarCluster")
					{
						barSeries.ParentSeriesGroup.Overlap = 0;//otherwise the bars are hidden behind each other
					}
					else
					{
						barSeries.ParentSeriesGroup.Overlap = 100;//what does this do? dont know why it is here
					}
				}

				if (barGapWidth != 0) //0 is the null parameter here
				{
					foreach (IChartSeries barSeries in chart.ChartData.Series)
					{
						barSeries.ParentSeriesGroup.GapWidth = (ushort)barGapWidth;
					}
				}

				if (chartType == ChartType.ScatterWithStraightLines)
				{
					chart.Legend.Entries[chart.ChartData.Series.Count - 1].Hide = true;
				}

				Color color = defaultColor; //just setting this to default because otherwise there's an error below. it will be changed when needed.

				switch (chartType)
				{
					case ChartType.Pie:
						//a pie comes in as a single series of data. each slice needs to be colored separately
						for (int colornumber = 0; colornumber < dataToAdd.Rows.Count; colornumber++)
						{
							IChartDataPoint point = series.DataPoints[colornumber];
							point.Format.Fill.FillType = FillType.Solid;
							color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colornumber].ItemArray[0]);
							point.Format.Fill.SolidFillColor.Color = color;
							//and labels for the pie
							IDataLabel lbl = point.Label;
							lbl.DataLabelFormat.ShowValue = false;
							lbl.DataLabelFormat.ShowPercentage = true;
							lbl.DataLabelFormat.Position = LegendDataLabelPosition.OutsideEnd;
							lbl.DataLabelFormat.TextFormat.PortionFormat.LatinFont = new FontData("Tahoma");
							lbl.DataLabelFormat.TextFormat.PortionFormat.FontHeight = 14;

							//and a line around the edge
							point.Format.Line.FillFormat.FillType = FillType.Solid;
							point.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
							point.Format.Line.Width = 0.5;

							//and labels below
							if (addManualLegend)
							{
								AddLegendBelowChart(slide, dataToAdd.Rows[colornumber].ItemArray[0].ToString(), color, colornumber);
							}

						}
						//color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colnum - 1].ItemArray[0]);
						//color = defaultColor;
						break;
					default:
						try
						{
							color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colnum - 1].ItemArray[0]);
						}
						catch (IndexOutOfRangeException)
						{
							color = defaultColor;
						}

						series.Format.Fill.FillType = FillType.Solid;
						series.Format.Fill.SolidFillColor.Color = color;
						series.Format.Line.FillFormat.FillType = FillType.Solid;

						break;
				}

				switch (chartType)
				{
					case ChartType.ScatterWithStraightLines:
					case ChartType.Line:
						series.Format.Line.FillFormat.SolidFillColor.Color = color;

						if (addManualLegend)
						{
							AddLegendBelowChart(slide, dataToAdd.Columns[colnum].ColumnName, color, colnum);
						}

						break;
					case ChartType.Area:
						series.Format.Line.FillFormat.SolidFillColor.Color = color;
						series.Format.Fill.FillType = FillType.Solid;
						series.Format.Fill.SolidFillColor.Color = color;
						break;
					case ChartType.StackedColumn:
						series.Format.Line.FillFormat.SolidFillColor.Color = Color.Black;

						if (addManualLegend)
						{
							AddLegendBelowChart(slide, dataToAdd.Columns[colnum].ColumnName, color, colnum);
						}
						break;
					case ChartType.PercentsStackedBar:
					case ChartType.ClusteredColumn:
						if (addManualLegend)
						{
							AddLegendBelowChart(slide, dataToAdd.Columns[colnum].ColumnName, color, colnum);
						}

						break;
					case ChartType.StackedBar:
						if (dataChartType == "Bar100")
						{
							if (addManualLegend)
							{
								AddLegendBelowChart(slide, dataToAdd.Columns[colnum].ColumnName, color, colnum);
							}
						}
						break;
					default:
						//in this case we don't want to anything because we don't know what we should do
						break;
				}

				lineFormat = ce.CheckForNull<String>(lineFormat, "");

				if (lineFormat != "")
					FormatLine (series, lineFormat);

				if (axis == true)
				{
					series.PlotOnSecondAxis = true;
					//hasRightVerticalAxis = true;

					//set secondary axis as fixed only if the value in SlideCharts is populated
					if (SecondYAxisMax != 0 || SecondYAxisMajorUnit != 0 || SecondYAxisMin != 0)//if they're both zero we didnt get anything so don't do anything
					{
						if (SecondYAxisMax != null)
						{
							chart.Axes.SecondaryVerticalAxis.IsAutomaticMaxValue = false;
							chart.Axes.SecondaryVerticalAxis.MaxValue = (Double)SecondYAxisMax;
						}
						if (SecondYAxisMajorUnit != null)
						{
							chart.Axes.SecondaryVerticalAxis.IsAutomaticMajorUnit = false;
							chart.Axes.SecondaryVerticalAxis.MajorUnit = (Double)SecondYAxisMajorUnit;
						}
						if (SecondYAxisMin != null)
						{
							chart.Axes.SecondaryVerticalAxis.IsAutomaticMinValue = false;
							chart.Axes.SecondaryVerticalAxis.MinValue = (Double)SecondYAxisMin;
						}
					}

					chart.Axes.SecondaryVerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					chart.Axes.SecondaryVerticalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					chart.Axes.SecondaryHorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					chart.Axes.SecondaryHorizontalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
					chart.Axes.SecondaryHorizontalAxis.IsVisible = false;


					FontData fd3 = new FontData("Tahoma");

					IChartPortionFormat txtCat2 = chart.Axes.SecondaryVerticalAxis.TextFormat.PortionFormat;
					txtCat2.FillFormat.FillType = FillType.Solid;
					txtCat2.FillFormat.SolidFillColor.Color = defaultColor;
					txtCat2.LatinFont = fd3;
					chart.Axes.SecondaryVerticalAxis.TextFormat.PortionFormat.FontHeight = 16;

					//Axis line color
					chart.Axes.SecondaryVerticalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
					chart.Axes.SecondaryVerticalAxis.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;


					//not using the chart axis titles
					chart.Axes.SecondaryVerticalAxis.HasTitle = false;

					//TODO: but do need to add a label when required by the data

					////////////////color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colnum - 1].ItemArray[0]);

					////////////////series.Format.Fill.FillType = FillType.Solid;
					////////////////series.Format.Fill.SolidFillColor.Color = color;
					////////////////series.Format.Line.FillFormat.FillType = FillType.Solid;
					series.InvertIfNegative = false;

				}
			}

			//and for p2
			for (int colnum = 1; colnum < dataToAdd.Columns.Count; colnum++) //skip the first column, it is the category names
				{
				S.DataTable copydataToAdd = dataToAdd.Copy();
				copydataToAdd.Columns[colnum].SetOrdinal(0);
				while (copydataToAdd.Columns.Count > 1)
					{
					copydataToAdd.Columns.RemoveAt(copydataToAdd.Columns.Count - 1);
					}

				DataSeries series = new DataSeries();
				series.chartType = chartType;
				//series.dataColumn = dataToAdd.Columns[colnum];
				series.dataTable = copydataToAdd;
				series.barGapWidth = barGapWidth;
				try
					{
					series.color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[colnum - 1].ItemArray[0]);
					}
				catch (IndexOutOfRangeException)
					{
					series.color = defaultColor;
					}
				series.addManualLegend = addManualLegend;
				series.lineFormat = lineFormat;
				series.axis = axis;
				series.secondYAxisMax = SecondYAxisMax;
				series.secondYAxisMin = SecondYAxisMin;
				series.secondYAxisMajorUnit = SecondYAxisMajorUnit;

				//p2.AddSeries(series);

				}
		}

		//private void GetLineFormat(Sa.PresBuilder.AsposeConnector.LineFormat lineFormat, String strLineFormat)
		//{

		//	foreach (S.DataRow formatRow in slideLineFormatsTable.Rows)
		//	{

		//		if (formatRow["SlideLineFormatID"].ToString() == strLineFormat)
		//		{

		//			String lineColor = ce.CheckForNull<String>(formatRow["Color"], "");
		//			if (lineColor != "")
		//			{
		//				lineFormat.lineColor = System.Drawing.ColorTranslator.FromHtml("#" + lineColor);
		//			}
		//			Double lineWidth = ce.CheckForNull<Double>(formatRow["LineWidth"], 0);
		//			if (lineWidth != 0)
		//			{
		//				lineFormat.lineWidth = lineWidth;
		//			}
		//	//		Int32 lineDashStyle = ce.CheckForNull<Int32>(formatRow["LineDashStyle"], -1);
		//	//		Int32 lineBeginArrow = ce.CheckForNull<Int32>(formatRow["LineBeginArrow"], 0);
		//	//		Int32 lineEndArrow = ce.CheckForNull<Int32>(formatRow["LineEndArrow"], 0);
		//	//		Int32 lineMarkerType = ce.CheckForNull<Int32>(formatRow["MarkerType"], -1);
		//	//		Int32 lineMarkerSize = ce.CheckForNull<Int32>(formatRow["MarkerSize"], 0);
		//	//		Int32 lineMarkerLastOnly = ce.CheckForNull<Int32>(formatRow["MarkerLastOnly"], 0);

		//	//		Color colorForLine;

		//	//		if (forceColor == "") colorForLine = System.Drawing.ColorTranslator.FromHtml("#" + lineColor);
		//	//		else //this is an optional color we want to use rather than the default (so far only used for Trends)
		//	//			colorForLine = System.Drawing.ColorTranslator.FromHtml("#" + forceColor);

		//	//		if (lineColor != "")
		//	//		{
		//	//			series.Format.Line.FillFormat.FillType = FillType.Solid;
		//	//			series.Format.Line.FillFormat.SolidFillColor.Color = colorForLine;
		//	//		}
		//	//		series.Format.Line.Width = lineWidth;
		//	//		series.Format.Line.DashStyle = (LineDashStyle)lineDashStyle;

		//	//		if (lineBeginArrow != 0) series.Format.Line.BeginArrowheadStyle = LineArrowheadStyle.Open;

		//	//		if (lineEndArrow != 0) series.Format.Line.EndArrowheadStyle = LineArrowheadStyle.Open;

		//	//		if (lineMarkerType != -1)
		//	//		{
		//	//			series.Marker.Format.Fill.FillType = FillType.Solid;
		//	//			series.Marker.Format.Fill.SolidFillColor.Color = colorForLine;
		//	//			series.Marker.Format.Line.FillFormat.FillType = FillType.Solid;
		//	//			series.Marker.Format.Line.FillFormat.SolidFillColor.Color = colorForLine;
		//	//			series.Marker.Symbol = (MarkerStyleType)lineMarkerType;
		//	//			series.Marker.Size = lineMarkerSize;
		//	//		}

		//	//		if (lineMarkerLastOnly == 1)//really should be lineLabelLastOnly
		//	//		{
		//	//			IDataLabel label = series.DataPoints[series.DataPoints.Count() - 1].Label;

		//	//			label.TextFormat.PortionFormat.LatinFont = new FontData("Tahoma");
		//	//			label.TextFormat.PortionFormat.FontBold = NullableBool.True;
		//	//			label.TextFormat.PortionFormat.FontHeight = labelFontSize;//was 18
		//	//			label.DataLabelFormat.Position = LegendDataLabelPosition.Right;
		//	//			label.DataLabelFormat.ShowSeriesName = true;
		//	//			label.TextFormat.PortionFormat.FillFormat.FillType = FillType.Solid;
		//	//			label.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = colorForLine;
		//	//		}
		//		}
		//	}

		//}

		private void FormatLine(IChartSeries series, String lineFormat, String forceColor = "")
			{

			foreach (S.DataRow formatRow in slideLineFormatsTable.Rows)
				{

				if (formatRow["SlideLineFormatID"].ToString() == lineFormat)
					{
					String lineColor = ce.CheckForNull<String>(formatRow["Color"], "");
					Double lineWidth = ce.CheckForNull<Double>(formatRow["LineWidth"], 3);
					Int32 lineDashStyle = ce.CheckForNull<Int32>(formatRow["LineDashStyle"], -1);
					Int32 lineBeginArrow = ce.CheckForNull<Int32>(formatRow["LineBeginArrow"], 0);
					Int32 lineEndArrow = ce.CheckForNull<Int32>(formatRow["LineEndArrow"], 0);
					Int32 lineMarkerType = ce.CheckForNull<Int32>(formatRow["MarkerType"], -1);
					Int32 lineMarkerSize = ce.CheckForNull<Int32>(formatRow["MarkerSize"], 0);
					Int32 lineMarkerLastOnly = ce.CheckForNull<Int32>(formatRow["MarkerLastOnly"], 0);

					Color colorForLine;

					if (forceColor == "") colorForLine = System.Drawing.ColorTranslator.FromHtml("#" + lineColor);
					else //this is an optional color we want to use rather than the default (so far only used for Trends)
						colorForLine = System.Drawing.ColorTranslator.FromHtml("#" + forceColor);

					if (lineColor != "")
					{
						series.Format.Line.FillFormat.FillType = FillType.Solid;
						series.Format.Line.FillFormat.SolidFillColor.Color = colorForLine;
					}
					series.Format.Line.Width = lineWidth;
					series.Format.Line.DashStyle = (LineDashStyle)lineDashStyle;

					if (lineBeginArrow != 0) series.Format.Line.BeginArrowheadStyle = LineArrowheadStyle.Open;

					if (lineEndArrow != 0) series.Format.Line.EndArrowheadStyle = LineArrowheadStyle.Open;

					if (lineMarkerType != -1)
						{
						series.Marker.Format.Fill.FillType = FillType.Solid;
						series.Marker.Format.Fill.SolidFillColor.Color = colorForLine;
						series.Marker.Format.Line.FillFormat.FillType = FillType.Solid;
						series.Marker.Format.Line.FillFormat.SolidFillColor.Color = colorForLine;
						series.Marker.Symbol = (MarkerStyleType)lineMarkerType;
						series.Marker.Size = lineMarkerSize;
						}

						if (lineMarkerLastOnly == 1)//really should be lineLabelLastOnly
						{
							IDataLabel label = series.DataPoints[series.DataPoints.Count() - 1].Label;

							label.TextFormat.PortionFormat.LatinFont = new FontData("Tahoma");
							label.TextFormat.PortionFormat.FontBold = NullableBool.True;
							label.TextFormat.PortionFormat.FontHeight = labelFontSize;//was 18
							label.DataLabelFormat.Position = LegendDataLabelPosition.Right;
							label.DataLabelFormat.ShowSeriesName = true;
							label.TextFormat.PortionFormat.FillFormat.FillType = FillType.Solid;
							label.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = colorForLine;
						}
					}
				}
			}

		private void AddLegendBelowChart(ISlide slide, String legendName, Color legendColor, Int32 groupnumber)
			{
			//TODO: fix the next line. this is temporary to solve a short-term problem but it needs a longer term solution
			if (legendName != "Q12" & legendName != "Q23" & legendName != "Q34")
				{
				//add a box of the right color, and add the text next to it

				//TODO: how to know where to add the shape?

				//we are using the text "GroupShape" + number in the alt text of the groupshape to identify them, per Aspose site direction
				//we can use this to iterate through them and find all of them to place them with

				float left = 0;//starting positions
				float top = 444;

				//Iterating through all shapes inside the slide
				for (int i = 0; i < slide.Shapes.Count; i++)
					{
					//If the alternative text of the slide matches with the required one then
					if (slide.Shapes[i].AlternativeText.Contains("GroupShape"))
						{
						if (slide.Shapes[i].X + slide.Shapes[i].Width > left)
							left = slide.Shapes[i].X + slide.Shapes[i].Width;
						}
					}

				left += 6;//a little space between labels

				IShapeCollection slideShapes = slide.Shapes;
				IGroupShape groupShape = slideShapes.AddGroupShape();
				groupShape.AlternativeText = "GroupShape" + groupnumber;
				IAutoShape shp;
				shp = groupShape.Shapes.AddAutoShape(ShapeType.Rectangle, left, top, 9, 9);
				shp.FillFormat.FillType = FillType.Solid;
				shp.FillFormat.SolidFillColor.Color = legendColor;
				shp.ShapeStyle.LineColor.Color = Color.Black;
				shp.LineFormat.Width = 0.75;

				//how wide should the next shape be?
				Bitmap b = new Bitmap(720, 540);
				Graphics g = Graphics.FromImage(b);
				Font f1 = new Font("Tahoma", 10, FontStyle.Regular);
				//TODO: why do I have to do 10 here if the font is 14 tall? size doesn't calc right, I don't know why, it seems to work if the size measured here is 4 points less than the actual font (e.g. 14 to 10, 16 to 12)
				SizeF size1 = new SizeF(g.MeasureString(legendName, f1));

				shp = groupShape.Shapes.AddAutoShape(ShapeType.Rectangle, left + 12, top - 4, size1.Width, 16); // + 12 gives room for the box drawn above, -4 evens out to the box, 16 is a nice number
				shp.FillFormat.FillType = FillType.NoFill;
				shp.ShapeStyle.LineColor.Color = Color.Transparent;

				ITextFrame tf = ((IAutoShape)shp).TextFrame;
				IParagraph para = tf.Paragraphs[0];
				para.ParagraphFormat.Alignment = TextAlignment.Left;
				tf.TextFrameFormat.WrapText = NullableBool.False;
				tf.TextFrameFormat.MarginLeft = 0;
				SetPortion(para.Portions[0], "Tahoma", defaultColor, fontHeight: 14, text: legendName);

				//IPortion port = para.Portions[0];
				//port.PortionFormat.LatinFont = new FontData("Tahoma");
				//port.PortionFormat.FillFormat.FillType = FillType.Solid;
				//port.PortionFormat.FillFormat.SolidFillColor.Color = defaultColor;
				//port.Text = legendName;
				//port.PortionFormat.FontHeight = 14;




				//TODO: logic to move the boxes around, to following rows etc.

				//foreach (IGroupShape gs in slideShapes.



				}

			}

		private void FixLegendPosition(ISlide slide)
		{
			//we labeled all the legend shapes with "GroupShape" when we put them on the page, we now find those shapes and center them on the page
			Int32 shapeCount = 0;
			float shapeWidth = 0;
			float[] shapeSizes = new float[100];

			//Iterating through all shapes inside the slide
			for (int i = 0; i < slide.Shapes.Count; i++)
			{
				if (slide.Shapes[i].AlternativeText.Contains("GroupShape"))
				{
					shapeCount++;
					shapeWidth += slide.Shapes[i].Width;
					shapeSizes[shapeCount-1] = slide.Shapes[i].Width;
				}
			}

			shapeSizes = shapeSizes.Take(shapeCount).ToArray();

			if (shapeCount > 0)
			{
				if (shapeWidth < 576)
				{
					//center it by moving every shape right the calculated amount
					float moveRight = (720 - shapeWidth) / 2;
					for (int i = 0; i < slide.Shapes.Count; i++)
					{
						//If the alternative text of the slide matches with the required one then
						if (slide.Shapes[i].AlternativeText.Contains("GroupShape"))
							slide.Shapes[i].X += moveRight;
					}
				}
				else
				{
					//too much for one row, we need to split it to two
					Int32 shapeNum = 0;
					Int32[] shapeRow = new Int32[shapeCount];

					float shapeWidth1 = 0;
					float shapeWidth2 = 0;

					//this figures out which row the text should go on (half on the first row, half on the second, odd one out goes to the second row)
					for (int i = 0; i < slide.Shapes.Count; i++)
					{
						if (slide.Shapes[i].AlternativeText.Contains("GroupShape"))
						{
							shapeNum++;
							if (shapeNum <= shapeCount / 2)
							{
								shapeRow[shapeNum - 1] = 1;
								shapeWidth1 += slide.Shapes[i].Width;
							}
							else 
							{
								shapeRow[shapeNum - 1] = 2;
								shapeWidth2 += slide.Shapes[i].Width;
							}
						}
					}


					float moveRight1 = (720 - shapeWidth1) / 2;
					float moveRight2 = (720 - shapeWidth2) / 2;

					float boxGap = 8;
					shapeNum = 0;
					////adjust for the gap between the labels
					for (int i = 0; i < slide.Shapes.Count; i++)
					{
						if (slide.Shapes[i].AlternativeText.Contains("GroupShape"))
						{
							shapeNum ++;
							if (shapeRow[shapeNum - 1] == 1)
							{
								moveRight1 -= boxGap/2;
							}
							else
							{
								moveRight2 -= boxGap/2;
							}
						}
					}

					//readjust because the first item on each row shouldnt have moved
					moveRight1 += boxGap/2;
					moveRight2 += boxGap/2;

					//then we actually do the moving, pushing the labels to the right and moving them to the next row if necessary
					shapeNum = 0;
					for (int i = 0; i < slide.Shapes.Count; i++)
					{
						if (slide.Shapes[i].AlternativeText.Contains("GroupShape"))
						{
							shapeNum ++;
							if (shapeRow[shapeNum - 1] == 1)
							{
								slide.Shapes[i].X = moveRight1;
								moveRight1 += slide.Shapes[i].Width;
								moveRight1 += boxGap;
							}
							else
							{
								slide.Shapes[i].X = moveRight2;
								moveRight2 += slide.Shapes[i].Width;
								moveRight2 += boxGap; 
								slide.Shapes[i].Y += 20;
							}
						}
					}

				}

			}
		}

		private void MessWithTrendChart(IChart chart)
			{

			chart.HasLegend = false;

			foreach (IChartSeries series in chart.ChartData.Series)
				{
				series.Marker.Symbol = MarkerStyleType.Square;
				series.Marker.Format.Fill.FillType = FillType.Solid;
				series.Marker.Format.Fill.SolidFillColor.Color = series.Format.Line.FillFormat.SolidFillColor.Color;
				series.Marker.Size = 8;
				series.Marker.Format.Line.FillFormat.FillType = FillType.Solid;
				series.Marker.Format.Line.FillFormat.SolidFillColor.Color = series.Format.Line.FillFormat.SolidFillColor.Color;

				//series.DataPoints[5].Label.DataLabelFormat.ShowCategoryName = true;
				//IDataLabel label = series.DataPoints[5].Label;
				//label.TextFrameForOverriding.Text = series.Name.ToString();

				}

			}

		private void AdjustDistList(IChart chart)
			{
			SetHorizontalAxisMinMax(chart, 0, 100);
			//chart.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
			//chart.Axes.HorizontalAxis.MaxValue = 100;//because it's a distlist it's going to go to 100

			}

		private void AddDistPoints(ISlide slide, S.DataTable dataToAdd, String slideColorGroup)
			{
			int dist = 0;

			//skim through all the shapes, get the first chart
			//TODO: this needs to be fixed for multiple charts
			for (int i = 0; i < slide.Shapes.Count; i++)
				{
				if (slide.Shapes[i].GetType() == typeof(Aspose.Slides.Charts.Chart))
					{
					dist = i;
					}
				}

			IChart chart = (IChart)slide.Shapes[dist];

            Int32 catCount = chart.ChartData.Categories.Count;
            Int32 serCount = chart.ChartData.Series.Count;

			//TODO: row below may not be the first series, how do we check that?
            //we set the distSeries to the first series (so it doesnt break later)
            //then we check all the series and look for any with more than one record (because a series must have just one, right?)
            //and the last of these will be the distSeries we need
            //what about if you put other things on the chart, like a line somewhere? it will also return more than 1.
            IChartSeries distSeries = chart.ChartData.Series[0];
            Int32 numberOfDataPoints = 0;

            foreach (IChartSeries s in chart.ChartData.Series)
            {
                numberOfDataPoints = numberOfDataPoints + s.DataPoints.Count;
                if (s.DataPoints.Count > 1)
                {
                    distSeries = s;
                }
            }
            //if (distSeries == null)
            //{
            //    distSeries = chart.ChartData.Series[0];
            //}
            //IChartSeries distSeries = chart.ChartData.Series[0];//must be the first series, right? also the only series at this point
			IChartDataWorkbook ws = chart.ChartData.ChartDataWorkbook;
			//distSeries.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(0, 0, 0);
			for(int i = 0; i < dataToAdd.Rows.Count; i ++)
				{
				//Double valToFind = (Double)dataToAdd.Rows[i].ItemArray[1]; //column 0 is the name, column 1 is the value
				Double valToFind;
				Double.TryParse(dataToAdd.Rows[i].ItemArray[1].ToString(), out valToFind);

				Double xVal = GetDistX(distSeries, ws, valToFind); //find the X value

				//and plot it
				//Getting the chart data worksheet
				IChartDataWorkbook fact3 = chart.ChartData.ChartDataWorkbook;
                
                //put the title in the workbook
				IChartSeries newSeries = chart.ChartData.Series.Add(fact3.GetCell(0, 0, i + 2 + serCount, dataToAdd.Rows[i].ItemArray[0].ToString()), ChartType.ScatterWithMarkers);

                //IChartCellCollection coll = chart.ChartData.ChartDataWorkbook.GetCellCollection();
                


				//TODO: row below says 102, why? need to validate that value in case there are more than 100 points in the first series, or more than one series
				//newSeries.DataPoints.AddDataPointForScatterSeries(fact3.GetCell(0, 102 + i, 0, xVal), fact3.GetCell(0, 102 + i, i + 2 + serCount, valToFind));
                newSeries.DataPoints.AddDataPointForScatterSeries(fact3.GetCell(0, numberOfDataPoints + i + 1, 0, xVal), fact3.GetCell(0, numberOfDataPoints + i + 1, i + 2 + serCount, valToFind));
				newSeries.Marker.Symbol = MarkerStyleType.None;

				IDataLabel label = newSeries.DataPoints[0].Label;

				Color labelColor = defaultColor;
				if (slideColorGroup != "")
				{
					//create a temporary color table, copy the main color table, delete rows that don't match, and use it
					S.DataTable shortColorsTable = GetShortColorTable(slideColorsTable, slideColorGroup);
					S.DataRow colorRow = shortColorsTable.Rows[0];
					labelColor = System.Drawing.ColorTranslator.FromHtml("#" + colorRow["Color"].ToString());
				}
				FormatLabel(label, "Arial", NullableBool.True, labelFontSize, LegendDataLabelPosition.Center, true, labelColor);

				}

			}

		private void FormatLabel(IDataLabel label, String fontName, NullableBool fontBold, Int32 fontHeight, LegendDataLabelPosition labelPosition, Boolean showSeriesName, Color fontColor, FillType fillType = FillType.Solid)
		{
			label.TextFormat.PortionFormat.LatinFont = new FontData(fontName);
			label.TextFormat.PortionFormat.FontBold = fontBold;
			label.TextFormat.PortionFormat.FontHeight = fontHeight;
			label.DataLabelFormat.Position = labelPosition;
			label.DataLabelFormat.Position = labelPosition;
			label.DataLabelFormat.ShowSeriesName = showSeriesName;
			label.TextFormat.PortionFormat.FillFormat.FillType = fillType;
			label.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = fontColor;


		}

		private void AddStackArea(IChart chart, S.DataTable dataToAdd, S.DataTable slideColors)
			{
			IChartDataWorkbook fact3 = chart.ChartData.ChartDataWorkbook;

			//find the last used column and row of the data, and add below it
			Tuple<int, int> tup = GetLastUsedCell(chart.ChartData);

			int lastrow = tup.Item1;
			int lastcol = tup.Item2;

			Int32 stackedAreaCount = 0;
			foreach (IChartSeries s in chart.ChartData.Series)
				{
				if (s.Type == ChartType.StackedArea)
					stackedAreaCount++;
				}
			DateTime pDate = new DateTime(2000,1,1);

			IChartSeries newSeries = chart.ChartData.Series.Add(fact3.GetCell(0, 0, lastcol + 1, "Stack " + chart.ChartData.Series.Count().ToString()), ChartType.StackedArea);
			for (int i = 0; i < dataToAdd.Rows.Count; i++)
				{
				newSeries.DataPoints.AddDataPointForAreaSeries( fact3.GetCell(0, i + lastrow + 1, lastcol + 1, dataToAdd.Rows[i].ItemArray[1]));
				if (stackedAreaCount == 0)//never been added before, so we put in the field names
					{
					//DateTime wkgDate = Convert.ToDateTime("1/1/1900");
					//Double valtoadd = Convert.ToDouble(dataToAdd.Rows[i].ItemArray[0]);

					//wkgDate = wkgDate.AddDays(valtoadd);
					//fact3.GetCell(0, i + lastrow + 1, 0).PresetNumberFormat = 14;
					//chart.ChartData.Categories.Add(fact3.GetCell(0, i + lastrow + 1, 0, wkgDate.ToShortDateString()));
					chart.ChartData.Categories.Add(fact3.GetCell(0, i + lastrow + 1, 0, dataToAdd.Rows[i].ItemArray[0]));
					fact3.GetCell(0, i + lastrow + 1, 0).PresetNumberFormat = 14;
					//DateTime newDate = pDate.AddDays(Convert.ToDouble(dataToAdd.Rows[i].ItemArray[0]));
					//chart.ChartData.Categories.Add(fact3.GetCell(0, i + lastrow + 1, 0, newDate));
//					pDate.AddDays(dataToAdd.Rows[i].ItemArray[0]

					}

				//newSeries.DataPoints.AddDataPointForScatterSeries(fact3.GetCell(0, i + lastrow + 1, 0, dataToAdd.Rows[i].ItemArray[0]), fact3.GetCell(0, i + lastrow + 1, lastcol + 1, dataToAdd.Rows[i].ItemArray[1]));
				//newSeries.Marker.Symbol = MarkerStyleType.None;

				}
			newSeries.PlotOnSecondAxis = true;
			//hasRightVerticalAxis = true;
			//chart.Axes.SecondaryHorizontalAxis.
			//chart.Axes.SecondaryHorizontalAxis.BaseUnitScale = TimeUnitType.Days;

			//chart.Axes.SecondaryHorizontalAxis.BaseUnitScale = TimeUnitType.Months;

			if (slideColors.Rows[0].ItemArray[0].ToString() == "000000")
				{
				newSeries.Format.Fill.FillType = FillType.NoFill;
				}
			else
				{
				if (slideColors.Rows.Count == 1)
					{//one color means color the box this color
					Color color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[0].ItemArray[0]);
					newSeries.Format.Fill.FillType = FillType.Solid;
					newSeries.Format.Fill.SolidFillColor.Color = color;
					}
				else
					{//two colors means we're doing a gradient
					Color color1 = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[0].ItemArray[0]);
					Color color2 = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[1].ItemArray[0]);
					newSeries.Format.Fill.FillType = FillType.Gradient;
					newSeries.Format.Fill.GradientFormat.GradientShape = GradientShape.Linear;
					newSeries.Format.Fill.GradientFormat.GradientDirection = GradientDirection.FromCorner1;
					//newSeries.Format.Fill.GradientFormat.LinearGradientAngle = 135;
					newSeries.Format.Fill.GradientFormat.GradientStops.Add(0.5f, color1);
					newSeries.Format.Fill.GradientFormat.GradientStops.Add(1f, color2);


					}
				}
			

			chart.Axes.SecondaryHorizontalAxis.IsAutomaticMaxValue = false;
			chart.Axes.SecondaryHorizontalAxis.IsAutomaticMinValue = false;
			//chart.Axes.SecondaryHorizontalAxis.NumberFormat = "mmm-yyyy";
			chart.Axes.SecondaryHorizontalAxis.MinValue = 2;// chart.Axes.HorizontalAxis.MinValue;
			chart.Axes.SecondaryHorizontalAxis.MaxValue = 100;// chart.Axes.HorizontalAxis.MaxValue;
			chart.Axes.SecondaryHorizontalAxis.IsVisible = false;


			chart.Axes.SecondaryVerticalAxis.IsVisible = false;
			chart.Axes.SecondaryVerticalAxis.MinValue = chart.Axes.VerticalAxis.MinValue;
			chart.Axes.SecondaryVerticalAxis.MaxValue = chart.Axes.VerticalAxis.MaxValue;

			chart.Axes.SecondaryVerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.SecondaryHorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.SecondaryVerticalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.SecondaryHorizontalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;


			}

		private void AddScatterLine(IChart chart, S.DataTable dataToAdd, S.DataTable slideColors, String lineFormat)
			{

			IChartDataWorkbook fact3 = chart.ChartData.ChartDataWorkbook;

			Tuple<int, int> tup = GetLastUsedCell(chart.ChartData);

			int lastrow = tup.Item1;
			int lastcol = tup.Item2;

			IChartSeries newSeries = chart.ChartData.Series.Add(fact3.GetCell(0, 0, lastcol + 1, "Line " + chart.ChartData.Series.Count().ToString()), ChartType.ScatterWithMarkers);
			for (int i = 0; i < dataToAdd.Rows.Count; i++)
				{
				newSeries.DataPoints.AddDataPointForScatterSeries(fact3.GetCell(0, i + lastrow + 1, 0, dataToAdd.Rows[i].ItemArray[0]), fact3.GetCell(0, i + lastrow + 1, lastcol + 1, dataToAdd.Rows[i].ItemArray[1]));
				newSeries.Marker.Symbol = MarkerStyleType.None;
				}


			lineFormat = ce.CheckForNull<String>(lineFormat, "");

			if (lineFormat != "")
				FormatLine(newSeries, lineFormat);

			//newSeries.Marker.Symbol = MarkerStyleType.None;
			//newSeries.Format.Line.FillFormat.FillType = FillType.Solid;
			//newSeries.Format.Line.Width = 1.5;

			//Color color = System.Drawing.ColorTranslator.FromHtml("#" + slideColors.Rows[0].ItemArray[0]);
			
			//newSeries.Format.Line.FillFormat.SolidFillColor.Color = color;

			}

		private void AddScatterPoints(IChart chart, S.DataTable dataToAdd, Int32 labelFormatType = 0)
			{

			IChartDataWorkbook fact3 = chart.ChartData.ChartDataWorkbook;

			////find the last used column and row of the data, and add below it
			Tuple<int, int> tup = GetLastUsedCell(chart.ChartData);

			int lastrow = tup.Item1;
			int lastcol = tup.Item2;

			for (int i = 0; i < dataToAdd.Rows.Count; i++)
				{

				Double xVal = Convert.ToDouble(dataToAdd.Rows[i].ItemArray[1]);
				Double yVal = Convert.ToDouble(dataToAdd.Rows[i].ItemArray[2]);

				IChartSeries newSeries = chart.ChartData.Series.Add(fact3.GetCell(0, 0, lastcol + i + 1, dataToAdd.Rows[i].ItemArray[0].ToString()), ChartType.ScatterWithMarkers);

				newSeries.DataPoints.AddDataPointForScatterSeries(fact3.GetCell(0, lastrow + i + 1, 0, xVal), fact3.GetCell(0, lastrow + i + 1, lastcol + i + 1, yVal));
				newSeries.Marker.Symbol = MarkerStyleType.None;

				IDataLabel label = newSeries.DataPoints[0].Label;

				//TODO: this is very temporary, need to come up with a real solution to setting the formatting
				switch (labelFormatType)
					{
					case 6://made this one up just for the Heat Rate labels
						FormatLabel(label, "Tahoma",NullableBool.True, 10, LegendDataLabelPosition.Center, true, Color.FromArgb(77, 77, 77));
						break;
					case 7://made this one up just for the Analysis By Major Process labels
						FormatLabel(label, "Tahoma", NullableBool.False, labelFontSize, LegendDataLabelPosition.Center, true, Color.FromArgb(77, 77, 77));
						break;
					default:
						FormatLabel(label, "Arial", NullableBool.True, labelFontSize, LegendDataLabelPosition.Center, true, defaultColor);
						break;
					}
				}
			}

		private  Double GetDistX(IChartSeries series, IChartDataWorkbook workbook, Double y)
		{
			//Give me the range and the Y value and I will give you the X value

			Double returnValue;

            //String x = workbook.GetCell(0, 1, 0).Value.ToString();
            //Double minX = Convert.ToDouble(workbook.GetCell(0, 1, 0).Value); //0;//assuming they're not going to be below 0? probably dangerous
            //Double minY = Convert.ToDouble(workbook.GetCell(0, 1, 1).Value);
            //Double maxX = Convert.ToDouble(workbook.GetCell(0, series.DataPoints.Count, 0).Value);
            //Double maxY = Convert.ToDouble(workbook.GetCell(0, series.DataPoints.Count, 1).Value);
            Double minX = Convert.ToDouble(series.DataPoints[0].XValue.AsISingleCellChartValue.AsCell.Value);
            Double minY = Convert.ToDouble(series.DataPoints[0].YValue.AsISingleCellChartValue.AsCell.Value);
            Double maxX = Convert.ToDouble(series.DataPoints[series.DataPoints.Count - 1].XValue.AsISingleCellChartValue.AsCell.Value);
            Double maxY = Convert.ToDouble(series.DataPoints[series.DataPoints.Count - 1].YValue.AsISingleCellChartValue.AsCell.Value);
			Double cellX;
			Double cellY;

			if (minY > maxY) //this is a reverse order chart where higher = better, e.g. Thermal Efficiency
			{
				returnValue = GetDistXReverse(series, workbook, y);
			}

			else
			{
				if (y < minY)
					returnValue = minX;
				else
					if (y > maxY)
						returnValue = maxX;
					else
					{
						for (int i = 0; i < series.DataPoints.Count; i++)
						{
                            //cellX = Convert.ToDouble(workbook.GetCell(0, i + 1, 0).Value);
                            //cellY = Convert.ToDouble(workbook.GetCell(0, i + 1, 1).Value);
                            cellX = Convert.ToDouble(series.DataPoints[i].XValue.AsISingleCellChartValue.AsCell.Value);
                            cellY = Convert.ToDouble(series.DataPoints[i].YValue.AsISingleCellChartValue.AsCell.Value);

							if (cellY <= y)
							{
								minY = cellY;
								minX = cellX;
							}
						}

						for (int i = series.DataPoints.Count-1; i > 0; i--)
						{
                            //cellX = Convert.ToDouble(workbook.GetCell(0, i + 1, 0).Value);
                            //cellY = Convert.ToDouble(workbook.GetCell(0, i + 1, 1).Value);
                            cellX = Convert.ToDouble(series.DataPoints[i].XValue.AsISingleCellChartValue.AsCell.Value);
                            cellY = Convert.ToDouble(series.DataPoints[i].YValue.AsISingleCellChartValue.AsCell.Value);

							if (cellY >= y)
							{
								maxY = cellY;
								maxX = cellX;
							}
						}

						if (minY == maxY)//the rare case where there are multiple values in a distribution that match, this will catch them
						{
							returnValue = Math.Max(minX, maxX);
						}
						else
						{
							returnValue = minX + ((maxX - minX) * ((y - minY) / (maxY - minY)));//pct of diff
						}
					}
			}
			return returnValue;
		}

		private  Double GetDistXReverse(IChartSeries series, IChartDataWorkbook workbook, Double y)
			{
			//Give me the range and the Y value and I will give you the X value

			Double returnValue;

			//String x = workbook.GetCell(0, 1, 0).Value.ToString();
			Double minX = Convert.ToDouble(workbook.GetCell(0, 1, 0).Value); //0;//assuming they're not going to be below 0? probably dangerous
			Double minY = Convert.ToDouble(workbook.GetCell(0, 1, 1).Value);
			Double maxX = Convert.ToDouble(workbook.GetCell(0, series.DataPoints.Count, 0).Value);
			Double maxY = Convert.ToDouble(workbook.GetCell(0, series.DataPoints.Count, 1).Value);
			Double cellX;
			Double cellY;

			if (y > minY)
				returnValue = minX;
			else
				if (y < maxY)
					returnValue = maxX;
				else
				{
					for (int i = 0; i < series.DataPoints.Count; i++)
					{
						cellX = Convert.ToDouble(workbook.GetCell(0, i + 1, 0).Value);
						cellY = Convert.ToDouble(workbook.GetCell(0, i + 1, 1).Value);

						if (cellY >= y)
						{
							minY = cellY;
							minX = cellX;
						}
					}

					for (int i = series.DataPoints.Count; i > 0; i--)
					{
						cellX = Convert.ToDouble(workbook.GetCell(0, i + 1, 0).Value);
						cellY = Convert.ToDouble(workbook.GetCell(0, i + 1, 1).Value);

						if (cellY <= y)
						{
							maxY = cellY;
							maxX = cellX;
						}
					}

					if (minY == maxY)//the rare case where there are multiple values in a distribution that match, this will catch them
					{
						returnValue = Math.Min(minX, maxX);
					}
					else
					{
						returnValue = minX + ((maxX - minX) * ((y - minY) / (maxY - minY)));//pct of diff
					}
				}
			return returnValue;
		}

		#endregion

		#region PerfSumBars

		private void PerfSumAddBar(IChart chart, S.DataRow dataRow, S.DataTable slideColors)
			{
			//this adds the bar in a perfsum chart, a blank white filler on the left followed by the four quartile bars, with labels and the pacesetter
			//TODO: do we always use labels? do we always have a pacesetter? maybe need flags for those
			//TODO: figure out which parts of this should go into the database, if any
			//what other parts can be variable?

			//format the chart
			chart.Axes.VerticalAxis.IsVisible = false;
			chart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.HorizontalAxis.IsVisible = false;
			chart.Axes.HorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			SetHorizontalAxisMinMax(chart, -10, 110);//this allows us to have some space out to the sides after we plot the data from 0-100
			chart.PlotArea.Width = 1f;
			chart.PlotArea.X = 0f;
			chart.PlotArea.Height = 0.75f;
			chart.PlotArea.Y = 0f;

			//add each of the bars
			chart.ChartData.Categories.Add(chart.ChartData.ChartDataWorkbook.GetCell(0, 0, 1, "X Value"));
			
			PerfSumAddOneBar(chart, 1, Double.Parse(dataRow["Minimum"].ToString()), slideColors.Rows[0].ItemArray[0].ToString());
			PerfSumAddOneBar(chart, 2, Double.Parse(dataRow["Q12"].ToString()), slideColors.Rows[1].ItemArray[0].ToString());
			PerfSumAddOneBar(chart, 3, Double.Parse(dataRow["Q23"].ToString()), slideColors.Rows[2].ItemArray[0].ToString());
			//TODO: think about this. Q34 should never be null (in the db it should be set to match the max), but if they are equal should we skip it? what would happen if we do?
			if (dataRow["Q34"] != System.DBNull.Value) PerfSumAddOneBar(chart, 4, Double.Parse(dataRow["Q34"].ToString()), slideColors.Rows[3].ItemArray[0].ToString());
			PerfSumAddOneBar(chart, 5, Double.Parse(dataRow["Maximum"].ToString()), slideColors.Rows[4].ItemArray[0].ToString());

			//plot each of the text labels
			AddPerfSumSeriesWithPoint(chart, dataRow["Minimum"].ToString(), Double.Parse(dataRow["Minimum"].ToString()), LegendDataLabelPosition.Left);//bar left value
			AddPerfSumSeriesWithPoint(chart, dataRow["Maximum"].ToString(), Double.Parse(dataRow["Maximum"].ToString()), LegendDataLabelPosition.Right);//bar right value
			AddPerfSumSeriesWithPoint(chart, dataRow["Q12"].ToString(), Double.Parse(dataRow["Q12"].ToString()), LegendDataLabelPosition.Bottom);
			AddPerfSumSeriesWithPoint(chart, dataRow["Q23"].ToString(), Double.Parse(dataRow["Q23"].ToString()), LegendDataLabelPosition.Bottom);
			if (dataRow["Q34"] != System.DBNull.Value)
				{
				if (dataRow["Q34"].ToString() != dataRow["Q23"].ToString())
					AddPerfSumSeriesWithPoint(chart, dataRow["Q34"].ToString(), Double.Parse(dataRow["Q34"].ToString()), LegendDataLabelPosition.Bottom);
				else
					AddPerfSumSeriesWithPoint(chart, dataRow["Q34"].ToString(), Double.Parse(dataRow["Q34"].ToString()), LegendDataLabelPosition.Bottom,false,"",false);//we don't want to show both when they match, which is when there are only three quartiles
				}
			AddPerfSumSeriesWithPoint(chart, dataRow["Pacesetter"].ToString(), Double.Parse(dataRow["Pacesetter"].ToString()), LegendDataLabelPosition.Center, true);//pacesetter box

			}

		private void RecalcPerfSums(ISlide slide)
			{
			Int32 chartCount = 0;
			IChart chart;

			//an array to store all the data in (the PPT likes to think of them as strings, not numbers, so we get to do a bunch of conversions)
			Double[,] bars = new Double[10, 20];//10 rows because surely we won't have more than 10 perfsums on a slide, 5 cols for the 5 data points, plus 15 cols for calcs

			//skim through all the shapes, count the charts, and store their data
			for (int i = 0; i < slide.Shapes.Count; i++)
				{
				if (slide.Shapes[i].GetType() == typeof(Aspose.Slides.Charts.Chart))
					{
					chart = (IChart)slide.Shapes[i];

					for (int j = 0; j < 5; j++)
						{
						bars[chartCount, j] = Double.Parse(chart.ChartData.Series[j].Name.ToString());
						}
					//TODO: fix the value in the row below
					//it was set to 10, but it is not 10 if there are null rows
					//so i changed it to chart.ChartData.Series.Count-1 as a temporary fix, but this needs to be corrected
					bars[chartCount, 5] = Double.Parse(chart.ChartData.Series[10].Name.ToString());//the PS
					chartCount += 1;
					}
				}

			//get just the populated bar data
			Double[,] barsWithoutBlanks = new Double[chartCount, 20];

			for (int i = 0; i < chartCount; i++)
				{
				for (int j = 0; j < 20; j++)
					{
					barsWithoutBlanks[i, j] = bars[i, j];
					}
				}

			//calculate the new position numbers for the bars
			bars = CalcPerfSums(barsWithoutBlanks);

			//reset the charts by pushing the new position numbers into them
			if (chartCount > 0)
				{
				Int32 chartNum = 0;
				for (int i = 0; i < slide.Shapes.Count; i++)
					{
					if (slide.Shapes[i].GetType() == typeof(Aspose.Slides.Charts.Chart))
						{
						IChart chart2 = (IChart)slide.Shapes[i];

						chart2.ChartData.ChartDataWorkbook.GetCell(0, 1, 1, bars[chartNum, 14]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 2, 1, bars[chartNum, 15] - bars[chartNum, 14]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 3, 1, bars[chartNum, 16] - bars[chartNum, 15]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 4, 1, bars[chartNum, 17] - bars[chartNum, 16]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 5, 1, bars[chartNum, 18] - bars[chartNum, 17]);

						chart2.ChartData.ChartDataWorkbook.GetCell(0, 6, 0, bars[chartNum, 14]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 7, 0, bars[chartNum, 18]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 11, 0, bars[chartNum, 19]);

						Double[] x = new Double[3];
						string[] y = new string[3];
						x[0] = bars[chartNum, 15];//passing values
						x[1] = bars[chartNum, 16];
						x[2] = bars[chartNum, 17];
						//y[0] = bars[chartNum, 1].ToString();//passing names
						//y[1] = bars[chartNum, 2].ToString();
						//y[2] = bars[chartNum, 3].ToString();
						if (bars[chartNum,1] > 2000)
							y[0] = String.Format("{0:n0}", bars[chartNum, 1]);
						else
							y[0] = String.Format("{0:n2}", bars[chartNum, 1]);
						
						if (bars[chartNum, 2] > 2000)
							y[1] = String.Format("{0:n0}", bars[chartNum, 2]);
						else
							y[1] = String.Format("{0:n2}", bars[chartNum, 2]);
						
						if (bars[chartNum, 3] > 2000)
							y[2] = String.Format("{0:n0}", bars[chartNum, 3]);
						else
							y[2] = String.Format("{0:n2}", bars[chartNum, 3]);

						x = CheckLabelDist(x, y);

						chart2.ChartData.ChartDataWorkbook.GetCell(0, 8, 0, x[0]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 9, 0, x[1]);
						chart2.ChartData.ChartDataWorkbook.GetCell(0, 10, 0, x[2]);

						//at this point we have updated the bar positions, the bar labels, and the pacesetter position
						//now we need to update the unit labels

						Int32 p = 12;//note that row 12 = col 8, you'll see that math below
						//we're going to go down the rows in the spreadsheet, starting at row 12 which is the first non-bar/label/ps cell
						//in each we're going to look and see if there is data (if it's not blank) and if there is, we're going to adjust the value
						while (chart2.ChartData.ChartDataWorkbook.GetCell(0, p, 0).Value != null)
							{
							//get the original data value from the column name (not the label name)
							Double unitValue = Double.Parse(chart2.ChartData.ChartDataWorkbook.GetCell(0, 0, p - 4).Value.ToString());
							unitValue = ((bars[chartNum, 18] - bars[chartNum, 14]) * (unitValue - bars[chartNum, 0]) / (bars[chartNum, 4] - bars[chartNum, 0])) + bars[chartNum, 14];
							chart2.ChartData.ChartDataWorkbook.GetCell(0, p, 0, unitValue);
							p++;
							}

						Spread(chart2, 12, 8, true);

						chartNum++;
						}
					}
				}

			}

		private void AddPerfSumSeriesWithPoint(IChart chart, String seriesName, Double seriesX, LegendDataLabelPosition labelPos, Boolean showPSBox = false, String originalValue = "", Boolean showLabel = true)
			{
			//adds a single label to a perfsumbar
			//TODO: several hardcoded items in here, including font name and size, label color etc which should be made variable
			//TODO: also sets the secondary axis every time, might be able to clean that up by setting it when the initial perfsum is built
			//IChartData cd = chart.ChartData;
			IChartDataWorkbook fact = chart.ChartData.ChartDataWorkbook;
			IChartSeries series;

			Double seriesY;
			Int32 fontHeight;
			String fontName = "Tahoma";
			NullableBool fontBold = NullableBool.False;

			if (labelPos == LegendDataLabelPosition.Bottom)//the quartile labels below the chart
				{
				seriesY = 0.8;
				fontHeight = 9;
				}
			else
				{//the minimum and maximum labels at each end of the chart
				seriesY = 1;
				if (originalValue == "")
					fontHeight = 12;
				else
					{
					fontHeight = labelFontSize;
					fontName = "Arial";
					fontBold = NullableBool.True;
					}
				}

			//find the first blank row and column
			Int32 seriesRow = 1;
			Int32 seriesCol = 1;
			while (fact.GetCell(0, seriesRow, 0).Value != null) seriesRow += 1;
			while (fact.GetCell(0, 0, seriesCol).Value != null) seriesCol += 1;

			if (originalValue == "")
				{
				Double seriesVal = Double.Parse(seriesName.Trim());//the following few lines format the number for the label, with the simple rule that if <2000 it will format like 0.00, if >2000 it will format like 2,000

				String barText;
				if (seriesVal > 2000)
					barText = String.Format("{0:n0}", seriesVal);
				else
					barText = String.Format("{0:n2}", seriesVal);

				series = chart.ChartData.Series.Add(fact.GetCell(0, 0, seriesCol, barText), ChartType.ScatterWithMarkers);
				}
			else
				{
				series = chart.ChartData.Series.Add(fact.GetCell(0, 0, seriesCol, originalValue.Trim()), ChartType.ScatterWithMarkers);
				}
			series.PlotOnSecondAxis = true;


			chart.Axes.SecondaryHorizontalAxis.IsAutomaticMaxValue = false;
			chart.Axes.SecondaryHorizontalAxis.IsAutomaticMinValue = false;
			chart.Axes.SecondaryHorizontalAxis.MinValue = -10;
			chart.Axes.SecondaryHorizontalAxis.MaxValue = 110;
			chart.Axes.SecondaryHorizontalAxis.IsVisible = false;
			chart.Axes.SecondaryVerticalAxis.IsVisible = false;
			chart.Axes.SecondaryVerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.SecondaryHorizontalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.SecondaryVerticalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;
			chart.Axes.SecondaryHorizontalAxis.MinorGridLinesFormat.Line.FillFormat.FillType = FillType.NoFill;

			series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, seriesRow, 0, seriesX), fact.GetCell(0, seriesRow, seriesCol, seriesY));

			if (showPSBox == false)
				{
				series.Marker.Symbol = MarkerStyleType.None;
				IDataLabel lbl = series.DataPoints[0].Label;
				if (originalValue == "")
					{
						FillType fillType = FillType.Solid;
						if (showLabel == false)
							fillType = FillType.NoFill;
						FormatLabel(lbl, fontName, fontBold, fontHeight, labelPos, true, defaultColor, fillType);
					}
				else
					{
					lbl.DataLabelFormat.ShowSeriesName = false;
					lbl.DataLabelFormat.Position = labelPos;
					lbl.TextFrameForOverriding.Text = seriesName.Trim();
					if (showLabel == false)
						lbl.TextFrameForOverriding.Paragraphs[0].Portions[0].PortionFormat.FillFormat.FillType = FillType.NoFill;
					else
						SetPortion(lbl.TextFrameForOverriding.Paragraphs[0].Portions[0], fontName, defaultColor, fontBold, fontHeight);
					}
				}
			else
				{//formatting the pacesetter box
				series.Marker.Symbol = MarkerStyleType.Square;
				series.Marker.Format.Fill.FillType = FillType.Solid;
				series.Marker.Format.Fill.SolidFillColor.Color = Color.White;
				series.Marker.Format.Line.FillFormat.FillType = FillType.Solid;
				series.Marker.Format.Line.FillFormat.SolidFillColor.Color = defaultColor;
				series.Marker.Size = 8;
				}

			}

		private  Double[,] CalcPerfSums(Double[,] bars)
			{

			//okay, stealing all this from the PerfSum calculator Excel file, just for ease
			//TODO: may redo this once done (haha) to get it more efficient

			Double minVal = 0;
			Double maxVal = 0;
			//cols 0-5 are the values from the db: min, Q12, Q23, Q34, max, ps
			//col 6 is range, col 7 is midpoint, col 8-13 are calced positions
			for (int n = 0; n < bars.GetUpperBound(0) + 1; n++)
				{
				bars[n, 6] = bars[n, 4] - bars[n, 0];

				bars[n, 7] = bars[n, 2];
				if (bars[n, 7] == 0) //need to check this, could be null?
					bars[n, 7] = bars[n, 1];

				for (int o = 8; o <= 13; o++)
					bars[n, o] = (bars[n, (o - 8)] - bars[n, 7]) / bars[n, 6];

				for (int o = 8; o <= 12; o++)
					{
					if (bars[n, o] < minVal) { minVal = bars[n, o]; }
					if (bars[n, o] > maxVal) { maxVal = bars[n, o]; }
					}
				}

			Double maxLength = maxVal - minVal;
			Double centerPosition = (1 - maxVal / maxLength) * 100;

			//cols 14-19 are the final positions for the bars
			for (int n = 0; n < bars.GetUpperBound(0) + 1; n++)
				{
				for (int o = 14; o <= 19; o++)
					bars[n, o] = bars[n, (o - 6)] / maxLength * 100 + centerPosition;
				}

			return bars;
			}

		private void PerfSumAddOneBar(IChart chart, Int32 rownum, Double barValue, String barColor)
			{
			//adds one segment of the bar to a perfsum
			IChartData cd = chart.ChartData;
			IChartDataWorkbook fact = chart.ChartData.ChartDataWorkbook;
			IChartSeries series;
			String barText = barValue.ToString();

			series = cd.Series.Add(fact.GetCell(0, rownum, 0, barText), chart.Type);//series name is the original value, so we can use it later when recalculating
			series.DataPoints.AddDataPointForBarSeries(fact.GetCell(0, rownum, 1, barValue));
			series.Format.Fill.FillType = FillType.Solid;
			series.Format.Fill.SolidFillColor.Color = System.Drawing.ColorTranslator.FromHtml("#" + barColor);
			//and add the border
			if (rownum == 1)//first bar is the spacer, which has no color and should have no line
				{
				series.Format.Line.FillFormat.FillType = FillType.NoFill;
				}
			else
				{
				series.Format.Line.FillFormat.FillType = FillType.Solid;
				series.Format.Line.FillFormat.SolidFillColor.Color = Color.Black;
				}

			series.ParentSeriesGroup.Overlap = 100;
			}

		private  Boolean LabelOverlap(ITextFrame label1, ITextFrame label2, Double label1X, Double label1Y, Double label2X, Double label2Y, Bitmap b)
			{
			Boolean overlap = false;

			Graphics g = Graphics.FromImage(b);

			IPortion p1 = label1.Paragraphs[0].Portions[0];
			IPortion por2 = label2.Paragraphs[0].Portions[0];
			Font f1 = new Font(p1.PortionFormat.LatinFont.FontName, p1.PortionFormat.FontHeight, FontStyle.Regular);
			Font f2 = new Font(por2.PortionFormat.LatinFont.FontName, por2.PortionFormat.FontHeight, FontStyle.Regular);
			SizeF size1 = new SizeF(g.MeasureString(label1.Text, f1));
			SizeF size2 = new SizeF(g.MeasureString(label2.Text, f2));
			float width1 = size1.Width - 14.4f;//padding 2/10 of inch (0.1 left and right)
			float height1 = size1.Height - 14.4f;//vertical padding 1/10 of inch (0.05 bot and top)
			float width2 = size2.Width - 14.4f;
			float height2 = size2.Height - 14.4f;

			Double mult = b.Width / 120;//need to figure out what the 120 should be, it is 120 because the chart goes from -10 to 110 but should be variable
			Double heightmult = b.Height / 2;//need to figure out what the 2 should be, it is 2 because the chart goes from 0 to 2 but should be variable

			Double x1left = (label1X * mult) - width1 / 2;
			Double x1right = (label1X * mult) + width1 / 2;
			Double x1bot = (label1Y * heightmult) - height1 / 2;
			Double x1top = (label1Y * heightmult) + height1 / 2;


			Double x2left = (label2X * mult) - width2 / 2;
			Double x2right = (label2X * mult) + width2 / 2;
			Double x2bot = (label2Y * heightmult) - height2 / 2;
			Double x2top = (label2Y * heightmult) + height2 / 2;

			if (x2left < x1right && x2right >= x1left)
				{
				if (x2bot < x1top && x2top >= x1bot)
					{
					overlap = true;
					}
				}

			return overlap;
			}

		private void Spread(IChart chart, Int32 startRow, Int32 startCol, Boolean upDown)
			{
			//if upDown = true then we're going to move them up and down, if false then left and right
			//the assumption with startRow and startCol is that anything from here down/right is something that needs to move, and anything above/left is a fixed position (like a perfsumbar)

			System.Diagnostics.Debug.Print("Start Spread");
			//Boolean isCollision = true;
			Int32 endRow = startRow;
			while (chart.ChartData.ChartDataWorkbook.GetCell(0, endRow, 0).Value != null)
				{
				endRow++;
				}
			endRow--;//adjust for the last row

			Bitmap b = new Bitmap(612, 60);
			//while (isCollision == true)
			//	{
			//	//fix any collisions we find
			for (int n = startRow; n < endRow; n++)
				{

				ITextFrame frame1 = chart.ChartData.Series[n - 1].DataPoints[0].Label.TextFrameForOverriding;
				ITextFrame frame2 = chart.ChartData.Series[n].DataPoints[0].Label.TextFrameForOverriding;

				Double frame1X = Double.Parse(chart.ChartData.Series[n - 1].DataPoints[0].XValue.Data.ToString());
				Double frame1Y = Double.Parse(chart.ChartData.Series[n - 1].DataPoints[0].YValue.Data.ToString());
				Double frame2X = Double.Parse(chart.ChartData.Series[n].DataPoints[0].XValue.Data.ToString());
				Double frame2Y = Double.Parse(chart.ChartData.Series[n].DataPoints[0].YValue.Data.ToString());

				if (LabelOverlap(frame1, frame2, frame1X, frame1Y, frame2X, frame2Y, b) == true)
					{
					System.Diagnostics.Debug.Print(chart.ChartData.Series[n - 1].DataPoints[0].Label.TextFrameForOverriding.Text + " collides with " + chart.ChartData.Series[n].DataPoints[0].Label.TextFrameForOverriding.Text);

					if (upDown == true)
						{
						Boolean isCollision = true;

						while (isCollision)
							{
							//frame1Y = Double.Parse(chart.ChartData.Series[n - 1].DataPoints[0].YValue.Data.ToString());
							frame1Y = Double.Parse(chart.ChartData.ChartDataWorkbook.GetCell(0, n, n - (startRow - startCol)).Value.ToString());
							frame1Y -= 0.01;
							frame2Y = Double.Parse(chart.ChartData.ChartDataWorkbook.GetCell(0, n + 1, n + 1 - (startRow - startCol)).Value.ToString());
							frame2Y += 0.01;
							if (frame1Y < 0.75)
								{
								frame1Y = 0.75;
								frame2Y += 0.01;
								}

							chart.ChartData.ChartDataWorkbook.GetCell(0, n, n - (startRow - startCol), frame1Y);//60 because the plot area is 60 points tall
							chart.ChartData.ChartDataWorkbook.GetCell(0, n + 1, n + 1 - (startRow - startCol), frame2Y);
							isCollision = LabelOverlap(frame1, frame2, frame1X, frame1Y, frame2X, frame2Y, b);
							}
						}
					}
				}

			////check for any leftover collisions
			//isCollision = true;
			//}

			}

		private  Double[] CheckLabelDist(Double[] orig, string[] names)
			{

			Double[,] newVals = new Double[3, 2];
			Int32 diffmin = 6;
			newVals[0, 0] = orig[0];
			newVals[1, 0] = orig[1];
			newVals[2, 0] = orig[2];

			Double[] widths = new Double[3];

			//set font, size & style
			Font f = new Font("Tahoma", 9, FontStyle.Regular);
			//create a bmp / graphic to use MeasureString on
			Bitmap b = new Bitmap(720, 540);
			Graphics g = Graphics.FromImage(b);
			//measure the string
			SizeF sizeOfString = new SizeF();

			sizeOfString = g.MeasureString(names[0], f);
			widths[0] = sizeOfString.Width;
			sizeOfString = g.MeasureString(names[1], f);
			widths[1] = sizeOfString.Width;
			sizeOfString = g.MeasureString(names[2], f);
			widths[2] = sizeOfString.Width;

			Double mult = 606.0 / 120.0;//504 because the chart is 504 pixels wide, 120 because the chart goes from -10 to 110

			newVals[0, 1] = newVals[0, 0] * mult;
			newVals[1, 1] = newVals[1, 0] * mult;
			newVals[2, 1] = newVals[2, 0] * mult;

			Double diff12 = (newVals[1, 1] - (widths[1] / 2) + 7.2) - (newVals[0, 1] + (widths[0] / 2) - 7.2);
			Double diff23 = (newVals[2, 1] - (widths[2] / 2) + 7.2) - (newVals[1, 1] + (widths[1] / 2) - 7.2);

			while (diff12 < diffmin || diff23 < diffmin)
				{
				if (diff12 < diffmin)
					{
					newVals[0, 0] -= 0.1;
					}
				if (diff23 < diffmin)
					{
					newVals[2, 0] += 0.1;
					}

				newVals[0, 1] = newVals[0, 0] * mult;
				newVals[1, 1] = newVals[1, 0] * mult;
				newVals[2, 1] = newVals[2, 0] * mult;

				diff12 = (newVals[1, 1] - (widths[1] / 2) + 7.2) - (newVals[0, 1] + (widths[0] / 2) - 7.2);//7.2 because the standard label internal margin is 0.1" left/right
				diff23 = (newVals[2, 1] - (widths[2] / 2) + 7.2) - (newVals[1, 1] + (widths[1] / 2) - 7.2);
				}

			Double[] outVals = new Double[3];
			outVals[0] = newVals[0, 0];
			outVals[1] = newVals[1, 0];
			outVals[2] = newVals[2, 0];

			return outVals;

			}

		#endregion

		private void stringDiagnostic(Int32 fontHeight, string stringToMeasure, Double seriesX)
		{
			//set font, size & style
			Font f = new Font("Tahoma", fontHeight, FontStyle.Regular);

			//create a bmp / graphic to use MeasureString on
			Bitmap b = new Bitmap(720, 540);
			Graphics g = Graphics.FromImage(b);

			//measure the string
			SizeF sizeOfString = new SizeF();
			sizeOfString = g.MeasureString(stringToMeasure, f);

		}

		private void MakeLabels(String inputString, IChartData cd, IChartDataWorkbook fact)
			{

			String[] splitted = inputString.Split(' ');
			int n = 6;

			IChartSeries series = cd.Series.Add(fact.GetCell(wsIndex, n, 0, inputString), ChartType.ScatterWithMarkers);
			series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(wsIndex, n, 2, 50), fact.GetCell(wsIndex, n, 1, 0.75));
			IDataLabel lbl = series.DataPoints[0].Label;
			//lbl.DataLabelFormat.ShowSeriesName = true;
			lbl.TextFrameForOverriding.Text = series.Name.ToString();
			}

		#region Annotations

		private void AddAnnotations(ISlide slide, S.DataRow annotation)
		{

            String text = fieldReplace(annotation["AnnotationText"].ToString());
			float left = (float)annotation["Left"];
			float top = (float)annotation["Top"];
			float width = (float)annotation["Width"];
			float height = (float)annotation["Height"];
			int fontHeight = (int)annotation["FontHeight"];
			Boolean wrap = Convert.IsDBNull(annotation["Wrap"]) ? false : Convert.ToBoolean(annotation["Wrap"]);
			Boolean backColor = Convert.IsDBNull(annotation["BackColor"]) ? false : Convert.ToBoolean(annotation["BackColor"]);
			int alignment = (int)annotation["Alignment"];
			TextAlignment align;

			switch (alignment)
			{
				case 1:
					align = TextAlignment.Left;
					break;
				case 2:
					align = TextAlignment.Center;
					break;
				case 3:
					align = TextAlignment.Right;
					break;
				default:
					align = TextAlignment.Left;
					break;
			}

			Color color = Convert.IsDBNull(annotation["Color"]) ? defaultColor : System.Drawing.ColorTranslator.FromHtml("#" + annotation["Color"].ToString()); ;

			//this needs to go into the database as much as possible
			IAutoShape shp;
			shp = slide.Shapes.AddAutoShape(ShapeType.Rectangle, left, top, width, height);
			SetAnnotation(shp, text, fontHeight, color, wrap, align, backColor);
		}

		private void SetAnnotation(IAutoShape shp, String text, Int32 fontHeight, Color color, Boolean wrap, TextAlignment align, Boolean backColor)
			{
			//NullableBool? backColor = NullableBool.False, 
			//if (color == null) { color = defaultColor; }; //can't set the color in the optional field, so found this way to do it on StackOverflow

			if (backColor == false)
				{
				shp.FillFormat.FillType = FillType.NoFill;
				shp.ShapeStyle.LineColor.Color = Color.Transparent;
				}
			else
				{
				shp.FillFormat.FillType = FillType.Solid;
				shp.FillFormat.SolidFillColor.Color = Color.FromArgb(234, 234, 234);
				shp.ShapeStyle.LineColor.Color = Color.Transparent;
				}

			ITextFrame tf = ((IAutoShape)shp).TextFrame;
			IParagraph para = tf.Paragraphs[0];
			para.ParagraphFormat.Alignment = align;
			tf.TextFrameFormat.WrapText = wrap ? NullableBool.True : NullableBool.False;//how to convert from Bool to NullableBool? 

			SetPortion(para.Portions[0], "Tahoma", color, fontHeight: fontHeight, text: text);
			
			}

		#endregion

		#region Functions

		public  int DateTimeToInt(DateTime theDate)
			{
			//converts a datetime to an integer, which is used in certain charts to get the x axis to float properly (because Aspose doesnt support it)
			return (int)(theDate.Date - new DateTime(1900, 1, 1)).TotalDays + 2;
			}
				
		private void SetHorizontalAxisMinMax(IChart chart, double minValue, double maxValue)
			{
			chart.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
			chart.Axes.HorizontalAxis.MaxValue = maxValue;
			chart.Axes.HorizontalAxis.IsAutomaticMinValue = false;
			chart.Axes.HorizontalAxis.MinValue = minValue;
			}

		private void AddVerticalAxisLabelLeft(ISlide slide, String yAxisTitle)
			{
			AddVerticalAxisLabel(slide, yAxisTitle, 45, 105);
			}

		private void AddVerticalAxisLabelRight(ISlide slide, String yAxisTitle)
			{
			AddVerticalAxisLabel(slide, yAxisTitle, 629, 105);
			}

		private void AddVerticalAxisLabel(ISlide slide, String yAxisTitle, Int32 chartX, Int32 chartY, Int32 width = 30, Int32 height = 274)
			{
			//width and height are standard values for a standard sized chart
			IAutoShape ashpVATitle = slide.Shapes.AddAutoShape(ShapeType.Rectangle, chartX, chartY, width, height);
			ashpVATitle.FillFormat.FillType = FillType.Solid;
			ashpVATitle.FillFormat.SolidFillColor.Color = Color.Transparent;
			ashpVATitle.ShapeStyle.LineColor.Color = Color.Transparent;
			ashpVATitle.AddTextFrame(" ");
			ITextFrame txtFrame = ashpVATitle.TextFrame;
			txtFrame.TextFrameFormat.TextVerticalType = TextVerticalType.Vertical270;
			IParagraph para = txtFrame.Paragraphs[0];
			//IPortion portion = para.Portions[0];
			Int32 fontHeight = 16;
			if (height < 274) fontHeight = 15;//small boxes
			if (yAxisTitle.Length > 35) fontHeight = 15;//long titles
			SetPortion(para.Portions[0], "Tahoma", defaultColor, NullableBool.False, fontHeight, yAxisTitle);
			}

		private void AddHorizontalAxisLabel(ISlide slide, String yAxisTitle)
			{
			IAutoShape ashpVATitle = slide.Shapes.AddAutoShape(ShapeType.Rectangle, 233, 404, 254, 30);
			ashpVATitle.FillFormat.FillType = FillType.Solid;
			ashpVATitle.FillFormat.SolidFillColor.Color = Color.Transparent;
			ashpVATitle.ShapeStyle.LineColor.Color = Color.Transparent;
			ashpVATitle.AddTextFrame(" ");
			ITextFrame txtFrame = ashpVATitle.TextFrame;
			IParagraph para = txtFrame.Paragraphs[0];
			//IPortion portion = para.Portions[0];
			SetPortion(para.Portions[0], "Tahoma", defaultColor, NullableBool.False, 16, yAxisTitle);
			}

		private void SetPortion(IPortion portion, String fontName = "", Color? color = null, NullableBool fontBold = NullableBool.NotDefined, float fontHeight = 0, String text = "")
			{
			//set portion values depending on what is passed to the function
			if (fontName != "")
				{
				FontData font = new FontData(fontName);
				portion.PortionFormat.LatinFont = font;
				}

			if (color != null)
				{
				portion.PortionFormat.FillFormat.FillType = FillType.Solid;
				portion.PortionFormat.FillFormat.SolidFillColor.Color = (System.Drawing.Color)color;
				}

			if (fontBold != NullableBool.NotDefined)
				portion.PortionFormat.FontBold = fontBold;
	
			if (fontHeight != 0)
				portion.PortionFormat.FontHeight = fontHeight;
			
			if (text != "")
				portion.Text = text;

			}

		//private  Double GetUSorMetricValue(String valueToConvert)
		//	{
		//	//sometimes we need either a US or a Metric value
		//	//it isn't possible to switch between them in the database, we need to put one value in
		//	//so I decided to put the value in both ways, divided by a #, e.g. "16000#17"
		//	//and the code will split it so that it will be 16000 for US units and 17 for Metric units (this is heat rate in Btu or MJ)
		//	String inputValue = (valueToConvert == "") ? "0" : valueToConvert;
		//	Double returnValue;
		//	int index = inputValue.IndexOf("#");
		//	if (index == -1)
		//		{
		//		returnValue = Convert.ToDouble(inputValue);
		//		}
		//	else
		//		{
		//		if (Metric)
		//			{
		//			returnValue = Convert.ToDouble(inputValue.Substring(index + 1));
		//			}
		//		else
		//			{
		//			returnValue = Convert.ToDouble(inputValue.Substring(0, index));
		//			}
		//		}

		//	return returnValue;

		//	}

		private Tuple<int, int> GetLastUsedCell(IChartData chartData)
		{
			//given a chartData block, find and return the maximum row and column numbers used in that block
			int lastrow = 0;
			int lastcol = 0;

			foreach (IChartSeries s in chartData.Series)
				{
				foreach (ChartDataPoint cdp in s.DataPoints)
					{
					if (cdp.Value.AsCell != null)
						{
						if (cdp.Value.AsCell.Row > lastrow)
							lastrow = cdp.Value.AsCell.Row;
						if (cdp.Value.AsCell.Column > lastcol)
							lastcol = cdp.Value.AsCell.Column;
						}
					if (cdp.XValue.AsCell != null)
						{
						if (cdp.XValue.AsCell.Row > lastrow)
							lastrow = cdp.XValue.AsCell.Row;
						if (cdp.XValue.AsCell.Column > lastcol)
							lastcol = cdp.XValue.AsCell.Column;
						if (cdp.YValue.AsCell.Row > lastrow)
							lastrow = cdp.YValue.AsCell.Row;
						if (cdp.YValue.AsCell.Column > lastcol)
							lastcol = cdp.YValue.AsCell.Column;
						}
					}
				}
			return Tuple.Create(lastrow, lastcol);
		}

		private  ChartType GetChartType(String dataChartType)
		{
			//TODO: add other chart types as we need them or know them
			ChartType chartType = new ChartType();

			switch (dataChartType)
				{
				case "Area":
					chartType = ChartType.Area;
					break;
				case "StackArea":
					chartType = ChartType.StackedArea;
					break;
				case "Bar100":
				case "BarHoriz":
					chartType = ChartType.StackedBar;
					break;
				case "BarCluster":
					chartType = ChartType.ClusteredColumn;
					break;
				case "Line":
					chartType = ChartType.Line;
					break;
				case "LineMarker":
					chartType = ChartType.LineWithMarkers;
					break;
				case "Pie":
					chartType = ChartType.Pie;
					break;
				case "Scatter":
					chartType = ChartType.ScatterWithStraightLines;
					break;
				case "Bar":
				default:
					chartType = ChartType.StackedColumn;
					break;
				}

			return chartType;
		}

        private String fieldReplace(String input)
        {

            input = input.Replace("@CurrencyCode", CurrencyToUse);
            input = input.Replace("@Option1", Option1);
            input = input.Replace("@Option2", Option2);
            input = input.Replace("@Option3", Option3);
            input = input.Replace("@Option4", Option4);
            input = input.Replace("@Option5", Option5);

            if (Metric == true)
            {
                input = input.Replace("@Btu", "MJ");
                input = input.Replace("@MBtu", "GJ");
            }
            else
            {
                input = input.Replace("@Btu", "Btu");
                input = input.Replace("@MBtu", "MBtu");
            }

            return input;
        }

		#endregion

		#region testing

		public  void ChartTest()
			{
			//this is some test code for a problem i was having, keeping it here just in case i need to test something else
			Presentation pres = new Presentation();
			ISlide sld = pres.Slides[0];
			IChart chart = sld.Shapes.AddChart(ChartType.ScatterWithMarkers, 100, 100, 400, 400);
			IChartDataWorkbook fact = chart.ChartData.ChartDataWorkbook;

			chart.ChartData.Series.Clear();
			chart.ChartData.Categories.Clear();

			chart.ChartData.Series.Add(fact.GetCell(0, 0, 1, "Series 1"), chart.Type);

			IChartSeries series = chart.ChartData.Series[0];

			series.DataPoints.AddDataPointForScatterSeries(fact.GetCell(0, 1, 0, 5), fact.GetCell(0,1,1,5));

			chart.ChartData.Series[0].Labels.DefaultDataLabelFormat.ShowValue = true;
			chart.ChartData.Series[0].Labels.DefaultDataLabelFormat.Position = LegendDataLabelPosition.Center;

			pres.Save(@"C:\Development\AsposeChart.pptx", SaveFormat.Pptx);
			}


		#endregion

		#region OpenXML fixes to Aspose
		//this is for certain items that dont work in the Aspose library, so after the pres is built we go back in with OpenXML to fix them

		//public  void FixCategoryDate(DOXP.PresentationDocument doc, Int32 slideNumber)
		//	{
		//	//given a presentation and a slide number
		//	//convert the category axis to a date axis
		//	//with appropriate fixes to make values work

		//	DOXP.PresentationPart pr = doc.PresentationPart;

		//	//Get the relationship id of the slide we want to change
		//	DOXPr.SlideIdList s = pr.Presentation.SlideIdList;
		//	DOXPr.SlideId nslideid = (DOXPr.SlideId)s.ElementAt(slideNumber - 1);
		//	string slidRelId = nslideid.RelationshipId;

		//	foreach (DOXP.SlidePart slidePart1 in pr.SlideParts)
		//		{
		//		string thisSlideno = pr.GetIdOfPart(slidePart1);
		//		if (thisSlideno == slidRelId)
		//			{
		//			DOXP.ChartPart chartPart1 = slidePart1.ChartParts.FirstOrDefault();

		//			foreach (DOX.OpenXmlElement x in chartPart1.ChartSpace.ChildElements)
		//				{
		//				if (x.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.Chart))//find the chart
		//					{
		//					DOXC.Chart c = (DOXC.Chart)x;
		//					DOXC.PlotArea p = c.PlotArea;
		//					DOXC.CategoryAxis ca;
		//					DOXC.DateAxis dateAxis1 = new DOXC.DateAxis();//create a date axis and copy the category axis values to it (most of them are the same, some get changes or additions)
		//					foreach (DOX.OpenXmlElement y in p.ChildElements)
		//						if (y.GetType() == typeof(DOXC.CategoryAxis))
		//							{
		//							ca = (DOXC.CategoryAxis)y;

		//							DOXC.AxisId caaxisid = ca.AxisId;
		//							DOXC.Scaling scale = ca.Scaling;
		//							DOXC.Delete del = ca.Delete;
		//							DOXC.AxisPosition ap = ca.AxisPosition;
		//							DOXC.NumberingFormat nf = ca.NumberingFormat;
		//							nf.SourceLinked = false;
		//							nf.FormatCode = "mm/dd/yy;@";
		//							DOXC.MajorTickMark mt = ca.MajorTickMark;
		//							DOXC.MinorTickMark mit = ca.MinorTickMark;
		//							DOXC.TickLabelPosition tl = ca.TickLabelPosition;
		//							DOXC.ChartShapeProperties csp = ca.ChartShapeProperties;
		//							DOXC.TextProperties tp = ca.TextProperties;
		//							DOXA.BodyProperties bp = tp.BodyProperties;
		//							bp.Rotation = -5400000;
		//							bp.Vertical = DOXA.TextVerticalValues.Horizontal;
		//							foreach (DOX.OpenXmlElement e in tp.ChildElements)
		//								{
		//								if (e.GetType() == typeof(DOXA.Paragraph))
		//									{
		//									DOXA.Paragraph para = (DOXA.Paragraph)e;
		//									DOXA.ParagraphProperties paraprop = para.ParagraphProperties;
		//									foreach (DOX.OpenXmlElement f in paraprop.ChildElements)
		//										{
		//										if (f.GetType() == typeof(DOXA.DefaultRunProperties))
		//											{
		//											DOXA.DefaultRunProperties drp = (DOXA.DefaultRunProperties)f;
		//											drp.FontSize = 1400;
		//											}
		//										}
		//									}
		//								}

		//							DOXC.CrossingAxis cax = ca.CrossingAxis;

		//							//and a few new items
		//							DOXC.Crosses crosses1 = new DOXC.Crosses() { Val = DOXC.CrossesValues.AutoZero };
		//							DOXC.AutoLabeled autoLabeled1 = new DOXC.AutoLabeled() { Val = false };
		//							DOXC.LabelOffset labelOffset1 = new DOXC.LabelOffset() { Val = (DOX.UInt16Value)100U };
		//							DOXC.BaseTimeUnit baseTimeUnit1 = new DOXC.BaseTimeUnit() { Val = DOXC.TimeUnitValues.Days };
		//							DOXC.MajorUnit majorUnit1 = new DOXC.MajorUnit() { Val = 1D };
		//							DOXC.MajorTimeUnit majorTimeUnit1 = new DOXC.MajorTimeUnit() { Val = DOXC.TimeUnitValues.Months };

		//							//add them all to the date axis
		//							dateAxis1.AppendChild(caaxisid.CloneNode(true));
		//							dateAxis1.AppendChild(scale.CloneNode(true));
		//							dateAxis1.AppendChild(del.CloneNode(true));
		//							dateAxis1.AppendChild(ap.CloneNode(true));
		//							dateAxis1.AppendChild(nf.CloneNode(true));
		//							dateAxis1.AppendChild(mt.CloneNode(true));
		//							dateAxis1.AppendChild(mit.CloneNode(true));
		//							if (tl != null)
		//								dateAxis1.AppendChild(tl.CloneNode(true));
		//							dateAxis1.AppendChild(csp.CloneNode(true));
		//							dateAxis1.AppendChild(tp.CloneNode(true));
		//							dateAxis1.AppendChild(cax.CloneNode(true));
		//							dateAxis1.Append(crosses1);
		//							dateAxis1.Append(autoLabeled1);
		//							dateAxis1.Append(labelOffset1);
		//							dateAxis1.Append(baseTimeUnit1);
		//							dateAxis1.Append(majorUnit1);
		//							dateAxis1.Append(majorTimeUnit1);

		//							//remove the category axis
		//							ca.Remove();
		//							}
		//					//and add the date axis to the plot area
		//					p.Append(dateAxis1);

		//					}
		//				}
		//			pr.Presentation.Save();

		//			}
		//		}
		//	}

		//public  void FixCategoryDateVersion2(DOXP.PresentationDocument doc, Int32 slideNumber)
		//	{
		//	//given a presentation and a slide number
		//	//convert the category axis to a date axis
		//	//with appropriate fixes to make values work

		//	DOXP.PresentationPart pr = doc.PresentationPart;

		//	//Get the relationship id of the slide we want to change
		//	DOXPr.SlideIdList s = pr.Presentation.SlideIdList;
		//	DOXPr.SlideId nslideid = (DOXPr.SlideId)s.ElementAt(slideNumber - 1);
		//	string slidRelId = nslideid.RelationshipId;

		//	foreach (DOXP.SlidePart slidePart1 in pr.SlideParts)
		//		{
		//		string thisSlideno = pr.GetIdOfPart(slidePart1);
		//		if (thisSlideno == slidRelId)
		//			{
		//			DOXP.ChartPart chartPart1 = slidePart1.ChartParts.FirstOrDefault();

		//			foreach (DOX.OpenXmlElement x in chartPart1.ChartSpace.ChildElements)
		//				{
		//				if (x.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.Chart))//find the chart
		//					{
		//					DOXC.Chart c = (DOXC.Chart)x;
		//					DOXC.PlotArea p = c.PlotArea;
		//					DOXC.CategoryAxis ca;
		//					DOXC.DateAxis dateAxis1 = new DOXC.DateAxis();//create a date axis and copy the category axis values to it (most of them are the same, some get changes or additions)
		//					foreach (DOX.OpenXmlElement y in p.ChildElements)
		//						if (y.GetType() == typeof(DOXC.CategoryAxis))
		//							{
		//							ca = (DOXC.CategoryAxis)y;

		//							DOXC.AxisId caaxisid = ca.AxisId;
		//							DOXC.Scaling scale = ca.Scaling;
		//							scale.MaxAxisValue = new DOXC.MaxAxisValue() { Val = 109D };
		//							scale.MinAxisValue = new DOXC.MinAxisValue() { Val = 11D };
		//							DOXC.Delete del = ca.Delete;
		//							DOXC.AxisPosition ap = ca.AxisPosition;
		//							DOXC.NumberingFormat nf = ca.NumberingFormat;
		//							DOXC.MajorTickMark mt = ca.MajorTickMark;
		//							DOXC.MinorTickMark mit = ca.MinorTickMark;
		//							DOXC.TickLabelPosition tl = ca.TickLabelPosition;
		//							DOXC.CrossingAxis cax = ca.CrossingAxis;

		//							DOXC.Crosses crosses1 = new DOXC.Crosses() { Val = DOXC.CrossesValues.Maximum };
		//							DOXC.AutoLabeled autoLabeled1 = new DOXC.AutoLabeled() { Val = false };
		//							DOXC.LabelOffset labelOffset1 = new DOXC.LabelOffset() { Val = (DOX.UInt16Value)100U };
		//							DOXC.BaseTimeUnit baseTimeUnit1 = new DOXC.BaseTimeUnit() { Val = DOXC.TimeUnitValues.Days };


		//							//DOXC.ChartShapeProperties csp = ca.ChartShapeProperties;
		//							//DOXC.TextProperties tp = ca.TextProperties;
		//							//DOXA.BodyProperties bp = tp.BodyProperties;
		//							//bp.Rotation = -5400000;
		//							//bp.Vertical = DOXA.TextVerticalValues.Horizontal;
		//							//foreach (DOX.OpenXmlElement e in tp.ChildElements)
		//							//	{
		//							//	if (e.GetType() == typeof(DOXA.Paragraph))
		//							//		{
		//							//		DOXA.Paragraph para = (DOXA.Paragraph)e;
		//							//		DOXA.ParagraphProperties paraprop = para.ParagraphProperties;
		//							//		foreach (DOX.OpenXmlElement f in paraprop.ChildElements)
		//							//			{
		//							//			if (f.GetType() == typeof(DOXA.DefaultRunProperties))
		//							//				{
		//							//				DOXA.DefaultRunProperties drp = (DOXA.DefaultRunProperties)f;
		//							//				drp.FontSize = 1400;
		//							//				}
		//							//			}
		//							//		}
		//							//	}


		//							//DOXC.MajorUnit majorUnit1 = new DOXC.MajorUnit() { Val = 1D };
		//							//DOXC.MajorTimeUnit majorTimeUnit1 = new DOXC.MajorTimeUnit() { Val = DOXC.TimeUnitValues.Months };

		//							//add them all to the date axis
		//							dateAxis1.AppendChild(caaxisid.CloneNode(true));
		//							dateAxis1.AppendChild(scale.CloneNode(true));
		//							dateAxis1.AppendChild(del.CloneNode(true));
		//							dateAxis1.AppendChild(ap.CloneNode(true));
		//							dateAxis1.AppendChild(nf.CloneNode(true));
		//							dateAxis1.AppendChild(mt.CloneNode(true));
		//							dateAxis1.AppendChild(mit.CloneNode(true));
		//							if (tl != null)
		//								dateAxis1.AppendChild(tl.CloneNode(true));
		//							//dateAxis1.AppendChild(csp.CloneNode(true));
		//							//dateAxis1.AppendChild(tp.CloneNode(true));
		//							dateAxis1.AppendChild(cax.CloneNode(true));
		//							dateAxis1.Append(crosses1);
		//							dateAxis1.Append(autoLabeled1);
		//							dateAxis1.Append(labelOffset1);
		//							dateAxis1.Append(baseTimeUnit1);
		//							//dateAxis1.Append(majorUnit1);
		//							//dateAxis1.Append(majorTimeUnit1);

		//							//remove the category axis
		//							ca.Remove();
		//							}
		//					//and add the date axis to the plot area
		//					p.Append(dateAxis1);

		//					}
		//				}
		//			pr.Presentation.Save();

		//			}
		//		}
		//	}

		 private void testslidepos(DOXP.PresentationDocument doc)
			{

			DOXP.PresentationPart pr = doc.PresentationPart;

			//Get the relationship id of the slide we want to change
			DOXPr.SlideIdList s = pr.Presentation.SlideIdList;
			DOXPr.SlideId nslideid = (DOXPr.SlideId)s.ElementAt(slideNumber - 1);
			string slidRelId = nslideid.RelationshipId;

			foreach (DOXP.SlidePart slidePart1 in pr.SlideParts)
				{
					DOXPr.Slide slide1 = slidePart1.Slide;
					DOXPr.CommonSlideData commonSlideData1 = slide1.GetFirstChild<DOXPr.CommonSlideData>();
					DOXPr.ShapeTree shapeTree1 = commonSlideData1.GetFirstChild<DOXPr.ShapeTree>();
					DOXPr.GraphicFrame graphicFrame1 = shapeTree1.GetFirstChild<DOXPr.GraphicFrame>();

					if (graphicFrame1 != null)
						{
						//NonVisualGraphicFrameProperties nonVisualGraphicFrameProperties1 = graphicFrame1.GetFirstChild<NonVisualGraphicFrameProperties>();
						DOXPr.Transform transform1 = graphicFrame1.GetFirstChild<DOXPr.Transform>();

						//ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties1 = nonVisualGraphicFrameProperties1.GetFirstChild<ApplicationNonVisualDrawingProperties>();

						//ApplicationNonVisualDrawingPropertiesExtensionList applicationNonVisualDrawingPropertiesExtensionList1 = new ApplicationNonVisualDrawingPropertiesExtensionList();

						//ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension1 = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };

						//P14.ModificationId modificationId1 = new P14.ModificationId() { Val = (UInt32Value)349955619U };
						//modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");

						//applicationNonVisualDrawingPropertiesExtension1.Append(modificationId1);

						//applicationNonVisualDrawingPropertiesExtensionList1.Append(applicationNonVisualDrawingPropertiesExtension1);
						//applicationNonVisualDrawingProperties1.Append(applicationNonVisualDrawingPropertiesExtensionList1);

						DOXA.Offset offset1 = transform1.GetFirstChild<DOXA.Offset>();
						//A.Extents extents1 = transform1.GetFirstChild<A.Extents>();
						//offset1.X = 799813L;
						//offset1.Y = 1147138L;
						System.Diagnostics.Debug.Print(offset1.X);
						System.Diagnostics.Debug.Print(offset1.Y);
						//offset1.X = 799813L;
						//offset1.Y = 1147138L;
						//extents1.Cy = 4613744L;
						}
				}

			}

		 //private void AdjustLayoutTarget(DOXP.PresentationDocument doc, Int32 slideNumber)
		 //   {

		 //   DOXP.PresentationPart pr = doc.PresentationPart;

		 //   //Get the relationship id of the slide we want to change
		 //   DOXPr.SlideIdList s = pr.Presentation.SlideIdList;
		 //   DOXPr.SlideId nslideid = (DOXPr.SlideId)s.ElementAt(slideNumber - 1);
		 //   string slidRelId = nslideid.RelationshipId;

		 //   foreach (DOXP.SlidePart slidePart1 in pr.SlideParts)
		 //	   {
		 //	   string thisSlideno = pr.GetIdOfPart(slidePart1);
		 //	   if (thisSlideno == slidRelId)
		 //		   {
		 //		   DOXP.ChartPart chartPart1 = slidePart1.ChartParts.FirstOrDefault();

		 //		   if (chartPart1 != null)
		 //			   {
		 //			   foreach (DOX.OpenXmlElement x in chartPart1.ChartSpace.ChildElements)
		 //				   {
		 //				   if (x.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.Chart))//find the chart
		 //					   {
		 //					   DOXC.Chart c = (DOXC.Chart)x;
		 //					   DOXC.PlotArea p = c.PlotArea;

		 //					   DOXC.Layout l = p.Layout;
		 //					   DOXC.ManualLayout ml = l.ManualLayout;
		 //					   DOXC.LayoutTarget lt = ml.LayoutTarget;

		 //					   if (lt == null)
		 //						   {
		 //						   lt = new DOXC.LayoutTarget() { Val = DOXC.LayoutTargetValues.Inner };
		 //						   ml.LayoutTarget = lt;

		 //						   //set values for top and bottom of each chart
		 //						   DOXC.Top top1 = new DOXC.Top() { Val = -0.1D };
		 //						   DOXC.Height height1 = new DOXC.Height() { Val = 0.7D };//.8D

		 //						   Double leftYAxisMax = 0;
		 //						   Double rightYAxisMax = 0;
		 //						   DOXC.BarDirection bd = null;

		 //						   foreach (DOX.OpenXmlElement el in p.ChildElements)
		 //							   {
		 //							   if (el.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.ValueAxis))
		 //								   {
		 //								   DOXC.ValueAxis va = (DOXC.ValueAxis)el;
		 //								   if (va.AxisPosition.Val == DocumentFormat.OpenXml.Drawing.Charts.AxisPositionValues.Left)
		 //									   {
		 //									   if (va.Scaling.MaxAxisValue != null)
		 //										   leftYAxisMax = va.Scaling.MaxAxisValue.Val;
		 //									   }
		 //								   if (va.AxisPosition.Val == DocumentFormat.OpenXml.Drawing.Charts.AxisPositionValues.Right)
		 //									   {
		 //									   if (va.Scaling.MaxAxisValue != null)
		 //										   rightYAxisMax = va.Scaling.MaxAxisValue.Val;
		 //									   }
		 //								   }
		 //							   if (el.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.BarChart))
		 //								   {
		 //								   DOXC.BarChart bc = (DOXC.BarChart)el;
		 //								   bd = bc.BarDirection;
		 //								   }
		 //							   }

		 //						   //variable values for left and right of each chart
		 //						   DOXC.Left left1 = new DOXC.Left();
		 //						   DOXC.Width width1 = new DOXC.Width();

		 //						   if (leftYAxisMax >= 0 && leftYAxisMax < 1000)
		 //							   {
		 //							   if (rightYAxisMax == 0)
		 //								   {
		 //								   left1.Val = 0.036D;
		 //								   width1.Val = 0.817D;
		 //								   }
		 //							   else
		 //								   {
		 //								   left1.Val = 0.008D;
		 //								   width1.Val = 0.760D;
		 //								   }
		 //							   }

		 //						   if (leftYAxisMax >= 1000 && leftYAxisMax < 100000)
		 //							   {
		 //							   if (rightYAxisMax == 0)
		 //								   {
		 //								   left1.Val = 0.052D;
		 //								   width1.Val = 0.785D;
		 //								   }
		 //							   else
		 //								   {
		 //								   left1.Val = 0.024D;
		 //								   width1.Val = 0.728D;
		 //								   }
		 //							   }

		 //						   if (leftYAxisMax == 0 && rightYAxisMax == 0 && bd != null && bd.Val == DOXC.BarDirectionValues.Bar)
		 //							   {
		 //							   left1.Val = 0.043;
		 //							   width1.Val = 0.627D;
		 //							   }

		 //						   ml.Left = left1;
		 //						   ml.Width = width1;
		 //						   ml.Top = top1;
		 //						   ml.Height = height1;

		 //						   }
		 //					   //DOXC.CategoryAxis ca;
		 //					   //DOXC.DateAxis dateAxis1 = new DOXC.DateAxis();//create a date axis and copy the category axis values to it (most of them are the same, some get changes or additions)
		 //					   //foreach (DOX.OpenXmlElement y in p.ChildElements)
		 //					   //if (y.GetType() == typeof(DOXC.CategoryAxis))
		 //					   //	{
		 //					   //	ca = (DOXC.CategoryAxis)y;

		 //					   //	DOXC.AxisId caaxisid = ca.AxisId;
		 //					   //	DOXC.Scaling scale = ca.Scaling;
		 //					   //	scale.MaxAxisValue = new DOXC.MaxAxisValue() { Val = 109D };
		 //					   //	scale.MinAxisValue = new DOXC.MinAxisValue() { Val = 11D };
		 //					   //	DOXC.Delete del = ca.Delete;
		 //					   //	DOXC.AxisPosition ap = ca.AxisPosition;
		 //					   //	DOXC.NumberingFormat nf = ca.NumberingFormat;
		 //					   //	DOXC.MajorTickMark mt = ca.MajorTickMark;
		 //					   //	DOXC.MinorTickMark mit = ca.MinorTickMark;
		 //					   //	DOXC.TickLabelPosition tl = ca.TickLabelPosition;
		 //					   //	DOXC.CrossingAxis cax = ca.CrossingAxis;

		 //					   //	DOXC.Crosses crosses1 = new DOXC.Crosses() { Val = DOXC.CrossesValues.Maximum };
		 //					   //	DOXC.AutoLabeled autoLabeled1 = new DOXC.AutoLabeled() { Val = false };
		 //					   //	DOXC.LabelOffset labelOffset1 = new DOXC.LabelOffset() { Val = (DOX.UInt16Value)100U };
		 //					   //	DOXC.BaseTimeUnit baseTimeUnit1 = new DOXC.BaseTimeUnit() { Val = DOXC.TimeUnitValues.Days };


		 //					   //	//DOXC.ChartShapeProperties csp = ca.ChartShapeProperties;
		 //					   //	//DOXC.TextProperties tp = ca.TextProperties;
		 //					   //	//DOXA.BodyProperties bp = tp.BodyProperties;
		 //					   //	//bp.Rotation = -5400000;
		 //					   //	//bp.Vertical = DOXA.TextVerticalValues.Horizontal;
		 //					   //	//foreach (DOX.OpenXmlElement e in tp.ChildElements)
		 //					   //	//	{
		 //					   //	//	if (e.GetType() == typeof(DOXA.Paragraph))
		 //					   //	//		{
		 //					   //	//		DOXA.Paragraph para = (DOXA.Paragraph)e;
		 //					   //	//		DOXA.ParagraphProperties paraprop = para.ParagraphProperties;
		 //					   //	//		foreach (DOX.OpenXmlElement f in paraprop.ChildElements)
		 //					   //	//			{
		 //					   //	//			if (f.GetType() == typeof(DOXA.DefaultRunProperties))
		 //					   //	//				{
		 //					   //	//				DOXA.DefaultRunProperties drp = (DOXA.DefaultRunProperties)f;
		 //					   //	//				drp.FontSize = 1400;
		 //					   //	//				}
		 //					   //	//			}
		 //					   //	//		}
		 //					   //	//	}


		 //					   //	//DOXC.MajorUnit majorUnit1 = new DOXC.MajorUnit() { Val = 1D };
		 //					   //	//DOXC.MajorTimeUnit majorTimeUnit1 = new DOXC.MajorTimeUnit() { Val = DOXC.TimeUnitValues.Months };

		 //					   //	//add them all to the date axis
		 //					   //	dateAxis1.AppendChild(caaxisid.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(scale.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(del.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(ap.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(nf.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(mt.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(mit.CloneNode(true));
		 //					   //	if (tl != null)
		 //					   //		dateAxis1.AppendChild(tl.CloneNode(true));
		 //					   //	//dateAxis1.AppendChild(csp.CloneNode(true));
		 //					   //	//dateAxis1.AppendChild(tp.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(cax.CloneNode(true));
		 //					   //	dateAxis1.Append(crosses1);
		 //					   //	dateAxis1.Append(autoLabeled1);
		 //					   //	dateAxis1.Append(labelOffset1);
		 //					   //	dateAxis1.Append(baseTimeUnit1);
		 //					   //	//dateAxis1.Append(majorUnit1);
		 //					   //	//dateAxis1.Append(majorTimeUnit1);

		 //					   //	//remove the category axis
		 //					   //	ca.Remove();
		 //					   //	}
		 //					   //and add the date axis to the plot area
		 //					   //p.Append(dateAxis1);

		 //					   }
		 //				   }
		 //			   }
		 //		   pr.Presentation.Save();

		 //		   }
		 //	   }
		 //   }

		 //private void Bar7AdjustLayoutTarget(DOXP.PresentationDocument doc, Int32 slideNumber)
		 //   {

		 //   DOXP.PresentationPart pr = doc.PresentationPart;

		 //   //Get the relationship id of the slide we want to change
		 //   DOXPr.SlideIdList s = pr.Presentation.SlideIdList;
		 //   DOXPr.SlideId nslideid = (DOXPr.SlideId)s.ElementAt(slideNumber-1);
		 //   string slidRelId = nslideid.RelationshipId;

		 //   foreach (DOXP.SlidePart slidePart1 in pr.SlideParts)
		 //	   {
		 //	   string thisSlideno = pr.GetIdOfPart(slidePart1);
		 //	   if (thisSlideno == slidRelId)
		 //		   {
		 //		   DOXP.ChartPart chartPart1 = slidePart1.ChartParts.FirstOrDefault();

		 //		   if (chartPart1 != null)
		 //			   {
		 //			   foreach (DOX.OpenXmlElement x in chartPart1.ChartSpace.ChildElements)
		 //				   {
		 //				   if (x.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.Chart))//find the chart
		 //					   {
		 //					   DOXC.Chart c = (DOXC.Chart)x;
		 //					   DOXC.PlotArea p = c.PlotArea;

		 //					   DOXC.Layout l = p.Layout;
		 //					   DOXC.ManualLayout ml = l.ManualLayout;
		 //					   DOXC.LayoutTarget lt = ml.LayoutTarget;

		 //					   if (lt == null)
		 //						   {
		 //						   lt = new DOXC.LayoutTarget() { Val = DOXC.LayoutTargetValues.Inner };
		 //						   ml.LayoutTarget = lt;

		 //						   //set values for top and bottom of each chart
		 //						   DOXC.Top top1 = new DOXC.Top() { Val = 0.0D };
		 //						   DOXC.Height height1 = new DOXC.Height() { Val = 0.9D };//.8D

		 //						   //Double leftYAxisMax = 0;
		 //						   //Double rightYAxisMax = 0;
		 //						   //DOXC.BarDirection bd = null;

		 //						   //foreach (DOX.OpenXmlElement el in p.ChildElements)
		 //						   //	{
		 //						   //	if (el.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.ValueAxis))
		 //						   //		{
		 //						   //		DOXC.ValueAxis va = (DOXC.ValueAxis)el;
		 //						   //		if (va.AxisPosition.Val == DocumentFormat.OpenXml.Drawing.Charts.AxisPositionValues.Left)
		 //						   //			{
		 //						   //			if (va.Scaling.MaxAxisValue != null)
		 //						   //				leftYAxisMax = va.Scaling.MaxAxisValue.Val;
		 //						   //			}
		 //						   //		if (va.AxisPosition.Val == DocumentFormat.OpenXml.Drawing.Charts.AxisPositionValues.Right)
		 //						   //			{
		 //						   //			if (va.Scaling.MaxAxisValue != null)
		 //						   //				rightYAxisMax = va.Scaling.MaxAxisValue.Val;
		 //						   //			}
		 //						   //		}
		 //						   //	if (el.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.BarChart))
		 //						   //		{
		 //						   //		DOXC.BarChart bc = (DOXC.BarChart)el;
		 //						   //		bd = bc.BarDirection;
		 //						   //		}
		 //						   //	}

		 //						   //variable values for left and right of each chart
		 //						   DOXC.Left left1 = new DOXC.Left();
		 //						   DOXC.Width width1 = new DOXC.Width();

		 //						   left1.Val = 0.057;
		 //						   width1.Val = 0.863;
		 //						   //if (leftYAxisMax >= 0 && leftYAxisMax < 1000)
		 //						   //	{
		 //						   //	if (rightYAxisMax == 0)
		 //						   //		{
		 //						   //		left1.Val = 0.036D;
		 //						   //		width1.Val = 0.817D;
		 //						   //		}
		 //						   //	else
		 //						   //		{
		 //						   //		left1.Val = 0.008D;
		 //						   //		width1.Val = 0.760D;
		 //						   //		}
		 //						   //	}

		 //						   //if (leftYAxisMax >= 1000 && leftYAxisMax < 100000)
		 //						   //	{
		 //						   //	if (rightYAxisMax == 0)
		 //						   //		{
		 //						   //		left1.Val = 0.052D;
		 //						   //		width1.Val = 0.785D;
		 //						   //		}
		 //						   //	else
		 //						   //		{
		 //						   //		left1.Val = 0.024D;
		 //						   //		width1.Val = 0.728D;
		 //						   //		}
		 //						   //	}

		 //						   //if (leftYAxisMax == 0 && rightYAxisMax == 0 && bd != null && bd.Val == DOXC.BarDirectionValues.Bar)
		 //						   //	{
		 //						   //	left1.Val = 0.043;
		 //						   //	width1.Val = 0.627D;
		 //						   //	}

		 //						   ml.Left = left1;
		 //						   ml.Width = width1;
		 //						   ml.Top = top1;
		 //						   ml.Height = height1;

		 //						   }
		 //					   //DOXC.CategoryAxis ca;
		 //					   //DOXC.DateAxis dateAxis1 = new DOXC.DateAxis();//create a date axis and copy the category axis values to it (most of them are the same, some get changes or additions)
		 //					   //foreach (DOX.OpenXmlElement y in p.ChildElements)
		 //					   //if (y.GetType() == typeof(DOXC.CategoryAxis))
		 //					   //	{
		 //					   //	ca = (DOXC.CategoryAxis)y;

		 //					   //	DOXC.AxisId caaxisid = ca.AxisId;
		 //					   //	DOXC.Scaling scale = ca.Scaling;
		 //					   //	scale.MaxAxisValue = new DOXC.MaxAxisValue() { Val = 109D };
		 //					   //	scale.MinAxisValue = new DOXC.MinAxisValue() { Val = 11D };
		 //					   //	DOXC.Delete del = ca.Delete;
		 //					   //	DOXC.AxisPosition ap = ca.AxisPosition;
		 //					   //	DOXC.NumberingFormat nf = ca.NumberingFormat;
		 //					   //	DOXC.MajorTickMark mt = ca.MajorTickMark;
		 //					   //	DOXC.MinorTickMark mit = ca.MinorTickMark;
		 //					   //	DOXC.TickLabelPosition tl = ca.TickLabelPosition;
		 //					   //	DOXC.CrossingAxis cax = ca.CrossingAxis;

		 //					   //	DOXC.Crosses crosses1 = new DOXC.Crosses() { Val = DOXC.CrossesValues.Maximum };
		 //					   //	DOXC.AutoLabeled autoLabeled1 = new DOXC.AutoLabeled() { Val = false };
		 //					   //	DOXC.LabelOffset labelOffset1 = new DOXC.LabelOffset() { Val = (DOX.UInt16Value)100U };
		 //					   //	DOXC.BaseTimeUnit baseTimeUnit1 = new DOXC.BaseTimeUnit() { Val = DOXC.TimeUnitValues.Days };


		 //					   //	//DOXC.ChartShapeProperties csp = ca.ChartShapeProperties;
		 //					   //	//DOXC.TextProperties tp = ca.TextProperties;
		 //					   //	//DOXA.BodyProperties bp = tp.BodyProperties;
		 //					   //	//bp.Rotation = -5400000;
		 //					   //	//bp.Vertical = DOXA.TextVerticalValues.Horizontal;
		 //					   //	//foreach (DOX.OpenXmlElement e in tp.ChildElements)
		 //					   //	//	{
		 //					   //	//	if (e.GetType() == typeof(DOXA.Paragraph))
		 //					   //	//		{
		 //					   //	//		DOXA.Paragraph para = (DOXA.Paragraph)e;
		 //					   //	//		DOXA.ParagraphProperties paraprop = para.ParagraphProperties;
		 //					   //	//		foreach (DOX.OpenXmlElement f in paraprop.ChildElements)
		 //					   //	//			{
		 //					   //	//			if (f.GetType() == typeof(DOXA.DefaultRunProperties))
		 //					   //	//				{
		 //					   //	//				DOXA.DefaultRunProperties drp = (DOXA.DefaultRunProperties)f;
		 //					   //	//				drp.FontSize = 1400;
		 //					   //	//				}
		 //					   //	//			}
		 //					   //	//		}
		 //					   //	//	}


		 //					   //	//DOXC.MajorUnit majorUnit1 = new DOXC.MajorUnit() { Val = 1D };
		 //					   //	//DOXC.MajorTimeUnit majorTimeUnit1 = new DOXC.MajorTimeUnit() { Val = DOXC.TimeUnitValues.Months };

		 //					   //	//add them all to the date axis
		 //					   //	dateAxis1.AppendChild(caaxisid.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(scale.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(del.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(ap.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(nf.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(mt.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(mit.CloneNode(true));
		 //					   //	if (tl != null)
		 //					   //		dateAxis1.AppendChild(tl.CloneNode(true));
		 //					   //	//dateAxis1.AppendChild(csp.CloneNode(true));
		 //					   //	//dateAxis1.AppendChild(tp.CloneNode(true));
		 //					   //	dateAxis1.AppendChild(cax.CloneNode(true));
		 //					   //	dateAxis1.Append(crosses1);
		 //					   //	dateAxis1.Append(autoLabeled1);
		 //					   //	dateAxis1.Append(labelOffset1);
		 //					   //	dateAxis1.Append(baseTimeUnit1);
		 //					   //	//dateAxis1.Append(majorUnit1);
		 //					   //	//dateAxis1.Append(majorTimeUnit1);

		 //					   //	//remove the category axis
		 //					   //	ca.Remove();
		 //					   //	}
		 //					   //and add the date axis to the plot area
		 //					   //p.Append(dateAxis1);

		 //					   }
		 //				   }
		 //			   }
		 //		   pr.Presentation.Save();

		 //		   }
		 //	   }


		 //   }

		private void TestCellBorders()
		{

			//Instantiate Presentation class that represents PPTX file
			using (Presentation pres = new Presentation())
				{

				//Access first slide
				ISlide sld = pres.Slides[0];

				//Define columns with widths and rows with heights
				double[] dblCols = { 50, 50 };
				double[] dblRows = { 50, 50 };

				//Add table shape to slide
				ITable tbl = sld.Shapes.AddTable(100, 100, dblCols, dblRows);

				//Fill all cells white
				tbl[0, 0].FillFormat.FillType = FillType.Solid;
				tbl[0, 0].FillFormat.SolidFillColor.Color = Color.White;
				tbl[0, 1].FillFormat.FillType = FillType.Solid;
				tbl[0, 1].FillFormat.SolidFillColor.Color = Color.White;
				tbl[1, 0].FillFormat.FillType = FillType.Solid;
				tbl[1, 0].FillFormat.SolidFillColor.Color = Color.White;
				tbl[1, 1].FillFormat.FillType = FillType.Solid;
				tbl[1, 1].FillFormat.SolidFillColor.Color = Color.White;

				tbl[0, 0].BorderBottom.FillFormat.FillType = FillType.Solid;
				tbl[0, 0].BorderBottom.FillFormat.SolidFillColor.Color = Color.Black;

				tbl[0, 1].BorderBottom.FillFormat.FillType = FillType.Solid;
				tbl[0, 1].BorderBottom.FillFormat.SolidFillColor.Color = System.Drawing.ColorTranslator.FromHtml("#4D4D4F");

				////Set border format for each cell
				//foreach (IRow row in tbl.Rows)
				//	foreach (ICell cell in row)
				//		{
				//		cell.BorderTop.FillFormat.FillType = FillType.Solid;
				//		cell.BorderTop.FillFormat.SolidFillColor.Color = Color.Red;
				//		cell.BorderTop.Width = 5;

				//		cell.BorderBottom.FillFormat.FillType = FillType.Solid;
				//		cell.BorderBottom.FillFormat.SolidFillColor.Color = Color.Red;
				//		cell.BorderBottom.Width = 5;

				//		cell.BorderLeft.FillFormat.FillType = FillType.Solid;
				//		cell.BorderLeft.FillFormat.SolidFillColor.Color = Color.Red;
				//		cell.BorderLeft.Width = 5;

				//		cell.BorderRight.FillFormat.FillType = FillType.Solid;
				//		cell.BorderRight.FillFormat.SolidFillColor.Color = Color.Red;
				//		cell.BorderRight.Width = 5;
				//		}

				////Merge cells 1 & 2 of row 1
				//tbl.MergeCells(tbl[0, 0], tbl[1, 0], false);

				////Add text to the merged cell
				//tbl[0, 0].TextFrame.Text = "Merged Cells";

				//Write PPTX to Disk
				pres.Save("C:\\Development\\TestTable.pptx", SaveFormat.Pptx);
				}


		}

		#endregion


	}
}

namespace CustomExtensions
{
	public static class ExtentionMethods
	{

		public static T CheckForNull<T>(this object value, T defaultValue)
		{
			if (value == null || value == DBNull.Value)
				return defaultValue;

			return (T)Convert.ChangeType(value, typeof(T));
		}
	}
}


