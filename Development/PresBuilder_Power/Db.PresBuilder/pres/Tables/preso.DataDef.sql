﻿CREATE TABLE [preso].[DataDef]
(
	[DataDef_Id]				INT					NOT	NULL	IDENTITY(1, 1),

	[Slide_Id]					INT					NOT	NULL	CONSTRAINT [FK_DataDef_Slides]				REFERENCES [preso].[Slides]([Slide_Id]),
	[SeriesNum]					TINYINT				NOT	NULL,
	[SeriesName]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL_DataDef_SeriesName]			CHECK([SeriesName] <> ''),

	[SqlSchema]					VARCHAR(24)			NOT	NULL	CONSTRAINT [CL_DataDef_SqlSchema]			CHECK([SqlSchema] <> ''),
	[SqlTvf]					VARCHAR(48)			NOT	NULL	CONSTRAINT [CL_DataDef_SqlTvf]				CHECK([SqlTvf] <> ''),
	[SqlTvfParameters]			VARCHAR(48)			NOT	NULL	CONSTRAINT [CL_DataDef_SqlTvfParameters]	CHECK([SqlTvfParameters] <> ''),


	CONSTRAINT [PK_DataDef]		PRIMARY KEY([DataDef_Id] ASC),
	CONSTRAINT [UK_DataDef]		UNIQUE CLUSTERED([Slide_Id] ASC, [SeriesNum] ASC)
);