﻿CREATE TABLE [org].[Presentations]
(
	[Id]						INT					NOT	NULL	IDENTITY(1, 1),

	[Agreement_Id]				INT					NOT	NULL,
	[Presentation_Id]			INT					NOT	NULL,

	CONSTRAINT [PK_Presentations]	PRIMARY KEY([Id]),
	CONSTRAINT [UK_Presentations]	UNIQUE CLUSTERED([Agreement_Id] ASC, [Presentation_Id] ASC)
);