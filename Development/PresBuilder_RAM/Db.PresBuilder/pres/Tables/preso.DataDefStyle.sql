﻿CREATE TABLE [preso].[DataDefStyle]
(
	[DataDefStyle_Id]			INT					NOT	NULL	IDENTITY(1, 1),

	[DataDef_Id]				INT					NOT	NULL	CONSTRAINT [FK_DataDefStyle_DataDef]	REFERENCES [preso].[DataDef]([DataDef_Id]),

	[xAxis]						TINYINT				NOT	NULL,
	[yAxis]						TINYINT				NOT	NULL,

	[MarkerShape]				INT						NULL,
	[MarkerSize]				INT						NULL,
	[MarkerFill]				INT						NULL,
	[MarkerAlpha]				TINYINT				NOT	NULL	CONSTRAINT [DF_DataDefStyle_MarkerAlpha]	DEFAULT(255),

	[MarkerLineColor]			INT						NULL,
	[MarkerLineStyle]			INT						NULL, --	Multiple options, width, color...

	[LineColor]					INT						NULL,
	[LineStyle]					INT						NULL, --	Multiple options, width, color...

	CONSTRAINT [PK_DataDefStyle]	PRIMARY KEY([DataDefStyle_Id] ASC),
	CONSTRAINT [UK_DataDefStyle]	UNIQUE CLUSTERED([DataDef_Id] ASC),
);