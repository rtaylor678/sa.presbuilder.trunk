﻿CREATE TABLE [preso].[DataDefAnimation]
(
	[DataDefAnimation_Id]		INT					NOT	NULL	IDENTITY(1, 1),

	[DataDef_Id]				INT					NOT	NULL	CONSTRAINT [FK_DataDefAnimation_DataDef]	REFERENCES [preso].[DataDef]([DataDef_Id]),

	[Animation_Id]				INT					NOT	NULL,

	CONSTRAINT [PK_DataDefAnimation]	PRIMARY KEY([DataDefAnimation_Id] ASC),
	CONSTRAINT [UK_DataDefAnimation]	UNIQUE CLUSTERED([DataDef_Id] ASC),
);