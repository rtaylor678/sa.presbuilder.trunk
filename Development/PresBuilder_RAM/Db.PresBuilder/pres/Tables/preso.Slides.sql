﻿CREATE TABLE [preso].[Slides]
(
	[Slide_Id]					INT					NOT	NULL	IDENTITY(1, 1),

	[Presentation_Id]			INT					NOT	NULL	CONSTRAINT [FK_Slides_Presentation]		REFERENCES [preso].[Presentations]([Presentation_Id]),

	[SlideTemplate_Id]			INT					NOT	NULL,	-- Choose the slide format from the slide deck
	[SlideTitle]				VARCHAR(48)			NOT	NULL	CONSTRAINT [CL_Slides_SlideTitle]		CHECK([SlideTitle] <> ''),
	[SlideTitleSub]				VARCHAR(48)				NULL	CONSTRAINT [CL_Slides_SlideTitleSub]	CHECK([SlideTitleSub] <> ''),
	[SlideFooter]				TINYINT				NOT	NULL	CONSTRAINT [DF_Slides_SlideFooter]		DEFAULT(1),

	CONSTRAINT [PK_Slides]		PRIMARY KEY([Slide_Id] ASC),
	CONSTRAINT [UK_Slides]		UNIQUE CLUSTERED([Presentation_Id] ASC, [SlideTemplate_Id] ASC)
);