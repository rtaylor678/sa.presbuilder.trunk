﻿CREATE TABLE [preso].[Presentations]
(
	[Presentation_Id]			INT					NOT	NULL	IDENTITY(1, 1),

	[Study_Id]					INT					NOT	NULL,
	[PresentationYear]			SMALLINT			NOT	NULL,
	[PresentationName]			VARCHAR(48)			NOT	NULL	CONSTRAINT [CL_Presentations_PresentationName] CHECK([PresentationName] <> ''),

	--	Assumption: A presentation will have its data in one database.
	[SqlServer]					VARCHAR(24)			NOT	NULL	CONSTRAINT [CL_Presentations_SqlServer]			CHECK([SqlServer] <> ''),
	[SqlInstance]				VARCHAR(24)				NULL	CONSTRAINT [CL_Presentations_SqlInstance]		CHECK([SqlInstance] <> ''),
	[SqlDatabase]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL_Presentations_SqlDatabase]		CHECK([SqlDatabase] <> ''),

	CONSTRAINT [PK_Presentations]		PRIMARY KEY([Presentation_Id] ASC),
	CONSTRAINT [UK_Presentations]		UNIQUE CLUSTERED([Study_Id] ASC, [PresentationYear] ASC, [PresentationName] ASC)
);