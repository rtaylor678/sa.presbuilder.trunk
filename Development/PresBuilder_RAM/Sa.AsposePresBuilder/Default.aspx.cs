﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Aspose.Slides;
using Aspose.Slides.Charts;
using Aspose.Slides.Export;

namespace AsposePresBuilder
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Creating empty presentation//Creating empty presentation
            Presentation pres = new Presentation();
            
            ISlideCollection slds = pres.Slides;
            slds.AddEmptySlide(pres.LayoutSlides[0]);
            ISlide slide = pres.Slides[0];
            IAutoShape ashp = pres.Slides[0].Shapes.AddAutoShape(ShapeType.Rectangle, 25, 5, 700, 50);
            ashp.FillFormat.FillType = FillType.NoFill;
            ashp.ShapeStyle.LineColor.Color = Color.Transparent;
            IAutoShape ashp2 = pres.Slides[0].Shapes.AddAutoShape(ShapeType.Rectangle, 25, 35, 700, 50);
            ashp2.FillFormat.FillType = FillType.NoFill;
            ashp2.ShapeStyle.LineColor.Color = Color.Transparent;

            //Accessing the first and second placeholder in the slide and typecasting it as AutoShape
            ITextFrame tf1 = ((IAutoShape)slide.Shapes[0]).TextFrame;
            ITextFrame tf2 = ((IAutoShape)slide.Shapes[1]).TextFrame;

            slide.Timeline.MainSequence.AddEffect(ashp, Aspose.Slides.Animation.EffectType.Wipe, Aspose.Slides.Animation.EffectSubtype.Left, Aspose.Slides.Animation.EffectTriggerType.AfterPrevious);

            //Accessing the first Paragraph
            IParagraph para1 = tf1.Paragraphs[0];
            IParagraph para2 = tf2.Paragraphs[0];
            para1.ParagraphFormat.Alignment = TextAlignment.Left;
            para2.ParagraphFormat.Alignment = TextAlignment.Left;

            //Accessing the first portion
            IPortion port1 = para1.Portions[0];
            IPortion port2 = para2.Portions[0];

            //Define new fonts
            FontData fd1 = new FontData("Tahoma");
            FontData fd2 = new FontData("Tahoma");

            //Assign new fonts to portion
            port1.PortionFormat.LatinFont = fd1;
            port2.PortionFormat.LatinFont = fd2;

            //Set font to Bold
            port1.PortionFormat.FontBold = NullableBool.True;
            port2.PortionFormat.FontBold = NullableBool.True;

            //Set font to Italic
            port1.PortionFormat.FontItalic = NullableBool.False;
            port2.PortionFormat.FontItalic = NullableBool.False;

            //Set font color
            port1.PortionFormat.FillFormat.FillType = FillType.Solid;
            port1.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(152, 0, 6);
            port2.PortionFormat.FillFormat.FillType = FillType.Solid;
            port2.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);

            port1.Text = "Total Maintenance Cost";
            port1.PortionFormat.FontHeight = 28;
            port2.Text = "Chemicals – Quartiles of Performance";
            port2.PortionFormat.FontHeight = 20;

            IChart chart = pres.Slides[0].Shapes.AddChart(ChartType.StackedColumn, 65, 85, 600, 400);
            //Delete default generated series and categories
            chart.ChartData.Series.Clear();
            chart.ChartData.Categories.Clear();
            int s = chart.ChartData.Series.Count;
            s = chart.ChartData.Categories.Count;
            FontData fd3 = new FontData("Tahoma");
            chart.Legend.Position = LegendPositionType.Bottom;
            chart.Legend.TextFormat.PortionFormat.LatinFont = fd3;
            chart.Legend.TextFormat.PortionFormat.FontHeight = 16;
            chart.Legend.TextFormat.PortionFormat.FillFormat.FillType = FillType.Solid;
            chart.Legend.TextFormat.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            chart.PlotArea.Format.Fill.FillType = FillType.Solid;
            chart.PlotArea.Format.Fill.SolidFillColor.Color = Color.FromArgb(234, 234, 234);
            chart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.Solid;
            chart.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.SolidFillColor.Color = Color.Transparent;
            IChartPortionFormat txtCat = chart.Axes.HorizontalAxis.TextFormat.PortionFormat;
            IChartPortionFormat txtCat2 = chart.Axes.VerticalAxis.TextFormat.PortionFormat;
            txtCat.FillFormat.FillType = FillType.Solid;
            txtCat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            txtCat2.FillFormat.FillType = FillType.Solid;
            txtCat2.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            txtCat.LatinFont = fd3;
            txtCat2.LatinFont = fd3;
            

            //Setting the index of chart data sheet
            int defaultWorksheetIndex = 0;

            //Getting the chart data worksheet
            IChartDataWorkbook fact = chart.ChartData.ChartDataWorkbook;

            //Adding new series
            chart.ChartData.Series.Add(fact.GetCell(defaultWorksheetIndex, 0, 1, "Labor"), chart.Type);
            chart.ChartData.Series.Add(fact.GetCell(defaultWorksheetIndex, 0, 2, "Materials"), chart.Type);
            chart.ChartData.Series.Add(fact.GetCell(defaultWorksheetIndex, 0, 3, "Indirects"), chart.Type);
           
            //Adding new categories
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 1, 0, "Q1"));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 2, 0, "Q2"));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 3, 0, "Q3"));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 4, 0, "Q4"));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 5, 0, ""));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 6, 0, "GM"));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 7, 0, "PS"));
            chart.ChartData.Categories.Add(fact.GetCell(defaultWorksheetIndex, 8, 0, "PA"));

            //Take first chart series
            IChartSeries series = chart.ChartData.Series[0];
            series.ParentSeriesGroup.Overlap = 100;
            series.Format.Fill.FillType = FillType.Solid;
            series.Format.Fill.SolidFillColor.Color = Color.FromArgb(3, 128, 205);
            series.Format.Line.FillFormat.FillType = FillType.Solid;
            series.Format.Line.FillFormat.SolidFillColor.Color = Color.Black;

            //Now populating series data
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 1, 1, .5));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 2, 1, .85));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 3, 1, 1.26));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 4, 1, 2.07));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 5, 1, 0));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 6, 1, 1));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 7, 1, .25));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 8, 1, .5));

            series = chart.ChartData.Series[1];
            series.Format.Fill.FillType = FillType.Solid;
            series.Format.Fill.SolidFillColor.Color = Color.FromArgb(255, 255, 255);
            series.Format.Line.FillFormat.FillType = FillType.Solid;
            series.Format.Line.FillFormat.SolidFillColor.Color = Color.Black;
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 1, 2, .3684));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 2, 2, .656));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 3, 2, .89));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 4, 2, 1.7));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 5, 2, 0));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 6, 2, .84));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 7, 2, .5));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 8, 2, .57));

            series = chart.ChartData.Series[2];
            series.Format.Fill.FillType = FillType.Solid;
            series.Format.Fill.SolidFillColor.Color = Color.FromArgb(255, 153, 51);
            series.Format.Line.FillFormat.FillType = FillType.Solid;
            series.Format.Line.FillFormat.SolidFillColor.Color = Color.Black;
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 1, 3, .14));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 2, 3, .24));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 3, 3, .4));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 4, 3, .5));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 5, 3, 0));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 6, 3, .15));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 7, 3, .05));
            series.DataPoints.AddDataPointForBarSeries(fact.GetCell(defaultWorksheetIndex, 8, 3, .07));

            //chart.Legend.Entries[3].Hide = true;
            //chart.Legend.Entries[4].Hide = true;

            //Setting value axis number format
            chart.Axes.VerticalAxis.IsNumberFormatLinkedToSource = false;
            //chart.Axes.VerticalAxis.DisplayUnit = DisplayUnitType.Hundreds;
            chart.Axes.VerticalAxis.NumberFormat = "0.0";

            //Setting value axis title -- Can't use built in axis title because there is no way to control the position.
            //chart.Axes.VerticalAxis.HasTitle = true;
            //chart.Axes.VerticalAxis.Title.AddTextFrameForOverriding("");
            //IPortion valtitle = chart.Axes.VerticalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0];
            //valtitle.PortionFormat.FillFormat.FillType = FillType.Solid;
            //valtitle.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            //valtitle.PortionFormat.LatinFont = fd3;
            //valtitle.PortionFormat.FontBold = NullableBool.False;
            //chart.Axes.VerticalAxis.Title.Overlay = false;
            //valtitle.Text = "MCI, % of PRV";
            //Add an AutoShape of Rectangle type
            IAutoShape ashpVATitle = slide.Shapes.AddAutoShape(ShapeType.Rectangle, 25, 185, 50, 150);
            ashpVATitle.FillFormat.FillType = FillType.Solid;
            ashpVATitle.FillFormat.SolidFillColor.Color = Color.Transparent;
            ashpVATitle.ShapeStyle.LineColor.Color = Color.Transparent;
            ashpVATitle.AddTextFrame(" ");
            ITextFrame txtFrame = ashpVATitle.TextFrame;
            txtFrame.TextFrameFormat.TextVerticalType = TextVerticalType.Vertical270;
            IParagraph para = txtFrame.Paragraphs[0];
            IPortion portion = para.Portions[0];
            portion.PortionFormat.FillFormat.FillType = FillType.Solid;
            portion.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            portion.PortionFormat.LatinFont = fd3;
            portion.PortionFormat.FontBold = NullableBool.False;
            portion.Text = "MCI, % of PRV";


            //Plot area outer border
            chart.PlotArea.Format.Line.FillFormat.FillType = FillType.Solid;
            chart.PlotArea.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);

            //Axis line color
            chart.Axes.HorizontalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
            chart.Axes.HorizontalAxis.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            chart.Axes.VerticalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
            chart.Axes.VerticalAxis.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);

            //Instantiate the ImageEx class
            System.Drawing.Image img = (System.Drawing.Image)new Bitmap("C:\\sandbox_20130326\\JBF.Trunk\\AsposePresBuilder\\AsposePresBuilder\\Images\\Logo3.jpg");
            IPPImage imgx = pres.Images.AddImage(img);

            //Add Picture Frame with height and width equivalent of Picture
            slide.Shapes.AddPictureFrame(ShapeType.Rectangle, 44, 506, 160, 25, imgx);


            //Footer copy
            IAutoShape ashpFooterRight = slide.Shapes.AddAutoShape(ShapeType.Rectangle, 532, 490, 150, 50);
            ashpFooterRight.FillFormat.FillType = FillType.Solid;
            ashpFooterRight.FillFormat.SolidFillColor.Color = Color.Transparent;
            ashpFooterRight.ShapeStyle.LineColor.Color = Color.Transparent;
            ashpFooterRight.AddTextFrame(" ");
            ITextFrame txtFrameFooterRight = ashpFooterRight.TextFrame;
            IParagraph paraFooterRight = txtFrameFooterRight.Paragraphs[0];
            paraFooterRight.ParagraphFormat.Alignment = TextAlignment.Right;            
            IPortion portionFooterRight = paraFooterRight.Portions[0];
            portionFooterRight.PortionFormat.FillFormat.FillType = FillType.Solid;
            portionFooterRight.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            portionFooterRight.PortionFormat.LatinFont = new FontData("Tahoma");
            portionFooterRight.PortionFormat.FontHeight = 6;
            portionFooterRight.PortionFormat.FontBold = NullableBool.False;
            portionFooterRight.Text = "Proprietary and Confidential\n© 2014 HSB Solomon Associates LLC\nwww.SolomonOnline.com";

            IAutoShape footerLine = slide.Shapes.AddAutoShape(ShapeType.Line, 44, 498, 632, 0);
            footerLine.ShapeStyle.LineColor.Color = Color.FromArgb(77, 77, 77);
            footerLine.LineFormat.Style = LineStyle.Single;
            footerLine.LineFormat.Width = 1.5;

            //### SLIDE TWO, SCATTER LINES
            pres.Slides.AddClone(slide);
            slide = pres.Slides[1];
            pres.Slides[2].Shapes[2].Hidden = true;
            //pres.Slides[2].Shapes[3].Hidden = true;
            IChart chart2 = pres.Slides[2].Shapes.AddChart(ChartType.ScatterWithStraightLines, 65, 85, 600, 396);
            
            chart2.HasLegend = false;

            chart2.PlotArea.Format.Fill.FillType = FillType.Solid;
            chart2.PlotArea.Format.Fill.SolidFillColor.Color = Color.FromArgb(234, 234, 234);
            chart2.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.FillType = FillType.Solid;
            chart2.Axes.VerticalAxis.MajorGridLinesFormat.Line.FillFormat.SolidFillColor.Color = Color.Transparent;
            IChartPortionFormat txtCatHor = chart2.Axes.HorizontalAxis.TextFormat.PortionFormat;
            IChartPortionFormat txtCatVert = chart2.Axes.VerticalAxis.TextFormat.PortionFormat;
            txtCatHor.FillFormat.FillType = FillType.Solid;
            txtCatHor.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            txtCatVert.FillFormat.FillType = FillType.Solid;
            txtCatVert.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            txtCatHor.LatinFont = fd3;
            txtCatVert.LatinFont = fd3;

            //Plot area outer border
            chart2.PlotArea.Format.Line.FillFormat.FillType = FillType.Solid;
            chart2.PlotArea.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);

            //Axis line color
            chart2.Axes.HorizontalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
            chart2.Axes.HorizontalAxis.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            chart2.Axes.VerticalAxis.Format.Line.FillFormat.FillType = FillType.Solid;
            chart2.Axes.VerticalAxis.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);

            chart2.ChartData.Series.Clear();
            chart2.ChartData.Categories.Clear();
            int s2 = chart2.ChartData.Series.Count;
            s2 = chart2.ChartData.Categories.Count;

            //Setting the index of chart data sheet
            int defaultWorksheetIndex2 = 0;

            //Getting the chart data worksheet
            IChartDataWorkbook fact2 = chart2.ChartData.ChartDataWorkbook;

            //Adding new series
            chart2.ChartData.Series.Add(fact2.GetCell(defaultWorksheetIndex2, 0, 0, "SitePRVMUSD"), chart2.Type);
            chart2.ChartData.Series.Add(fact2.GetCell(defaultWorksheetIndex2, 0, 1, "MCI"), chart2.Type);
            chart2.ChartData.Series.Add(fact2.GetCell(defaultWorksheetIndex2, 0, 2, "PS"), chart2.Type);

            //Take first chart series
            IChartSeries seriesX = chart2.ChartData.Series[0];
            seriesX.ParentSeriesGroup.Overlap = 100;
            seriesX.Format.Fill.FillType = FillType.Solid;
            seriesX.Format.Fill.SolidFillColor.Color = Color.FromArgb(3, 128, 205);
            seriesX.Format.Line.FillFormat.FillType = FillType.Solid;
            seriesX.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(0, 112, 192);

            //Now populating series data
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 1, 0, 0), fact2.GetCell(defaultWorksheetIndex2, 1, 1, 0.3577));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 2, 0, 1), fact2.GetCell(defaultWorksheetIndex2, 2, 1, 0.570283299));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 3, 0, 2), fact2.GetCell(defaultWorksheetIndex2, 3, 1, 0.600696418));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 4, 0, 3), fact2.GetCell(defaultWorksheetIndex2, 4, 1, 0.710468695));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 5, 0, 4), fact2.GetCell(defaultWorksheetIndex2, 5, 1, 0.735551276));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 6, 0, 5), fact2.GetCell(defaultWorksheetIndex2, 6, 1, 0.824231196));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 7, 0, 6), fact2.GetCell(defaultWorksheetIndex2, 7, 1, 0.827678078));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 8, 0, 7), fact2.GetCell(defaultWorksheetIndex2, 8, 1, 0.840410635));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 9, 0, 8), fact2.GetCell(defaultWorksheetIndex2, 9, 1, 0.87491531));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 10, 0, 9), fact2.GetCell(defaultWorksheetIndex2, 10, 1, 0.926693797));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 11, 0, 10), fact2.GetCell(defaultWorksheetIndex2, 11, 1, 0.946978099));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 12, 0, 11), fact2.GetCell(defaultWorksheetIndex2, 12, 1, 0.963996464));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 13, 0, 12), fact2.GetCell(defaultWorksheetIndex2, 13, 1, 1.032258767));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 14, 0, 13), fact2.GetCell(defaultWorksheetIndex2, 14, 1, 1.055681667));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 15, 0, 14), fact2.GetCell(defaultWorksheetIndex2, 15, 1, 1.104618942));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 16, 0, 15), fact2.GetCell(defaultWorksheetIndex2, 16, 1, 1.12091248));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 17, 0, 16), fact2.GetCell(defaultWorksheetIndex2, 17, 1, 1.132847333));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 18, 0, 17), fact2.GetCell(defaultWorksheetIndex2, 18, 1, 1.154809272));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 19, 0, 18), fact2.GetCell(defaultWorksheetIndex2, 19, 1, 1.176363343));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 20, 0, 19), fact2.GetCell(defaultWorksheetIndex2, 20, 1, 1.228443535));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 21, 0, 20), fact2.GetCell(defaultWorksheetIndex2, 21, 1, 1.26168612));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 22, 0, 21), fact2.GetCell(defaultWorksheetIndex2, 22, 1, 1.27801971));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 23, 0, 22), fact2.GetCell(defaultWorksheetIndex2, 23, 1, 1.27912498));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 24, 0, 23), fact2.GetCell(defaultWorksheetIndex2, 24, 1, 1.355152784));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 25, 0, 24), fact2.GetCell(defaultWorksheetIndex2, 25, 1, 1.365796176));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 26, 0, 25), fact2.GetCell(defaultWorksheetIndex2, 26, 1, 1.392523812));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 27, 0, 26), fact2.GetCell(defaultWorksheetIndex2, 27, 1, 1.422296847));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 28, 0, 27), fact2.GetCell(defaultWorksheetIndex2, 28, 1, 1.453033136));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 29, 0, 28), fact2.GetCell(defaultWorksheetIndex2, 29, 1, 1.489197475));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 30, 0, 29), fact2.GetCell(defaultWorksheetIndex2, 30, 1, 1.517270201));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 31, 0, 30), fact2.GetCell(defaultWorksheetIndex2, 31, 1, 1.536761538));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 32, 0, 31), fact2.GetCell(defaultWorksheetIndex2, 32, 1, 1.56030694));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 33, 0, 32), fact2.GetCell(defaultWorksheetIndex2, 33, 1, 1.582862824));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 34, 0, 33), fact2.GetCell(defaultWorksheetIndex2, 34, 1, 1.667489138));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 35, 0, 34), fact2.GetCell(defaultWorksheetIndex2, 35, 1, 1.673623744));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 36, 0, 35), fact2.GetCell(defaultWorksheetIndex2, 36, 1, 1.683314583));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 37, 0, 36), fact2.GetCell(defaultWorksheetIndex2, 37, 1, 1.69307305));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 38, 0, 37), fact2.GetCell(defaultWorksheetIndex2, 38, 1, 1.71295508));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 39, 0, 38), fact2.GetCell(defaultWorksheetIndex2, 39, 1, 1.750921894));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 40, 0, 39), fact2.GetCell(defaultWorksheetIndex2, 40, 1, 1.7573839));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 41, 0, 40), fact2.GetCell(defaultWorksheetIndex2, 41, 1, 1.767692503));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 42, 0, 41), fact2.GetCell(defaultWorksheetIndex2, 42, 1, 1.774328964));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 43, 0, 42), fact2.GetCell(defaultWorksheetIndex2, 43, 1, 1.798438945));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 44, 0, 43), fact2.GetCell(defaultWorksheetIndex2, 44, 1, 1.809115822));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 45, 0, 44), fact2.GetCell(defaultWorksheetIndex2, 45, 1, 1.836303307));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 46, 0, 45), fact2.GetCell(defaultWorksheetIndex2, 46, 1, 1.894997681));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 47, 0, 46), fact2.GetCell(defaultWorksheetIndex2, 47, 1, 1.903274841));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 48, 0, 47), fact2.GetCell(defaultWorksheetIndex2, 48, 1, 1.928287759));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 49, 0, 48), fact2.GetCell(defaultWorksheetIndex2, 49, 1, 1.98919738));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 50, 0, 49), fact2.GetCell(defaultWorksheetIndex2, 50, 1, 2.015580192));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 51, 0, 50), fact2.GetCell(defaultWorksheetIndex2, 51, 1, 2.038176177));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 52, 0, 51), fact2.GetCell(defaultWorksheetIndex2, 52, 1, 2.055930106));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 53, 0, 52), fact2.GetCell(defaultWorksheetIndex2, 53, 1, 2.083571167));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 54, 0, 53), fact2.GetCell(defaultWorksheetIndex2, 54, 1, 2.108384948));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 55, 0, 54), fact2.GetCell(defaultWorksheetIndex2, 55, 1, 2.124331642));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 56, 0, 55), fact2.GetCell(defaultWorksheetIndex2, 56, 1, 2.167806449));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 57, 0, 56), fact2.GetCell(defaultWorksheetIndex2, 57, 1, 2.218308704));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 58, 0, 57), fact2.GetCell(defaultWorksheetIndex2, 58, 1, 2.271715449));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 59, 0, 58), fact2.GetCell(defaultWorksheetIndex2, 59, 1, 2.28061191));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 60, 0, 59), fact2.GetCell(defaultWorksheetIndex2, 60, 1, 2.293079465));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 61, 0, 60), fact2.GetCell(defaultWorksheetIndex2, 61, 1, 2.315022565));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 62, 0, 61), fact2.GetCell(defaultWorksheetIndex2, 62, 1, 2.366889407));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 63, 0, 62), fact2.GetCell(defaultWorksheetIndex2, 63, 1, 2.392083215));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 64, 0, 63), fact2.GetCell(defaultWorksheetIndex2, 64, 1, 2.398674632));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 65, 0, 64), fact2.GetCell(defaultWorksheetIndex2, 65, 1, 2.401330762));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 66, 0, 65), fact2.GetCell(defaultWorksheetIndex2, 66, 1, 2.410044619));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 67, 0, 66), fact2.GetCell(defaultWorksheetIndex2, 67, 1, 2.443301865));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 68, 0, 67), fact2.GetCell(defaultWorksheetIndex2, 68, 1, 2.461367674));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 69, 0, 68), fact2.GetCell(defaultWorksheetIndex2, 69, 1, 2.545022037));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 70, 0, 69), fact2.GetCell(defaultWorksheetIndex2, 70, 1, 2.594416222));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 71, 0, 70), fact2.GetCell(defaultWorksheetIndex2, 71, 1, 2.611199358));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 72, 0, 71), fact2.GetCell(defaultWorksheetIndex2, 72, 1, 2.616368136));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 73, 0, 72), fact2.GetCell(defaultWorksheetIndex2, 73, 1, 2.629615043));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 74, 0, 73), fact2.GetCell(defaultWorksheetIndex2, 74, 1, 2.671118949));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 75, 0, 74), fact2.GetCell(defaultWorksheetIndex2, 75, 1, 2.682620625));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 76, 0, 75), fact2.GetCell(defaultWorksheetIndex2, 76, 1, 2.786964874));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 77, 0, 76), fact2.GetCell(defaultWorksheetIndex2, 77, 1, 2.809111486));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 78, 0, 77), fact2.GetCell(defaultWorksheetIndex2, 78, 1, 2.928838673));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 79, 0, 78), fact2.GetCell(defaultWorksheetIndex2, 79, 1, 2.972733948));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 80, 0, 79), fact2.GetCell(defaultWorksheetIndex2, 80, 1, 2.979193719));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 81, 0, 80), fact2.GetCell(defaultWorksheetIndex2, 81, 1, 3.068560502));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 82, 0, 81), fact2.GetCell(defaultWorksheetIndex2, 82, 1, 3.087078903));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 83, 0, 82), fact2.GetCell(defaultWorksheetIndex2, 83, 1, 3.263443011));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 84, 0, 83), fact2.GetCell(defaultWorksheetIndex2, 84, 1, 3.300113832));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 85, 0, 84), fact2.GetCell(defaultWorksheetIndex2, 85, 1, 3.367980982));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 86, 0, 85), fact2.GetCell(defaultWorksheetIndex2, 86, 1, 3.438069268));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 87, 0, 86), fact2.GetCell(defaultWorksheetIndex2, 87, 1, 3.496922947));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 88, 0, 87), fact2.GetCell(defaultWorksheetIndex2, 88, 1, 3.61076265));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 89, 0, 88), fact2.GetCell(defaultWorksheetIndex2, 89, 1, 3.643583478));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 90, 0, 89), fact2.GetCell(defaultWorksheetIndex2, 90, 1, 3.654035369));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 91, 0, 90), fact2.GetCell(defaultWorksheetIndex2, 91, 1, 3.757875471));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 92, 0, 91), fact2.GetCell(defaultWorksheetIndex2, 92, 1, 3.880126493));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 93, 0, 92), fact2.GetCell(defaultWorksheetIndex2, 93, 1, 3.907543545));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 94, 0, 93), fact2.GetCell(defaultWorksheetIndex2, 94, 1, 4.182712682));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 95, 0, 94), fact2.GetCell(defaultWorksheetIndex2, 95, 1, 4.444443053));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 96, 0, 95), fact2.GetCell(defaultWorksheetIndex2, 96, 1, 4.86161118));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 97, 0, 96), fact2.GetCell(defaultWorksheetIndex2, 97, 1, 5.164404242));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 98, 0, 97), fact2.GetCell(defaultWorksheetIndex2, 98, 1, 5.739313327));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 99, 0, 98), fact2.GetCell(defaultWorksheetIndex2, 99, 1, 5.848462158));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 100, 0, 99), fact2.GetCell(defaultWorksheetIndex2, 100, 1, 5.993594299));
            seriesX.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 101, 0, 100), fact2.GetCell(defaultWorksheetIndex2, 101, 1, 11.4604));

            //Take second chart series
            IChartSeries seriesPS = chart2.ChartData.Series[2];
            seriesPS.ParentSeriesGroup.Overlap = 100;
            seriesPS.Format.Fill.FillType = FillType.Solid;
            seriesPS.Format.Fill.SolidFillColor.Color = Color.FromArgb(3, 128, 205);
            seriesPS.Format.Line.FillFormat.FillType = FillType.Solid;
            seriesPS.Format.Line.FillFormat.SolidFillColor.Color = Color.FromArgb(0, 112, 192);
            seriesPS.Type = ChartType.ScatterWithMarkers;

            //Now populating series data
            seriesPS.DataPoints.AddDataPointForScatterSeries(fact2.GetCell(defaultWorksheetIndex2, 1, 2, "PS"), fact2.GetCell(defaultWorksheetIndex2, 6, 2, 0.83));
            seriesPS.Marker.Symbol = MarkerStyleType.None;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.LatinFont = fd1;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.FontBold = NullableBool.True;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.FontHeight = 14;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.EffectFormat.EnableOuterShadowEffect();
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.EffectFormat.OuterShadowEffect.BlurRadius = 0;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.EffectFormat.OuterShadowEffect.Direction = 45;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.EffectFormat.OuterShadowEffect.Distance = 4;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.EffectFormat.OuterShadowEffect.ShadowColor.Color = Color.Gray;
            seriesPS.DataPoints[0].Label.TextFormat.PortionFormat.EffectFormat.OuterShadowEffect.RectangleAlign = RectangleAlignment.TopLeft;
            seriesPS.DataPoints[0].Label.DataLabelFormat.ShowCategoryName = true;

            chart2.Axes.HorizontalAxis.IsAutomaticMaxValue = false;
            chart2.Axes.HorizontalAxis.MaxValue = 100;
            chart2.Axes.HorizontalAxis.MajorUnit = 25;

            //Setting value axis title
            chart2.Axes.HorizontalAxis.HasTitle = true;
            chart2.Axes.HorizontalAxis.Title.AddTextFrameForOverriding("");
            IPortion haltitle = chart2.Axes.HorizontalAxis.Title.TextFrameForOverriding.Paragraphs[0].Portions[0];
            haltitle.PortionFormat.FillFormat.FillType = FillType.Solid;
            haltitle.PortionFormat.FillFormat.SolidFillColor.Color = Color.FromArgb(77, 77, 77);
            haltitle.PortionFormat.LatinFont = fd3;
            haltitle.PortionFormat.FontBold = NullableBool.False;
            haltitle.PortionFormat.FontHeight = 16;
            chart2.Axes.HorizontalAxis.Title.Overlay = false;
            haltitle.Text = "Total Participation, %";


            pres.Slides[1].Remove();

            //pres.Slides[0].SlideShowTransition.Type = Aspose.Slides.SlideShow.TransitionType.Fade;

            //Saving presentation
            pres.Save("c:\\temp\\ChartTrendLines.pptx", SaveFormat.Pptx);

            MakeDS();
        }

        public void MakeDS()
        {
            System.Data.DataTable table = new System.Data.DataTable();
            table.Columns.Add("Value1", typeof(Single));
            table.Columns.Add("Value2", typeof(Single));

            table.Rows.Add(0, 0.3577);
            table.Rows.Add(1, 0.570283299);
            table.Rows.Add(2, 0.600696418);
            table.Rows.Add(3, 0.710468695);
            table.Rows.Add(4, 0.735551276);
            table.Rows.Add(5, 0.824231196);
            table.Rows.Add(6, 0.827678078);
            table.Rows.Add(7, 0.840410635);
            table.Rows.Add(8, 0.87491531);
            table.Rows.Add(9, 0.926693797);
            table.Rows.Add(10, 0.946978099);
            table.Rows.Add(11, 0.963996464);
            table.Rows.Add(12, 1.032258767);
            table.Rows.Add(13, 1.055681667);
            table.Rows.Add(14, 1.104618942);
            table.Rows.Add(15, 1.12091248);
            table.Rows.Add(16, 1.132847333);
            table.Rows.Add(17, 1.154809272);
            table.Rows.Add(18, 1.176363343);
            table.Rows.Add(19, 1.228443535);
            table.Rows.Add(20, 1.26168612);
            table.Rows.Add(21, 1.27801971);
            table.Rows.Add(22, 1.27912498);
            table.Rows.Add(23, 1.355152784);
            table.Rows.Add(24, 1.365796176);
            table.Rows.Add(25, 1.392523812);
            table.Rows.Add(26, 1.422296847);
            table.Rows.Add(27, 1.453033136);
            table.Rows.Add(28, 1.489197475);
            table.Rows.Add(29, 1.517270201);
            table.Rows.Add(30, 1.536761538);
            table.Rows.Add(31, 1.56030694);
            table.Rows.Add(32, 1.582862824);
            table.Rows.Add(33, 1.667489138);
            table.Rows.Add(34, 1.673623744);
            table.Rows.Add(35, 1.683314583);
            table.Rows.Add(36, 1.69307305);
            table.Rows.Add(37, 1.71295508);
            table.Rows.Add(38, 1.750921894);
            table.Rows.Add(39, 1.7573839);
            table.Rows.Add(40, 1.767692503);
            table.Rows.Add(41, 1.774328964);
            table.Rows.Add(42, 1.798438945);
            table.Rows.Add(43, 1.809115822);
            table.Rows.Add(44, 1.836303307);
            table.Rows.Add(45, 1.894997681);
            table.Rows.Add(46, 1.903274841);
            table.Rows.Add(47, 1.928287759);
            table.Rows.Add(48, 1.98919738);
            table.Rows.Add(49, 2.015580192);
            table.Rows.Add(50, 2.038176177);
            table.Rows.Add(51, 2.055930106);
            table.Rows.Add(52, 2.083571167);
            table.Rows.Add(53, 2.108384948);
            table.Rows.Add(54, 2.124331642);
            table.Rows.Add(55, 2.167806449);
            table.Rows.Add(56, 2.218308704);
            table.Rows.Add(57, 2.271715449);
            table.Rows.Add(58, 2.28061191);
            table.Rows.Add(59, 2.293079465);
            table.Rows.Add(60, 2.315022565);
            table.Rows.Add(61, 2.366889407);
            table.Rows.Add(62, 2.392083215);
            table.Rows.Add(63, 2.398674632);
            table.Rows.Add(64, 2.401330762);
            table.Rows.Add(65, 2.410044619);
            table.Rows.Add(66, 2.443301865);
            table.Rows.Add(67, 2.461367674);
            table.Rows.Add(68, 2.545022037);
            table.Rows.Add(69, 2.594416222);
            table.Rows.Add(70, 2.611199358);
            table.Rows.Add(71, 2.616368136);
            table.Rows.Add(72, 2.629615043);
            table.Rows.Add(73, 2.671118949);
            table.Rows.Add(74, 2.682620625);
            table.Rows.Add(75, 2.786964874);
            table.Rows.Add(76, 2.809111486);
            table.Rows.Add(77, 2.928838673);
            table.Rows.Add(78, 2.972733948);
            table.Rows.Add(79, 2.979193719);
            table.Rows.Add(80, 3.068560502);
            table.Rows.Add(81, 3.087078903);
            table.Rows.Add(82, 3.263443011);
            table.Rows.Add(83, 3.300113832);
            table.Rows.Add(84, 3.367980982);
            table.Rows.Add(85, 3.438069268);
            table.Rows.Add(86, 3.496922947);
            table.Rows.Add(87, 3.61076265);
            table.Rows.Add(88, 3.643583478);
            table.Rows.Add(89, 3.654035369);
            table.Rows.Add(90, 3.757875471);
            table.Rows.Add(91, 3.880126493);
            table.Rows.Add(92, 3.907543545);
            table.Rows.Add(93, 4.182712682);
            table.Rows.Add(94, 4.444443053);
            table.Rows.Add(95, 4.86161118);
            table.Rows.Add(96, 5.164404242);
            table.Rows.Add(97, 5.739313327);
            table.Rows.Add(98, 5.848462158);
            table.Rows.Add(99, 5.993594299);
            table.Rows.Add(100, 11.4604);

            System.Data.DataSet ds = new System.Data.DataSet();
            ds.Tables.Add(table);

            Single mew = GetDistX(ds, Convert.ToSingle(".83"));
            
        }

       

        public Single GetDistX(System.Data.DataSet r, Single y)
        {
            //Give me the range and the Y value and I will give you the X value
            int i;
            Single Delta1;
            Single Delta2;
            Single Pcnt1 = 0;
            Single Pcnt2 = 0;
			//Single Temp;
            Single Val1;
            Single Val2 = 0;
            Single GetDistX = 0;
            Val1 = y;
            Delta1 = 10000;
            Delta2 = 10000;
            for (i = 0; i < r.Tables[0].Rows.Count; i++)
            {
                if (Math.Abs(y - Convert.ToSingle(r.Tables[0].Rows[i]["Value2"].ToString())) < Delta1)
                {
                    Delta2 = Delta1;
                    Delta1 = Math.Abs(y - Convert.ToSingle(r.Tables[0].Rows[i]["Value2"].ToString()));
                    Pcnt2 = Pcnt1;
                    Val2 = Val1;
                    Pcnt1 = Convert.ToSingle(r.Tables[0].Rows[i]["Value1"].ToString());
                    Val1 = Convert.ToSingle(r.Tables[0].Rows[i]["Value2"].ToString());
                }
                else if (Math.Abs(y - Convert.ToSingle(r.Tables[0].Rows[i]["Value2"].ToString())) < Delta2)
                {
                    Delta2 = Math.Abs(y - Convert.ToSingle(r.Tables[0].Rows[i]["Value2"].ToString()));
                    Val2 = Convert.ToSingle(r.Tables[0].Rows[i]["Value2"].ToString());
                    Pcnt2 = Convert.ToSingle(r.Tables[0].Rows[i]["Value1"].ToString());
                }

                if ((Delta1 == 0) || (Val1 == Val2) || (Math.Sign(Val1 - y) == Math.Sign(Val2 - y)))
                    GetDistX = Pcnt1;
                else
                    GetDistX = Pcnt1 - (Val1 - y) * (Pcnt1 - Pcnt2) / (Val1 - Val2);
            }
            return GetDistX;
        }
    }
}