﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sa.PresBuilder.console
{
    class Program
    {
        static void Main(string[] args)
        {
			//String filepath = @"C:\Development\PresentationFromFilename.pptx";
			//Sa.PresBuilder.Class1.CreatePresentation(filepath);

			//String filepath2 = @"C:\Development\PresentationFromFilenameWithChart.pptx";
			//Sa.PresBuilder.GeneratedClass.CreatePackage(filepath2);

			//String filepath3 = @"C:\Development\AsposeTest.pptx";
			//Sa.PresBuilder.class2.Page_Load(filepath3);

			//	String filepath4 = @"C:\Development\AsposeSW.pptx";
			//	Sa.PresBuilder.AsposeTest.CreatePres(filepath4);

			//String filepath4 = @"C:\Development\AsposeSW.pptx";
			//Sa.AsposePresBuilder.AsposeTest.CreatePres(filepath4);

			//String template = @"K:\STUDY\Power\2014\Presentations\Template\14 Power Template 5a.pptx";//the template to use
			//String filepath5 = @"C:\Development\AsposeSWFromTemplate.pptx";//where to save to
			//String option1 = @"Power14: ENEL Brayton";//the list to use
			//String option2 = @"Power14";//the parent list (used for comparisons)
			//String option3 = @"2014";//study year
			//String option4 = "";
			//String option5 = "";
			//String Currency = @"EUR";//what currency
			//Boolean metric = true;//metric or US units

			//and run it with all these
			//Sa.AsposePresBuilder.AsposeTest.CreatePresFromTemplate(template, filepath5, CompanyList, StudyList, StudyYear, Currency, metric);
			Sa.AsposePresBuilder.AsposeTest instance = new Sa.AsposePresBuilder.AsposeTest();
			
			instance.templateToUse = @"K:\STUDY\Power\2014\Presentations\Template\14 Power Template 5a.pptx";
			instance.filenameToSaveAs = @"C:\Development\AsposeSWFromTemplate.pptx";
			instance.Option1 = @"Power14: ENEL Brayton";//the list to use
			instance.Option2 = @"Power14";//the parent list (used for comparisons)
			instance.Option3 = @"2014";//study year
			instance.Option4 = "";
			instance.Option5 = "";
			instance.CurrencyToUse = @"EUR";
			instance.Metric = true;
            instance.server = "DBS1";
            instance.database = "Power";


			//instance.CreatePresFromTemplate(template, filepath5, option1, option2, option3, option4, option5, Currency, metric);
			instance.CreatePresFromTemplate();
			
			//Sa.AsposePresBuilder.AsposeTest.ChartTest();
			//Sa.PresTest.Test.ChartTest2();

        }
    }
}
