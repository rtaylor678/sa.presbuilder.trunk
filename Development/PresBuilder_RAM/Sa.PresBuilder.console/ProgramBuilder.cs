﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Sa.Pres;

namespace Sa.PresBuilder
{
	class ProgramBuilder
	{
		static void Main(string[] args)
		{
			string pptx = @"C:\Development\PresentationFromFilename.pptx";
			Builder.CreatePresentation(pptx);

			string docx = @"C:\Development\DocumentFromFilename.docx";
			Builder.HelloWorld(docx);

			Console.WriteLine("Press any key to continue");
			Console.ReadKey();
		}
	}
}