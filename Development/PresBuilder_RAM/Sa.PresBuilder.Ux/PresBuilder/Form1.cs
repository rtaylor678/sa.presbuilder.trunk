﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Microsoft.Office.Core;
using ppt = Microsoft.Office.Interop.PowerPoint;
using SQL = System.Data.SqlClient;




namespace PresBuilder
{
	public partial class Form1 : Form
	{
        public String ConnectionString = frmDBLogin.connectionString;
        Int32 workingSlide;
        String ErrorMsg;

        string curParmID = string.Empty;
        string LatestParmID = string.Empty;
        string OldestParmID = string.Empty;
        System.Data.DataTable parmTable_All;

        public Form1()
		{
			InitializeComponent();
			this.saveLocation.Text = Properties.Settings.Default.SaveTo;
			this.TemplateToUse.Text = Properties.Settings.Default.Template;
			this.Option1.Text = Properties.Settings.Default.Option1;
			this.Option2.Text = Properties.Settings.Default.Option2;
			this.Option3.Text = Properties.Settings.Default.Option3;
			this.Option4.Text = Properties.Settings.Default.Option4;
			this.Option5.Text = Properties.Settings.Default.Option5;
            this.Option8.Text = Properties.Settings.Default.Option8;
            this.currencyCode.Text = Properties.Settings.Default.Currency;
			if (Properties.Settings.Default.USUnits == true)
			{
				this.rdoUSUnits.Checked = true;
			}
			else
			{
				this.rdoMetricUnits.Checked = true;
			}
            this.option1label.Text = Properties.Settings.Default.Option1Name;
            this.option2label.Text = Properties.Settings.Default.Option2Name;
            this.option3label.Text = Properties.Settings.Default.Option3Name;
            this.option4label.Text = Properties.Settings.Default.Option4Name;
            this.option5label.Text = Properties.Settings.Default.Option5Name;
            this.option8label.Text = Properties.Settings.Default.Option8Name;
            this.chkOpenPres.Checked = Properties.Settings.Default.OpenPres;

			this.lblVersion.Text = Application.ProductVersion;
        }

		private void Form1_Load(object sender, EventArgs e)
		{
            fillSlideGroupComboBox();
            InitializeParmScrollButtons();
        }

  
        private void InitializeParmScrollButtons()
        {
            
            int ParmRowCount =0;

            DataTable parmTable = getCurrentParmDataTable();

            if (parmTable.Rows.Count > 0)
            {
                curParmID = parmTable.Rows[0]["ID"].ToString();
            }

            parmTable_All = getParmDataTable_All();

            ParmRowCount = parmTable_All.Rows.Count;
            if (ParmRowCount > 0)
            {
                LatestParmID = parmTable_All.Rows[0]["ID"].ToString();
                OldestParmID = parmTable_All.Rows[ParmRowCount - 1]["ID"].ToString();
                if (OldestParmID == LatestParmID)
                {
                    if (curParmID != string.Empty)
                    {
                        btnScrollForward.Enabled = false;
                        btnScrollBack.Enabled = false;
                    }
                    else
                    {
                        btnScrollForward.Enabled = true; //only one record
                        btnScrollBack.Enabled = false;
                    }
                }
                else //two more records
                {
                    if (curParmID != string.Empty || curParmID != "")
                    {
                        if(Convert.ToInt16(curParmID) < Convert.ToInt16(LatestParmID) && Convert.ToInt16(curParmID) > Convert.ToInt16(OldestParmID)) 
                        {
                            btnScrollForward.Enabled = true;
                            btnScrollBack.Enabled = true;
                        }
                        else if (Convert.ToInt16(curParmID) == Convert.ToInt16(LatestParmID))
                        {
                            btnScrollForward.Enabled = false;
                            btnScrollBack.Enabled = true;
                        }
                        else if (Convert.ToInt16(curParmID) == Convert.ToInt16(OldestParmID))
                        {
                            btnScrollForward.Enabled = true;
                            btnScrollBack.Enabled = false;
                        }
                    }
                    else
                    {
                        btnScrollForward.Enabled = true; 
                        btnScrollBack.Enabled = true;
                    }
                }
                txtPBID.Text = curParmID;
            }
            //else //no saved parameters
            //{
            //    btnScrollBack.Enabled = false;
            //    btnScrollForward.Enabled = false;
            //    MessageBox.Show("You don't have screen parameters saved.");
            //}
        }

        private System.Data.DataTable getParmDataTable_All()
        {
            string sqlQuery = "Select * from PBHistory Order by ID Desc";
            using (var conn = new SQL.SqlConnection(ConnectionString))
            {
                using (var wkgCommand = new SQL.SqlCommand(sqlQuery, conn))
                {
                    conn.Open();
                    System.Data.DataTable Parm_Table = new System.Data.DataTable();
                    SQL.SqlDataAdapter parmAdapter = new SQL.SqlDataAdapter(wkgCommand);
                    parmAdapter.SelectCommand.CommandTimeout = 0;
                    Parm_Table.Clear();
                    parmAdapter.Fill(Parm_Table);
                    return Parm_Table;
                }
            }
        }
        private System.Data.DataTable getCurrentParmDataTable()
        {
            string sqlQuery = "Select * from PBHistory where tSaveTo='"
                + saveLocation.Text.Trim() + "' And " +
                                                          "tTemplate='" + TemplateToUse.Text.Trim() + "' And " +
                                                          "tOption1='" + Option1.Text.Trim() + "' And " +
                                                          "tOption2='" + Option2.Text.Trim() + "' And " +
                                                          "tOption3='" + Option3.Text.Trim() + "' And " +
                                                          "tOption4='" + Option4.Text.Trim() + "' And " +
                                                          "tOption5='" + Option5.Text.Trim() + "' And " +
                                                          "tOption8='" + Option8.Text.Trim() + "' And " +
                                                          "tcurrency='" + currencyCode.Text.Trim() + "' And " +
                                                          "tUnits='" + rdoMetricUnits.Checked + "' And " +
                                                          "tSlideID='" + txtSlideID.Text.Trim() + "'" ;
            if (cboSlideGroup.SelectedValue != null)
                sqlQuery = sqlQuery + " And " + "tSGroup='" + cboSlideGroup.SelectedValue.ToString() + "'";
            else
                sqlQuery = sqlQuery + " And " + "tServer='" + frmDBLogin.Curserver + "' And " + "tDatabase='" + frmDBLogin.Curdatabase + "' And " + "tOpen='" + chkOpenPres.Checked + "'";
            SQL.SqlConnection conn;
            conn = new SQL.SqlConnection(ConnectionString);
             conn.Open();
            SQL.SqlCommand wkgCommand = new SQL.SqlCommand(sqlQuery, conn);
            System.Data.DataTable currentParm_Table = new System.Data.DataTable();
            SQL.SqlDataAdapter parmAdapter = new SQL.SqlDataAdapter(wkgCommand);
            parmAdapter.SelectCommand.CommandTimeout = 0;
            currentParm_Table.Clear();
            parmAdapter.Fill(currentParm_Table);
            conn.Close();
            return currentParm_Table;
        }
        private void fillSlideGroupComboBox()
        {
            //Fill in the Slide Group combo box from SlideGroup_LU table
            SQL.SqlConnection conn;
            conn = new SQL.SqlConnection(ConnectionString);
            conn.Open();
            SQL.SqlCommand wkgCommand = new SQL.SqlCommand("Select GroupNoID, GroupName from SlideGroup_LU order by GroupNoID", conn);
            System.Data.DataTable slideGroup_LUTable = new System.Data.DataTable();
            SQL.SqlDataAdapter dapSlides = new SQL.SqlDataAdapter(wkgCommand);
            dapSlides.SelectCommand.CommandTimeout = 0;
            slideGroup_LUTable.Clear();
            dapSlides.Fill(slideGroup_LUTable);
            conn.Close();
            cboSlideGroup.ValueMember = "GroupNoID";
            cboSlideGroup.DisplayMember = "GroupName";
            cboSlideGroup.DataSource = slideGroup_LUTable;
            cboSlideGroup.SelectedIndex = cboSlideGroup.FindString("All");
        }
		private void button2_Click(object sender, EventArgs e)
		{
			saveFileDialog1.Filter = "Powerpoint (*.pptx)|*.pptx|All files (*.*)|*.*";
			if (saveFileDialog1.ShowDialog() == DialogResult.OK)
			{
				saveLocation.Text = saveFileDialog1.FileName;
			}
			
		}

		private void changeTemplate_Click(object sender, EventArgs e)
		{
			openFileDialog1.Filter = "Powerpoint (*.pptx)|*.pptx|All files (*.*)|*.*";
			if (openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				TemplateToUse.Text = openFileDialog1.FileName;
			}

		}

		private void button1_Click(object sender, EventArgs e)
		{
            errorLabel.Text = "";//just reset it

			//check for a valid save location
			String filepath = saveLocation.Text;//where to save to
			String fileResult = CheckFileName(filepath);

			if (fileResult != "")
			{
				errorLabel.Text = fileResult;
				return;
			}

			//if the file path doesn't end with .pptx we append it here (after checking the rest for validity)
			//may end up with something like .ppt.pptx but that's okay, that's user error and we can't catch everything
			if (!filepath.EndsWith(".pptx"))
			{
				saveLocation.Text = saveLocation.Text + ".pptx";
				filepath = saveLocation.Text;
			}

			//check for a valid template file
			String template = TemplateToUse.Text;//the template to use

			fileResult = CheckFileExists(template, "Template");
			if (fileResult != "")
			{
				errorLabel.Text = fileResult;
				return;
			}

			errorLabel.Text = "Presentation started";

			btnRunPresentation.Enabled = false;

			//create the object and populate it with data from the form

			Sa.PresBuilder.PresBuilder instance = new Sa.PresBuilder.PresBuilder();
			instance.templateToUse = TemplateToUse.Text;
			instance.filenameToSaveAs = saveLocation.Text;
			instance.Option1 = Option1.Text;
			instance.Option2 = Option2.Text;
			instance.Option3 = Option3.Text;
			instance.Option4 = Option4.Text;
			instance.Option5 = Option5.Text;
            instance.Option8 = Option8.Text;
            instance.CurrencyToUse = currencyCode.Text;
			instance.Metric = rdoMetricUnits.Checked;
			instance.server = frmDBLogin.Curserver;
			instance.database = frmDBLogin.Curdatabase;
            instance.slideid = txtSlideID.Text;
            instance.slidegroupid = cboSlideGroup.SelectedValue.ToString();

            //then run it as a background worker (so we can display the progress)
            backgroundWorker1.RunWorkerAsync(instance);


        }

        
        
        private String CheckFileName(String fileName, String source = "")
		{
			//just needs to be a name we can save to, whether it exists or not
			System.IO.FileInfo fi = null;
			try
			{
				fi = new System.IO.FileInfo(fileName);
			}
			catch (Exception) { return "ERROR: " + source + " filename is not valid."; }

			if (fi.Name == "")
			{
				return "ERROR: " + source + " filename is not valid.";
			}

			if (ReferenceEquals(fi, null))
			{
				// file name is not valid
				return "ERROR: " + source + " filename is not valid.";
			}
				
			// file name is valid
			return "";

		}

		private String CheckFileExists(String fileName, String source = "")
		{
			//must exist already
			System.IO.FileInfo fi = null;
			try
			{
				fi = new System.IO.FileInfo(fileName);
			}
			catch (Exception) { return "ERROR: " + source + " filename is not valid."; }

			if (fi.Exists == false)
			{
				return "ERROR: " + source + " file does not exist.";
			}

			//file exists
			return "";
		}

		private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
            btnRunPresentation.Enabled = true;
            if (e.Error != null)
            {
				errorLabel.Text = "Slide " + (workingSlide + 1).ToString() + " error: " + e.Error.Message + e.Error.TargetSite + Environment.NewLine + ErrorMsg.Replace("|", "");
            }
            else
            {
                errorLabel.Text = "Complete";
				if (this.chkOpenPres.Checked == true)
				{
					ppt.Application app = new ppt.Application();
					ppt.Presentations pres = app.Presentations;
					try
					{
						pres.Open(this.saveLocation.Text, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoTrue);
					}
					catch (System.Runtime.InteropServices.COMException)
					{
						errorLabel.Text = "Program completed, but there was an error in the generated PPT.";
					}
				}
            }
		}

		private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{

			Sa.PresBuilder.PresBuilder.CurrentState state = (Sa.PresBuilder.PresBuilder.CurrentState)e.UserState;
            ErrorMsg = state.errMsg;

            if (state.status == "")
			{
				errorLabel.Text = (state.workingSlideNumber+1).ToString() + " of " + state.totalSlides.ToString();
				workingSlide = state.workingSlideNumber;
			}
			else
			{
                errorLabel.Text = state.status;
			}

		}

		private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
		{
			System.ComponentModel.BackgroundWorker worker;
			worker = (System.ComponentModel.BackgroundWorker)sender;

			Sa.PresBuilder.PresBuilder wkg = (Sa.PresBuilder.PresBuilder)e.Argument;
			wkg.CreatePresFromTemplate(worker, e);

		}

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{

			//save all the settings
			Properties.Settings.Default.SaveTo = this.saveLocation.Text;
			Properties.Settings.Default.Template = this.TemplateToUse.Text;
			Properties.Settings.Default.Option1 = this.Option1.Text;
			Properties.Settings.Default.Option2 = this.Option2.Text;
			Properties.Settings.Default.Option3 = this.Option3.Text;
			Properties.Settings.Default.Option4 = this.Option4.Text;
			Properties.Settings.Default.Option5 = this.Option5.Text;
            Properties.Settings.Default.Option8 = this.Option8.Text;
            Properties.Settings.Default.Currency = this.currencyCode.Text;
            Properties.Settings.Default.ServerName = frmDBLogin.Curserver;
            Properties.Settings.Default.DBName = frmDBLogin.Curdatabase;
            Properties.Settings.Default.UserName = frmDBLogin.DbUserName;
            Properties.Settings.Default.Password = frmDBLogin.DbPassword;
            Properties.Settings.Default.USUnits = this.rdoUSUnits.Checked;//because we're tracking if the USUnits are checked or not

            Properties.Settings.Default.Option1Name = this.option1label.Text;
            Properties.Settings.Default.Option2Name = this.option2label.Text;
            Properties.Settings.Default.Option3Name = this.option3label.Text;
            Properties.Settings.Default.Option4Name = this.option4label.Text;
            Properties.Settings.Default.Option5Name = this.option5label.Text;
            Properties.Settings.Default.Option8Name = this.option8label.Text;
            Properties.Settings.Default.OpenPres = this.chkOpenPres.Checked;
           

            Properties.Settings.Default.Save();

        }



        private void btnSaveScreenParms_Click(object sender, EventArgs e)
        {
            SQL.SqlConnection conn;
            conn = new SQL.SqlConnection(ConnectionString);
            string sqlString = "insert into PBHistory (tSaveTo, tTemplate, tOption1, tOption2, tOption3, tOption4, tOption5, tOption8, tcurrency, tUnits, tSlideID, tSGroup, tServer, tDatabase, tOpen) values (" +
                                                "'" + saveLocation.Text.Trim() + "', '" +
                                                    TemplateToUse.Text.Trim() + "', '" +
                                                    Option1.Text.Trim() + "', '" +
                                                    Option2.Text.Trim() + "', '" +
                                                    Option3.Text.Trim() + "', '" +
                                                    Option4.Text.Trim() + "', '" +
                                                    Option5.Text.Trim() + "', '" +
                                                    Option8.Text.Trim() + "', '" +
                                                    currencyCode.Text.Trim() + "', '" +
                                                    rdoMetricUnits.Checked + "', '" +
                                                    txtSlideID.Text.Trim() + "', '" +
                                                    cboSlideGroup.SelectedValue.ToString() + "', '" +
                                                    frmDBLogin.Curserver + "', '" +
                                                    frmDBLogin.Curdatabase + "', '" +
                                                    chkOpenPres.Checked +  "')";
            try
            {
                conn.Open();
                SQL.SqlCommand wkgCommand = new SQL.SqlCommand(sqlString, conn);
                wkgCommand.ExecuteNonQuery();
                MessageBox.Show("Screen parameters saved", "PresBuilder", MessageBoxButtons.OK);
                InitializeParmScrollButtons();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error on inserting parameters to database: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }



        private void btnScrollBack_Click(object sender, EventArgs e)
        {
            DataView parmView = new DataView(parmTable_All);
            if (curParmID != string.Empty || curParmID != "")
                parmView.RowFilter = "ID < " + curParmID;
            else { 
                curParmID = LatestParmID;
                txtPBID.Text = curParmID;
            }
            parmView.Sort = "ID DESC";

            System.Data.DataTable backParmTable = parmView.ToTable();
            if (backParmTable.Rows.Count > 0)
            {
                if (curParmID != backParmTable.Rows[0]["ID"].ToString())
                {
                    curParmID = backParmTable.Rows[0]["ID"].ToString();
                    txtPBID.Text = curParmID;
                }
                setScreenParms(backParmTable.Rows[0]);
                if (curParmID == OldestParmID)
                {
                    btnScrollBack.Enabled = false;
                    btnScrollForward.Enabled = true;
                } else
                {
                    btnScrollBack.Enabled = true;
                    btnScrollForward.Enabled = true;
                }
            }
        }


         private void setScreenParms(DataRow curParmTableRows)
        {
            this.saveLocation.Text = curParmTableRows["tSaveTo"].ToString();
            this.TemplateToUse.Text = curParmTableRows["tTemplate"].ToString();
            this.Option1.Text = curParmTableRows["tOption1"].ToString();
            this.Option2.Text = curParmTableRows["tOption2"].ToString();
            this.Option3.Text = curParmTableRows["tOption3"].ToString();
            this.Option4.Text = curParmTableRows["tOption4"].ToString();
            this.Option5.Text = curParmTableRows["tOption5"].ToString();
            this.Option8.Text = curParmTableRows["tOption8"].ToString();
            this.currencyCode.Text = curParmTableRows["tcurrency"].ToString();
            this.rdoMetricUnits.Checked = Convert.ToBoolean(curParmTableRows["tUnits"]);
            this.txtSlideID.Text = curParmTableRows["tSlideID"].ToString();
            this.cboSlideGroup.SelectedValue = curParmTableRows["tSGroup"].ToString();
            this.chkOpenPres.Checked = Convert.ToBoolean(curParmTableRows["tOpen"]);
        }

        private void btnScrollForward_Click(object sender, EventArgs e)
        {
            DataView parmView = new DataView(parmTable_All);
            if (curParmID != string.Empty || curParmID != "")
                parmView.RowFilter = "ID > " + curParmID;
            else { 
                curParmID = OldestParmID;
                txtPBID.Text = curParmID;
            }
            parmView.Sort = "ID";

            System.Data.DataTable ForwardParmTable = parmView.ToTable();
            if (ForwardParmTable.Rows.Count > 0)
            {
                if (curParmID != ForwardParmTable.Rows[0]["ID"].ToString())
                {
                    curParmID = ForwardParmTable.Rows[0]["ID"].ToString();
                    txtPBID.Text = curParmID;
                }
                setScreenParms(ForwardParmTable.Rows[0]);
                if (curParmID == LatestParmID)
                {
                    btnScrollBack.Enabled = true;
                    btnScrollForward.Enabled = false;
                }
            }
            
        }

        private void txtPBID_TextChanged(object sender, EventArgs e)
        {
            DataView parmView = new DataView(parmTable_All);
            if (txtPBID.Text.Trim() != string.Empty || txtPBID.Text.Trim() != "")
                parmView.RowFilter = "ID = " + txtPBID.Text.Trim();
            else { 
                MessageBox.Show("Please eneter an ID number", "PresBuilder", MessageBoxButtons.OK);
                return;
            }
            parmView.Sort = "ID";

            System.Data.DataTable TextParmTable = parmView.ToTable();
            if (TextParmTable.Rows.Count > 0)
            {
                if (curParmID != TextParmTable.Rows[0]["ID"].ToString())
                {
                    curParmID = TextParmTable.Rows[0]["ID"].ToString();
                    txtPBID.Text = curParmID;
                }
                setScreenParms(TextParmTable.Rows[0]);
                if (curParmID == LatestParmID)
                {
                    btnScrollBack.Enabled = true;
                    btnScrollForward.Enabled = false;
                }
                else if (curParmID == OldestParmID)
                {
                    btnScrollBack.Enabled = false;
                    btnScrollForward.Enabled = true;
                }
                else
                {
                    btnScrollBack.Enabled = true;
                    btnScrollForward.Enabled = true;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtPBID.Text.Trim() != string.Empty || txtPBID.Text.Trim() != "")
            {
                SQL.SqlConnection conn;
                conn = new SQL.SqlConnection(ConnectionString);
                string sqlString = "delete PBHistory where ID = " + txtPBID.Text.Trim();
                try
                {
                    conn.Open();
                    SQL.SqlCommand wkgCommand = new SQL.SqlCommand(sqlString, conn);
                    wkgCommand.ExecuteNonQuery();
                    MessageBox.Show("Screen parameters deleted", "PresBuilder", MessageBoxButtons.OK);
                    if (OldestParmID != string.Empty)
                    {
                        curParmID = OldestParmID;
                    }
                    else if (LatestParmID != string.Empty)
                    {
                        curParmID = LatestParmID;
                    }
                    InitializeParmScrollButtons();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error on deleting parameters to database: " + ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                MessageBox.Show("Please eneter an PBID number", "PresBuilder", MessageBoxButtons.OK);
                return;
            }

        }

    }
}
