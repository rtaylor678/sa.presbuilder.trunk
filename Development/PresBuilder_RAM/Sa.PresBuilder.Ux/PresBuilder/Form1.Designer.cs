﻿namespace PresBuilder
	{
	partial class Form1
		{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
			{
			if (disposing && (components != null))
				{
				components.Dispose();
				}
			base.Dispose(disposing);
			}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
			{
            this.label1 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rdoUSUnits = new System.Windows.Forms.RadioButton();
            this.rdoMetricUnits = new System.Windows.Forms.RadioButton();
            this.btnRunPresentation = new System.Windows.Forms.Button();
            this.changeSaveLocation = new System.Windows.Forms.Button();
            this.saveLocation = new System.Windows.Forms.TextBox();
            this.changeTemplate = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.TemplateToUse = new System.Windows.Forms.TextBox();
            this.Option1 = new System.Windows.Forms.TextBox();
            this.Option2 = new System.Windows.Forms.TextBox();
            this.Option3 = new System.Windows.Forms.TextBox();
            this.currencyCode = new System.Windows.Forms.TextBox();
            this.errorLabel = new System.Windows.Forms.Label();
            this.Option4 = new System.Windows.Forms.TextBox();
            this.Option5 = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.option1label = new System.Windows.Forms.TextBox();
            this.option2label = new System.Windows.Forms.TextBox();
            this.option3label = new System.Windows.Forms.TextBox();
            this.option4label = new System.Windows.Forms.TextBox();
            this.option5label = new System.Windows.Forms.TextBox();
            this.chkOpenPres = new System.Windows.Forms.CheckBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.pnlSlideID = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPBID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnScrollForward = new System.Windows.Forms.Button();
            this.btnScrollBack = new System.Windows.Forms.Button();
            this.btnSaveScreenParms = new System.Windows.Forms.Button();
            this.cboSlideGroup = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSlideID = new System.Windows.Forms.TextBox();
            this.lblSlideID = new System.Windows.Forms.Label();
            this.Option8 = new System.Windows.Forms.TextBox();
            this.option8label = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlSlideID.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 25);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Save To:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Template To Use:";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 374);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Currency:";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 425);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 25);
            this.label7.TabIndex = 6;
            this.label7.Text = "US or Metric:";
            // 
            // rdoUSUnits
            // 
            this.rdoUSUnits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rdoUSUnits.AutoSize = true;
            this.rdoUSUnits.Checked = true;
            this.rdoUSUnits.Location = new System.Drawing.Point(156, 422);
            this.rdoUSUnits.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoUSUnits.Name = "rdoUSUnits";
            this.rdoUSUnits.Size = new System.Drawing.Size(107, 29);
            this.rdoUSUnits.TabIndex = 11;
            this.rdoUSUnits.TabStop = true;
            this.rdoUSUnits.Text = "US units";
            this.rdoUSUnits.UseVisualStyleBackColor = true;
            // 
            // rdoMetricUnits
            // 
            this.rdoMetricUnits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rdoMetricUnits.AutoSize = true;
            this.rdoMetricUnits.Location = new System.Drawing.Point(256, 422);
            this.rdoMetricUnits.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rdoMetricUnits.Name = "rdoMetricUnits";
            this.rdoMetricUnits.Size = new System.Drawing.Size(132, 29);
            this.rdoMetricUnits.TabIndex = 12;
            this.rdoMetricUnits.TabStop = true;
            this.rdoMetricUnits.Text = "Metric units";
            this.rdoMetricUnits.UseVisualStyleBackColor = true;
            // 
            // btnRunPresentation
            // 
            this.btnRunPresentation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRunPresentation.BackColor = System.Drawing.Color.SpringGreen;
            this.btnRunPresentation.Location = new System.Drawing.Point(705, 445);
            this.btnRunPresentation.Name = "btnRunPresentation";
            this.btnRunPresentation.Size = new System.Drawing.Size(285, 52);
            this.btnRunPresentation.TabIndex = 17;
            this.btnRunPresentation.Text = "Run Presentation";
            this.btnRunPresentation.UseVisualStyleBackColor = false;
            this.btnRunPresentation.Click += new System.EventHandler(this.button1_Click);
            // 
            // changeSaveLocation
            // 
            this.changeSaveLocation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.changeSaveLocation.Location = new System.Drawing.Point(810, 20);
            this.changeSaveLocation.Name = "changeSaveLocation";
            this.changeSaveLocation.Size = new System.Drawing.Size(180, 31);
            this.changeSaveLocation.TabIndex = 2;
            this.changeSaveLocation.Text = "Change Save Location";
            this.changeSaveLocation.UseVisualStyleBackColor = true;
            this.changeSaveLocation.Click += new System.EventHandler(this.button2_Click);
            // 
            // saveLocation
            // 
            this.saveLocation.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.saveLocation.Location = new System.Drawing.Point(156, 16);
            this.saveLocation.Multiline = true;
            this.saveLocation.Name = "saveLocation";
            this.saveLocation.Size = new System.Drawing.Size(637, 47);
            this.saveLocation.TabIndex = 1;
            this.saveLocation.Text = "K:\\STUDY\\RAM\\2016\\Presentations\\INEOS\\RAM16_INEOS_09Aug17.pptx";
            // 
            // changeTemplate
            // 
            this.changeTemplate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.changeTemplate.Location = new System.Drawing.Point(810, 70);
            this.changeTemplate.Name = "changeTemplate";
            this.changeTemplate.Size = new System.Drawing.Size(180, 31);
            this.changeTemplate.TabIndex = 4;
            this.changeTemplate.Text = "Change Template";
            this.changeTemplate.UseVisualStyleBackColor = true;
            this.changeTemplate.Click += new System.EventHandler(this.changeTemplate_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // TemplateToUse
            // 
            this.TemplateToUse.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TemplateToUse.Location = new System.Drawing.Point(156, 69);
            this.TemplateToUse.Multiline = true;
            this.TemplateToUse.Name = "TemplateToUse";
            this.TemplateToUse.Size = new System.Drawing.Size(637, 47);
            this.TemplateToUse.TabIndex = 3;
            this.TemplateToUse.Text = "K:\\STUDY\\RAM\\2016\\Template\\RAM16Templatereviewed.pptm";
            // 
            // Option1
            // 
            this.Option1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option1.Location = new System.Drawing.Point(156, 122);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(456, 30);
            this.Option1.TabIndex = 5;
            this.Option1.Text = "Power16: Ineos";
            // 
            // Option2
            // 
            this.Option2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option2.Location = new System.Drawing.Point(156, 173);
            this.Option2.Name = "Option2";
            this.Option2.Size = new System.Drawing.Size(456, 30);
            this.Option2.TabIndex = 6;
            // 
            // Option3
            // 
            this.Option3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option3.Location = new System.Drawing.Point(156, 222);
            this.Option3.Name = "Option3";
            this.Option3.Size = new System.Drawing.Size(456, 30);
            this.Option3.TabIndex = 7;
            this.Option3.Text = "2016";
            // 
            // currencyCode
            // 
            this.currencyCode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.currencyCode.Location = new System.Drawing.Point(156, 373);
            this.currencyCode.Name = "currencyCode";
            this.currencyCode.Size = new System.Drawing.Size(164, 30);
            this.currencyCode.TabIndex = 10;
            this.currencyCode.Text = "USD";
            // 
            // errorLabel
            // 
            this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorLabel.ForeColor = System.Drawing.Color.Red;
            this.errorLabel.Location = new System.Drawing.Point(9, 513);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(981, 89);
            this.errorLabel.TabIndex = 18;
            // 
            // Option4
            // 
            this.Option4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option4.Location = new System.Drawing.Point(156, 273);
            this.Option4.Name = "Option4";
            this.Option4.Size = new System.Drawing.Size(456, 30);
            this.Option4.TabIndex = 8;
            // 
            // Option5
            // 
            this.Option5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option5.Location = new System.Drawing.Point(156, 322);
            this.Option5.Name = "Option5";
            this.Option5.Size = new System.Drawing.Size(456, 30);
            this.Option5.TabIndex = 9;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // option1label
            // 
            this.option1label.BackColor = System.Drawing.Color.LightSkyBlue;
            this.option1label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option1label.Location = new System.Drawing.Point(19, 125);
            this.option1label.Name = "option1label";
            this.option1label.Size = new System.Drawing.Size(121, 23);
            this.option1label.TabIndex = 31;
            this.option1label.Text = "Option 1:";
            // 
            // option2label
            // 
            this.option2label.BackColor = System.Drawing.Color.LightSkyBlue;
            this.option2label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option2label.Location = new System.Drawing.Point(19, 176);
            this.option2label.Name = "option2label";
            this.option2label.Size = new System.Drawing.Size(124, 23);
            this.option2label.TabIndex = 32;
            this.option2label.Text = "Option 2:";
            // 
            // option3label
            // 
            this.option3label.BackColor = System.Drawing.Color.LightSkyBlue;
            this.option3label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option3label.Location = new System.Drawing.Point(19, 225);
            this.option3label.Name = "option3label";
            this.option3label.Size = new System.Drawing.Size(124, 23);
            this.option3label.TabIndex = 33;
            this.option3label.Text = "Option 3:";
            // 
            // option4label
            // 
            this.option4label.BackColor = System.Drawing.Color.LightSkyBlue;
            this.option4label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option4label.Location = new System.Drawing.Point(19, 276);
            this.option4label.Name = "option4label";
            this.option4label.Size = new System.Drawing.Size(124, 23);
            this.option4label.TabIndex = 34;
            this.option4label.Text = "Option 4:";
            // 
            // option5label
            // 
            this.option5label.BackColor = System.Drawing.Color.LightSkyBlue;
            this.option5label.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.option5label.Location = new System.Drawing.Point(19, 325);
            this.option5label.Name = "option5label";
            this.option5label.Size = new System.Drawing.Size(124, 23);
            this.option5label.TabIndex = 35;
            this.option5label.Text = "Option 5:";
            // 
            // chkOpenPres
            // 
            this.chkOpenPres.AutoSize = true;
            this.chkOpenPres.Location = new System.Drawing.Point(705, 375);
            this.chkOpenPres.Name = "chkOpenPres";
            this.chkOpenPres.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkOpenPres.Size = new System.Drawing.Size(269, 29);
            this.chkOpenPres.TabIndex = 36;
            this.chkOpenPres.Text = "Open Pres when Complete";
            this.chkOpenPres.UseVisualStyleBackColor = true;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.Location = new System.Drawing.Point(9, 570);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(100, 13);
            this.lblVersion.TabIndex = 37;
            this.lblVersion.Visible = false;
            // 
            // pnlSlideID
            // 
            this.pnlSlideID.BackColor = System.Drawing.SystemColors.Control;
            this.pnlSlideID.Controls.Add(this.btnDelete);
            this.pnlSlideID.Controls.Add(this.label4);
            this.pnlSlideID.Controls.Add(this.txtPBID);
            this.pnlSlideID.Controls.Add(this.label3);
            this.pnlSlideID.Controls.Add(this.btnScrollForward);
            this.pnlSlideID.Controls.Add(this.btnScrollBack);
            this.pnlSlideID.Controls.Add(this.btnSaveScreenParms);
            this.pnlSlideID.Location = new System.Drawing.Point(705, 122);
            this.pnlSlideID.Name = "pnlSlideID";
            this.pnlSlideID.Size = new System.Drawing.Size(285, 130);
            this.pnlSlideID.TabIndex = 41;
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(3, 98);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(121, 29);
            this.btnDelete.TabIndex = 48;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(109, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 20);
            this.label4.TabIndex = 47;
            this.label4.Text = "PBID";
            // 
            // txtPBID
            // 
            this.txtPBID.Location = new System.Drawing.Point(88, 67);
            this.txtPBID.Name = "txtPBID";
            this.txtPBID.Size = new System.Drawing.Size(110, 30);
            this.txtPBID.TabIndex = 46;
            this.txtPBID.TextChanged += new System.EventHandler(this.txtPBID_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(66, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 20);
            this.label3.TabIndex = 45;
            this.label3.Text = "Parms Management";
            // 
            // btnScrollForward
            // 
            this.btnScrollForward.Location = new System.Drawing.Point(217, 68);
            this.btnScrollForward.Name = "btnScrollForward";
            this.btnScrollForward.Size = new System.Drawing.Size(64, 29);
            this.btnScrollForward.TabIndex = 44;
            this.btnScrollForward.Text = ">>";
            this.btnScrollForward.UseVisualStyleBackColor = true;
            this.btnScrollForward.Click += new System.EventHandler(this.btnScrollForward_Click);
            // 
            // btnScrollBack
            // 
            this.btnScrollBack.Location = new System.Drawing.Point(3, 68);
            this.btnScrollBack.Name = "btnScrollBack";
            this.btnScrollBack.Size = new System.Drawing.Size(64, 29);
            this.btnScrollBack.TabIndex = 43;
            this.btnScrollBack.Text = "<<";
            this.btnScrollBack.UseVisualStyleBackColor = true;
            this.btnScrollBack.Click += new System.EventHandler(this.btnScrollBack_Click);
            // 
            // btnSaveScreenParms
            // 
            this.btnSaveScreenParms.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSaveScreenParms.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveScreenParms.Location = new System.Drawing.Point(164, 98);
            this.btnSaveScreenParms.Name = "btnSaveScreenParms";
            this.btnSaveScreenParms.Size = new System.Drawing.Size(117, 29);
            this.btnSaveScreenParms.TabIndex = 42;
            this.btnSaveScreenParms.Text = "Save";
            this.btnSaveScreenParms.UseVisualStyleBackColor = true;
            this.btnSaveScreenParms.Click += new System.EventHandler(this.btnSaveScreenParms_Click);
            // 
            // cboSlideGroup
            // 
            this.cboSlideGroup.FormattingEnabled = true;
            this.cboSlideGroup.Location = new System.Drawing.Point(132, 62);
            this.cboSlideGroup.Name = "cboSlideGroup";
            this.cboSlideGroup.Size = new System.Drawing.Size(135, 33);
            this.cboSlideGroup.TabIndex = 41;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 65);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 25);
            this.label10.TabIndex = 40;
            this.label10.Text = "Slide Group:";
            // 
            // txtSlideID
            // 
            this.txtSlideID.Location = new System.Drawing.Point(133, 14);
            this.txtSlideID.Name = "txtSlideID";
            this.txtSlideID.Size = new System.Drawing.Size(135, 30);
            this.txtSlideID.TabIndex = 39;
            // 
            // lblSlideID
            // 
            this.lblSlideID.AutoSize = true;
            this.lblSlideID.Location = new System.Drawing.Point(3, 14);
            this.lblSlideID.Name = "lblSlideID";
            this.lblSlideID.Size = new System.Drawing.Size(86, 25);
            this.lblSlideID.TabIndex = 38;
            this.lblSlideID.Text = "Slide ID:";
            // 
            // Option8
            // 
            this.Option8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Option8.Location = new System.Drawing.Point(156, 467);
            this.Option8.Name = "Option8";
            this.Option8.Size = new System.Drawing.Size(456, 30);
            this.Option8.TabIndex = 43;
            // 
            // option8label
            // 
            this.option8label.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.option8label.AutoSize = true;
            this.option8label.Location = new System.Drawing.Point(14, 470);
            this.option8label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.option8label.Name = "option8label";
            this.option8label.Size = new System.Drawing.Size(92, 25);
            this.option8label.TabIndex = 42;
            this.option8label.Text = "Option 8:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.cboSlideGroup);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtSlideID);
            this.panel1.Controls.Add(this.lblSlideID);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(705, 249);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(285, 111);
            this.panel1.TabIndex = 44;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1034, 618);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Option8);
            this.Controls.Add(this.option8label);
            this.Controls.Add(this.pnlSlideID);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.chkOpenPres);
            this.Controls.Add(this.option5label);
            this.Controls.Add(this.option4label);
            this.Controls.Add(this.option3label);
            this.Controls.Add(this.option2label);
            this.Controls.Add(this.option1label);
            this.Controls.Add(this.Option5);
            this.Controls.Add(this.Option4);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.currencyCode);
            this.Controls.Add(this.Option3);
            this.Controls.Add(this.Option2);
            this.Controls.Add(this.Option1);
            this.Controls.Add(this.TemplateToUse);
            this.Controls.Add(this.changeTemplate);
            this.Controls.Add(this.saveLocation);
            this.Controls.Add(this.changeSaveLocation);
            this.Controls.Add(this.btnRunPresentation);
            this.Controls.Add(this.rdoMetricUnits);
            this.Controls.Add(this.rdoUSUnits);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(2);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PresBuilder";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlSlideID.ResumeLayout(false);
            this.pnlSlideID.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

			}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.RadioButton rdoUSUnits;
		private System.Windows.Forms.RadioButton rdoMetricUnits;
		private System.Windows.Forms.Button btnRunPresentation;
		private System.Windows.Forms.Button changeSaveLocation;
		private System.Windows.Forms.TextBox saveLocation;
		private System.Windows.Forms.Button changeTemplate;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.TextBox TemplateToUse;
		private System.Windows.Forms.TextBox Option1;
		private System.Windows.Forms.TextBox Option2;
		private System.Windows.Forms.TextBox Option3;
		private System.Windows.Forms.TextBox currencyCode;
		private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.TextBox Option4;
        private System.Windows.Forms.TextBox Option5;
		private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox option1label;
        private System.Windows.Forms.TextBox option2label;
        private System.Windows.Forms.TextBox option3label;
        private System.Windows.Forms.TextBox option4label;
        private System.Windows.Forms.TextBox option5label;
		private System.Windows.Forms.CheckBox chkOpenPres;
		private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Panel pnlSlideID;
        private System.Windows.Forms.TextBox txtSlideID;
        private System.Windows.Forms.Label lblSlideID;
        private System.Windows.Forms.ComboBox cboSlideGroup;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnScrollForward;
        private System.Windows.Forms.Button btnScrollBack;
        private System.Windows.Forms.Button btnSaveScreenParms;
        private System.Windows.Forms.TextBox Option8;
        private System.Windows.Forms.Label option8label;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPBID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDelete;
    }
	}

