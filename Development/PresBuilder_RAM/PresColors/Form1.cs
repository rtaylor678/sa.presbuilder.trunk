﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQL = System.Data.SqlClient;


namespace PresColors
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			LoadColors();
		}

		private void LoadColors()
		{

			String connString = "Server=DBS1;Database=GlobalDB;Trusted_Connection=True; ";
			SQL.SqlConnection conn = new SQL.SqlConnection(connString);
			conn.Open(); // opens the database connection

			System.Data.DataTable slideColorsTable = LoadDataTable(conn, "SELECT * FROM SlideColors ORDER BY SlideColorGroup, ColorOrder", "slideColorsTable");

			//get the distinct values in the first column
			DataView view = new DataView(slideColorsTable);
			String rowname = "SlideColorGroup";
			System.Data.DataTable distinctRows = view.ToTable(true, rowname);

			String colname = "ColorOrder";
			System.Data.DataTable distinctCols = view.ToTable(true, colname);

			dataGridView1.Columns.Add("Group", "Group");
			dataGridView1.Columns[0].Width = 50;
			dataGridView1.Columns.Add("ColorGroupName", "ColorGroupName");
			dataGridView1.Columns[1].Width = 200;
			//dataGridView1.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

			for (int c = 0; c < distinctCols.Rows.Count; c++)
			{
				dataGridView1.Columns.Add(distinctCols.Rows[c].ItemArray[0].ToString(), distinctCols.Rows[c].ItemArray[0].ToString());
			}

			for (int r = 0; r < distinctRows.Rows.Count; r++)
			{
				dataGridView1.Rows.Add();
			}

			Int32 columnSCG = slideColorsTable.Columns["SlideColorGroup"].Ordinal;
			Int32 columnCO = slideColorsTable.Columns["ColorOrder"].Ordinal;
			Int32 columnC = slideColorsTable.Columns["Color"].Ordinal;
			Int32 columnCGN = slideColorsTable.Columns["ColorGroupName"].Ordinal;

			foreach (System.Data.DataRow row in slideColorsTable.Rows)
			{
				dataGridView1.Rows[Convert.ToInt32(row.ItemArray[columnSCG].ToString()) - 1].Cells[0].Value = row.ItemArray[columnSCG].ToString();
				dataGridView1.Rows[Convert.ToInt32(row.ItemArray[columnSCG].ToString()) - 1].Cells[1].Value = row.ItemArray[columnCGN].ToString();
				dataGridView1.Rows[Convert.ToInt32(row.ItemArray[columnSCG].ToString())-1].Cells[Convert.ToInt32(row.ItemArray[columnCO].ToString()) + 1].Style.BackColor = System.Drawing.ColorTranslator.FromHtml("#" + row.ItemArray[columnC].ToString());

				dataGridView1.Rows[Convert.ToInt32(row.ItemArray[columnSCG].ToString()) - 1].Cells[Convert.ToInt32(row.ItemArray[columnCO].ToString()) + 1].Value = "x";
				dataGridView1.Rows[Convert.ToInt32(row.ItemArray[columnSCG].ToString()) - 1].Cells[Convert.ToInt32(row.ItemArray[columnCO].ToString()) + 1].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
				
			}

			for (int c = 2; c < dataGridView1.Columns.Count; c++)
			{
				dataGridView1.Columns[c].Width = 25;
			}

		}

		private System.Data.DataTable LoadDataTable(SQL.SqlConnection conn, String SQLCommand, String tableName)
		{

			SQL.SqlCommand slidesCommand = new SQL.SqlCommand(SQLCommand, conn);
			System.Data.DataTable slidesTable = new System.Data.DataTable(tableName);
			SQL.SqlDataAdapter dapSlides = new SQL.SqlDataAdapter(slidesCommand);
			slidesTable.Clear();
			dapSlides.Fill(slidesTable);

			return slidesTable;
		}

		private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (dataGridView1.CurrentCell.ColumnIndex < 2)
			{
				label1.Text = "";
			}
			else
			{
				label1.Text = "Selected color = " + dataGridView1.CurrentCell.Style.BackColor.ToString();
			}
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			dataGridView1.Rows.Clear();
			dataGridView1.Columns.Clear();
			label1.Text = "";
			LoadColors();

		}


	}
}
