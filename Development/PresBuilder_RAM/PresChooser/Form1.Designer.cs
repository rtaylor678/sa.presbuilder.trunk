﻿namespace PresChooser
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtServer = new System.Windows.Forms.TextBox();
			this.txtDatabase = new System.Windows.Forms.TextBox();
			this.lblServer = new System.Windows.Forms.Label();
			this.lblDatabase = new System.Windows.Forms.Label();
			this.cmbPresNames = new System.Windows.Forms.ComboBox();
			this.lblPresentation = new System.Windows.Forms.Label();
			this.btnGetData = new System.Windows.Forms.Button();
			this.btnGetPresData = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtServer
			// 
			this.txtServer.Location = new System.Drawing.Point(556, 10);
			this.txtServer.Name = "txtServer";
			this.txtServer.Size = new System.Drawing.Size(56, 20);
			this.txtServer.TabIndex = 0;
			// 
			// txtDatabase
			// 
			this.txtDatabase.Location = new System.Drawing.Point(677, 10);
			this.txtDatabase.Name = "txtDatabase";
			this.txtDatabase.Size = new System.Drawing.Size(90, 20);
			this.txtDatabase.TabIndex = 1;
			// 
			// lblServer
			// 
			this.lblServer.AutoSize = true;
			this.lblServer.Location = new System.Drawing.Point(512, 13);
			this.lblServer.Name = "lblServer";
			this.lblServer.Size = new System.Drawing.Size(38, 13);
			this.lblServer.TabIndex = 2;
			this.lblServer.Text = "Server";
			// 
			// lblDatabase
			// 
			this.lblDatabase.AutoSize = true;
			this.lblDatabase.Location = new System.Drawing.Point(618, 13);
			this.lblDatabase.Name = "lblDatabase";
			this.lblDatabase.Size = new System.Drawing.Size(53, 13);
			this.lblDatabase.TabIndex = 3;
			this.lblDatabase.Text = "Database";
			// 
			// cmbPresNames
			// 
			this.cmbPresNames.FormattingEnabled = true;
			this.cmbPresNames.Location = new System.Drawing.Point(85, 10);
			this.cmbPresNames.Name = "cmbPresNames";
			this.cmbPresNames.Size = new System.Drawing.Size(403, 21);
			this.cmbPresNames.TabIndex = 4;
			this.cmbPresNames.SelectedIndexChanged += new System.EventHandler(this.cmbPresNames_SelectedIndexChanged);
			// 
			// lblPresentation
			// 
			this.lblPresentation.AutoSize = true;
			this.lblPresentation.Location = new System.Drawing.Point(13, 13);
			this.lblPresentation.Name = "lblPresentation";
			this.lblPresentation.Size = new System.Drawing.Size(66, 13);
			this.lblPresentation.TabIndex = 5;
			this.lblPresentation.Text = "Presentation";
			// 
			// btnGetData
			// 
			this.btnGetData.Location = new System.Drawing.Point(773, 8);
			this.btnGetData.Name = "btnGetData";
			this.btnGetData.Size = new System.Drawing.Size(75, 23);
			this.btnGetData.TabIndex = 6;
			this.btnGetData.Text = "Get Pres List";
			this.btnGetData.UseVisualStyleBackColor = true;
			this.btnGetData.Click += new System.EventHandler(this.btnGetData_Click);
			// 
			// btnGetPresData
			// 
			this.btnGetPresData.Location = new System.Drawing.Point(183, 37);
			this.btnGetPresData.Name = "btnGetPresData";
			this.btnGetPresData.Size = new System.Drawing.Size(136, 23);
			this.btnGetPresData.TabIndex = 7;
			this.btnGetPresData.Text = "Get Presentation Data";
			this.btnGetPresData.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(860, 642);
			this.Controls.Add(this.btnGetPresData);
			this.Controls.Add(this.btnGetData);
			this.Controls.Add(this.lblPresentation);
			this.Controls.Add(this.cmbPresNames);
			this.Controls.Add(this.lblDatabase);
			this.Controls.Add(this.lblServer);
			this.Controls.Add(this.txtDatabase);
			this.Controls.Add(this.txtServer);
			this.Name = "Form1";
			this.Text = "Form1";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtServer;
		private System.Windows.Forms.TextBox txtDatabase;
		private System.Windows.Forms.Label lblServer;
		private System.Windows.Forms.Label lblDatabase;
		private System.Windows.Forms.ComboBox cmbPresNames;
		private System.Windows.Forms.Label lblPresentation;
		private System.Windows.Forms.Button btnGetData;
		private System.Windows.Forms.Button btnGetPresData;
	}
}

