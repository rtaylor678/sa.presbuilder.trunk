﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SQL = System.Data.SqlClient;

namespace PresChooser
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			this.txtServer.Text = Properties.Settings.Default.Server;
			this.txtDatabase.Text = Properties.Settings.Default.Database;
			GetCurrentPres();

		}

		private void btnGetData_Click(object sender, EventArgs e)
		{
			GetCurrentPres();
			//TODO: blank the data
		}

		private void GetCurrentPres()
		{
			if (txtServer.Text != "" && txtDatabase.Text != "")
			{
				String connString = "Server=" + txtServer.Text + ";Database=" + txtDatabase.Text + ";Trusted_Connection=True; ";
				SQL.SqlConnection conn = new SQL.SqlConnection(connString);
				conn.Open(); // opens the database connection

				String SQLCommand = "SELECT PresName FROM Presentations";


				SQL.SqlCommand slidesCommand = new SQL.SqlCommand(SQLCommand, conn);
				DataTable presNameTable = new DataTable("PresNames");
				SQL.SqlDataAdapter dapSlides = new SQL.SqlDataAdapter(slidesCommand);
				presNameTable.Clear();
				dapSlides.Fill(presNameTable);

				cmbPresNames.DataSource = presNameTable;
				cmbPresNames.DisplayMember = "PresName";
				cmbPresNames.ValueMember = "PresName";

				conn.Close();
			}
		}

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			Properties.Settings.Default.Server = this.lblServer.Text;
			Properties.Settings.Default.Database = this.lblDatabase.Text;
			Properties.Settings.Default.Presentation = this.cmbPresNames.ValueMember;
		}

		private void cmbPresNames_SelectedIndexChanged(object sender, EventArgs e)
		{
			//TODO: check for a save
			//TODO: get the data
		}
	}
}
