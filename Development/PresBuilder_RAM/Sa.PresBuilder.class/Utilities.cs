﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Drawing;

using DOX = DocumentFormat.OpenXml;
using DOXP = DocumentFormat.OpenXml.Packaging;
using DOXC = DocumentFormat.OpenXml.Drawing.Charts;
using DOXA = DocumentFormat.OpenXml.Drawing;
using DOXPr = DocumentFormat.OpenXml.Presentation;
using System.IO;


namespace Sa.PresBuilder
{
	public static class Utilities
	{

		public static String GetFormatString(String format, String formatToFind)
		{

			String output = "";
			if (format.Contains(formatToFind))
			{
				output = format.Substring(format.IndexOf(formatToFind));
				if (output.IndexOf(" ") > 0)
					output = output.Substring(0, output.IndexOf(" "));
				output = output.Substring(formatToFind.Length);
			}

			return output;
		}

		public static Double GetUSorMetricValue(String valueToConvert, Boolean Metric = false)
		{
			//sometimes we need either a US or a Metric value
			//it isn't possible to switch between them in the database, we need to put one value in
			//so I decided to put the value in both ways, divided by a #, e.g. "16000#17"
			//and the code will split it so that it will be 16000 for US units and 17 for Metric units (this is heat rate in Btu or MJ)
			String inputValue = (valueToConvert == "") ? "0" : valueToConvert;
			Double returnValue;
			int index = inputValue.IndexOf("#");
			if (index == -1)
			{
				returnValue = Convert.ToDouble(inputValue);
			}
			else
			{
				if (Metric)
				{
					returnValue = Convert.ToDouble(inputValue.Substring(index + 1));
				}
				else
				{
					returnValue = Convert.ToDouble(inputValue.Substring(0, index));
				}
			}

			return returnValue;

		}

		public static T CheckForNull<T>(this object value, T defaultValue)
		{
			if (value == null || value == DBNull.Value)
				return defaultValue;

			return (T)Convert.ChangeType(value, typeof(T));
		}

		public static DataTable GetShortColorTable(DataTable slideColorsTable, Int32 colorToUse)
		{

			//get a temporary color table with only the colors that match the appropriate SlideColorGroup

			DataRow[] colors = slideColorsTable.Select("SlideColorGroup = " + colorToUse.ToString());

			DataTable shortColorsTable;

			if (colors.Length > 0)
			{
				shortColorsTable = colors.CopyToDataTable();
				shortColorsTable.Columns.RemoveAt(0);
			}
			else
			{
				shortColorsTable = slideColorsTable.Clone();
			}
			return shortColorsTable;

		}

		public static DataTable SplitMultiData(DataTable queryDataTable, String slideToMake)
		{
			//deletes any rows where the first column does not equal the slideToMake string, then deletes the first column
			//for (int r = 0; r < queryDataTable.Rows.Count; r++)
			for (int r = queryDataTable.Rows.Count - 1; r >= 0; r--)
			{
				if (queryDataTable.Rows[r].ItemArray[0].ToString() != slideToMake)//remove any record not in this unit
					queryDataTable.Rows[r].Delete();
			}

			queryDataTable.AcceptChanges();
			queryDataTable.Columns.RemoveAt(0);

			return queryDataTable;
		}

		public static SizeF GetStringLengthInPixels(String stringToCheck, String fontName = "Tahoma", Int32 fontHeight = 16, FontStyle fontStyle = FontStyle.Regular)
		{
			Font f = new Font(fontName, fontHeight, fontStyle);

			Bitmap b = new Bitmap(720, 540);//10x7.5 is standard size for presentation
			b.SetResolution(72, 72);

			Graphics g = Graphics.FromImage(b);

			SizeF sizeOfString = new SizeF(g.MeasureString(stringToCheck, f));

			return sizeOfString;
		}

	}

	public static class OpenXML
	{
        public static  void PostPresCleanup(String filenameToSaveAs, DataTable chartFixes)//, DataTable chartPosition, DataTable bar7ChartPosition, DataTable multiLevelFix, DataTable chartYAxisFix)
        {
            //these are things that the Aspose library cannot handle properly, so here we switch out to using the OpenXML stuff from Microsoft
            //painful but necessary unless/until Aspose makes changes in their library (which doesn't seem likely)

            if (chartFixes.Rows.Count > 0)
            {
                DataView chartView = new DataView(chartFixes);
                chartFixes = chartView.ToTable(true, "SlideNumber", "FixType");//we add this because it's possible there were multiple fixes to one chart, and we only need to pass it once
                //Wait to make sure the file complete saving
                try
                {
                    using (var stream = new FileStream(filenameToSaveAs, FileMode.Open, FileAccess.Read)) { }
                }
                catch (IOException)
                {
                    System.Threading.Thread.Sleep(5000);
                }

                using (DOXP.PresentationDocument doc = DOXP.PresentationDocument.Open(filenameToSaveAs, true))
                {
                    GetTheChart(doc, chartFixes);
                }
            }

        }

        private static void GetTheChart(DOXP.PresentationDocument doc, DataTable chartFixes)// Int32 slideNumber, String functionToCall)
		{
			DOXP.PresentationPart pres = doc.PresentationPart;

			DOXPr.SlideIdList slideList = pres.Presentation.SlideIdList;

			foreach (DataRow chartRow in chartFixes.Rows)//loop through each fix row
			{
				//Get the relationship id of the slide we want to change
				DOXPr.SlideId slideID = (DOXPr.SlideId)slideList.ElementAt(Convert.ToInt32(chartRow["SlideNumber"]) - 1);
				string slideRelId = slideID.RelationshipId;

				DOXP.SlidePart slidePart = pres.GetPartById(slideRelId) as DOXP.SlidePart;

				foreach (DOXP.ChartPart chartPart in slidePart.ChartParts)
				{
					foreach (DOXC.Chart chart in chartPart.ChartSpace.Elements<DOXC.Chart>())
					{
						DOXC.PlotArea plotArea = chart.PlotArea;
						DOXPr.Slide slide = slidePart.Slide;
						String chartID = slidePart.GetIdOfPart(chartPart);

						switch (Convert.ToInt32(chartRow["FixType"]))
						{
							case 1:
								FixCategoryDate(chart, plotArea);
								break;
							case 2:
								FixCategoryDateVersion2(chart, plotArea);
								break;
							case 3:
								AdjustLayoutTarget(chart, plotArea);
								break;
							case 4:
								Bar7AdjustLayoutTarget(chart, plotArea);
								break;
							case 5:
								FixGroupingLevel(chart, plotArea);
								break;
							case 6:
								CheckYAxisLabels(chart, plotArea, slide, chartID);
								break;
							default:
								break;
						}
					}
				}
			}

		}

		private static void CheckYAxisLabels(DOXC.Chart chart, DOXC.PlotArea plotArea, DOXPr.Slide slide, String chartID)//DOXP.PresentationDocument doc, Int32 slideNumber)
		{

			DOXPr.ShapeTree shapeTree = slide.CommonSlideData.ShapeTree;

			DOX.Int64Value xposition = -1;//setting these to default values
			DOX.Int64Value chartWidth = -1;
			
			//first try and find the correct graphic frame for the chart, if we get it then we get the x pos and chart width
			foreach (DOXPr.GraphicFrame graphicFrame in shapeTree.Elements<DOXPr.GraphicFrame>())
			{
				//go through the graphic frames and get details of the charts (Description is the alternate name)
				//String chartDescription = graphicFrame.NonVisualGraphicFrameProperties.NonVisualDrawingProperties.Description;
				DOXC.ChartReference chartReference = (DOXC.ChartReference)graphicFrame.Graphic.GraphicData.Elements<DOXC.ChartReference>().FirstOrDefault();
				if (chartReference != null)
				{
					String crID = chartReference.Id;

					if (crID == chartID)//we have the right graphic frame for our chart
					{
						xposition = graphicFrame.Transform.Offset.X;
						chartWidth = graphicFrame.Transform.Extents.Cx;
					}
				}
			}

			//now try and set the new left position for the chart's axis title
			foreach (DOXC.ValueAxis valueAxis in plotArea.Elements<DOXC.ValueAxis>())
			{
				if (valueAxis.AxisPosition.Val != DOXC.AxisPositionValues.Bottom)//we're not touching the horizontal label (yet)
				{
					DOXC.Title title = valueAxis.Title;
					if (title != null)
					{
						DOXC.Layout layout = title.Layout;
						if (layout == null)
						{
							layout = new DOXC.Layout();
							title.AppendChild(layout);
						}

						DOXC.ManualLayout manualLayout = layout.ManualLayout;
						if (manualLayout == null)
						{
							manualLayout = new DOXC.ManualLayout();
							layout.AppendChild(manualLayout);
						}

						manualLayout.LeftMode = new DOXC.LeftMode() { Val = DOXC.LayoutModeValues.Edge };

						DOXC.Left left = new DOXC.Left();

						if (valueAxis.AxisPosition.Val == DOXC.AxisPositionValues.Left)
						{
							if (xposition == 0 && chartWidth == 9144000)//default chart position
							{
								left.Val = 0.068;//0.06
							}
							else
							{
								left.Val = 0.0;//if not a default chart
							}
						}
						else //assumes only left and right
						{
							if (xposition == 0 && chartWidth == 9144000)
							{
								left.Val = 0.877;//0.872
							}
							else
							{
								left.Val = 0.9;//hoping there aren't too many of these, non-defaults with a y-axis. guess we'll deal with that when it happens
							}
						}

						manualLayout.Left = left;

					}
				}
			}

		}

		private static void FixGroupingLevel(DOXC.Chart chart, DOXC.PlotArea plotArea)
		{

			Int32 manualLevels = 0;

			foreach (DOXC.BarChart bc in plotArea.Elements<DOXC.BarChart>())
			{
				foreach (DOXC.BarChartSeries bcs in bc.Elements<DOXC.BarChartSeries>())
				{
					foreach (DOXC.CategoryAxisData cad in bcs.Elements<DOXC.CategoryAxisData>())
					{
						foreach (DOXC.MultiLevelStringReference mlsr in cad.Elements<DOXC.MultiLevelStringReference>())
						{
							DOXC.MultiLevelStringCache mlsc = mlsr.MultiLevelStringCache;

							Boolean doneCount = false;
							Int32 mlscLevels = 0;

							foreach (DOXC.Level lvl in mlsc.Elements<DOXC.Level>())
							{
								DOX.UInt32Value ptcount = 0;
								mlscLevels++;

								for (int sp = lvl.ChildElements.Count() - 1; sp >= 0; sp--)
								{
									if (lvl.ChildElements[sp].GetType() == typeof(DOXC.StringPoint))
									{
										DOXC.StringPoint stp = (DOXC.StringPoint)lvl.ChildElements[sp];
										if (stp.InnerText == "")
										{
											lvl.ChildElements[sp].Remove();
										}
										else
										{
											ptcount ++;
										}
									}
								}

								if (doneCount == false) //hoping it's always the first one we come across
								{
									mlsc.PointCount.Val = ptcount;
									doneCount = true;
								}
																
							}

							//how to set the formula?
							//it will be in the format "Sheet1!$A$2:$G$13" where the G and the 13 will vary (the A2 should always be the same?)
							String workingFormula = mlsr.Formula.Text;
							//we need to leave the 13 alone
							//we need to change the G to the appropriate letter (could be A, B, C etc, but probably not much more than C?)
							//how to find the position of the G (or whatever it is)?
							//have to look for the :$
							Int32 colonPos = workingFormula.IndexOf(":$");
							String colChar = workingFormula.Substring(colonPos + 2, 1);

							String newFormula = workingFormula.Substring(0, colonPos + 2);
							if (mlscLevels == 1 && colChar != "A")
							{
								newFormula += "A";
							}

							if (mlscLevels == 2 && colChar != "B")
							{
								newFormula += "B";
							}

							if (mlscLevels == 3 && colChar != "C")
							{
								newFormula += "C";
							}

							newFormula += workingFormula.Substring(colonPos + 3);

							mlsr.Formula.Text = newFormula;

							if (mlscLevels == 3)
							{
								//first level should be the individual column names
								//all others are multi levels
								//we switch levels 2 and 3, because that's where Aspose screws up
								DOX.OpenXmlElement origElement2 = (DOX.OpenXmlElement)mlsc.ChildElements.ElementAt(2);
								DOX.OpenXmlElement origElement3 = (DOX.OpenXmlElement)mlsc.ChildElements.ElementAt(3);

								DOX.OpenXmlElement cloneElement2 = (DOX.OpenXmlElement)origElement2.Clone();
								DOX.OpenXmlElement cloneElement3 = (DOX.OpenXmlElement)origElement3.Clone();

								origElement2.RemoveAllChildren();
								origElement3.RemoveAllChildren();

								foreach (DOX.OpenXmlElement child in cloneElement3.ChildElements)
								{
									DOX.OpenXmlElement newChild = (DOX.OpenXmlElement)child.Clone();
									origElement2.AppendChild(newChild);
								}
								foreach (DOX.OpenXmlElement child in cloneElement2.ChildElements)
								{
									DOX.OpenXmlElement newChild = (DOX.OpenXmlElement)child.Clone();
									origElement3.AppendChild(newChild);
								}

							}

							manualLevels = mlscLevels;

						}
					}
				}


				if (manualLevels == 3)
				{
					DOXC.Layout layout = plotArea.Layout;
					DOXC.ManualLayout manualLayout = layout.ManualLayout;
					DOXC.Height height = new DOXC.Height() { Val = 0.4D };
					manualLayout.Height = height;
				}

			}

		}

		private static void FixCategoryDate(DOXC.Chart chart, DOXC.PlotArea plotArea)
		{
			//given a chart convert the category axis to a date axis, with appropriate fixes to make values work
			
			foreach (DOXC.CategoryAxis catAxis in plotArea.Elements<DOXC.CategoryAxis>())
			{
				DOXC.DateAxis dateAxis = new DOXC.DateAxis();//create a date axis and copy the category axis values to it (most of them are the same, some get changes or additions)

				dateAxis.AppendChild(catAxis.AxisId.CloneNode(true));//DOXC.AxisId catAxisID = catAxis.AxisId;
				dateAxis.AppendChild(catAxis.Scaling.CloneNode(true));//DOXC.Scaling scale = catAxis.Scaling;
				dateAxis.AppendChild(catAxis.Delete.CloneNode(true));//DOXC.Delete del = catAxis.Delete;
				dateAxis.AppendChild(catAxis.AxisPosition.CloneNode(true));//DOXC.AxisPosition ap = catAxis.AxisPosition;

				dateAxis.AppendChild(catAxis.NumberingFormat.CloneNode(true));
				dateAxis.NumberingFormat.SourceLinked = false;
				dateAxis.NumberingFormat.FormatCode = "mm/dd/yy;@";
				//DOXC.NumberingFormat nf = catAxis.NumberingFormat;
				//if (nf != null)
				//{
				//	nf.SourceLinked = false;
				//	nf.FormatCode = "mm/dd/yy;@";
				//}
				dateAxis.AppendChild(catAxis.MajorTickMark.CloneNode(true));//DOXC.MajorTickMark mt = catAxis.MajorTickMark;
				dateAxis.AppendChild(catAxis.MinorTickMark.CloneNode(true));//DOXC.MinorTickMark mit = catAxis.MinorTickMark;
				//if (tl != null)
				dateAxis.AppendChild(catAxis.TickLabelPosition.CloneNode(true));//DOXC.TickLabelPosition tl = catAxis.TickLabelPosition;
				dateAxis.AppendChild(catAxis.ChartShapeProperties.CloneNode(true));//DOXC.ChartShapeProperties csp = catAxis.ChartShapeProperties;
				dateAxis.AppendChild(catAxis.TextProperties.CloneNode(true));//DOXC.TextProperties tp = catAxis.TextProperties;
				dateAxis.TextProperties.BodyProperties.Rotation = -5400000;
				dateAxis.TextProperties.BodyProperties.Vertical = DOXA.TextVerticalValues.Horizontal;

				foreach (DOXA.Paragraph para in dateAxis.TextProperties.Elements<DOXA.Paragraph>())
				{
					DOXA.ParagraphProperties paraprop = para.ParagraphProperties;
					foreach (DOXA.DefaultRunProperties drp in paraprop.Elements < DOXA.DefaultRunProperties>())
					{
						drp.FontSize = 1400;
					}
				}

				dateAxis.AppendChild(catAxis.CrossingAxis.CloneNode(true));
				dateAxis.Append(new DOXC.Crosses() { Val = DOXC.CrossesValues.AutoZero });
				dateAxis.Append(new DOXC.AutoLabeled() { Val = false });
				dateAxis.Append(new DOXC.LabelOffset() { Val = (DOX.UInt16Value)100U });
				dateAxis.Append(new DOXC.BaseTimeUnit() { Val = DOXC.TimeUnitValues.Days });
				dateAxis.Append(new DOXC.MajorUnit() { Val = 1D });
				dateAxis.Append(new DOXC.MajorTimeUnit() { Val = DOXC.TimeUnitValues.Months });

				//remove the category axis
				catAxis.Remove();
				//and add the date axis to the plot area
				plotArea.Append(dateAxis);
			
			}

		}

		private static void FixCategoryDateVersion2(DOXC.Chart chart, DOXC.PlotArea plotArea)//DOXP.PresentationDocument doc, Int32 slideNumber)
		{
			//given a presentation and a slide number
			//convert the category axis to a date axis
			//with appropriate fixes to make values work

			foreach (DOXC.CategoryAxis catAxis in plotArea.Elements < DOXC.CategoryAxis>())
			{
				DOXC.DateAxis dateAxis = new DOXC.DateAxis();//create a date axis and copy the category axis values to it (most of them are the same, some get changes or additions)

				dateAxis.AppendChild(catAxis.AxisId.CloneNode(true));
				dateAxis.AppendChild(catAxis.Scaling.CloneNode(true));
				dateAxis.Scaling.MaxAxisValue = new DOXC.MaxAxisValue() { Val = 109D };
				dateAxis.Scaling.MinAxisValue = new DOXC.MinAxisValue() { Val = 11D };
				dateAxis.AppendChild(catAxis.Delete.CloneNode(true));
				dateAxis.AppendChild(catAxis.AxisPosition.CloneNode(true));

				//if (nf != null)
				dateAxis.AppendChild(catAxis.NumberingFormat.CloneNode(true));

				dateAxis.AppendChild(catAxis.MajorTickMark.CloneNode(true));
				dateAxis.AppendChild(catAxis.MinorTickMark.CloneNode(true));

				if (catAxis.TickLabelPosition != null)
				{
					dateAxis.AppendChild(catAxis.TickLabelPosition.CloneNode(true));
				}
	
				dateAxis.AppendChild(catAxis.CrossingAxis.CloneNode(true));
				dateAxis.Append(new DOXC.Crosses() { Val = DOXC.CrossesValues.Maximum });
				dateAxis.Append(new DOXC.AutoLabeled() { Val = false });
				dateAxis.Append(new DOXC.LabelOffset() { Val = (DOX.UInt16Value)100U });
				dateAxis.Append(new DOXC.BaseTimeUnit() { Val = DOXC.TimeUnitValues.Days });

				//remove the category axis
				catAxis.Remove();
				//and add the date axis to the plot area
				plotArea.Append(dateAxis);
			
			}

		}

		private static void AdjustLayoutTarget(DOXC.Chart chart, DOXC.PlotArea plotArea)
		{

			DOXC.Layout layout = plotArea.Layout;
			DOXC.ManualLayout manualLayout = layout.ManualLayout;
			DOXC.LayoutTarget layoutTarget = manualLayout.LayoutTarget;

			if (layoutTarget == null)
			{
				layoutTarget = new DOXC.LayoutTarget() { Val = DOXC.LayoutTargetValues.Inner };
				manualLayout.LayoutTarget = layoutTarget;

				//set values for top and bottom of each chart
				DOXC.Top top = new DOXC.Top() { Val = 0.049D };
				DOXC.Height height = new DOXC.Height() { Val = 0.7D };//.8D

				Double leftYAxisMax = 0;
				Double rightYAxisMax = 0;
				DOXC.BarDirection bd = null;

				foreach (DOXC.ValueAxis va in plotArea.Elements<DOXC.ValueAxis>())
				{
					if (va.Scaling.MaxAxisValue != null)
					{
						if (va.Delete.Val == false)
						{
							if (va.AxisPosition.Val == DocumentFormat.OpenXml.Drawing.Charts.AxisPositionValues.Left)
							{
								leftYAxisMax = va.Scaling.MaxAxisValue.Val;
							}
							if (va.AxisPosition.Val == DocumentFormat.OpenXml.Drawing.Charts.AxisPositionValues.Right)
							{
								rightYAxisMax = va.Scaling.MaxAxisValue.Val;
							}
						}
					}
				}

				foreach(DOXC.BarChart bc in plotArea.Elements<DOXC.BarChart>())
				{
					bd = bc.BarDirection;
				}

				//variable values for left and right of each chart
				DOXC.Left left = new DOXC.Left();
				DOXC.Width width = new DOXC.Width();

				//System.Diagnostics.Debug.Print(leftYAxisMax.ToString());

				if (leftYAxisMax >= 0 && leftYAxisMax < 1000)
				{
					left.Val = 0.172D;//0.036//0.128D
					if (rightYAxisMax == 0)
					{
						width.Val = 0.684D;//0.817//0.8155D
					}
					else
					{
						width.Val = 0.636D;//0.760D
					}
				}

				if (leftYAxisMax >= 1000 && leftYAxisMax < 100000)
				{
					left.Val = 0.199D;//0.052//0.1595D

					if (rightYAxisMax == 0)
					{
						width.Val = 0.657D;//0.785//0.784D
					}
					else
					{
						width.Val = 0.609D;//0.728D
					}
				}

				if (leftYAxisMax == 0 && rightYAxisMax == 0 && bd != null && bd.Val == DOXC.BarDirectionValues.Bar)
				{
					left.Val = 0.199D;
					width.Val = 0.6565D;
				}

				manualLayout.Left = left;
				manualLayout.Width = width;
				manualLayout.Top = top;
				manualLayout.Height = height;

			}

		}

		private static void Bar7AdjustLayoutTarget(DOXC.Chart chart, DOXC.PlotArea plotArea)
		{

			DOXC.Layout layout = plotArea.Layout;
			DOXC.ManualLayout manualLayout = layout.ManualLayout;
			DOXC.LayoutTarget layoutTarget = manualLayout.LayoutTarget;

			if (layoutTarget == null)
			{
				layoutTarget = new DOXC.LayoutTarget() { Val = DOXC.LayoutTargetValues.Inner };
				manualLayout.LayoutTarget = layoutTarget;

				//set values for top and bottom of each chart
				DOXC.Top top = new DOXC.Top() { Val = 0.0D };
				DOXC.Height height = new DOXC.Height() { Val = 0.9D };//.8D

				//set values for left and right of each chart in the Bar7 chart
				DOXC.Left left = new DOXC.Left() { Val = 0.125D };
				DOXC.Width width = new DOXC.Width() { Val = 0.863D };

				manualLayout.Left = left;
				manualLayout.Width = width;
				manualLayout.Top = top;
				manualLayout.Height = height;

			}
		}

	}

}

namespace CustomExtensions
{
	public static class ExtentionMethods
	{

		public static T CheckForNull<T>(this object value, T defaultValue)
		{
			if (value == null || value == DBNull.Value)
				return defaultValue;

			return (T)Convert.ChangeType(value, typeof(T));
		}
	}
}

