﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;
using P = DocumentFormat.OpenXml.Presentation;
using D = DocumentFormat.OpenXml.Drawing;
using C = DocumentFormat.OpenXml.Drawing.Charts; 
//using DocumentFormat.OpenXml.Packaging.PresentationDocument;
//using Drawing = DocumentFormat.OpenXml.Drawing;
using A = DocumentFormat.OpenXml.Drawing;
using C14 = DocumentFormat.OpenXml.Office2010.Drawing.Charts;

using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using P14 = DocumentFormat.OpenXml.Office2010.PowerPoint;
using Ap = DocumentFormat.OpenXml.ExtendedProperties;
using Vt = DocumentFormat.OpenXml.VariantTypes;
using System.Data.SqlClient;
using System.Data;
using ss = DocumentFormat.OpenXml.Spreadsheet;




namespace Sa.PresBuilder
{
	public class Class1
	{

		public static void CreatePresentation(string filepath)
		{
			// Create a presentation at a specified file path. The presentation document type is pptx, by default.
			PresentationDocument presentationDoc = PresentationDocument.Create(filepath, PresentationDocumentType.Presentation);
            
			PresentationPart presentationPart = presentationDoc.AddPresentationPart();
			presentationPart.Presentation = new Presentation();

			CreatePresentationParts(presentationPart);

			// Close the presentation handle.
			presentationDoc.Close();
		}
		
		private static void CreatePresentationParts(PresentationPart presentationPart)
		{
			SlideMasterIdList slideMasterIdList1 = new SlideMasterIdList(new SlideMasterId() { Id = (UInt32Value)2147483648U, RelationshipId = "rId1" });
			SlideIdList slideIdList1 = new SlideIdList(new SlideId() { Id = (UInt32Value)256U, RelationshipId = "rId2" });
			SlideSize slideSize1 = new SlideSize() { Cx = 9144000, Cy = 6858000, Type = SlideSizeValues.Screen4x3 };
			NotesSize notesSize1 = new NotesSize() { Cx = 6858000, Cy = 9144000 };
			DefaultTextStyle defaultTextStyle1 = new DefaultTextStyle();

			presentationPart.Presentation.Append(slideMasterIdList1, slideIdList1, slideSize1, notesSize1, defaultTextStyle1);

			SlidePart slidePart1;
			SlideLayoutPart slideLayoutPart1;
			SlideMasterPart slideMasterPart1;
			ThemePart themePart1;
			

			slidePart1 = CreateSlidePart(presentationPart);
			slideLayoutPart1 = CreateSlideLayoutPart(slidePart1);
			slideMasterPart1 = CreateSlideMasterPart(slideLayoutPart1);
			themePart1 = CreateTheme(slideMasterPart1); 

			slideMasterPart1.AddPart(slideLayoutPart1, "rId1");
			presentationPart.AddPart(slideMasterPart1, "rId1");
			presentationPart.AddPart(themePart1, "rId5");

		}

		private static SlidePart CreateSlidePart(PresentationPart presentationPart)        
		{
			SlidePart slidePart1 = presentationPart.AddNewPart<SlidePart>("rId2");
				slidePart1.Slide = new Slide(
						new CommonSlideData(
							new ShapeTree(
								new P.NonVisualGroupShapeProperties(
									new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
									new P.NonVisualGroupShapeDrawingProperties(),
									new ApplicationNonVisualDrawingProperties()),
								new GroupShapeProperties(new TransformGroup()),
								new P.Shape(
									new P.NonVisualShapeProperties(
										new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "Title 1" },
										new P.NonVisualShapeDrawingProperties(new ShapeLocks() { NoGrouping = true }),
										new ApplicationNonVisualDrawingProperties(new PlaceholderShape())),
									new P.ShapeProperties(),
									new P.TextBody(
										new BodyProperties(),
										new ListStyle(),
										new Paragraph(new EndParagraphRunProperties() { Language = "en-US" }))))),
						new ColorMapOverride(new MasterColorMapping()));
				return slidePart1;
			} 
   
		private static SlideLayoutPart CreateSlideLayoutPart(SlidePart slidePart1)
		{
			SlideLayoutPart slideLayoutPart1 = slidePart1.AddNewPart<SlideLayoutPart>("rId1");
			SlideLayout slideLayout = new SlideLayout(
			new CommonSlideData(new ShapeTree(
				new P.NonVisualGroupShapeProperties(
				new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
				new P.NonVisualGroupShapeDrawingProperties(),
				new ApplicationNonVisualDrawingProperties()),
				new GroupShapeProperties(new TransformGroup()),
				new P.Shape(
				new P.NonVisualShapeProperties(
				new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "" },
				new P.NonVisualShapeDrawingProperties(new ShapeLocks() { NoGrouping = true }),
				new ApplicationNonVisualDrawingProperties(new PlaceholderShape())),
				new P.ShapeProperties(),
				new P.TextBody(
				new BodyProperties(),
				new ListStyle(),
				new Paragraph(new EndParagraphRunProperties()))))),
			new ColorMapOverride(new MasterColorMapping()));
			slideLayoutPart1.SlideLayout = slideLayout;
			return slideLayoutPart1;
			}

		private static SlideMasterPart CreateSlideMasterPart(SlideLayoutPart slideLayoutPart1)
		{
			SlideMasterPart slideMasterPart1 = slideLayoutPart1.AddNewPart<SlideMasterPart>("rId1");
			SlideMaster slideMaster = new SlideMaster(
			new CommonSlideData(new ShapeTree(
				new P.NonVisualGroupShapeProperties(
				new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
				new P.NonVisualGroupShapeDrawingProperties(),
				new ApplicationNonVisualDrawingProperties()),
				new GroupShapeProperties(new TransformGroup()),
				new P.Shape(
				new P.NonVisualShapeProperties(
				new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "Title Placeholder 1" },
				new P.NonVisualShapeDrawingProperties(new ShapeLocks() { NoGrouping = true }),
				new ApplicationNonVisualDrawingProperties(new PlaceholderShape() { Type = PlaceholderValues.Title })),
				new P.ShapeProperties(),
				new P.TextBody(
				new BodyProperties(),
				new ListStyle(),
				new Paragraph())))),
			new P.ColorMap() { Background1 = D.ColorSchemeIndexValues.Light1, Text1 = D.ColorSchemeIndexValues.Dark1, Background2 = D.ColorSchemeIndexValues.Light2, Text2 = D.ColorSchemeIndexValues.Dark2, Accent1 = D.ColorSchemeIndexValues.Accent1, Accent2 = D.ColorSchemeIndexValues.Accent2, Accent3 = D.ColorSchemeIndexValues.Accent3, Accent4 = D.ColorSchemeIndexValues.Accent4, Accent5 = D.ColorSchemeIndexValues.Accent5, Accent6 = D.ColorSchemeIndexValues.Accent6, Hyperlink = D.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = D.ColorSchemeIndexValues.FollowedHyperlink },
			new SlideLayoutIdList(new SlideLayoutId() { Id = (UInt32Value)2147483649U, RelationshipId = "rId1" }),
			new TextStyles(new TitleStyle(), new BodyStyle(), new OtherStyle()));
			slideMasterPart1.SlideMaster = slideMaster;

			return slideMasterPart1;
		}

		private static ThemePart CreateTheme(SlideMasterPart slideMasterPart1)
		{
			ThemePart themePart1 = slideMasterPart1.AddNewPart<ThemePart>("rId5");
			D.Theme theme1 = new D.Theme() { Name = "Office Theme" };

			D.ThemeElements themeElements1 = new D.ThemeElements(
			new D.ColorScheme(
				new D.Dark1Color(new D.SystemColor() { Val = D.SystemColorValues.WindowText, LastColor = "000000" }),
				new D.Light1Color(new D.SystemColor() { Val = D.SystemColorValues.Window, LastColor = "FFFFFF" }),
				new D.Dark2Color(new D.RgbColorModelHex() { Val = "1F497D" }),
				new D.Light2Color(new D.RgbColorModelHex() { Val = "EEECE1" }),
				new D.Accent1Color(new D.RgbColorModelHex() { Val = "4F81BD" }),
				new D.Accent2Color(new D.RgbColorModelHex() { Val = "C0504D" }),
				new D.Accent3Color(new D.RgbColorModelHex() { Val = "9BBB59" }),
				new D.Accent4Color(new D.RgbColorModelHex() { Val = "8064A2" }),
				new D.Accent5Color(new D.RgbColorModelHex() { Val = "4BACC6" }),
				new D.Accent6Color(new D.RgbColorModelHex() { Val = "F79646" }),
				new D.Hyperlink(new D.RgbColorModelHex() { Val = "0000FF" }),
				new D.FollowedHyperlinkColor(new D.RgbColorModelHex() { Val = "800080" })) { Name = "Office" },
				new D.FontScheme(
				new D.MajorFont(
				new D.LatinFont() { Typeface = "Calibri" },
				new D.EastAsianFont() { Typeface = "" },
				new D.ComplexScriptFont() { Typeface = "" }),
				new D.MinorFont(
				new D.LatinFont() { Typeface = "Calibri" },
				new D.EastAsianFont() { Typeface = "" },
				new D.ComplexScriptFont() { Typeface = "" })) { Name = "Office" },
				new D.FormatScheme(
				new D.FillStyleList(
				new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
				new D.GradientFill(
				new D.GradientStopList(
				new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 50000 },
					new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
				new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 37000 },
				new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 35000 },
				new D.GradientStop(new D.SchemeColor(new D.Tint() { Val = 15000 },
				new D.SaturationModulation() { Val = 350000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 100000 }
				),
				new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
				new D.NoFill(),
				new D.PatternFill(),
				new D.GroupFill()),
				new D.LineStyleList(
				new D.Outline(
				new D.SolidFill(
				new D.SchemeColor(
					new D.Shade() { Val = 95000 },
					new D.SaturationModulation() { Val = 105000 }) { Val = D.SchemeColorValues.PhColor }),
				new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
				{
					Width = 9525,
					CapType = D.LineCapValues.Flat,
					CompoundLineType = D.CompoundLineValues.Single,
					Alignment = D.PenAlignmentValues.Center
				},
				new D.Outline(
				new D.SolidFill(
				new D.SchemeColor(
					new D.Shade() { Val = 95000 },
					new D.SaturationModulation() { Val = 105000 }) { Val = D.SchemeColorValues.PhColor }),
				new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
				{
					Width = 9525,
					CapType = D.LineCapValues.Flat,
					CompoundLineType = D.CompoundLineValues.Single,
					Alignment = D.PenAlignmentValues.Center
				},
				new D.Outline(
				new D.SolidFill(
				new D.SchemeColor(
					new D.Shade() { Val = 95000 },
					new D.SaturationModulation() { Val = 105000 }) { Val = D.SchemeColorValues.PhColor }),
				new D.PresetDash() { Val = D.PresetLineDashValues.Solid })
				{
					Width = 9525,
					CapType = D.LineCapValues.Flat,
					CompoundLineType = D.CompoundLineValues.Single,
					Alignment = D.PenAlignmentValues.Center
				}),
				new D.EffectStyleList(
				new D.EffectStyle(
				new D.EffectList(
				new D.OuterShadow(
					new D.RgbColorModelHex(
					new D.Alpha() { Val = 38000 }) { Val = "000000" }) { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
				new D.EffectStyle(
				new D.EffectList(
				new D.OuterShadow(
					new D.RgbColorModelHex(
					new D.Alpha() { Val = 38000 }) { Val = "000000" }) { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false })),
				new D.EffectStyle(
				new D.EffectList(
				new D.OuterShadow(
					new D.RgbColorModelHex(
					new D.Alpha() { Val = 38000 }) { Val = "000000" }) { BlurRadius = 40000L, Distance = 20000L, Direction = 5400000, RotateWithShape = false }))),
				new D.BackgroundFillStyleList(
				new D.SolidFill(new D.SchemeColor() { Val = D.SchemeColorValues.PhColor }),
				new D.GradientFill(
				new D.GradientStopList(
				new D.GradientStop(
					new D.SchemeColor(new D.Tint() { Val = 50000 },
					new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
				new D.GradientStop(
					new D.SchemeColor(new D.Tint() { Val = 50000 },
					new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
				new D.GradientStop(
					new D.SchemeColor(new D.Tint() { Val = 50000 },
					new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 }),
				new D.LinearGradientFill() { Angle = 16200000, Scaled = true }),
				new D.GradientFill(
				new D.GradientStopList(
				new D.GradientStop(
					new D.SchemeColor(new D.Tint() { Val = 50000 },
					new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 },
				new D.GradientStop(
					new D.SchemeColor(new D.Tint() { Val = 50000 },
					new D.SaturationModulation() { Val = 300000 }) { Val = D.SchemeColorValues.PhColor }) { Position = 0 }),
				new D.LinearGradientFill() { Angle = 16200000, Scaled = true }))) { Name = "Office" });

			theme1.Append(themeElements1);
			theme1.Append(new D.ObjectDefaults());
			theme1.Append(new D.ExtraColorSchemeList());

			themePart1.Theme = theme1;
			return themePart1;

			}



		// Insert a slide into the specified presentation.
		public static void InsertNewSlide(string presentationFile, int position, string slideTitle)
		{
			//// Open the source document as read/write. 
			using (PresentationDocument presentationDocument = PresentationDocument.Open(presentationFile, true))
			{
				// Pass the source document and the position and title of the slide to be inserted to the next method.
				Insert_NewSlide(presentationDocument, position, slideTitle);
			}
		}

		// Insert the specified slide into the presentation at the specified position.
		public static void Insert_NewSlide(PresentationDocument presentationDocument, int position, string slideTitle)
		{

			if (presentationDocument == null)
			{
				throw new ArgumentNullException("presentationDocument");
			}

			if (slideTitle == null)
			{
				throw new ArgumentNullException("slideTitle");
			}

			PresentationPart presentationPart = presentationDocument.PresentationPart;

			// Verify that the presentation is not empty.
			if (presentationPart == null)
			{
				throw new InvalidOperationException("The presentation document is empty.");
			}

			// Declare and instantiate a new slide.
			Slide slide = new Slide(new CommonSlideData(new ShapeTree()));
			uint drawingObjectId = 1;

			// Construct the slide content.            
			// Specify the non-visual properties of the new slide.
			P.NonVisualGroupShapeProperties nonVisualProperties = slide.CommonSlideData.ShapeTree.AppendChild(new P.NonVisualGroupShapeProperties());
			nonVisualProperties.NonVisualDrawingProperties = new P.NonVisualDrawingProperties() { Id = 1, Name = "" };
			nonVisualProperties.NonVisualGroupShapeDrawingProperties = new P.NonVisualGroupShapeDrawingProperties();
			nonVisualProperties.ApplicationNonVisualDrawingProperties = new ApplicationNonVisualDrawingProperties();

			// Specify the group shape properties of the new slide.
			slide.CommonSlideData.ShapeTree.AppendChild(new GroupShapeProperties());

			// Declare and instantiate the title shape of the new slide.
			P.Shape titleShape = slide.CommonSlideData.ShapeTree.AppendChild(new P.Shape());

			drawingObjectId++;

			// Specify the required shape properties for the title shape. 
			titleShape.NonVisualShapeProperties = new P.NonVisualShapeProperties
				(new P.NonVisualDrawingProperties() { Id = drawingObjectId, Name = "Title" },
				new P.NonVisualShapeDrawingProperties(new D.ShapeLocks() { NoGrouping = true }),
				new ApplicationNonVisualDrawingProperties(new PlaceholderShape() { Type = PlaceholderValues.Title }));
			titleShape.ShapeProperties = new P.ShapeProperties();

			// Specify the text of the title shape.
			titleShape.TextBody = new P.TextBody(new D.BodyProperties(),
					new D.ListStyle(),
					new D.Paragraph(new D.Run(new D.Text() { Text = slideTitle })));

			// Declare and instantiate the body shape of the new slide.
			P.Shape bodyShape = slide.CommonSlideData.ShapeTree.AppendChild(new P.Shape());
			drawingObjectId++;

			// Specify the required shape properties for the body shape.
			bodyShape.NonVisualShapeProperties = new P.NonVisualShapeProperties(new P.NonVisualDrawingProperties() { Id = drawingObjectId, Name = "Content Placeholder" },
					new P.NonVisualShapeDrawingProperties(new D.ShapeLocks() { NoGrouping = true }),
					new ApplicationNonVisualDrawingProperties(new PlaceholderShape() { Index = 1 }));
			bodyShape.ShapeProperties = new P.ShapeProperties();

			// Specify the text of the body shape.
			bodyShape.TextBody = new P.TextBody(new D.BodyProperties(),
					new D.ListStyle(),
					new D.Paragraph());

			D.Run run1 = new D.Run();
			D.RunProperties runProperties1 = new D.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			D.Text text1 = new D.Text();
			text1.Text = "Technical Unavailability";

			run1.Append(runProperties1);
			run1.Append(text1);

			D.Paragraph paragraph1 = new D.Paragraph();
			D.ParagraphProperties paragraphProperties1 = new D.ParagraphProperties() { EastAsianLineBreak = true, Height = true };
			paragraph1.Append(paragraphProperties1);
			paragraph1.Append(run1);

			D.TextBody textBody1 = new D.TextBody();
			textBody1.Append(paragraph1);
			bodyShape.Append(textBody1);

			// Create the slide part for the new slide.
			SlidePart slidePart = presentationPart.AddNewPart<SlidePart>();

			// Save the new slide part.
			slide.Save(slidePart);

			// Modify the slide ID list in the presentation part.
			// The slide ID list should not be null.
			SlideIdList slideIdList = presentationPart.Presentation.SlideIdList;

			// Find the highest slide ID in the current list.
			uint maxSlideId = 1;
			SlideId prevSlideId = null;

			foreach (SlideId slideId in slideIdList.ChildElements)
			{
				if (slideId.Id > maxSlideId)
				{
					maxSlideId = slideId.Id;
				}

				position--;
				if (position == 0)
				{
					prevSlideId = slideId;
				}

			}

			maxSlideId++;

			// Get the ID of the previous slide.
			SlidePart lastSlidePart;

			if (prevSlideId != null)
			{
				lastSlidePart = (SlidePart)presentationPart.GetPartById(prevSlideId.RelationshipId);
			}
			else
			{
				lastSlidePart = (SlidePart)presentationPart.GetPartById(((SlideId)(slideIdList.ChildElements[0])).RelationshipId);
			}

			// Use the same slide layout as that of the previous slide.
			if (null != lastSlidePart.SlideLayoutPart)
			{
				slidePart.AddPart(lastSlidePart.SlideLayoutPart);
			}

			// Insert the new slide into the slide list after the previous slide.
			SlideId newSlideId = slideIdList.InsertAfter(new SlideId(), prevSlideId);
			newSlideId.Id = maxSlideId;
			newSlideId.RelationshipId = presentationPart.GetIdOfPart(slidePart);

			// Save the modified presentation.
			presentationPart.Presentation.Save();
		}

	}


	public class GeneratedClass
		{
		// Creates a PresentationDocument.
		public static void CreatePackage(String filePath)
			{
			//using (PresentationDocument package = PresentationDocument.Create(filePath, PresentationDocumentType.Presentation))
			using (PresentationDocument package = PresentationDocument.Open("C:\\Development\\13 Power Template.pptm",true))
				{

				//PresentationPart presentationPart = InitialPresentationSetup(package); //makes blank pres with 0 slides
				PresentationPart presentationPart = package.PresentationPart;

				SlidePart slidePart;
				
				//this is where we would begin looping through the data
				//slidePart = AddSlide(presentationPart, "Technical Unavailability", "Coal something slide 1","Unavailability Factors, %");
				//slidePart = AddSlide(presentationPart, "", "and subtitle slide 2", "");
				//slidePart = AddSlide(presentationPart, "Title slide 3", "", "");
				//slidePart = AddSlide(presentationPart, "Title", "Subtitle slide 4", "");

				SqlConnection conn = new SqlConnection("Data Source=DBS5; Initial Catalog=Power; User ID=sw; Password=sw@1840");

				conn.Open(); // opens the database connection

				SqlCommand slidesCommand = new SqlCommand("SELECT * FROM Slides", conn);
				DataTable slideTable = new DataTable("Slides");
				SqlDataAdapter dapSlides = new SqlDataAdapter(slidesCommand);

				SqlCommand dataCommand = new SqlCommand("SELECT * FROM SlideData", conn);
				DataTable dataTable = new DataTable("Data");
				SqlDataAdapter dapData = new SqlDataAdapter(dataCommand);


				DataTable colorTable = new DataTable("color");
				DataTable queryTable = new DataTable("query");


				//for (int n = 0; n < 100; n++)
				//	{
					slideTable.Clear();
					dapSlides.Fill(slideTable);
					dataTable.Clear();
					dapData.Fill(dataTable);

					foreach (DataRow slideRow in slideTable.Rows)
						{
						//SqlCommand command2 = new SqlCommand("SELECT * FROM " + row["DataSource"].ToString() + "('Power13: Westar',2013,'" + row["Filter1"].ToString() + "')",conn);
						//SqlDataAdapter dap2 = new SqlDataAdapter(command2);
						//queryTable.Clear();
						//dap2.Fill(queryTable);

						//need some code here to decide if it is a record with 0 data, or if the query returned 0 data.
						//if the first, we should still build the slide, because it may just be a static slide
						//if the second, we may also need a flag to show whether the user wants to run a blank template
						//if (queryTable.Rows.Count > 0)
						//	{


						//we add a blank slide with some header info
						slidePart = AddSlide(presentationPart, slideRow["Title"].ToString(), slideRow["SubTitle"].ToString());//, row["XAxisTitle"].ToString(), queryTable);

						//int numDataRows = 0;
						//C.PlotArea plotArea = new C.PlotArea();
						////ChartPart chartPart = new ChartPart();

						C.PlotArea plotArea = new C.PlotArea(); ; //need this to be null to begin
						Boolean plotFlag = false; //this tells us if we need to create the new plotArea or not
						String xAxisTitle = "";

						//////we look to find any matching data rows so we can populate their data
						foreach (DataRow dataRow in dataTable.Rows)
							{

							if (dataRow["ParentID"].ToString() == slideRow["SlideID"].ToString())
								{
								//there's going to be a chart added, so insert it first
								if (plotFlag == false)
									{
									plotArea = AddPlotArea(slidePart);
									plotFlag = true;
									}

								//get the data
								SqlCommand command2 = new SqlCommand("SELECT * FROM " + dataRow["DataSource"].ToString() + "('" + dataRow["Filter1"].ToString() + "','" + dataRow["Filter2"].ToString() + "','" + dataRow["Filter3"].ToString() + "')", conn);
								SqlDataAdapter dap2 = new SqlDataAdapter(command2);
								queryTable.Reset();
								dap2.Fill(queryTable);

								//get the colors
								colorTable.Reset();
								if (dataRow["SlideColorGroup"] != null)
									{
									SqlCommand command3 = new SqlCommand("SELECT Color, GradientColor FROM SlideColors WHERE SlideColorGroup = " + dataRow["SlideColorGroup"], conn);
									SqlDataAdapter dap3 = new SqlDataAdapter(command3);
									dap3.Fill(colorTable);
									}

								xAxisTitle = dataRow["AxisTitle"].ToString();
								//AddChart(slidePart, queryTable, dataRow["DataType"].ToString(), colorTable, dataRow["AxisTitle"].ToString());
								AddDataToChart(plotArea, queryTable, dataRow["DataType"].ToString(), colorTable);

								}
							}
						//after we've gone through all the data rows for this slide, close the plot area if we need to
						if (plotFlag == true)
							{
							//if true, we created a plot area, now need to close it
							ClosePlotArea(slidePart, plotArea, xAxisTitle);
							}
						}
					//}
				conn.Close();


				//experiment on adding slide from another presentation
				//for (int n = 0; n < 30; n++)
				//	{
					//AddSlideFromAnotherPresentation(presentationPart, 0);
				//	//AddSlideFromAnotherPresentation(presentationPart, 1);
				//	//AddSlideFromAnotherPresentation(presentationPart, 2);
				//	}

				// Save the modified presentation.
				presentationPart.Presentation.Save();


				}
			}

		#region Slide Setup

		private static void AddSlideFromAnotherPresentation(PresentationPart presentationPart, int slidenumber)
			{

			//using (PresentationDocument newDocument = PresentationDocument.Open(OutputFileText.Text, true))
			//	{
			using (PresentationDocument templateDocument = PresentationDocument.Open("K:\\STUDY\\Power\\2013\\Presentations\\Template\\13 Power Template.pptm", false))
				{

				for (int n = 0; n < 30; n++)
					{
					uint uniqueId = GetMaxIdFromChild(presentationPart.Presentation.SlideMasterIdList);
					uint maxId = GetMaxIdFromChild(presentationPart.Presentation.SlideIdList);

					//SlidePart oldPart = GetSlidePartByTagName(templateDocument, SlideToCopyText.Text);
					SlideIdList oldIdList = templateDocument.PresentationPart.Presentation.SlideIdList;
					SlideId oldSlideId = oldIdList.ChildElements[n] as SlideId;
					String oldSlideRelId = oldSlideId.RelationshipId;
					SlidePart oldPart = templateDocument.PresentationPart.GetPartById(oldSlideRelId) as SlidePart;
					//SlideLayoutPart oldslp = oldPart.SlideLayoutPart;
					//oldPart.DeletePart(oldslp);

					//oldPart.SlideLayoutPart = null;
					//oldPart.DeletePart(SlideLayoutPart);
					//				oldPart
					//SlidePart oldPart = templateDocument.PresentationPart.SlideParts.FirstOrDefault();
					SlidePart newPart = presentationPart.AddPart<SlidePart>(oldPart);
					//newPart.SlideLayoutPart = null;
					
					
					SlideMasterPart slideMasterPart = presentationPart.SlideMasterParts.First();
					SlideLayoutPart slideLayoutPart = slideMasterPart.SlideLayoutParts.SingleOrDefault();
					SlideLayoutPart oldLP = newPart.SlideLayoutPart;
					newPart.DeletePart(oldLP);
					newPart.AddPart(slideLayoutPart);


					//SlideMasterPart newMasterPart = presentationPart.AddPart(newPart.SlideLayoutPart.SlideMasterPart);
					//				SlideMasterPart slideMasterPart = presentationPart.SlideMasterParts.FirstOrDefault();
					SlideIdList idList = presentationPart.Presentation.SlideIdList;

					// create new slide ID
					maxId++;
					SlideId newId = new SlideId();
					newId.Id = maxId;
					newId.RelationshipId = presentationPart.GetIdOfPart(newPart);
					idList.Append(newId);

					// Create new master slide ID
					//uniqueId++;
					//SlideMasterId newMasterId = new SlideMasterId();
					//newMasterId.Id = uniqueId;
					//newMasterId.RelationshipId = presentationPart.GetIdOfPart(slideMasterPart);
					//presentationPart.Presentation.SlideMasterIdList.Append(newMasterId);

					// change slide layout ID
					FixSlideLayoutIds(presentationPart, uniqueId);

					}
				//newPart.Slide.Save();
				//newDocument.PresentationPart.Presentation.Save();
				//}
				}


			//PresentationDocument template = PresentationDocument.Open("K:\\STUDY\\Power\\2013\\Presentations\\Template\\13 Power Template.pptm",false);

			////uint uniqueId = GetMaxIdFromChild(template.PresentationPart.Presentation.SlideMasterIdList);
			////uint maxId = GetMaxIdFromChild(template.PresentationPart.Presentation.SlideIdList);

			//SlidePart oldPart = template.PresentationPart.SlideParts.FirstOrDefault();
			//SlidePart newPart = presentationPart.AddPart<SlidePart>(oldPart);
			////ConnectBlankSlide(presentationPart, newPart);


			//SlideMasterPart slideMasterPart = presentationPart.SlideMasterParts.First();
			////SlideLayoutPart slideLayoutPart = slideMasterPart.SlideLayoutParts.SingleOrDefault();

			//SlideIdList idList = presentationPart.Presentation.SlideIdList;

			//uint maxId = GetMaxIdFromChild(idList);
			//maxId++;

			//SlideId newSlideId = idList.AppendChild<SlideId>(new SlideId());
			//newSlideId.Id = maxId;
			//newSlideId.RelationshipId = presentationPart.GetIdOfPart(newPart);

			//////newPart.AddPart(slideLayoutPart, "rId34");
			//////and add it to the slideIDList
			////SlideIdList slideIdList = presentationPart.Presentation.SlideIdList;

			//////create a slideID for the new slide, which will be 256 (minimum slideID per the spec) plus the count of slides
			////uint slideId = 256U;
			////slideId += Convert.ToUInt32(presentationPart.Presentation.SlideIdList.Count());

			//////append a new slide Id, set it to the calculated slideId value, and relate it to the new slidePart
			////SlideId newSlideId = slideIdList.AppendChild<SlideId>(new SlideId());

			////newSlideId.Id = slideId;
			////newSlideId.RelationshipId = presentationPart.GetIdOfPart(newPart);

			}

		static void FixSlideLayoutIds(PresentationPart presPart, uint uniqueId)
			{
			//Need to make sure all slide layouts have unique ids
			foreach (SlideMasterPart slideMasterPart in presPart.SlideMasterParts)
				{
				foreach (SlideLayoutId slideLayoutId in slideMasterPart.SlideMaster.SlideLayoutIdList)
					{
					uniqueId++;
					slideLayoutId.Id = (uint)uniqueId;
					}
				slideMasterPart.SlideMaster.Save();
				}
			}

		static uint GetMaxIdFromChild(OpenXmlElement el)
			{
			uint max = 1;
			//Get max id value from set of children
			foreach (OpenXmlElement child in el.ChildElements)
				{
				OpenXmlAttribute attribute = child.GetAttribute("id", "");

				uint id = uint.Parse(attribute.Value);
				if (id > max)
					max = id;
				}
			return max;
			}

		private static SlidePart AddSlide(PresentationPart presentationPart, String slideTitle, String slideSubtitle)//, String xAxisLabel, DataTable queryTable)
			{

			SlidePart slidePart = presentationPart.AddNewPart<SlidePart>("rId" + (presentationPart.SlideParts.Count()+10));
			slidePart.Slide = CreateBlankSlide();
			ConnectBlankSlide(presentationPart, slidePart);

			//now we've created and connected the slide, so do something with it
			AddTitle(slidePart, slideTitle, slideSubtitle);

			//AddChart(slidePart, xAxisLabel, queryTable);





			return slidePart;

			}

		private static Slide CreateBlankSlide()
			{
			
			Slide slide = new Slide();

			CommonSlideData commonSlideData = new CommonSlideData();

			ShapeTree shapeTree = new ShapeTree();

			P.NonVisualGroupShapeProperties nonVisualGroupShapeProperties = new P.NonVisualGroupShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties = new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" };
			P.NonVisualGroupShapeDrawingProperties nonVisualGroupShapeDrawingProperties = new P.NonVisualGroupShapeDrawingProperties();
			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties = new ApplicationNonVisualDrawingProperties();

			nonVisualGroupShapeProperties.Append(nonVisualDrawingProperties);
			nonVisualGroupShapeProperties.Append(nonVisualGroupShapeDrawingProperties);
			nonVisualGroupShapeProperties.Append(applicationNonVisualDrawingProperties);

			GroupShapeProperties groupShapeProperties = new GroupShapeProperties();

			shapeTree.Append(nonVisualGroupShapeProperties);
			shapeTree.Append(groupShapeProperties);

			commonSlideData.Append(shapeTree);
			slide.Append(commonSlideData);

			return slide;
			}

		private static void ConnectBlankSlide(PresentationPart presentationPart, SlidePart slidePart)
			{

			SlideLayoutPart slideLayoutPart;// = new SlideLayoutPart();
			SlideMasterPart slideMasterPart;// = new SlideMasterPart();

			if (presentationPart.SlideMasterParts.Count<SlideMasterPart>() == 0)
				{
				//if we don't have a SlideMaster then we don't have a SlideLayout either, so create both

				//add a SlideLayout which is required in a presentation
				slideLayoutPart = AddSlideLayout(slidePart);

				//add a SlideMaster which is required in a presentation
				slideMasterPart = AddSlideMaster(slideLayoutPart);

				//add some styles to the master
				//AddTextStylesToMaster(slideMasterPart);

				slideMasterPart.AddPart(presentationPart.ThemePart, "rId2");

				//connect the slideLayout to the slideMaster
				slideMasterPart.AddPart(slideLayoutPart, "rId1");

				//connect the slideMaster to the presentation
				presentationPart.AddPart(slideMasterPart, "rId1");

				}
			else
				{
				//we have a SlideMaster, connect the new slide to the slideLayout
				slideMasterPart = presentationPart.SlideMasterParts.First();
				slideLayoutPart = slideMasterPart.SlideLayoutParts.FirstOrDefault();
				slidePart.AddPart(slideLayoutPart, "rId21");

				//and add it to the slideIDList
				SlideIdList slideIdList = presentationPart.Presentation.SlideIdList;

				//create a slideID for the new slide, which will be 256 (minimum slideID per the spec) plus the count of slides
				uint slideId = 256U;
				//slideId += Convert.ToUInt32(presentationPart.Presentation.SlideIdList.Count());

				foreach (SlideId slId in presentationPart.Presentation.SlideIdList)
					{
					if (slId.Id > slideId)
						{
						slideId = slId.Id;
						}
					}
				slideId++;

				//append a new slide Id, set it to the calculated slideId value, and relate it to the new slidePart
				SlideId newSlideId = slideIdList.AppendChild<SlideId>(new SlideId());

				newSlideId.Id = slideId;
				newSlideId.RelationshipId = presentationPart.GetIdOfPart(slidePart);
				}

			}

		private static PresentationPart InitialPresentationSetup(PresentationDocument presentationDoc)
			{

			//this creates a blank presentationPart, adds some Id lists and a theme to it (there will be no slides at the end of this)

			PresentationPart presentationPart = presentationDoc.AddPresentationPart();
			presentationPart.Presentation = new Presentation();

			SlideMasterIdList slideMasterIdList1 = new SlideMasterIdList(new SlideMasterId() { Id = (UInt32Value)2147483648U, RelationshipId = "rId1" });
			SlideIdList slideIdList1 = new SlideIdList(new SlideId() { Id = (UInt32Value)256U, RelationshipId = "rId10" });
			SlideSize slideSize1 = new SlideSize() { Cx = 9144000, Cy = 6858000 };
			NotesSize notesSize1 = new NotesSize() { Cx = 9144000, Cy = 6858000 };
			DefaultTextStyle defaultTextStyle1 = new DefaultTextStyle();

			presentationPart.Presentation.Append(slideMasterIdList1, slideIdList1, slideSize1, notesSize1, defaultTextStyle1);

			ThemePart themePart = AddTheme(presentationPart);

			return presentationPart;

			}

		private static SlideLayoutPart AddSlideLayout(SlidePart slidePart)
			{
			 
			SlideLayoutPart slideLayoutPart = slidePart.AddNewPart<SlideLayoutPart>("rId1");
			SlideLayout slideLayout = new SlideLayout(
			new CommonSlideData(new ShapeTree(
							new P.NonVisualGroupShapeProperties(
								new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
								new P.NonVisualGroupShapeDrawingProperties(),
								new ApplicationNonVisualDrawingProperties()),
							new GroupShapeProperties())));
			slideLayoutPart.SlideLayout = slideLayout;

			return slideLayoutPart;

			}

		private static void AddTextStylesToMaster(SlideMasterPart slideMasterPart)
			{
			TextStyles textStyles1 = new TextStyles();

			TitleStyle titleStyle1 = new TitleStyle();

			A.Level1ParagraphProperties level1ParagraphProperties9 = new A.Level1ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, EastAsianLineBreak = false, FontAlignment = A.TextFontAlignmentValues.Baseline, Height = false };

			A.SpaceBefore spaceBefore13 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent27 = new A.SpacingPercent() { Val = 0 };

			spaceBefore13.Append(spacingPercent27);

			A.SpaceAfter spaceAfter13 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent28 = new A.SpacingPercent() { Val = 0 };

			spaceAfter13.Append(spacingPercent28);

			A.DefaultRunProperties defaultRunProperties37 = new A.DefaultRunProperties() { FontSize = 3000, Bold = true };

			A.SolidFill solidFill44 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex186 = new A.RgbColorModelHex() { Val = "98002E" };

			solidFill44.Append(rgbColorModelHex186);
			A.LatinFont latinFont29 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont23 = new A.EastAsianFont() { Typeface = "+mj-ea" };
			A.ComplexScriptFont complexScriptFont23 = new A.ComplexScriptFont() { Typeface = "+mj-cs" };

			defaultRunProperties37.Append(solidFill44);
			defaultRunProperties37.Append(latinFont29);
			defaultRunProperties37.Append(eastAsianFont23);
			defaultRunProperties37.Append(complexScriptFont23);

			level1ParagraphProperties9.Append(spaceBefore13);
			level1ParagraphProperties9.Append(spaceAfter13);
			level1ParagraphProperties9.Append(defaultRunProperties37);


			titleStyle1.Append(level1ParagraphProperties9);

			textStyles1.Append(titleStyle1);

			slideMasterPart.SlideMaster.Append(textStyles1);

			}

		private static SlideMasterPart AddSlideMaster(SlideLayoutPart slideLayoutPart)
			{

			SlideMasterPart slideMasterPart = slideLayoutPart.AddNewPart<SlideMasterPart>("rId1");
			//SlideMaster slideMaster = 
			//	new SlideMaster
			//		(
			//		new CommonSlideData
			//			(
			//			new ShapeTree
			//				(
			//				new P.NonVisualGroupShapeProperties
			//					(
			//					new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" },
			//					new P.NonVisualGroupShapeDrawingProperties(),
			//					new ApplicationNonVisualDrawingProperties()
			//					),
			//				new GroupShapeProperties
			//					(
			//					new TransformGroup()
			//					)
			//				)
			//			),
			//		new P.ColorMap() { Background1 = D.ColorSchemeIndexValues.Light1, Text1 = D.ColorSchemeIndexValues.Dark1, Background2 = D.ColorSchemeIndexValues.Light2, Text2 = D.ColorSchemeIndexValues.Dark2, Accent1 = D.ColorSchemeIndexValues.Accent1, Accent2 = D.ColorSchemeIndexValues.Accent2, Accent3 = D.ColorSchemeIndexValues.Accent3, Accent4 = D.ColorSchemeIndexValues.Accent4, Accent5 = D.ColorSchemeIndexValues.Accent5, Accent6 = D.ColorSchemeIndexValues.Accent6, Hyperlink = D.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = D.ColorSchemeIndexValues.FollowedHyperlink },
			//		new SlideLayoutIdList(new SlideLayoutId() { Id = (UInt32Value)2147483649U, RelationshipId = "rId1" })
			//		);







			SlideMaster slideMaster1 = new SlideMaster();// { Preserve = true };
			//slideMaster1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");
			//slideMaster1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
			//slideMaster1.AddNamespaceDeclaration("p", "http://schemas.openxmlformats.org/presentationml/2006/main");

			CommonSlideData commonSlideData2 = new CommonSlideData();

			//Background background2 = new Background();

			//BackgroundProperties backgroundProperties1 = new BackgroundProperties();

			//A.SolidFill solidFill40 = new A.SolidFill();
			//A.SchemeColor schemeColor56 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			//solidFill40.Append(schemeColor56);
			//A.EffectList effectList4 = new A.EffectList();

			//backgroundProperties1.Append(solidFill40);
			//backgroundProperties1.Append(effectList4);

			//background2.Append(backgroundProperties1);

			ShapeTree shapeTree2 = new ShapeTree();

			P.NonVisualGroupShapeProperties nonVisualGroupShapeProperties2 = new P.NonVisualGroupShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties8 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)1U, Name = "" };
			P.NonVisualGroupShapeDrawingProperties nonVisualGroupShapeDrawingProperties2 = new P.NonVisualGroupShapeDrawingProperties();
			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties8 = new ApplicationNonVisualDrawingProperties();

			nonVisualGroupShapeProperties2.Append(nonVisualDrawingProperties8);
			nonVisualGroupShapeProperties2.Append(nonVisualGroupShapeDrawingProperties2);
			nonVisualGroupShapeProperties2.Append(applicationNonVisualDrawingProperties8);

			GroupShapeProperties groupShapeProperties2 = new GroupShapeProperties();

			A.TransformGroup transformGroup2 = new A.TransformGroup();
			A.Offset offset8 = new A.Offset() { X = 0L, Y = 0L };
			A.Extents extents8 = new A.Extents() { Cx = 0L, Cy = 0L };
			A.ChildOffset childOffset2 = new A.ChildOffset() { X = 0L, Y = 0L };
			A.ChildExtents childExtents2 = new A.ChildExtents() { Cx = 0L, Cy = 0L };

			transformGroup2.Append(offset8);
			transformGroup2.Append(extents8);
			transformGroup2.Append(childOffset2);
			transformGroup2.Append(childExtents2);

			groupShapeProperties2.Append(transformGroup2);

			P.Shape shape7 = new P.Shape();

			P.NonVisualShapeProperties nonVisualShapeProperties7 = new P.NonVisualShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties9 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)589826U, Name = "Rectangle 2" };

			P.NonVisualShapeDrawingProperties nonVisualShapeDrawingProperties7 = new P.NonVisualShapeDrawingProperties();
			A.ShapeLocks shapeLocks7 = new A.ShapeLocks() { NoGrouping = true, NoChangeArrowheads = true };

			nonVisualShapeDrawingProperties7.Append(shapeLocks7);

			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties9 = new ApplicationNonVisualDrawingProperties();
			PlaceholderShape placeholderShape7 = new PlaceholderShape() { Type = PlaceholderValues.Title };

			applicationNonVisualDrawingProperties9.Append(placeholderShape7);

			nonVisualShapeProperties7.Append(nonVisualDrawingProperties9);
			nonVisualShapeProperties7.Append(nonVisualShapeDrawingProperties7);
			nonVisualShapeProperties7.Append(applicationNonVisualDrawingProperties9);

			P.ShapeProperties shapeProperties7 = new P.ShapeProperties() { BlackWhiteMode = A.BlackWhiteModeValues.Auto };

			A.Transform2D transform2D7 = new A.Transform2D();
			A.Offset offset9 = new A.Offset() { X = 457200L, Y = 182563L };
			A.Extents extents9 = new A.Extents() { Cx = 8343900L, Cy = 533400L };

			transform2D7.Append(offset9);
			transform2D7.Append(extents9);

			A.PresetGeometry presetGeometry7 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
			A.AdjustValueList adjustValueList7 = new A.AdjustValueList();

			presetGeometry7.Append(adjustValueList7);
			A.NoFill noFill2 = new A.NoFill();

			A.Outline outline13 = new A.Outline();
			A.NoFill noFill3 = new A.NoFill();

			outline13.Append(noFill3);

			A.ShapePropertiesExtensionList shapePropertiesExtensionList1 = new A.ShapePropertiesExtensionList();

			A.ShapePropertiesExtension shapePropertiesExtension1 = new A.ShapePropertiesExtension() { Uri = "{909E8E84-426E-40DD-AFC4-6F175D3DCCD1}" };

			A14.HiddenFillProperties hiddenFillProperties1 = new A14.HiddenFillProperties();
			hiddenFillProperties1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.SolidFill solidFill41 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex14 = new A.RgbColorModelHex() { Val = "FFFFFF" };

			solidFill41.Append(rgbColorModelHex14);

			hiddenFillProperties1.Append(solidFill41);

			shapePropertiesExtension1.Append(hiddenFillProperties1);

			A.ShapePropertiesExtension shapePropertiesExtension2 = new A.ShapePropertiesExtension() { Uri = "{91240B29-F687-4F45-9708-019B960494DF}" };

			A14.HiddenLineProperties hiddenLineProperties1 = new A14.HiddenLineProperties() { Width = 9525 };
			hiddenLineProperties1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.SolidFill solidFill42 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex15 = new A.RgbColorModelHex() { Val = "000000" };

			solidFill42.Append(rgbColorModelHex15);
			A.Miter miter1 = new A.Miter() { Limit = 800000 };
			A.HeadEnd headEnd1 = new A.HeadEnd();
			A.TailEnd tailEnd1 = new A.TailEnd();

			hiddenLineProperties1.Append(solidFill42);
			hiddenLineProperties1.Append(miter1);
			hiddenLineProperties1.Append(headEnd1);
			hiddenLineProperties1.Append(tailEnd1);

			shapePropertiesExtension2.Append(hiddenLineProperties1);

			shapePropertiesExtensionList1.Append(shapePropertiesExtension1);
			shapePropertiesExtensionList1.Append(shapePropertiesExtension2);

			shapeProperties7.Append(transform2D7);
			shapeProperties7.Append(presetGeometry7);
			shapeProperties7.Append(noFill2);
			shapeProperties7.Append(outline13);
			shapeProperties7.Append(shapePropertiesExtensionList1);

			P.TextBody textBody7 = new P.TextBody();

			A.BodyProperties bodyProperties7 = new A.BodyProperties() { Vertical = A.TextVerticalValues.Horizontal, Wrap = A.TextWrappingValues.Square, LeftInset = 91440, TopInset = 45720, RightInset = 91440, BottomInset = 45720, ColumnCount = 1, Anchor = A.TextAnchoringTypeValues.Top, AnchorCenter = false, CompatibleLineSpacing = true };

			A.PresetTextWrap presetTextWrap1 = new A.PresetTextWrap() { Preset = A.TextShapeValues.TextNoShape };
			A.AdjustValueList adjustValueList8 = new A.AdjustValueList();

			presetTextWrap1.Append(adjustValueList8);
			A.ShapeAutoFit shapeAutoFit1 = new A.ShapeAutoFit();

			bodyProperties7.Append(presetTextWrap1);
			bodyProperties7.Append(shapeAutoFit1);
			A.ListStyle listStyle7 = new A.ListStyle();

			A.Paragraph paragraph11 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties6 = new A.ParagraphProperties() { Level = 0 };

			A.Run run6 = new A.Run();
			A.RunProperties runProperties8 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			A.Text text8 = new A.Text();
			text8.Text = "Click to edit Master title style";

			run6.Append(runProperties8);
			run6.Append(text8);

			paragraph11.Append(paragraphProperties6);
			paragraph11.Append(run6);

			textBody7.Append(bodyProperties7);
			textBody7.Append(listStyle7);
			textBody7.Append(paragraph11);

			shape7.Append(nonVisualShapeProperties7);
			shape7.Append(shapeProperties7);
			shape7.Append(textBody7);







			P.Shape shape8 = new P.Shape();

			P.NonVisualShapeProperties nonVisualShapeProperties8 = new P.NonVisualShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties10 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)589827U, Name = "Line 3" };

			P.NonVisualShapeDrawingProperties nonVisualShapeDrawingProperties8 = new P.NonVisualShapeDrawingProperties();
			A.ShapeLocks shapeLocks8 = new A.ShapeLocks() { NoChangeShapeType = true };

			nonVisualShapeDrawingProperties8.Append(shapeLocks8);
			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties10 = new ApplicationNonVisualDrawingProperties();

			nonVisualShapeProperties8.Append(nonVisualDrawingProperties10);
			nonVisualShapeProperties8.Append(nonVisualShapeDrawingProperties8);
			nonVisualShapeProperties8.Append(applicationNonVisualDrawingProperties10);

			P.ShapeProperties shapeProperties8 = new P.ShapeProperties() { BlackWhiteMode = A.BlackWhiteModeValues.Auto };

			A.Transform2D transform2D8 = new A.Transform2D();
			A.Offset offset10 = new A.Offset() { X = 563563L, Y = 6323013L };
			A.Extents extents10 = new A.Extents() { Cx = 8018462L, Cy = 0L };

			transform2D8.Append(offset10);
			transform2D8.Append(extents10);

			A.PresetGeometry presetGeometry8 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Line };
			A.AdjustValueList adjustValueList9 = new A.AdjustValueList();

			presetGeometry8.Append(adjustValueList9);
			A.NoFill noFill4 = new A.NoFill();

			A.Outline outline14 = new A.Outline() { Width = 19050 };

			A.SolidFill solidFill43 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex16 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill43.Append(rgbColorModelHex16);
			A.Round round1 = new A.Round();
			A.HeadEnd headEnd2 = new A.HeadEnd();
			A.TailEnd tailEnd2 = new A.TailEnd();

			outline14.Append(solidFill43);
			outline14.Append(round1);
			outline14.Append(headEnd2);
			outline14.Append(tailEnd2);
			A.EffectList effectList5 = new A.EffectList();

			A.ShapePropertiesExtensionList shapePropertiesExtensionList2 = new A.ShapePropertiesExtensionList();

			A.ShapePropertiesExtension shapePropertiesExtension3 = new A.ShapePropertiesExtension() { Uri = "{909E8E84-426E-40DD-AFC4-6F175D3DCCD1}" };

			A14.HiddenFillProperties hiddenFillProperties2 = new A14.HiddenFillProperties();
			hiddenFillProperties2.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");
			A.NoFill noFill5 = new A.NoFill();

			hiddenFillProperties2.Append(noFill5);

			shapePropertiesExtension3.Append(hiddenFillProperties2);

			A.ShapePropertiesExtension shapePropertiesExtension4 = new A.ShapePropertiesExtension() { Uri = "{AF507438-7753-43E0-B8FC-AC1667EBCBE1}" };

			A14.HiddenEffectsProperties hiddenEffectsProperties1 = new A14.HiddenEffectsProperties();
			hiddenEffectsProperties1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.EffectList effectList6 = new A.EffectList();

			A.OuterShadow outerShadow4 = new A.OuterShadow() { Distance = 35921L, Direction = 2700000, Alignment = A.RectangleAlignmentValues.Center, RotateWithShape = false };
			A.SchemeColor schemeColor57 = new A.SchemeColor() { Val = A.SchemeColorValues.Background2 };

			outerShadow4.Append(schemeColor57);

			effectList6.Append(outerShadow4);

			hiddenEffectsProperties1.Append(effectList6);

			shapePropertiesExtension4.Append(hiddenEffectsProperties1);

			shapePropertiesExtensionList2.Append(shapePropertiesExtension3);
			shapePropertiesExtensionList2.Append(shapePropertiesExtension4);

			shapeProperties8.Append(transform2D8);
			shapeProperties8.Append(presetGeometry8);
			shapeProperties8.Append(noFill4);
			shapeProperties8.Append(outline14);
			shapeProperties8.Append(effectList5);
			shapeProperties8.Append(shapePropertiesExtensionList2);

			P.TextBody textBody8 = new P.TextBody();
			A.BodyProperties bodyProperties8 = new A.BodyProperties();
			A.ListStyle listStyle8 = new A.ListStyle();

			A.Paragraph paragraph12 = new A.Paragraph();

			A.ParagraphProperties paragraphProperties7 = new A.ParagraphProperties() { FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore1 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent1 = new A.SpacingPercent() { Val = 0 };

			spaceBefore1.Append(spacingPercent1);

			A.SpaceAfter spaceAfter1 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent2 = new A.SpacingPercent() { Val = 0 };

			spaceAfter1.Append(spacingPercent2);

			paragraphProperties7.Append(spaceBefore1);
			paragraphProperties7.Append(spaceAfter1);

			A.EndParagraphRunProperties endParagraphRunProperties7 = new A.EndParagraphRunProperties() { Language = "en-US", FontSize = 1400 };

			A.SolidFill solidFill44 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex17 = new A.RgbColorModelHex() { Val = "FFFFFF" };

			solidFill44.Append(rgbColorModelHex17);

			endParagraphRunProperties7.Append(solidFill44);

			paragraph12.Append(paragraphProperties7);
			paragraph12.Append(endParagraphRunProperties7);

			textBody8.Append(bodyProperties8);
			textBody8.Append(listStyle8);
			textBody8.Append(paragraph12);

			shape8.Append(nonVisualShapeProperties8);
			shape8.Append(shapeProperties8);
			shape8.Append(textBody8);







			P.Shape shape59 = new P.Shape();

			P.NonVisualShapeProperties nonVisualShapeProperties59 = new P.NonVisualShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties62 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)589880U, Name = "Text Box 56" };

			P.NonVisualShapeDrawingProperties nonVisualShapeDrawingProperties59 = new P.NonVisualShapeDrawingProperties() { TextBox = true };
			A.ShapeLocks shapeLocks59 = new A.ShapeLocks() { NoChangeArrowheads = true };

			nonVisualShapeDrawingProperties59.Append(shapeLocks59);
			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties62 = new ApplicationNonVisualDrawingProperties();

			nonVisualShapeProperties59.Append(nonVisualDrawingProperties62);
			nonVisualShapeProperties59.Append(nonVisualShapeDrawingProperties59);
			nonVisualShapeProperties59.Append(applicationNonVisualDrawingProperties62);

			P.ShapeProperties shapeProperties59 = new P.ShapeProperties() { BlackWhiteMode = A.BlackWhiteModeValues.Auto };

			A.Transform2D transform2D59 = new A.Transform2D();
			A.Offset offset62 = new A.Offset() { X = 7288463L, Y = 6396038L };
			A.Extents extents62 = new A.Extents() { Cx = 1287212L, Cy = 276999L };

			transform2D59.Append(offset62);
			transform2D59.Append(extents62);

			A.PresetGeometry presetGeometry12 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
			A.AdjustValueList adjustValueList60 = new A.AdjustValueList();

			presetGeometry12.Append(adjustValueList60);
			A.NoFill noFill56 = new A.NoFill();

			A.Outline outline65 = new A.Outline();
			A.NoFill noFill57 = new A.NoFill();

			outline65.Append(noFill57);
			A.EffectList effectList7 = new A.EffectList();

			A.ShapePropertiesExtensionList shapePropertiesExtensionList53 = new A.ShapePropertiesExtensionList();

			A.ShapePropertiesExtension shapePropertiesExtension55 = new A.ShapePropertiesExtension() { Uri = "{909E8E84-426E-40DD-AFC4-6F175D3DCCD1}" };

			A14.HiddenFillProperties hiddenFillProperties3 = new A14.HiddenFillProperties();
			hiddenFillProperties3.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.SolidFill solidFill222 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex195 = new A.RgbColorModelHex() { Val = "3366CC" };

			solidFill222.Append(rgbColorModelHex195);

			hiddenFillProperties3.Append(solidFill222);

			shapePropertiesExtension55.Append(hiddenFillProperties3);

			A.ShapePropertiesExtension shapePropertiesExtension56 = new A.ShapePropertiesExtension() { Uri = "{91240B29-F687-4F45-9708-019B960494DF}" };

			A14.HiddenLineProperties hiddenLineProperties52 = new A14.HiddenLineProperties() { Width = 9525 };
			hiddenLineProperties52.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.SolidFill solidFill223 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex196 = new A.RgbColorModelHex() { Val = "FFFFFF" };

			solidFill223.Append(rgbColorModelHex196);
			A.Miter miter2 = new A.Miter() { Limit = 800000 };
			A.HeadEnd headEnd53 = new A.HeadEnd();
			A.TailEnd tailEnd53 = new A.TailEnd();

			hiddenLineProperties52.Append(solidFill223);
			hiddenLineProperties52.Append(miter2);
			hiddenLineProperties52.Append(headEnd53);
			hiddenLineProperties52.Append(tailEnd53);

			shapePropertiesExtension56.Append(hiddenLineProperties52);

			A.ShapePropertiesExtension shapePropertiesExtension57 = new A.ShapePropertiesExtension() { Uri = "{AF507438-7753-43E0-B8FC-AC1667EBCBE1}" };

			A14.HiddenEffectsProperties hiddenEffectsProperties2 = new A14.HiddenEffectsProperties();
			hiddenEffectsProperties2.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.EffectList effectList8 = new A.EffectList();

			A.OuterShadow outerShadow5 = new A.OuterShadow() { Distance = 35921L, Direction = 2700000, Alignment = A.RectangleAlignmentValues.Center, RotateWithShape = false };
			A.SchemeColor schemeColor85 = new A.SchemeColor() { Val = A.SchemeColorValues.Background2 };

			outerShadow5.Append(schemeColor85);

			effectList8.Append(outerShadow5);

			hiddenEffectsProperties2.Append(effectList8);

			shapePropertiesExtension57.Append(hiddenEffectsProperties2);

			shapePropertiesExtensionList53.Append(shapePropertiesExtension55);
			shapePropertiesExtensionList53.Append(shapePropertiesExtension56);
			shapePropertiesExtensionList53.Append(shapePropertiesExtension57);

			shapeProperties59.Append(transform2D59);
			shapeProperties59.Append(presetGeometry12);
			shapeProperties59.Append(noFill56);
			shapeProperties59.Append(outline65);
			shapeProperties59.Append(effectList7);
			shapeProperties59.Append(shapePropertiesExtensionList53);

			P.TextBody textBody59 = new P.TextBody();

			A.BodyProperties bodyProperties59 = new A.BodyProperties() { Wrap = A.TextWrappingValues.None, LeftInset = 0, TopInset = 0, RightInset = 0, BottomInset = 0 };
			A.ShapeAutoFit shapeAutoFit2 = new A.ShapeAutoFit();

			bodyProperties59.Append(shapeAutoFit2);
			A.ListStyle listStyle59 = new A.ListStyle();

			//A.Paragraph paragraph63 = new A.Paragraph();

			//A.ParagraphProperties paragraphProperties58 = new A.ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Right, FontAlignment = A.TextFontAlignmentValues.Baseline };

			//A.SpaceBefore spaceBefore79 = new A.SpaceBefore();
			//A.SpacingPercent spacingPercent142 = new A.SpacingPercent() { Val = 0 };

			//spaceBefore79.Append(spacingPercent142);

			//A.SpaceAfter spaceAfter64 = new A.SpaceAfter();
			//A.SpacingPercent spacingPercent143 = new A.SpacingPercent() { Val = 0 };

			//spaceAfter64.Append(spacingPercent143);

			//paragraphProperties58.Append(spaceBefore79);
			//paragraphProperties58.Append(spaceAfter64);

			//A.Run run7 = new A.Run();

			//A.RunProperties runProperties9 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", FontSize = 600, Dirty = false };

			//A.SolidFill solidFill224 = new A.SolidFill();
			//A.RgbColorModelHex rgbColorModelHex197 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			//solidFill224.Append(rgbColorModelHex197);

			//runProperties9.Append(solidFill224);
			//A.Text text9 = new A.Text();
			//text9.Text = "Proprietary and Confidential";

			//run7.Append(runProperties9);
			//run7.Append(text9);
			//A.Run run7 = MakeRun("Proprietary and Confidential", 6, "4D4D4F");

			//paragraph63.Append(paragraphProperties58);
			//paragraph63.Append(run7);

			//A.Paragraph paragraph64 = new A.Paragraph();

			//A.ParagraphProperties paragraphProperties59 = new A.ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Right, FontAlignment = A.TextFontAlignmentValues.Baseline };

			//A.SpaceBefore spaceBefore80 = new A.SpaceBefore();
			//A.SpacingPercent spacingPercent144 = new A.SpacingPercent() { Val = 0 };

			//spaceBefore80.Append(spacingPercent144);

			//A.SpaceAfter spaceAfter65 = new A.SpaceAfter();
			//A.SpacingPercent spacingPercent145 = new A.SpacingPercent() { Val = 0 };

			//spaceAfter65.Append(spacingPercent145);

			//paragraphProperties59.Append(spaceBefore80);
			//paragraphProperties59.Append(spaceAfter65);

			//A.Run run8 = new A.Run();

			//A.RunProperties runProperties10 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", FontSize = 600, Dirty = false };

			//A.SolidFill solidFill225 = new A.SolidFill();
			//A.RgbColorModelHex rgbColorModelHex198 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			//solidFill225.Append(rgbColorModelHex198);

			//runProperties10.Append(solidFill225);
			//A.Text text10 = new A.Text();
			//text10.Text = "© ";

			//run8.Append(runProperties10);
			//run8.Append(text10);

			//A.Run run9 = new A.Run();

			//A.RunProperties runProperties11 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", FontSize = 600, Dirty = false, SmartTagClean = false };

			//A.SolidFill solidFill226 = new A.SolidFill();
			//A.RgbColorModelHex rgbColorModelHex199 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			//solidFill226.Append(rgbColorModelHex199);

			//runProperties11.Append(solidFill226);
			//A.Text text11 = new A.Text();
			//text11.Text = "2014 HSB ";

			//run9.Append(runProperties11);
			//run9.Append(text11);

			//A.Run run10 = new A.Run();

			//A.RunProperties runProperties12 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", FontSize = 600, Dirty = false };

			//A.SolidFill solidFill227 = new A.SolidFill();
			//A.RgbColorModelHex rgbColorModelHex200 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			//solidFill227.Append(rgbColorModelHex200);

			//runProperties12.Append(solidFill227);
			//A.Text text12 = new A.Text();
			//text12.Text = "Solomon Associates LLC";

			//run10.Append(runProperties12);
			//run10.Append(text12);
			//A.Run run8 = MakeRun("© 2014 HSB Solomon Associates LLC", 6, "4D4D4F");
			//paragraph64.Append(paragraphProperties59);
			//paragraph64.Append(run8);
			//paragraph64.Append(run9);
			//paragraph64.Append(run10);

			A.Paragraph paragraph65 = new A.Paragraph();

			//A.ParagraphProperties paragraphProperties60 = new A.ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Right, FontAlignment = A.TextFontAlignmentValues.Baseline };

			//A.SpaceBefore spaceBefore81 = new A.SpaceBefore();
			//A.SpacingPercent spacingPercent146 = new A.SpacingPercent() { Val = 0 };

			//spaceBefore81.Append(spacingPercent146);

			//A.SpaceAfter spaceAfter66 = new A.SpaceAfter();
			//A.SpacingPercent spacingPercent147 = new A.SpacingPercent() { Val = 0 };

			//spaceAfter66.Append(spacingPercent147);

			//paragraphProperties60.Append(spaceBefore81);
			//paragraphProperties60.Append(spaceAfter66);

			A.Run run11 = MakeRun("Proprietary and Confidential" + System.Environment.NewLine + "© 2014 HSB Solomon Associates LLC" + System.Environment.NewLine + "www.SolomonOnline.com", 6, "4D4D4F");

			//A.EndParagraphRunProperties endParagraphRunProperties58 = new A.EndParagraphRunProperties() { Language = "en-US", AlternativeLanguage = "en-US", FontSize = 600, Dirty = false };

			//A.SolidFill solidFill229 = new A.SolidFill();
			//A.RgbColorModelHex rgbColorModelHex202 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			//solidFill229.Append(rgbColorModelHex202);

			//endParagraphRunProperties58.Append(solidFill229);

			//paragraph65.Append(paragraphProperties60);
			//paragraph65.Append(run7);
			//paragraph65.Append(run8); 
			paragraph65.Append(run11);
			//paragraph65.Append(endParagraphRunProperties58);

			textBody59.Append(bodyProperties59);
			textBody59.Append(listStyle59);
			//textBody59.Append(paragraph63);
			//textBody59.Append(paragraph64);
			textBody59.Append(paragraph65);

			shape59.Append(nonVisualShapeProperties59);
			shape59.Append(shapeProperties59);
			shape59.Append(textBody59);

			P.Shape shape60 = new P.Shape();

			P.NonVisualShapeProperties nonVisualShapeProperties60 = new P.NonVisualShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties63 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)589881U, Name = "Rectangle 57" };

			P.NonVisualShapeDrawingProperties nonVisualShapeDrawingProperties60 = new P.NonVisualShapeDrawingProperties();
			A.ShapeLocks shapeLocks60 = new A.ShapeLocks() { NoGrouping = true, NoChangeArrowheads = true };

			nonVisualShapeDrawingProperties60.Append(shapeLocks60);

			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties63 = new ApplicationNonVisualDrawingProperties();
			PlaceholderShape placeholderShape8 = new PlaceholderShape() { Type = PlaceholderValues.Body, Index = (UInt32Value)1U };

			applicationNonVisualDrawingProperties63.Append(placeholderShape8);

			nonVisualShapeProperties60.Append(nonVisualDrawingProperties63);
			nonVisualShapeProperties60.Append(nonVisualShapeDrawingProperties60);
			nonVisualShapeProperties60.Append(applicationNonVisualDrawingProperties63);

			P.ShapeProperties shapeProperties60 = new P.ShapeProperties() { BlackWhiteMode = A.BlackWhiteModeValues.Auto };

			A.Transform2D transform2D60 = new A.Transform2D();
			A.Offset offset63 = new A.Offset() { X = 457200L, Y = 1207008L };
			A.Extents extents63 = new A.Extents() { Cx = 8229600L, Cy = 4525963L };

			transform2D60.Append(offset63);
			transform2D60.Append(extents63);

			A.PresetGeometry presetGeometry13 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
			A.AdjustValueList adjustValueList61 = new A.AdjustValueList();

			presetGeometry13.Append(adjustValueList61);
			A.NoFill noFill58 = new A.NoFill();

			A.Outline outline66 = new A.Outline();
			A.NoFill noFill59 = new A.NoFill();

			outline66.Append(noFill59);
			A.EffectList effectList9 = new A.EffectList();

			A.ShapePropertiesExtensionList shapePropertiesExtensionList54 = new A.ShapePropertiesExtensionList();

			A.ShapePropertiesExtension shapePropertiesExtension58 = new A.ShapePropertiesExtension() { Uri = "{909E8E84-426E-40DD-AFC4-6F175D3DCCD1}" };

			A14.HiddenFillProperties hiddenFillProperties4 = new A14.HiddenFillProperties();
			hiddenFillProperties4.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.SolidFill solidFill230 = new A.SolidFill();
			A.SchemeColor schemeColor86 = new A.SchemeColor() { Val = A.SchemeColorValues.Accent1 };

			solidFill230.Append(schemeColor86);

			hiddenFillProperties4.Append(solidFill230);

			shapePropertiesExtension58.Append(hiddenFillProperties4);

			A.ShapePropertiesExtension shapePropertiesExtension59 = new A.ShapePropertiesExtension() { Uri = "{91240B29-F687-4F45-9708-019B960494DF}" };

			A14.HiddenLineProperties hiddenLineProperties53 = new A14.HiddenLineProperties() { Width = 9525 };
			hiddenLineProperties53.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.SolidFill solidFill231 = new A.SolidFill();
			A.SchemeColor schemeColor87 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill231.Append(schemeColor87);
			A.Miter miter3 = new A.Miter() { Limit = 800000 };
			A.HeadEnd headEnd54 = new A.HeadEnd();
			A.TailEnd tailEnd54 = new A.TailEnd();

			hiddenLineProperties53.Append(solidFill231);
			hiddenLineProperties53.Append(miter3);
			hiddenLineProperties53.Append(headEnd54);
			hiddenLineProperties53.Append(tailEnd54);

			shapePropertiesExtension59.Append(hiddenLineProperties53);

			A.ShapePropertiesExtension shapePropertiesExtension60 = new A.ShapePropertiesExtension() { Uri = "{AF507438-7753-43E0-B8FC-AC1667EBCBE1}" };

			A14.HiddenEffectsProperties hiddenEffectsProperties3 = new A14.HiddenEffectsProperties();
			hiddenEffectsProperties3.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			A.EffectList effectList10 = new A.EffectList();

			A.OuterShadow outerShadow6 = new A.OuterShadow() { Distance = 35921L, Direction = 2700000, Alignment = A.RectangleAlignmentValues.Center, RotateWithShape = false };
			A.SchemeColor schemeColor88 = new A.SchemeColor() { Val = A.SchemeColorValues.Background2 };

			outerShadow6.Append(schemeColor88);

			effectList10.Append(outerShadow6);

			hiddenEffectsProperties3.Append(effectList10);

			shapePropertiesExtension60.Append(hiddenEffectsProperties3);

			shapePropertiesExtensionList54.Append(shapePropertiesExtension58);
			shapePropertiesExtensionList54.Append(shapePropertiesExtension59);
			shapePropertiesExtensionList54.Append(shapePropertiesExtension60);

			shapeProperties60.Append(transform2D60);
			shapeProperties60.Append(presetGeometry13);
			shapeProperties60.Append(noFill58);
			shapeProperties60.Append(outline66);
			shapeProperties60.Append(effectList9);
			shapeProperties60.Append(shapePropertiesExtensionList54);

			P.TextBody textBody60 = new P.TextBody();

			A.BodyProperties bodyProperties60 = new A.BodyProperties() { Vertical = A.TextVerticalValues.Horizontal, Wrap = A.TextWrappingValues.Square, LeftInset = 91440, TopInset = 45720, RightInset = 91440, BottomInset = 45720, ColumnCount = 1, Anchor = A.TextAnchoringTypeValues.Top, AnchorCenter = false, CompatibleLineSpacing = true };

			A.PresetTextWrap presetTextWrap2 = new A.PresetTextWrap() { Preset = A.TextShapeValues.TextNoShape };
			A.AdjustValueList adjustValueList62 = new A.AdjustValueList();

			presetTextWrap2.Append(adjustValueList62);

			bodyProperties60.Append(presetTextWrap2);
			A.ListStyle listStyle60 = new A.ListStyle();

			A.Paragraph paragraph66 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties61 = new A.ParagraphProperties() { Level = 0 };

			A.Run run12 = new A.Run();
			A.RunProperties runProperties14 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			A.Text text14 = new A.Text();
			text14.Text = "Click to edit Master text styles";

			run12.Append(runProperties14);
			run12.Append(text14);

			paragraph66.Append(paragraphProperties61);
			paragraph66.Append(run12);

			A.Paragraph paragraph67 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties62 = new A.ParagraphProperties() { Level = 1 };

			A.Run run13 = new A.Run();
			A.RunProperties runProperties15 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			A.Text text15 = new A.Text();
			text15.Text = "Second level";

			run13.Append(runProperties15);
			run13.Append(text15);

			paragraph67.Append(paragraphProperties62);
			paragraph67.Append(run13);

			A.Paragraph paragraph68 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties63 = new A.ParagraphProperties() { Level = 2 };

			A.Run run14 = new A.Run();
			A.RunProperties runProperties16 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			A.Text text16 = new A.Text();
			text16.Text = "Third level";

			run14.Append(runProperties16);
			run14.Append(text16);

			paragraph68.Append(paragraphProperties63);
			paragraph68.Append(run14);

			A.Paragraph paragraph69 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties64 = new A.ParagraphProperties() { Level = 3 };

			A.Run run15 = new A.Run();
			A.RunProperties runProperties17 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			A.Text text17 = new A.Text();
			text17.Text = "Fourth level";

			run15.Append(runProperties17);
			run15.Append(text17);

			paragraph69.Append(paragraphProperties64);
			paragraph69.Append(run15);

			A.Paragraph paragraph70 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties65 = new A.ParagraphProperties() { Level = 4 };

			A.Run run16 = new A.Run();
			A.RunProperties runProperties18 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };
			A.Text text18 = new A.Text();
			text18.Text = "Fifth level";

			run16.Append(runProperties18);
			run16.Append(text18);

			paragraph70.Append(paragraphProperties65);
			paragraph70.Append(run16);

			textBody60.Append(bodyProperties60);
			textBody60.Append(listStyle60);
			textBody60.Append(paragraph66);
			textBody60.Append(paragraph67);
			textBody60.Append(paragraph68);
			textBody60.Append(paragraph69);
			textBody60.Append(paragraph70);

			shape60.Append(nonVisualShapeProperties60);
			shape60.Append(shapeProperties60);
			shape60.Append(textBody60);

			P.Shape shape61 = new P.Shape();

			P.NonVisualShapeProperties nonVisualShapeProperties61 = new P.NonVisualShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties64 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)58U, Name = "Rectangle 8" };

			P.NonVisualShapeDrawingProperties nonVisualShapeDrawingProperties61 = new P.NonVisualShapeDrawingProperties() { TextBox = true };
			A.ShapeLocks shapeLocks61 = new A.ShapeLocks() { NoChangeArrowheads = true };

			nonVisualShapeDrawingProperties61.Append(shapeLocks61);
			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties64 = new ApplicationNonVisualDrawingProperties();

			nonVisualShapeProperties61.Append(nonVisualDrawingProperties64);
			nonVisualShapeProperties61.Append(nonVisualShapeDrawingProperties61);
			nonVisualShapeProperties61.Append(applicationNonVisualDrawingProperties64);

			P.ShapeProperties shapeProperties61 = new P.ShapeProperties() { BlackWhiteMode = A.BlackWhiteModeValues.Auto };

			A.Transform2D transform2D61 = new A.Transform2D();
			A.Offset offset64 = new A.Offset() { X = 5191125L, Y = 6388099L };
			A.Extents extents64 = new A.Extents() { Cx = 2133600L, Cy = 355600L };

			transform2D61.Append(offset64);
			transform2D61.Append(extents64);

			A.PresetGeometry presetGeometry14 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
			A.AdjustValueList adjustValueList63 = new A.AdjustValueList();

			presetGeometry14.Append(adjustValueList63);
			A.NoFill noFill60 = new A.NoFill();

			A.Outline outline67 = new A.Outline() { Width = 9525 };
			A.NoFill noFill61 = new A.NoFill();
			A.Miter miter4 = new A.Miter() { Limit = 800000 };
			A.HeadEnd headEnd55 = new A.HeadEnd();
			A.TailEnd tailEnd55 = new A.TailEnd();

			outline67.Append(noFill61);
			outline67.Append(miter4);
			outline67.Append(headEnd55);
			outline67.Append(tailEnd55);
			A.EffectList effectList11 = new A.EffectList();

			shapeProperties61.Append(transform2D61);
			shapeProperties61.Append(presetGeometry14);
			shapeProperties61.Append(noFill60);
			shapeProperties61.Append(outline67);
			shapeProperties61.Append(effectList11);

			P.TextBody textBody61 = new P.TextBody();

			A.BodyProperties bodyProperties61 = new A.BodyProperties() { Vertical = A.TextVerticalValues.Horizontal, Wrap = A.TextWrappingValues.Square, LeftInset = 91440, TopInset = 45720, RightInset = 91440, BottomInset = 45720, ColumnCount = 1, Anchor = A.TextAnchoringTypeValues.Top, AnchorCenter = false, CompatibleLineSpacing = true };

			A.PresetTextWrap presetTextWrap3 = new A.PresetTextWrap() { Preset = A.TextShapeValues.TextNoShape };
			A.AdjustValueList adjustValueList64 = new A.AdjustValueList();

			presetTextWrap3.Append(adjustValueList64);

			bodyProperties61.Append(presetTextWrap3);

			A.ListStyle listStyle61 = new A.ListStyle();

			A.DefaultParagraphProperties defaultParagraphProperties2 = new A.DefaultParagraphProperties();
			A.DefaultRunProperties defaultRunProperties51 = new A.DefaultRunProperties() { Language = "en-US" };

			defaultParagraphProperties2.Append(defaultRunProperties51);

			A.Level1ParagraphProperties level1ParagraphProperties10 = new A.Level1ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Right, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore82 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent148 = new A.SpacingPercent() { Val = 0 };

			spaceBefore82.Append(spacingPercent148);

			A.SpaceAfter spaceAfter67 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent149 = new A.SpacingPercent() { Val = 0 };

			spaceAfter67.Append(spacingPercent149);

			A.DefaultRunProperties defaultRunProperties52 = new A.DefaultRunProperties() { FontSize = 1000, Kerning = 1200 };

			A.SolidFill solidFill232 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex203 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill232.Append(rgbColorModelHex203);
			A.LatinFont latinFont51 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont21 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont21 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties52.Append(solidFill232);
			defaultRunProperties52.Append(latinFont51);
			defaultRunProperties52.Append(eastAsianFont21);
			defaultRunProperties52.Append(complexScriptFont21);

			level1ParagraphProperties10.Append(spaceBefore82);
			level1ParagraphProperties10.Append(spaceAfter67);
			level1ParagraphProperties10.Append(defaultRunProperties52);

			A.Level2ParagraphProperties level2ParagraphProperties6 = new A.Level2ParagraphProperties() { LeftMargin = 457200, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore83 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent150 = new A.SpacingPercent() { Val = 0 };

			spaceBefore83.Append(spacingPercent150);

			A.SpaceAfter spaceAfter68 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent151 = new A.SpacingPercent() { Val = 0 };

			spaceAfter68.Append(spacingPercent151);

			A.DefaultRunProperties defaultRunProperties53 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill233 = new A.SolidFill();
			A.SchemeColor schemeColor89 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill233.Append(schemeColor89);
			A.LatinFont latinFont52 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont22 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont22 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties53.Append(solidFill233);
			defaultRunProperties53.Append(latinFont52);
			defaultRunProperties53.Append(eastAsianFont22);
			defaultRunProperties53.Append(complexScriptFont22);

			level2ParagraphProperties6.Append(spaceBefore83);
			level2ParagraphProperties6.Append(spaceAfter68);
			level2ParagraphProperties6.Append(defaultRunProperties53);

			A.Level3ParagraphProperties level3ParagraphProperties6 = new A.Level3ParagraphProperties() { LeftMargin = 914400, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore84 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent152 = new A.SpacingPercent() { Val = 0 };

			spaceBefore84.Append(spacingPercent152);

			A.SpaceAfter spaceAfter69 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent153 = new A.SpacingPercent() { Val = 0 };

			spaceAfter69.Append(spacingPercent153);

			A.DefaultRunProperties defaultRunProperties54 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill234 = new A.SolidFill();
			A.SchemeColor schemeColor90 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill234.Append(schemeColor90);
			A.LatinFont latinFont53 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont23 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont23 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties54.Append(solidFill234);
			defaultRunProperties54.Append(latinFont53);
			defaultRunProperties54.Append(eastAsianFont23);
			defaultRunProperties54.Append(complexScriptFont23);

			level3ParagraphProperties6.Append(spaceBefore84);
			level3ParagraphProperties6.Append(spaceAfter69);
			level3ParagraphProperties6.Append(defaultRunProperties54);

			A.Level4ParagraphProperties level4ParagraphProperties6 = new A.Level4ParagraphProperties() { LeftMargin = 1371600, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore85 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent154 = new A.SpacingPercent() { Val = 0 };

			spaceBefore85.Append(spacingPercent154);

			A.SpaceAfter spaceAfter70 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent155 = new A.SpacingPercent() { Val = 0 };

			spaceAfter70.Append(spacingPercent155);

			A.DefaultRunProperties defaultRunProperties55 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill235 = new A.SolidFill();
			A.SchemeColor schemeColor91 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill235.Append(schemeColor91);
			A.LatinFont latinFont54 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont24 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont24 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties55.Append(solidFill235);
			defaultRunProperties55.Append(latinFont54);
			defaultRunProperties55.Append(eastAsianFont24);
			defaultRunProperties55.Append(complexScriptFont24);

			level4ParagraphProperties6.Append(spaceBefore85);
			level4ParagraphProperties6.Append(spaceAfter70);
			level4ParagraphProperties6.Append(defaultRunProperties55);

			A.Level5ParagraphProperties level5ParagraphProperties6 = new A.Level5ParagraphProperties() { LeftMargin = 1828800, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore86 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent156 = new A.SpacingPercent() { Val = 0 };

			spaceBefore86.Append(spacingPercent156);

			A.SpaceAfter spaceAfter71 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent157 = new A.SpacingPercent() { Val = 0 };

			spaceAfter71.Append(spacingPercent157);

			A.DefaultRunProperties defaultRunProperties56 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill236 = new A.SolidFill();
			A.SchemeColor schemeColor92 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill236.Append(schemeColor92);
			A.LatinFont latinFont55 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont25 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont25 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties56.Append(solidFill236);
			defaultRunProperties56.Append(latinFont55);
			defaultRunProperties56.Append(eastAsianFont25);
			defaultRunProperties56.Append(complexScriptFont25);

			level5ParagraphProperties6.Append(spaceBefore86);
			level5ParagraphProperties6.Append(spaceAfter71);
			level5ParagraphProperties6.Append(defaultRunProperties56);

			A.Level6ParagraphProperties level6ParagraphProperties6 = new A.Level6ParagraphProperties() { LeftMargin = 2286000, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties57 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill237 = new A.SolidFill();
			A.SchemeColor schemeColor93 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill237.Append(schemeColor93);
			A.LatinFont latinFont56 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont26 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont26 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties57.Append(solidFill237);
			defaultRunProperties57.Append(latinFont56);
			defaultRunProperties57.Append(eastAsianFont26);
			defaultRunProperties57.Append(complexScriptFont26);

			level6ParagraphProperties6.Append(defaultRunProperties57);

			A.Level7ParagraphProperties level7ParagraphProperties6 = new A.Level7ParagraphProperties() { LeftMargin = 2743200, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties58 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill238 = new A.SolidFill();
			A.SchemeColor schemeColor94 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill238.Append(schemeColor94);
			A.LatinFont latinFont57 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont27 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont27 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties58.Append(solidFill238);
			defaultRunProperties58.Append(latinFont57);
			defaultRunProperties58.Append(eastAsianFont27);
			defaultRunProperties58.Append(complexScriptFont27);

			level7ParagraphProperties6.Append(defaultRunProperties58);

			A.Level8ParagraphProperties level8ParagraphProperties6 = new A.Level8ParagraphProperties() { LeftMargin = 3200400, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties59 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill239 = new A.SolidFill();
			A.SchemeColor schemeColor95 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill239.Append(schemeColor95);
			A.LatinFont latinFont58 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont28 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont28 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties59.Append(solidFill239);
			defaultRunProperties59.Append(latinFont58);
			defaultRunProperties59.Append(eastAsianFont28);
			defaultRunProperties59.Append(complexScriptFont28);

			level8ParagraphProperties6.Append(defaultRunProperties59);

			A.Level9ParagraphProperties level9ParagraphProperties6 = new A.Level9ParagraphProperties() { LeftMargin = 3657600, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties60 = new A.DefaultRunProperties() { FontSize = 1400, Kerning = 1200 };

			A.SolidFill solidFill240 = new A.SolidFill();
			A.SchemeColor schemeColor96 = new A.SchemeColor() { Val = A.SchemeColorValues.Background1 };

			solidFill240.Append(schemeColor96);
			A.LatinFont latinFont59 = new A.LatinFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.EastAsianFont eastAsianFont29 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont29 = new A.ComplexScriptFont() { Typeface = "Arial", PitchFamily = 34, CharacterSet = 0 };

			defaultRunProperties60.Append(solidFill240);
			defaultRunProperties60.Append(latinFont59);
			defaultRunProperties60.Append(eastAsianFont29);
			defaultRunProperties60.Append(complexScriptFont29);

			level9ParagraphProperties6.Append(defaultRunProperties60);

			listStyle61.Append(defaultParagraphProperties2);
			listStyle61.Append(level1ParagraphProperties10);
			listStyle61.Append(level2ParagraphProperties6);
			listStyle61.Append(level3ParagraphProperties6);
			listStyle61.Append(level4ParagraphProperties6);
			listStyle61.Append(level5ParagraphProperties6);
			listStyle61.Append(level6ParagraphProperties6);
			listStyle61.Append(level7ParagraphProperties6);
			listStyle61.Append(level8ParagraphProperties6);
			listStyle61.Append(level9ParagraphProperties6);

			A.Paragraph paragraph71 = new A.Paragraph();

			A.Field field3 = new A.Field() { Id = "{E5DCE6BD-165F-43E3-9FA5-67E3A2A56821}", Type = "slidenum" };
			A.RunProperties runProperties19 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", SmartTagClean = false };
			A.ParagraphProperties paragraphProperties66 = new A.ParagraphProperties();
			A.Text text19 = new A.Text();
			text19.Text = "‹#›";

			field3.Append(runProperties19);
			field3.Append(paragraphProperties66);
			field3.Append(text19);
			A.EndParagraphRunProperties endParagraphRunProperties59 = new A.EndParagraphRunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false };

			paragraph71.Append(field3);
			paragraph71.Append(endParagraphRunProperties59);

			textBody61.Append(bodyProperties61);
			textBody61.Append(listStyle61);
			textBody61.Append(paragraph71);

			shape61.Append(nonVisualShapeProperties61);
			shape61.Append(shapeProperties61);
			shape61.Append(textBody61);

			shapeTree2.Append(nonVisualGroupShapeProperties2);
			shapeTree2.Append(groupShapeProperties2);
			shapeTree2.Append(shape7);
			shapeTree2.Append(shape8);
			////shapeTree2.Append(groupShape1);
			shapeTree2.Append(shape59);
			shapeTree2.Append(shape60);
			shapeTree2.Append(shape61);

			//CommonSlideDataExtensionList commonSlideDataExtensionList2 = new CommonSlideDataExtensionList();

			//CommonSlideDataExtension commonSlideDataExtension2 = new CommonSlideDataExtension() { Uri = "{BB962C8B-B14F-4D97-AF65-F5344CB8AC3E}" };

			//P14.CreationId creationId2 = new P14.CreationId() { Val = (UInt32Value)3524121875U };
			//creationId2.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");

			//commonSlideDataExtension2.Append(creationId2);

			//commonSlideDataExtensionList2.Append(commonSlideDataExtension2);

			//commonSlideData2.Append(background2);
			commonSlideData2.Append(shapeTree2);
			//commonSlideData2.Append(commonSlideDataExtensionList2);
			P.ColorMap colorMap2 = new P.ColorMap() { Background1 = A.ColorSchemeIndexValues.Light1, Text1 = A.ColorSchemeIndexValues.Dark1, Background2 = A.ColorSchemeIndexValues.Light2, Text2 = A.ColorSchemeIndexValues.Dark2, Accent1 = A.ColorSchemeIndexValues.Accent1, Accent2 = A.ColorSchemeIndexValues.Accent2, Accent3 = A.ColorSchemeIndexValues.Accent3, Accent4 = A.ColorSchemeIndexValues.Accent4, Accent5 = A.ColorSchemeIndexValues.Accent5, Accent6 = A.ColorSchemeIndexValues.Accent6, Hyperlink = A.ColorSchemeIndexValues.Hyperlink, FollowedHyperlink = A.ColorSchemeIndexValues.FollowedHyperlink };

			//Transition transition1 = new Transition();
			//FadeTransition fadeTransition1 = new FadeTransition();

			//transition1.Append(fadeTransition1);

			//Timing timing1 = new Timing();

			//TimeNodeList timeNodeList1 = new TimeNodeList();

			//ParallelTimeNode parallelTimeNode1 = new ParallelTimeNode();
			//CommonTimeNode commonTimeNode1 = new CommonTimeNode() { Id = (UInt32Value)1U, Duration = "indefinite", Restart = TimeNodeRestartValues.Never, NodeType = TimeNodeValues.TmingRoot };

			//parallelTimeNode1.Append(commonTimeNode1);

			//timeNodeList1.Append(parallelTimeNode1);

			//timing1.Append(timeNodeList1);
			//HeaderFooter headerFooter1 = new HeaderFooter() { Header = false, Footer = false, DateTime = false };

			TextStyles textStyles1 = new TextStyles();

			//TitleStyle titleStyle1 = new TitleStyle();

			TitleStyle titleStyle1 = CreateTitleStyle();




			BodyStyle bodyStyle1 = new BodyStyle();

			A.Level1ParagraphProperties level1ParagraphProperties12 = new A.Level1ParagraphProperties() { LeftMargin = 290513, Indent = -290513, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore96 = new A.SpaceBefore();
			A.SpacingPoints spacingPoints1 = new A.SpacingPoints() { Val = 1000 };

			spaceBefore96.Append(spacingPoints1);

			A.SpaceAfter spaceAfter81 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent176 = new A.SpacingPercent() { Val = 0 };

			spaceAfter81.Append(spacingPercent176);
			A.BulletSizePercentage bulletSizePercentage1 = new A.BulletSizePercentage() { Val = 90000 };
			A.BulletFont bulletFont1 = new A.BulletFont() { Typeface = "Tahoma", PitchFamily = 34, CharacterSet = 0 };
			A.CharacterBullet characterBullet28 = new A.CharacterBullet() { Char = "•" };

			A.DefaultRunProperties defaultRunProperties70 = new A.DefaultRunProperties() { FontSize = 2000 };

			A.SolidFill solidFill250 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex213 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill250.Append(rgbColorModelHex213);
			A.LatinFont latinFont69 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont31 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont39 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties70.Append(solidFill250);
			defaultRunProperties70.Append(latinFont69);
			defaultRunProperties70.Append(eastAsianFont31);
			defaultRunProperties70.Append(complexScriptFont39);

			level1ParagraphProperties12.Append(spaceBefore96);
			level1ParagraphProperties12.Append(spaceAfter81);
			level1ParagraphProperties12.Append(bulletSizePercentage1);
			level1ParagraphProperties12.Append(bulletFont1);
			level1ParagraphProperties12.Append(characterBullet28);
			level1ParagraphProperties12.Append(defaultRunProperties70);

			A.Level2ParagraphProperties level2ParagraphProperties8 = new A.Level2ParagraphProperties() { LeftMargin = 747713, Indent = -285750, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore97 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent177 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore97.Append(spacingPercent177);

			A.SpaceAfter spaceAfter82 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent178 = new A.SpacingPercent() { Val = 0 };

			spaceAfter82.Append(spacingPercent178);

			A.BulletColor bulletColor28 = new A.BulletColor();
			A.RgbColorModelHex rgbColorModelHex214 = new A.RgbColorModelHex() { Val = "7D0049" };

			bulletColor28.Append(rgbColorModelHex214);
			A.BulletFont bulletFont2 = new A.BulletFont() { Typeface = "Wingdings", PitchFamily = 2, CharacterSet = 2 };
			A.CharacterBullet characterBullet29 = new A.CharacterBullet() { Char = "§" };

			A.DefaultRunProperties defaultRunProperties71 = new A.DefaultRunProperties() { FontSize = 1800 };

			A.SolidFill solidFill251 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex215 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill251.Append(rgbColorModelHex215);
			A.LatinFont latinFont70 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont40 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties71.Append(solidFill251);
			defaultRunProperties71.Append(latinFont70);
			defaultRunProperties71.Append(complexScriptFont40);

			level2ParagraphProperties8.Append(spaceBefore97);
			level2ParagraphProperties8.Append(spaceAfter82);
			level2ParagraphProperties8.Append(bulletColor28);
			level2ParagraphProperties8.Append(bulletFont2);
			level2ParagraphProperties8.Append(characterBullet29);
			level2ParagraphProperties8.Append(defaultRunProperties71);

			A.Level3ParagraphProperties level3ParagraphProperties8 = new A.Level3ParagraphProperties() { LeftMargin = 1139825, Indent = -225425, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore98 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent179 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore98.Append(spacingPercent179);

			A.SpaceAfter spaceAfter83 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent180 = new A.SpacingPercent() { Val = 0 };

			spaceAfter83.Append(spacingPercent180);
			A.CharacterBullet characterBullet30 = new A.CharacterBullet() { Char = "•" };

			A.DefaultRunProperties defaultRunProperties72 = new A.DefaultRunProperties() { FontSize = 1600 };

			A.SolidFill solidFill252 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex216 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill252.Append(rgbColorModelHex216);
			A.LatinFont latinFont71 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont41 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties72.Append(solidFill252);
			defaultRunProperties72.Append(latinFont71);
			defaultRunProperties72.Append(complexScriptFont41);

			level3ParagraphProperties8.Append(spaceBefore98);
			level3ParagraphProperties8.Append(spaceAfter83);
			level3ParagraphProperties8.Append(characterBullet30);
			level3ParagraphProperties8.Append(defaultRunProperties72);

			A.Level4ParagraphProperties level4ParagraphProperties8 = new A.Level4ParagraphProperties() { LeftMargin = 1603375, Indent = -233363, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore99 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent181 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore99.Append(spacingPercent181);

			A.SpaceAfter spaceAfter84 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent182 = new A.SpacingPercent() { Val = 0 };

			spaceAfter84.Append(spacingPercent182);
			A.CharacterBullet characterBullet31 = new A.CharacterBullet() { Char = "–" };

			A.DefaultRunProperties defaultRunProperties73 = new A.DefaultRunProperties() { FontSize = 1600 };

			A.SolidFill solidFill253 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex217 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill253.Append(rgbColorModelHex217);
			A.LatinFont latinFont72 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont42 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties73.Append(solidFill253);
			defaultRunProperties73.Append(latinFont72);
			defaultRunProperties73.Append(complexScriptFont42);

			level4ParagraphProperties8.Append(spaceBefore99);
			level4ParagraphProperties8.Append(spaceAfter84);
			level4ParagraphProperties8.Append(characterBullet31);
			level4ParagraphProperties8.Append(defaultRunProperties73);

			A.Level5ParagraphProperties level5ParagraphProperties8 = new A.Level5ParagraphProperties() { LeftMargin = 2054225, Indent = -225425, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore100 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent183 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore100.Append(spacingPercent183);

			A.SpaceAfter spaceAfter85 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent184 = new A.SpacingPercent() { Val = 0 };

			spaceAfter85.Append(spacingPercent184);
			A.CharacterBullet characterBullet32 = new A.CharacterBullet() { Char = "»" };

			A.DefaultRunProperties defaultRunProperties74 = new A.DefaultRunProperties() { FontSize = 1400 };

			A.SolidFill solidFill254 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex218 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill254.Append(rgbColorModelHex218);
			A.LatinFont latinFont73 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont43 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties74.Append(solidFill254);
			defaultRunProperties74.Append(latinFont73);
			defaultRunProperties74.Append(complexScriptFont43);

			level5ParagraphProperties8.Append(spaceBefore100);
			level5ParagraphProperties8.Append(spaceAfter85);
			level5ParagraphProperties8.Append(characterBullet32);
			level5ParagraphProperties8.Append(defaultRunProperties74);

			A.Level6ParagraphProperties level6ParagraphProperties8 = new A.Level6ParagraphProperties() { LeftMargin = 2511425, Indent = -225425, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore101 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent185 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore101.Append(spacingPercent185);

			A.SpaceAfter spaceAfter86 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent186 = new A.SpacingPercent() { Val = 0 };

			spaceAfter86.Append(spacingPercent186);
			A.CharacterBullet characterBullet33 = new A.CharacterBullet() { Char = "»" };

			A.DefaultRunProperties defaultRunProperties75 = new A.DefaultRunProperties() { FontSize = 1600 };

			A.SolidFill solidFill255 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex219 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill255.Append(rgbColorModelHex219);
			A.LatinFont latinFont74 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont44 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties75.Append(solidFill255);
			defaultRunProperties75.Append(latinFont74);
			defaultRunProperties75.Append(complexScriptFont44);

			level6ParagraphProperties8.Append(spaceBefore101);
			level6ParagraphProperties8.Append(spaceAfter86);
			level6ParagraphProperties8.Append(characterBullet33);
			level6ParagraphProperties8.Append(defaultRunProperties75);

			A.Level7ParagraphProperties level7ParagraphProperties8 = new A.Level7ParagraphProperties() { LeftMargin = 2968625, Indent = -225425, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore102 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent187 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore102.Append(spacingPercent187);

			A.SpaceAfter spaceAfter87 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent188 = new A.SpacingPercent() { Val = 0 };

			spaceAfter87.Append(spacingPercent188);
			A.CharacterBullet characterBullet34 = new A.CharacterBullet() { Char = "»" };

			A.DefaultRunProperties defaultRunProperties76 = new A.DefaultRunProperties() { FontSize = 1600 };

			A.SolidFill solidFill256 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex220 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill256.Append(rgbColorModelHex220);
			A.LatinFont latinFont75 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont45 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties76.Append(solidFill256);
			defaultRunProperties76.Append(latinFont75);
			defaultRunProperties76.Append(complexScriptFont45);

			level7ParagraphProperties8.Append(spaceBefore102);
			level7ParagraphProperties8.Append(spaceAfter87);
			level7ParagraphProperties8.Append(characterBullet34);
			level7ParagraphProperties8.Append(defaultRunProperties76);

			A.Level8ParagraphProperties level8ParagraphProperties8 = new A.Level8ParagraphProperties() { LeftMargin = 3425825, Indent = -225425, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore103 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent189 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore103.Append(spacingPercent189);

			A.SpaceAfter spaceAfter88 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent190 = new A.SpacingPercent() { Val = 0 };

			spaceAfter88.Append(spacingPercent190);
			A.CharacterBullet characterBullet35 = new A.CharacterBullet() { Char = "»" };

			A.DefaultRunProperties defaultRunProperties77 = new A.DefaultRunProperties() { FontSize = 1600 };

			A.SolidFill solidFill257 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex221 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill257.Append(rgbColorModelHex221);
			A.LatinFont latinFont76 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont46 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties77.Append(solidFill257);
			defaultRunProperties77.Append(latinFont76);
			defaultRunProperties77.Append(complexScriptFont46);

			level8ParagraphProperties8.Append(spaceBefore103);
			level8ParagraphProperties8.Append(spaceAfter88);
			level8ParagraphProperties8.Append(characterBullet35);
			level8ParagraphProperties8.Append(defaultRunProperties77);

			A.Level9ParagraphProperties level9ParagraphProperties8 = new A.Level9ParagraphProperties() { LeftMargin = 3883025, Indent = -225425, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };

			A.SpaceBefore spaceBefore104 = new A.SpaceBefore();
			A.SpacingPercent spacingPercent191 = new A.SpacingPercent() { Val = 20000 };

			spaceBefore104.Append(spacingPercent191);

			A.SpaceAfter spaceAfter89 = new A.SpaceAfter();
			A.SpacingPercent spacingPercent192 = new A.SpacingPercent() { Val = 0 };

			spaceAfter89.Append(spacingPercent192);
			A.CharacterBullet characterBullet36 = new A.CharacterBullet() { Char = "»" };

			A.DefaultRunProperties defaultRunProperties78 = new A.DefaultRunProperties() { FontSize = 1600 };

			A.SolidFill solidFill258 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex222 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill258.Append(rgbColorModelHex222);
			A.LatinFont latinFont77 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.ComplexScriptFont complexScriptFont47 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties78.Append(solidFill258);
			defaultRunProperties78.Append(latinFont77);
			defaultRunProperties78.Append(complexScriptFont47);

			level9ParagraphProperties8.Append(spaceBefore104);
			level9ParagraphProperties8.Append(spaceAfter89);
			level9ParagraphProperties8.Append(characterBullet36);
			level9ParagraphProperties8.Append(defaultRunProperties78);

			bodyStyle1.Append(level1ParagraphProperties12);
			bodyStyle1.Append(level2ParagraphProperties8);
			bodyStyle1.Append(level3ParagraphProperties8);
			bodyStyle1.Append(level4ParagraphProperties8);
			bodyStyle1.Append(level5ParagraphProperties8);
			bodyStyle1.Append(level6ParagraphProperties8);
			bodyStyle1.Append(level7ParagraphProperties8);
			bodyStyle1.Append(level8ParagraphProperties8);
			bodyStyle1.Append(level9ParagraphProperties8);

			OtherStyle otherStyle1 = new OtherStyle();

			A.DefaultParagraphProperties defaultParagraphProperties3 = new A.DefaultParagraphProperties();
			A.DefaultRunProperties defaultRunProperties79 = new A.DefaultRunProperties() { Language = "en-US" };

			defaultParagraphProperties3.Append(defaultRunProperties79);

			A.Level1ParagraphProperties level1ParagraphProperties13 = new A.Level1ParagraphProperties() { LeftMargin = 0, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties80 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill259 = new A.SolidFill();
			A.SchemeColor schemeColor97 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill259.Append(schemeColor97);
			A.LatinFont latinFont78 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont32 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont48 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties80.Append(solidFill259);
			defaultRunProperties80.Append(latinFont78);
			defaultRunProperties80.Append(eastAsianFont32);
			defaultRunProperties80.Append(complexScriptFont48);

			level1ParagraphProperties13.Append(defaultRunProperties80);

			A.Level2ParagraphProperties level2ParagraphProperties9 = new A.Level2ParagraphProperties() { LeftMargin = 457200, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties81 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill260 = new A.SolidFill();
			A.SchemeColor schemeColor98 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill260.Append(schemeColor98);
			A.LatinFont latinFont79 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont33 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont49 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties81.Append(solidFill260);
			defaultRunProperties81.Append(latinFont79);
			defaultRunProperties81.Append(eastAsianFont33);
			defaultRunProperties81.Append(complexScriptFont49);

			level2ParagraphProperties9.Append(defaultRunProperties81);

			A.Level3ParagraphProperties level3ParagraphProperties9 = new A.Level3ParagraphProperties() { LeftMargin = 914400, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties82 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill261 = new A.SolidFill();
			A.SchemeColor schemeColor99 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill261.Append(schemeColor99);
			A.LatinFont latinFont80 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont34 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont50 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties82.Append(solidFill261);
			defaultRunProperties82.Append(latinFont80);
			defaultRunProperties82.Append(eastAsianFont34);
			defaultRunProperties82.Append(complexScriptFont50);

			level3ParagraphProperties9.Append(defaultRunProperties82);

			A.Level4ParagraphProperties level4ParagraphProperties9 = new A.Level4ParagraphProperties() { LeftMargin = 1371600, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties83 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill262 = new A.SolidFill();
			A.SchemeColor schemeColor100 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill262.Append(schemeColor100);
			A.LatinFont latinFont81 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont35 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont51 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties83.Append(solidFill262);
			defaultRunProperties83.Append(latinFont81);
			defaultRunProperties83.Append(eastAsianFont35);
			defaultRunProperties83.Append(complexScriptFont51);

			level4ParagraphProperties9.Append(defaultRunProperties83);

			A.Level5ParagraphProperties level5ParagraphProperties9 = new A.Level5ParagraphProperties() { LeftMargin = 1828800, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties84 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill263 = new A.SolidFill();
			A.SchemeColor schemeColor101 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill263.Append(schemeColor101);
			A.LatinFont latinFont82 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont36 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont52 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties84.Append(solidFill263);
			defaultRunProperties84.Append(latinFont82);
			defaultRunProperties84.Append(eastAsianFont36);
			defaultRunProperties84.Append(complexScriptFont52);

			level5ParagraphProperties9.Append(defaultRunProperties84);

			A.Level6ParagraphProperties level6ParagraphProperties9 = new A.Level6ParagraphProperties() { LeftMargin = 2286000, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties85 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill264 = new A.SolidFill();
			A.SchemeColor schemeColor102 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill264.Append(schemeColor102);
			A.LatinFont latinFont83 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont37 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont53 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties85.Append(solidFill264);
			defaultRunProperties85.Append(latinFont83);
			defaultRunProperties85.Append(eastAsianFont37);
			defaultRunProperties85.Append(complexScriptFont53);

			level6ParagraphProperties9.Append(defaultRunProperties85);

			A.Level7ParagraphProperties level7ParagraphProperties9 = new A.Level7ParagraphProperties() { LeftMargin = 2743200, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties86 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill265 = new A.SolidFill();
			A.SchemeColor schemeColor103 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill265.Append(schemeColor103);
			A.LatinFont latinFont84 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont38 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont54 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties86.Append(solidFill265);
			defaultRunProperties86.Append(latinFont84);
			defaultRunProperties86.Append(eastAsianFont38);
			defaultRunProperties86.Append(complexScriptFont54);

			level7ParagraphProperties9.Append(defaultRunProperties86);

			A.Level8ParagraphProperties level8ParagraphProperties9 = new A.Level8ParagraphProperties() { LeftMargin = 3200400, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties87 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill266 = new A.SolidFill();
			A.SchemeColor schemeColor104 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill266.Append(schemeColor104);
			A.LatinFont latinFont85 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont39 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont55 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties87.Append(solidFill266);
			defaultRunProperties87.Append(latinFont85);
			defaultRunProperties87.Append(eastAsianFont39);
			defaultRunProperties87.Append(complexScriptFont55);

			level8ParagraphProperties9.Append(defaultRunProperties87);

			A.Level9ParagraphProperties level9ParagraphProperties9 = new A.Level9ParagraphProperties() { LeftMargin = 3657600, Alignment = A.TextAlignmentTypeValues.Left, DefaultTabSize = 914400, RightToLeft = false, EastAsianLineBreak = true, LatinLineBreak = false, Height = true };

			A.DefaultRunProperties defaultRunProperties88 = new A.DefaultRunProperties() { FontSize = 1800, Kerning = 1200 };

			A.SolidFill solidFill267 = new A.SolidFill();
			A.SchemeColor schemeColor105 = new A.SchemeColor() { Val = A.SchemeColorValues.Text1 };

			solidFill267.Append(schemeColor105);
			A.LatinFont latinFont86 = new A.LatinFont() { Typeface = "+mn-lt" };
			A.EastAsianFont eastAsianFont40 = new A.EastAsianFont() { Typeface = "+mn-ea" };
			A.ComplexScriptFont complexScriptFont56 = new A.ComplexScriptFont() { Typeface = "+mn-cs" };

			defaultRunProperties88.Append(solidFill267);
			defaultRunProperties88.Append(latinFont86);
			defaultRunProperties88.Append(eastAsianFont40);
			defaultRunProperties88.Append(complexScriptFont56);

			level9ParagraphProperties9.Append(defaultRunProperties88);

			otherStyle1.Append(defaultParagraphProperties3);
			otherStyle1.Append(level1ParagraphProperties13);
			otherStyle1.Append(level2ParagraphProperties9);
			otherStyle1.Append(level3ParagraphProperties9);
			otherStyle1.Append(level4ParagraphProperties9);
			otherStyle1.Append(level5ParagraphProperties9);
			otherStyle1.Append(level6ParagraphProperties9);
			otherStyle1.Append(level7ParagraphProperties9);
			otherStyle1.Append(level8ParagraphProperties9);
			otherStyle1.Append(level9ParagraphProperties9);

			textStyles1.Append(titleStyle1);
			textStyles1.Append(bodyStyle1);
			textStyles1.Append(otherStyle1);

			SlideLayoutIdList slideLayoutIdList = new SlideLayoutIdList(new SlideLayoutId() { Id = (UInt32Value)2147483649U, RelationshipId = "rId1" });

			slideMaster1.Append(commonSlideData2);
			slideMaster1.Append(colorMap2);
			slideMaster1.Append(slideLayoutIdList);
			//slideMaster1.Append(transition1);
			//slideMaster1.Append(timing1);
			//slideMaster1.Append(headerFooter1);
			slideMaster1.Append(textStyles1);




			slideMasterPart.SlideMaster = slideMaster1;
			//slideMasterPart.SlideMaster = slideMaster;

			return slideMasterPart;

			}

		private static A.DefaultRunProperties GetDefaultRunProperties(Int32Value fontSize, Boolean bold, String color, String latinTypeface, String eastAsianTypeface, String complexTypeface)
			{
			A.DefaultRunProperties defaultRunProperties = new A.DefaultRunProperties();
			A.SolidFill solidFill = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex = new A.RgbColorModelHex() { Val = color };
			solidFill.Append(rgbColorModelHex);

			defaultRunProperties.FontSize = fontSize;
			defaultRunProperties.Bold = bold;
			defaultRunProperties.Append(solidFill);

			if (latinTypeface != "")
				{
				A.LatinFont latinFont = new A.LatinFont() { Typeface = latinTypeface };
				defaultRunProperties.Append(latinFont);
				}
			if (eastAsianTypeface != "")
				{
				A.EastAsianFont eastAsianFont = new A.EastAsianFont() { Typeface = eastAsianTypeface };
				defaultRunProperties.Append(eastAsianFont);
				}
			if (complexTypeface != "")
				{
				A.ComplexScriptFont complexScriptFont = new A.ComplexScriptFont() { Typeface = complexTypeface };
				defaultRunProperties.Append(complexScriptFont);
				}


			return defaultRunProperties;
			}

		private static TitleStyle CreateTitleStyle()
			{
			TitleStyle titleStyle = new TitleStyle();

			A.SpacingPercent spacingPercent = new A.SpacingPercent() { Val = 0 };

			A.SpaceBefore spaceBefore = new A.SpaceBefore();
			spaceBefore.Append(spacingPercent);

			A.SpaceAfter spaceAfter = new A.SpaceAfter();
			spaceAfter.Append(spacingPercent.CloneNode(true));


			A.Level1ParagraphProperties level1ParagraphProperties11 = new A.Level1ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level1ParagraphProperties11.Append(spaceBefore.CloneNode(true));
			level1ParagraphProperties11.Append(spaceAfter.CloneNode(true));
			level1ParagraphProperties11.Append(GetDefaultRunProperties(2800,true,"98002E","+mj-lt","+mj-ea","+mj-cs"));

			A.Level2ParagraphProperties level2ParagraphProperties7 = new A.Level2ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level2ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level2ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level2ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level3ParagraphProperties level3ParagraphProperties7 = new A.Level3ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level3ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level3ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level3ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level4ParagraphProperties level4ParagraphProperties7 = new A.Level4ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level4ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level4ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level4ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level5ParagraphProperties level5ParagraphProperties7 = new A.Level5ParagraphProperties() { Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level5ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level5ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level5ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level6ParagraphProperties level6ParagraphProperties7 = new A.Level6ParagraphProperties() { LeftMargin = 457200, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level6ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level6ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level6ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level7ParagraphProperties level7ParagraphProperties7 = new A.Level7ParagraphProperties() { LeftMargin = 914400, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level7ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level7ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level7ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level8ParagraphProperties level8ParagraphProperties7 = new A.Level8ParagraphProperties() { LeftMargin = 1371600, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level8ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level8ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level8ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			A.Level9ParagraphProperties level9ParagraphProperties7 = new A.Level9ParagraphProperties() { LeftMargin = 1828800, Alignment = A.TextAlignmentTypeValues.Left, RightToLeft = false, FontAlignment = A.TextFontAlignmentValues.Baseline };
			level9ParagraphProperties7.Append(spaceBefore.CloneNode(true));
			level9ParagraphProperties7.Append(spaceAfter.CloneNode(true));
			level9ParagraphProperties7.Append(GetDefaultRunProperties(2900, true, "98002E", "Tahoma", "", "Arial"));

			titleStyle.Append(level1ParagraphProperties11);
			titleStyle.Append(level2ParagraphProperties7);
			titleStyle.Append(level3ParagraphProperties7);
			titleStyle.Append(level4ParagraphProperties7);
			titleStyle.Append(level5ParagraphProperties7);
			titleStyle.Append(level6ParagraphProperties7);
			titleStyle.Append(level7ParagraphProperties7);
			titleStyle.Append(level8ParagraphProperties7);
			titleStyle.Append(level9ParagraphProperties7);

			return titleStyle;

			}

		private static ThemePart AddTheme(PresentationPart presentationPart)
			{
			// Create and add the Theme to the presentationPart
			ThemePart themePart = presentationPart.AddNewPart<ThemePart>("rId7");

			D.ThemeElements themeElements = new D.ThemeElements(
			new D.ColorScheme(
				new D.Dark1Color(new D.SystemColor() { Val = D.SystemColorValues.WindowText, LastColor = "000000" }),
				new D.Light1Color(new D.SystemColor() { Val = D.SystemColorValues.Window, LastColor = "FFFFFF" }),
				new D.Dark2Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Light2Color(new D.RgbColorModelHex() { Val = "FFFFFF" }),
				new D.Accent1Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Accent2Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Accent3Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Accent4Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Accent5Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Accent6Color(new D.RgbColorModelHex() { Val = "000000" }),
				new D.Hyperlink(new D.RgbColorModelHex() { Val = "000000" }),
				new D.FollowedHyperlinkColor(new D.RgbColorModelHex() { Val = "000000" })) { Name = "Blank" },

				new D.FontScheme(
				new D.MajorFont(new D.LatinFont() { Typeface = "" }, new D.EastAsianFont() { Typeface = "" }, new D.ComplexScriptFont() { Typeface = "" }),
				new D.MinorFont(new D.LatinFont() { Typeface = "" }, new D.EastAsianFont() { Typeface = "" }, new D.ComplexScriptFont() { Typeface = "" })) { Name = "Blank" },

				new D.FormatScheme(
				new D.FillStyleList(new D.NoFill(), new D.NoFill(), new D.NoFill(), new D.NoFill()),
				new D.LineStyleList(new D.Outline(new D.NoFill()), new D.Outline(new D.NoFill()), new D.Outline(new D.NoFill())),

				new D.EffectStyleList(new D.EffectStyle(new D.EffectList()), new D.EffectStyle(new D.EffectList()), new D.EffectStyle(new D.EffectList())),
				new D.BackgroundFillStyleList(new D.NoFill(), new D.NoFill(), new D.NoFill(), new D.NoFill())) { Name = "Blank" });

			D.Theme theme1 = new D.Theme(themeElements) { Name = "Office Theme" };
			themePart.Theme = theme1;

			return themePart;
			}

		private static void AddTitle(SlidePart slidePart, String slideTitle, String slideSubtitle)
			{

			P.Shape shape = new P.Shape();

			P.NonVisualShapeProperties nonVisualShapeProperties2 = new P.NonVisualShapeProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties3 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)12292U, Name = "Rectangle 25" };

			P.NonVisualShapeDrawingProperties nonVisualShapeDrawingProperties2 = new P.NonVisualShapeDrawingProperties();
			A.ShapeLocks shapeLocks2 = new A.ShapeLocks() { NoGrouping = true, NoChangeArrowheads = true };

			nonVisualShapeDrawingProperties2.Append(shapeLocks2);

			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties3 = new ApplicationNonVisualDrawingProperties();
			PlaceholderShape placeholderShape2 = new PlaceholderShape() { Type = PlaceholderValues.Title, Index = (UInt32Value)4294967295U };

			applicationNonVisualDrawingProperties3.Append(placeholderShape2);

			nonVisualShapeProperties2.Append(nonVisualDrawingProperties3);
			nonVisualShapeProperties2.Append(nonVisualShapeDrawingProperties2);
			nonVisualShapeProperties2.Append(applicationNonVisualDrawingProperties3);

			P.ShapeProperties shapeProperties2 = new P.ShapeProperties();

			A.Transform2D transform2D1 = new A.Transform2D();
			A.Offset offset2 = new A.Offset() { X = 457200L, Y = 274638L };
			A.Extents extents2 = new A.Extents() { Cx = 8343900L, Cy = 884237L };

			transform2D1.Append(offset2);
			transform2D1.Append(extents2);

			shapeProperties2.Append(transform2D1);

			P.TextBody textBody2 = new P.TextBody();
			A.BodyProperties bodyProperties2 = new A.BodyProperties();
			A.ListStyle listStyle2 = new A.ListStyle();

			A.Paragraph paragraph2 = new A.Paragraph();
			A.ParagraphProperties paragraphProperties3 = new A.ParagraphProperties() { EastAsianLineBreak = true, Height = true };

			A.Break break1 = new A.Break();
			A.RunProperties runProperties8 = new A.RunProperties() { Language = "en-US", AlternativeLanguage = "en-US", Dirty = false, SmartTagClean = false };

			break1.Append(runProperties8);

			paragraph2.Append(paragraphProperties3);
			paragraph2.Append(MakeRun(slideTitle));
			paragraph2.Append(break1);
			paragraph2.Append(MakeRun(slideSubtitle,2200,"4D4D4F"));

			textBody2.Append(bodyProperties2);
			textBody2.Append(listStyle2);
			textBody2.Append(paragraph2);

			shape.Append(nonVisualShapeProperties2);
			shape.Append(shapeProperties2);
			shape.Append(textBody2);

			slidePart.Slide.CommonSlideData.ShapeTree.Append(shape);
			}

		private static A.Run MakeRun(String runText, Int32 fontSize = 0, String fontColor = "")
			{
			//creating a text run, with optional font size and color (if not supplied the appropriate level paragraph properties are used)
			A.Run run = new A.Run();
			A.Text text = new A.Text();
			A.RunProperties runProperties = new A.RunProperties();

			if (fontSize != 0)
				{
				if (fontSize < 100) { fontSize *= 100; } //because the math in this is crazy
				runProperties = new A.RunProperties() { FontSize = fontSize };
				}

			if (fontColor != "")
				{
				A.SolidFill solidFill = new A.SolidFill();
				A.RgbColorModelHex rgbColorModelHex = new A.RgbColorModelHex() { Val = fontColor };
				solidFill.Append(rgbColorModelHex);
				runProperties.Append(solidFill);
				}

			text.Text = runText;
			run.Append(runProperties);
			run.Append(text);

			return run;

			}

		#endregion

		#region Charts

		private static C.PlotArea AddPlotArea(SlidePart slidePart)
			{

			P.GraphicFrame graphicFrame = slidePart.Slide.CommonSlideData.ShapeTree.Elements<P.GraphicFrame>().FirstOrDefault();
			//ChartPart chartPart;
			//C.ChartSpace chartSpace;
			//C.Chart chart;
			C.PlotArea plotArea;

			//chartPart = slidePart.AddNewPart<ChartPart>("rId17");

			P.GraphicFrame graphicFrame1 = new P.GraphicFrame();

			P.NonVisualGraphicFrameProperties nonVisualGraphicFrameProperties1 = new P.NonVisualGraphicFrameProperties();
			P.NonVisualDrawingProperties nonVisualDrawingProperties9 = new P.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "BarChart" };

			P.NonVisualGraphicFrameDrawingProperties nonVisualGraphicFrameDrawingProperties1 = new P.NonVisualGraphicFrameDrawingProperties();
			A.GraphicFrameLocks graphicFrameLocks1 = new A.GraphicFrameLocks() { NoChangeAspect = true };

			nonVisualGraphicFrameDrawingProperties1.Append(graphicFrameLocks1);

			ApplicationNonVisualDrawingProperties applicationNonVisualDrawingProperties9 = new ApplicationNonVisualDrawingProperties();

			ApplicationNonVisualDrawingPropertiesExtensionList applicationNonVisualDrawingPropertiesExtensionList1 = new ApplicationNonVisualDrawingPropertiesExtensionList();

			ApplicationNonVisualDrawingPropertiesExtension applicationNonVisualDrawingPropertiesExtension1 = new ApplicationNonVisualDrawingPropertiesExtension() { Uri = "{D42A27DB-BD31-4B8C-83A1-F6EECF244321}" };

			P14.ModificationId modificationId1 = new P14.ModificationId() { Val = (UInt32Value)375584539U };
			modificationId1.AddNamespaceDeclaration("p14", "http://schemas.microsoft.com/office/powerpoint/2010/main");

			applicationNonVisualDrawingPropertiesExtension1.Append(modificationId1);

			applicationNonVisualDrawingPropertiesExtensionList1.Append(applicationNonVisualDrawingPropertiesExtension1);

			applicationNonVisualDrawingProperties9.Append(applicationNonVisualDrawingPropertiesExtensionList1);

			nonVisualGraphicFrameProperties1.Append(nonVisualDrawingProperties9);
			nonVisualGraphicFrameProperties1.Append(nonVisualGraphicFrameDrawingProperties1);
			nonVisualGraphicFrameProperties1.Append(applicationNonVisualDrawingProperties9);

			Transform transform1 = new Transform();
			A.Offset offset11 = new A.Offset() { X = 341349L, Y = 1072147L };
			A.Extents extents11 = new A.Extents() { Cx = 8542338L, Cy = 4613275L };

			transform1.Append(offset11);
			transform1.Append(extents11);

			A.Graphic graphic1 = new A.Graphic();

			A.GraphicData graphicData1 = new A.GraphicData() { Uri = "http://schemas.openxmlformats.org/drawingml/2006/chart" };

			C.ChartReference chartReference1 = new C.ChartReference() { Id = "rId17" };
			chartReference1.AddNamespaceDeclaration("c", "http://schemas.openxmlformats.org/drawingml/2006/chart");
			chartReference1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");

			graphicData1.Append(chartReference1);

			graphic1.Append(graphicData1);

			graphicFrame1.Append(nonVisualGraphicFrameProperties1);
			graphicFrame1.Append(transform1);
			graphicFrame1.Append(graphic1);

			slidePart.Slide.CommonSlideData.ShapeTree.Append(graphicFrame1);

			//chartSpace = new C.ChartSpace();
			//chart = new C.Chart();
			plotArea = new C.PlotArea();

			return plotArea;

			}

		private static void ClosePlotArea(SlidePart slidePart, C.PlotArea plotArea, String xAxisTitle)
			{

			C.Chart chart = new C.Chart();
			C.ChartSpace chartSpace = new C.ChartSpace();
			ChartPart chartPart = slidePart.AddNewPart<ChartPart>("rId17");

			plotArea.Append(AddChartCategoryAxis());
			plotArea.Append(AddChartValueAxis(xAxisTitle));//xAxisLabel
			plotArea.Append(AddChartShapeProperties());


			//plotArea.Append(layout);
			chart.Append(plotArea);

			C.ExternalData externalData = new C.ExternalData() { Id = "rId1" };
			C.AutoUpdate autoUpdate = new C.AutoUpdate() { Val = false };

			externalData.Append(autoUpdate);

			chartSpace.Append(chart);
			chartSpace.Append(externalData);

			chartPart.ChartSpace = chartSpace;

			EmbeddedPackagePart embeddedPackagePart1 = chartPart.AddNewPart<EmbeddedPackagePart>("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "rId1");
			//GenerateEmbeddedPackagePart1Content(embeddedPackagePart1);
			//using (System.IO.Stream s = embeddedPackagePart1.GetStream())
			//	ms.WriteTo(s);
			//embeddedPackagePart1.FeedData(BinaryData());
			GetBinaryData(embeddedPackagePart1);

			}

		private static void AddDataToChart(C.PlotArea plotArea, DataTable dataTable, String dataType, DataTable colorTable)
			{
			if (dataType == "Bar")
				{
				plotArea.Append(AddBarChart(dataTable, colorTable));
				}
			if (dataType == "Line")
				{
				plotArea.Append(AddLineChart(dataTable, colorTable));
				}
			}

		private static C.BarChart AddBarChart(DataTable queryTable, DataTable colorTable)
			{

			C.BarChart barChart = new C.BarChart();

			barChart.Append(new C.BarDirection() { Val = C.BarDirectionValues.Column });
			barChart.Append(new C.BarGrouping() { Val = C.BarGroupingValues.Stacked });

			String colorval = "000000";
			String gradientcolor = "000000";
			DataTable table = new DataTable();

			int o = 0;
			foreach (DataColumn col in queryTable.Columns)
				{
				//copy the column to a new table
				table = new DataTable();
				table.Columns.Add(queryTable.Columns[o].ColumnName, queryTable.Columns[o].DataType);
				//then copy the data row by row (how else to do this?
				foreach (DataRow row in queryTable.Rows)
					{
					var r = table.Rows.Add();
					r[queryTable.Columns[o].ColumnName] = row[queryTable.Columns[o].ColumnName];
					}
				

//			foreach (DataRow row in queryTable.Rows)

//				{
				if (o > 0)//because the first column is the titles
					{
					colorval = colorTable.Rows[o -1].Field<String>("Color");
					//gradientcolor = colorTable.Rows[o - 1].Field<String>("GradientColor");
					}
				//if (o == 1) { colorval = "588FCC"; gradientcolor = "263F59"; } //to fix
				//if (o == 2) { colorval = "FFFFFF"; gradientcolor = "707070"; } //to fix
				barChart.Append(MakeBarChartSeries(table, Convert.ToInt32(o), colorval, gradientcolor));
				o++;
				}

			barChart.Append(new C.GapWidth() { Val = (UInt16Value)100U });
			barChart.Append(new C.Overlap() { Val = 100 });
			barChart.Append(new C.AxisId() { Val = (UInt32Value)114615424U });
			barChart.Append(new C.AxisId() { Val = (UInt32Value)114616960U });
			
			return barChart;
			}

		private static String GetExcelColumnName(Int32 columnNumber)
			{
			Int32 dividend = columnNumber;
			string columnName = String.Empty;
			Int32 modulo;

			while (dividend > 0)
				{
				modulo = (dividend - 1) % 26;
				columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
				dividend = (Int32)((dividend - modulo) / 26);
				}

			return columnName;
			}

		private static C.BarChartSeries MakeBarChartSeries(DataTable table, Int32 ordervalue, String barColor, String gradientColor)
			{

			C.BarChartSeries barChartSeries = new C.BarChartSeries();
			C.Index index = new C.Index() { Val = ((Convert.ToUInt32(ordervalue) + 1) * 2) + 1 };
			C.Order order = new C.Order() { Val = ((Convert.ToUInt32(ordervalue) + 1) * 2) };

			C.SeriesText seriesText = new C.SeriesText();

			C.StringReference stringReference = new C.StringReference();
			C.Formula formula = new C.Formula();
			formula.Text = "Sheet1!$" + GetExcelColumnName(ordervalue+1) + "$1";//fix

			C.StringCache stringCache = new C.StringCache();
			C.PointCount pointCount = new C.PointCount() { Val = (UInt32Value)1U };//what is this?

			C.StringPoint stringPoint = new C.StringPoint() { Index = (UInt32Value)0U };//what is this?
			C.NumericValue numericValue = new C.NumericValue();
			numericValue.Text = table.Columns[0].ColumnName;// "EPOF";//fix

			stringPoint.Append(numericValue);

			stringCache.Append(pointCount);
			stringCache.Append(stringPoint);

			stringReference.Append(formula);
			stringReference.Append(stringCache);

			seriesText.Append(stringReference);

			C.ChartShapeProperties chartShapeProperties2 = new C.ChartShapeProperties();

			A.GradientFill gradientFill10 = new A.GradientFill() { RotateWithShape = false };

			A.GradientStopList gradientStopList10 = new A.GradientStopList();

			A.GradientStop gradientStop26 = new A.GradientStop() { Position = 0 };

			A.RgbColorModelHex rgbColorModelHex209 = new A.RgbColorModelHex() { Val = gradientColor, LegacySpreadsheetColorIndex = 9, MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "a14" } };
			rgbColorModelHex209.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
			rgbColorModelHex209.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");
			A.Gamma gamma3 = new A.Gamma();
			A.Shade shade17 = new A.Shade() { Val = 66275 };
			A.InverseGamma inverseGamma3 = new A.InverseGamma();

			rgbColorModelHex209.Append(gamma3);
			rgbColorModelHex209.Append(shade17);
			rgbColorModelHex209.Append(inverseGamma3);

			gradientStop26.Append(rgbColorModelHex209);

			A.GradientStop gradientStop27 = new A.GradientStop() { Position = 50000 };

			A.RgbColorModelHex rgbColorModelHex210 = new A.RgbColorModelHex() { Val = barColor, LegacySpreadsheetColorIndex = 9, MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "a14" } };
			rgbColorModelHex210.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
			rgbColorModelHex210.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

			gradientStop27.Append(rgbColorModelHex210);

			A.GradientStop gradientStop28 = new A.GradientStop() { Position = 100000 };

			A.RgbColorModelHex rgbColorModelHex211 = new A.RgbColorModelHex() { Val = gradientColor, LegacySpreadsheetColorIndex = 9, MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "a14" } };
			rgbColorModelHex211.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
			rgbColorModelHex211.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");
			A.Gamma gamma4 = new A.Gamma();
			A.Shade shade18 = new A.Shade() { Val = 66275 };
			A.InverseGamma inverseGamma4 = new A.InverseGamma();

			rgbColorModelHex211.Append(gamma4);
			rgbColorModelHex211.Append(shade18);
			rgbColorModelHex211.Append(inverseGamma4);

			gradientStop28.Append(rgbColorModelHex211);

			gradientStopList10.Append(gradientStop26);
			gradientStopList10.Append(gradientStop27);
			gradientStopList10.Append(gradientStop28);
			A.LinearGradientFill linearGradientFill6 = new A.LinearGradientFill() { Angle = 0, Scaled = true };

			gradientFill10.Append(gradientStopList10);
			gradientFill10.Append(linearGradientFill6);

			A.Outline outline25 = new A.Outline() { Width = 12568 };

			A.SolidFill solidFill72 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex212 = new A.RgbColorModelHex() { Val = "000000" };

			solidFill72.Append(rgbColorModelHex212);
			A.PresetDash presetDash10 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };

			outline25.Append(solidFill72);
			outline25.Append(presetDash10);

			chartShapeProperties2.Append(gradientFill10);
			chartShapeProperties2.Append(outline25);
			C.InvertIfNegative invertIfNegative2 = new C.InvertIfNegative() { Val = false };


			C.Values values2 = new C.Values();

			C.NumberReference numberReference2 = new C.NumberReference();
			C.Formula formula6 = new C.Formula();
			formula6.Text = "Sheet1!$" + GetExcelColumnName(ordervalue + 1) + "$2:$" + GetExcelColumnName(ordervalue + 1) + "$15";

			C.NumberingCache numberingCache2 = new C.NumberingCache();
			C.FormatCode formatCode2 = new C.FormatCode();
			formatCode2.Text = "General";
			C.PointCount pointCount6 = new C.PointCount() { Val = (UInt32Value)14U };

			numberingCache2.Append(formatCode2);
			numberingCache2.Append(pointCount6);

//DataRow.i
			//int n = queryRow.ItemArray.Count();
			UInt32Value n = 1;
			//foreach (object ob in queryRow.ItemArray)
			//	{
					//foreach(DataRow row in queryCol.r
			//	}
			//if (ordervalue > 0)
			//	{
				//for (int n = 1; n < queryCol.ItemArray.Count(); n++)
				foreach (DataRow row in table.Rows)
					{

					C.NumericPoint numericPoint = new C.NumericPoint() { Index = (UInt32Value)(n) };
					C.NumericValue numericValue2 = new C.NumericValue();
					if (ordervalue == 0)
						{
						numericValue2.Text = "0";
						}
					else
						{
						numericValue2.Text = row[0].ToString();
						}

					numericPoint.Append(numericValue2);
					numberingCache2.Append(numericPoint);
					n++;
					}



				numberReference2.Append(formula6);
				numberReference2.Append(numberingCache2);

				values2.Append(numberReference2);

				barChartSeries.Append(index);
				barChartSeries.Append(order);
				barChartSeries.Append(seriesText);
				barChartSeries.Append(chartShapeProperties2);
				barChartSeries.Append(invertIfNegative2);
				//barChartSeries2.Append(categoryAxisData2);
				//}
			if (ordervalue == 0)
				{
				C.CategoryAxisData categoryAxisData1 = new C.CategoryAxisData();

				C.StringReference stringReference2 = new C.StringReference();
				C.Formula formula2 = new C.Formula();
				formula2.Text = "Sheet1!$A$2:$A$15";

				C.StringCache stringCache2 = new C.StringCache();
				C.PointCount pointCount2 = new C.PointCount() { Val = (UInt32Value)6U };


				stringCache2.Append(pointCount2);

				n = 1;
				foreach (DataRow row in table.Rows)
					{

					C.StringPoint stringPoint2 = new C.StringPoint() { Index = (UInt32Value)n };
					C.NumericValue numericValue2 = new C.NumericValue();
					numericValue2.Text = row[0].ToString();

					stringPoint2.Append(numericValue2);
					stringCache2.Append(stringPoint2);
					n++;
					}

				stringReference2.Append(formula2);
				stringReference2.Append(stringCache2);

				categoryAxisData1.Append(stringReference2);
				barChartSeries.Append(categoryAxisData1);
				}

			barChartSeries.Append(values2);


			return barChartSeries;
			}

		private static C.LineChart AddLineChart(DataTable queryTable, DataTable colorTable)
			{
			C.LineChart lineChart = new C.LineChart();
			C.Grouping grouping = new C.Grouping() { Val = C.GroupingValues.Standard };
			C.AxisId axisId1 = new C.AxisId() { Val = (UInt32Value)114615424U };
			C.AxisId axisId2 = new C.AxisId() { Val = (UInt32Value)114616960U };
			lineChart.Append(grouping);

			String colorval = "000000";
			//String gradientcolor = "000000";

			DataTable table = new DataTable();
			int o = 0;
			foreach (DataColumn col in queryTable.Columns)
				{
				//copy the column to a new table
				table = new DataTable();
				table.Columns.Add(queryTable.Columns[o].ColumnName, queryTable.Columns[o].DataType);
				//then copy the data row by row (how else to do this?
				foreach (DataRow row in queryTable.Rows)
					{
					var r = table.Rows.Add();
					r[queryTable.Columns[o].ColumnName] = row[queryTable.Columns[o].ColumnName];
					}


				//			foreach (DataRow row in queryTable.Rows)

				//				{
				//if (o == 1) { colorval = "588FCC"; gradientcolor = "263F59"; } //to fix
				//if (o == 2) { colorval = "FFFFFF"; gradientcolor = "707070"; } //to fix
				//barChart.Append(MakeBarChartSeries(table, Convert.ToUInt32(o), colorval, gradientcolor));
				//colorval = colorTable.Rows[o].Field<String>("Color");
				if (o > 0)//because the first column is the titles
					{
					colorval = colorTable.Rows[o - 1].Field<String>("Color");
					//gradientcolor = colorTable.Rows[o - 1].Field<String>("GradientColor");
					}
				//gradientcolor = colorTable.Rows[o].Field<String>("GradientColor");
				lineChart.Append(MakeLineChartSeries(table, Convert.ToUInt32(o),colorval));
				o++;
				}


			lineChart.Append(axisId1); //these are required, don't know what the ID number should be though
			lineChart.Append(axisId2);

			return lineChart;
			}

		private static C.LineChartSeries MakeLineChartSeries(DataTable table, UInt32Value ordervalue, String colorval)
			{
			//following order is Formula for header, Text for header, Color Value of line, formula for data, point counts (not sure if this is needed yet)
			//on the second row are the 14 data points in this sample
			//string[,] array = { { "Sheet1!$E$1", "Sheet1!$F$1", "Sheet1!$G$1", "Sheet1!$H$1" }, { "1Q/2Q Break", "2Q/3Q", "3Q/4Q","4Q/5Q" }, { "4FC774", "FFFF00", "F32525","FF00FF" }, { "Sheet1!$E$2:$E$15", "Sheet1!$F$2:$F$15", "Sheet1!$G$2:$G$15", "Sheet1!$h$2:$H$15" },{"14","14","14","14"},
			//				  {"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"},{"6.57","9.16","16.5","20"}};

			C.LineChartSeries lineChartSeries = new C.LineChartSeries();
			C.Index index = new C.Index() { Val = ((ordervalue + 1) * 2) + 101 };//creating a unique index number
			C.Order order = new C.Order() { Val = ((ordervalue + 1) * 2) + 100 };//creating a unique order number

			C.SeriesText seriesText = new C.SeriesText();

			C.StringReference stringReference = new C.StringReference();
			C.Formula formula = new C.Formula();
			formula.Text = "test";//to fix array[0];

			C.StringCache stringCache = new C.StringCache();
			C.PointCount pointCount = new C.PointCount() { Val = (UInt32Value)1U };//what is this?

			C.StringPoint stringPoint = new C.StringPoint() { Index = (UInt32Value)0U };//what is this?
			C.NumericValue numericValue = new C.NumericValue();
			numericValue.Text = table.Columns[0].ColumnName;//array[1];

			stringPoint.Append(numericValue);

			stringCache.Append(pointCount);
			stringCache.Append(stringPoint);

			stringReference.Append(formula);
			stringReference.Append(stringCache);

			seriesText.Append(stringReference);

			C.ChartShapeProperties chartShapeProperties4 = new C.ChartShapeProperties();

			A.Outline outline = new A.Outline() { Width = 37705 };

			A.SolidFill solidFill = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex = new A.RgbColorModelHex() { Val = colorval };

			solidFill.Append(rgbColorModelHex);
			A.PresetDash presetDash12 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };

			if (ordervalue > 0)
				{
				outline.Append(solidFill);
				}
			outline.Append(presetDash12);

			chartShapeProperties4.Append(outline);

			C.Marker marker2 = new C.Marker();
			C.Symbol symbol2 = new C.Symbol() { Val = C.MarkerStyleValues.None };

			marker2.Append(symbol2);


			C.Values values = new C.Values();

			C.NumberReference numberReference = new C.NumberReference();
			C.Formula formula12 = new C.Formula();
			formula12.Text = "test2";//to fix array[3];

			C.NumberingCache numberingCache4 = new C.NumberingCache();
			C.FormatCode formatCode4 = new C.FormatCode();
			formatCode4.Text = "General";//probably need to pass this somehow?
			C.PointCount pointCount12 = new C.PointCount() { Val = Convert.ToUInt32(table.Rows.Count) };

			numberingCache4.Append(formatCode4);
			numberingCache4.Append(pointCount12);

			//if (Convert.ToUInt32(array[4]) > 0) //there is data included
			//	{
			//	for (int n = 5; n < (5 + Convert.ToUInt32(array[4])); n++)
			//		{
			//		//why 5 above and below? because the first 5 elements are used above
			//		//in the next line, index counts up from 0
			//		C.NumericPoint numericPoint = new C.NumericPoint() { Index = (UInt32Value)(Convert.ToUInt32(n) - 5) };
			//		C.NumericValue numericValue2 = new C.NumericValue();
			//		numericValue2.Text = array[n];

			//		numericPoint.Append(numericValue2);
			//		numberingCache4.Append(numericPoint);

			//		}
			//	}
			UInt32Value n = 0;
			foreach (DataRow row in table.Rows)
				{

				if (ordervalue > 0)
					{
					C.NumericPoint numericPoint = new C.NumericPoint() { Index = (UInt32Value)(n) };
					C.NumericValue numericValue2 = new C.NumericValue();
					//if (ordervalue == 0)
					//	{
					//	numericValue2.Text = "0";
					//	}
					//else
					//	{
					numericValue2.Text = row[0].ToString();
					//}

					numericPoint.Append(numericValue2);
					numberingCache4.Append(numericPoint);
					}
				n++;
				}


			numberReference.Append(formula12);
			numberReference.Append(numberingCache4);

			values.Append(numberReference);
			C.Smooth smooth2 = new C.Smooth() { Val = false };

			lineChartSeries.Append(index);
			lineChartSeries.Append(order);
			lineChartSeries.Append(seriesText);
			lineChartSeries.Append(chartShapeProperties4);
			lineChartSeries.Append(marker2);
			//lineChartSeries2.Append(categoryAxisData4);
			if (ordervalue == 0)
				{
				C.CategoryAxisData categoryAxisData1 = new C.CategoryAxisData();

				C.StringReference stringReference2 = new C.StringReference();
				C.Formula formula2 = new C.Formula();
				formula2.Text = "Sheet1!$A$2:$A$15";

				C.StringCache stringCache2 = new C.StringCache();
				C.PointCount pointCount2 = new C.PointCount() { Val = (UInt32Value)6U };


				stringCache2.Append(pointCount2);

				n = 1;
				foreach (DataRow row in table.Rows)
					{

					C.StringPoint stringPoint2 = new C.StringPoint() { Index = (UInt32Value)n };
					C.NumericValue numericValue2 = new C.NumericValue();
					numericValue2.Text = row[0].ToString();
					if (numericValue2.Text == "0" | numericValue2.Text == "25" | numericValue2.Text == "50" | numericValue2.Text == "75" | numericValue2.Text == "100")
						{

						}
					else
						{
						numericValue2.Text = "";
						}

					stringPoint2.Append(numericValue2);
					stringCache2.Append(stringPoint2);
					n++;
					}

				stringReference2.Append(formula2);
				stringReference2.Append(stringCache2);

				categoryAxisData1.Append(stringReference2);
				lineChartSeries.Append(categoryAxisData1);
				}


			lineChartSeries.Append(values);
			lineChartSeries.Append(smooth2);

			return lineChartSeries;
			}

		private static C.CategoryAxis AddChartCategoryAxis()
			{

			C.CategoryAxis categoryAxis = new C.CategoryAxis();
			C.AxisId axisId5 = new C.AxisId() { Val = (UInt32Value)114615424U };

			C.Scaling scaling1 = new C.Scaling();
			C.Orientation orientation1 = new C.Orientation() { Val = C.OrientationValues.MinMax };

			scaling1.Append(orientation1);
			C.Delete delete1 = new C.Delete() { Val = false };
			C.AxisPosition axisPosition1 = new C.AxisPosition() { Val = C.AxisPositionValues.Bottom };
			C.NumberingFormat numberingFormat1 = new C.NumberingFormat() { FormatCode = "General", SourceLinked = true };
			C.MajorTickMark majorTickMark1 = new C.MajorTickMark() { Val = C.TickMarkValues.None };
			C.MinorTickMark minorTickMark1 = new C.MinorTickMark() { Val = C.TickMarkValues.None };
			C.TickLabelPosition tickLabelPosition1 = new C.TickLabelPosition() { Val = C.TickLabelPositionValues.NextTo };

			C.ChartShapeProperties chartShapeProperties8 = new C.ChartShapeProperties();

			A.Outline outline31 = new A.Outline() { Width = 12568 };

			A.SolidFill solidFill77 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex217 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill77.Append(rgbColorModelHex217);
			A.PresetDash presetDash15 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };

			outline31.Append(solidFill77);
			outline31.Append(presetDash15);

			chartShapeProperties8.Append(outline31);

			C.TextProperties textProperties1 = new C.TextProperties();
			A.BodyProperties bodyProperties15 = new A.BodyProperties() { Rotation = 0, Vertical = A.TextVerticalValues.Horizontal };
			A.ListStyle listStyle15 = new A.ListStyle();

			A.Paragraph paragraph21 = new A.Paragraph();

			A.ParagraphProperties paragraphProperties23 = new A.ParagraphProperties();

			A.DefaultRunProperties defaultRunProperties68 = new A.DefaultRunProperties() { FontSize = 1188, Bold = false, Italic = false, Underline = A.TextUnderlineValues.None, Strike = A.TextStrikeValues.NoStrike, Baseline = 0 };

			A.SolidFill solidFill78 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex218 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill78.Append(rgbColorModelHex218);
			A.LatinFont latinFont56 = new A.LatinFont() { Typeface = "Tahoma" };
			A.EastAsianFont eastAsianFont34 = new A.EastAsianFont() { Typeface = "Tahoma" };
			A.ComplexScriptFont complexScriptFont50 = new A.ComplexScriptFont() { Typeface = "Tahoma" };

			defaultRunProperties68.Append(solidFill78);
			defaultRunProperties68.Append(latinFont56);
			defaultRunProperties68.Append(eastAsianFont34);
			defaultRunProperties68.Append(complexScriptFont50);

			paragraphProperties23.Append(defaultRunProperties68);
			A.EndParagraphRunProperties endParagraphRunProperties9 = new A.EndParagraphRunProperties() { Language = "en-US" };

			paragraph21.Append(paragraphProperties23);
			paragraph21.Append(endParagraphRunProperties9);

			textProperties1.Append(bodyProperties15);
			textProperties1.Append(listStyle15);
			textProperties1.Append(paragraph21);
			C.CrossingAxis crossingAxis1 = new C.CrossingAxis() { Val = (UInt32Value)114616960U };
			C.Crosses crosses1 = new C.Crosses() { Val = C.CrossesValues.AutoZero };
			C.AutoLabeled autoLabeled1 = new C.AutoLabeled() { Val = true };
			C.LabelAlignment labelAlignment1 = new C.LabelAlignment() { Val = C.LabelAlignmentValues.Center };
			C.LabelOffset labelOffset1 = new C.LabelOffset() { Val = (UInt16Value)100U };
			C.TickLabelSkip tickLabelSkip1 = new C.TickLabelSkip() { Val = 1 };
			C.TickMarkSkip tickMarkSkip1 = new C.TickMarkSkip() { Val = 1 };
			C.NoMultiLevelLabels noMultiLevelLabels1 = new C.NoMultiLevelLabels() { Val = false };

			categoryAxis.Append(axisId5);
			categoryAxis.Append(scaling1);
			categoryAxis.Append(delete1);
			categoryAxis.Append(axisPosition1);
			categoryAxis.Append(numberingFormat1);
			categoryAxis.Append(majorTickMark1);
			categoryAxis.Append(minorTickMark1);
			categoryAxis.Append(tickLabelPosition1);
			categoryAxis.Append(chartShapeProperties8);
			categoryAxis.Append(textProperties1);
			categoryAxis.Append(crossingAxis1);
			categoryAxis.Append(crosses1);
			categoryAxis.Append(autoLabeled1);
			categoryAxis.Append(labelAlignment1);
			categoryAxis.Append(labelOffset1);
			categoryAxis.Append(tickLabelSkip1);
			categoryAxis.Append(tickMarkSkip1);
			categoryAxis.Append(noMultiLevelLabels1);

			return categoryAxis;
			}

		private static C.ValueAxis AddChartValueAxis(String xAxisLabel)
			{

			C.ValueAxis valueAxis = new C.ValueAxis();
			C.AxisId axisId6 = new C.AxisId() { Val = (UInt32Value)114616960U };

			C.Scaling scaling2 = new C.Scaling();
			C.Orientation orientation2 = new C.Orientation() { Val = C.OrientationValues.MinMax };

			scaling2.Append(orientation2);
			C.Delete delete2 = new C.Delete() { Val = false };
			C.AxisPosition axisPosition2 = new C.AxisPosition() { Val = C.AxisPositionValues.Left };

			C.Title title1 = new C.Title();

			C.ChartText chartText1 = new C.ChartText();

			C.RichText richText1 = new C.RichText();
			A.BodyProperties bodyProperties16 = new A.BodyProperties();
			A.ListStyle listStyle16 = new A.ListStyle();

			A.Paragraph paragraph22 = new A.Paragraph();

			A.ParagraphProperties paragraphProperties24 = new A.ParagraphProperties();

			A.DefaultRunProperties defaultRunProperties69 = new A.DefaultRunProperties() { FontSize = 1781, Bold = false, Italic = false, Underline = A.TextUnderlineValues.None, Strike = A.TextStrikeValues.NoStrike, Baseline = 0 };

			A.SolidFill solidFill79 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex219 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill79.Append(rgbColorModelHex219);
			A.LatinFont latinFont57 = new A.LatinFont() { Typeface = "Tahoma" };
			A.EastAsianFont eastAsianFont35 = new A.EastAsianFont() { Typeface = "Tahoma" };
			A.ComplexScriptFont complexScriptFont51 = new A.ComplexScriptFont() { Typeface = "Tahoma" };

			defaultRunProperties69.Append(solidFill79);
			defaultRunProperties69.Append(latinFont57);
			defaultRunProperties69.Append(eastAsianFont35);
			defaultRunProperties69.Append(complexScriptFont51);

			paragraphProperties24.Append(defaultRunProperties69);

			paragraph22.Append(paragraphProperties24);
			paragraph22.Append(MakeRun(xAxisLabel));

			richText1.Append(bodyProperties16);
			richText1.Append(listStyle16);
			richText1.Append(paragraph22);

			chartText1.Append(richText1);

			C.Layout layout2 = new C.Layout();

			C.ManualLayout manualLayout2 = new C.ManualLayout();
			C.LeftMode leftMode2 = new C.LeftMode() { Val = C.LayoutModeValues.Edge };
			C.TopMode topMode2 = new C.TopMode() { Val = C.LayoutModeValues.Edge };
			C.Left left2 = new C.Left() { Val = 3.3296337402885681E-3D };
			C.Top top2 = new C.Top() { Val = 0.18571428571428572D };

			manualLayout2.Append(leftMode2);
			manualLayout2.Append(topMode2);
			manualLayout2.Append(left2);
			manualLayout2.Append(top2);

			layout2.Append(manualLayout2);
			C.Overlay overlay1 = new C.Overlay() { Val = false };

			C.ChartShapeProperties chartShapeProperties9 = new C.ChartShapeProperties();
			A.NoFill noFill25 = new A.NoFill();

			A.Outline outline32 = new A.Outline() { Width = 25137 };
			A.NoFill noFill26 = new A.NoFill();

			outline32.Append(noFill26);

			chartShapeProperties9.Append(noFill25);
			chartShapeProperties9.Append(outline32);

			title1.Append(chartText1);
			title1.Append(layout2);
			title1.Append(overlay1);
			title1.Append(chartShapeProperties9);
			C.NumberingFormat numberingFormat2 = new C.NumberingFormat() { FormatCode = "0", SourceLinked = false };
			C.MajorTickMark majorTickMark2 = new C.MajorTickMark() { Val = C.TickMarkValues.Outside };
			C.MinorTickMark minorTickMark2 = new C.MinorTickMark() { Val = C.TickMarkValues.None };
			C.TickLabelPosition tickLabelPosition2 = new C.TickLabelPosition() { Val = C.TickLabelPositionValues.NextTo };

			C.ChartShapeProperties chartShapeProperties10 = new C.ChartShapeProperties();

			A.Outline outline33 = new A.Outline() { Width = 12568 };

			A.SolidFill solidFill80 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex220 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill80.Append(rgbColorModelHex220);
			A.PresetDash presetDash16 = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };

			outline33.Append(solidFill80);
			outline33.Append(presetDash16);

			chartShapeProperties10.Append(outline33);

			C.TextProperties textProperties2 = new C.TextProperties();
			A.BodyProperties bodyProperties17 = new A.BodyProperties() { Rotation = 0, Vertical = A.TextVerticalValues.Horizontal };
			A.ListStyle listStyle17 = new A.ListStyle();

			A.Paragraph paragraph23 = new A.Paragraph();

			A.ParagraphProperties paragraphProperties25 = new A.ParagraphProperties();

			A.DefaultRunProperties defaultRunProperties70 = new A.DefaultRunProperties() { FontSize = 1781, Bold = false, Italic = false, Underline = A.TextUnderlineValues.None, Strike = A.TextStrikeValues.NoStrike, Baseline = 0 };

			A.SolidFill solidFill81 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex221 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill81.Append(rgbColorModelHex221);
			A.LatinFont latinFont58 = new A.LatinFont() { Typeface = "Tahoma" };
			A.EastAsianFont eastAsianFont36 = new A.EastAsianFont() { Typeface = "Tahoma" };
			A.ComplexScriptFont complexScriptFont52 = new A.ComplexScriptFont() { Typeface = "Tahoma" };

			defaultRunProperties70.Append(solidFill81);
			defaultRunProperties70.Append(latinFont58);
			defaultRunProperties70.Append(eastAsianFont36);
			defaultRunProperties70.Append(complexScriptFont52);

			paragraphProperties25.Append(defaultRunProperties70);
			A.EndParagraphRunProperties endParagraphRunProperties10 = new A.EndParagraphRunProperties() { Language = "en-US" };

			paragraph23.Append(paragraphProperties25);
			paragraph23.Append(endParagraphRunProperties10);

			textProperties2.Append(bodyProperties17);
			textProperties2.Append(listStyle17);
			textProperties2.Append(paragraph23);
			C.CrossingAxis crossingAxis2 = new C.CrossingAxis() { Val = (UInt32Value)114615424U };
			C.Crosses crosses2 = new C.Crosses() { Val = C.CrossesValues.AutoZero };
			C.CrossBetween crossBetween1 = new C.CrossBetween() { Val = C.CrossBetweenValues.MidpointCategory };
			C.MinorUnit minorUnit1 = new C.MinorUnit() { Val = 0.5D };

			valueAxis.Append(axisId6);
			valueAxis.Append(scaling2);
			valueAxis.Append(delete2);
			valueAxis.Append(axisPosition2);
			valueAxis.Append(title1);
			valueAxis.Append(numberingFormat2);
			valueAxis.Append(majorTickMark2);
			valueAxis.Append(minorTickMark2);
			valueAxis.Append(tickLabelPosition2);
			valueAxis.Append(chartShapeProperties10);
			valueAxis.Append(textProperties2);
			valueAxis.Append(crossingAxis2);
			valueAxis.Append(crosses2);
			valueAxis.Append(crossBetween1);
			valueAxis.Append(minorUnit1);

			return valueAxis;

			}

		private static C.ShapeProperties AddChartShapeProperties()
			{

			C.ShapeProperties shapeProperties = new C.ShapeProperties();

			A.SolidFill solidFill1 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex1 = new A.RgbColorModelHex() { Val = "DDDDDD" };
			solidFill1.Append(rgbColorModelHex1);

			shapeProperties.Append(solidFill1);

			A.Outline outline = new A.Outline() { Width = 12568 };

			A.SolidFill solidFill2 = new A.SolidFill();
			A.RgbColorModelHex rgbColorModelHex2 = new A.RgbColorModelHex() { Val = "4D4D4F" };

			solidFill2.Append(rgbColorModelHex2);
			A.PresetDash presetDash = new A.PresetDash() { Val = A.PresetLineDashValues.Solid };

			outline.Append(solidFill2);
			outline.Append(presetDash);

			shapeProperties.Append(outline);

			return shapeProperties;

			}

		#endregion

		// Generates content of embeddedPackagePart1.
		public static void GenerateEmbeddedPackagePart1Content(EmbeddedPackagePart embeddedPackagePart1)
			{
			System.IO.Stream data = GetBinaryDataStream(embeddedPackagePart1Data);
			embeddedPackagePart1.FeedData(data);
			data.Close();
			}

		#region Binary Data

		//private static string embeddedPackagePart1Data = "UEsDBBQABgAIAAAAIQBi7p1oYQEAAJAEAAATAAgCW0NvbnRlbnRfVHlwZXNdLnhtbCCiBAIooAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACslE1PwzAMhu9I/IcqV9Rm44AQWrcDH0eYxPgBoXHXaGkSxd7Y/j1u9iGEyqaJXRq1sd/3iWtnNFm3NltBRONdKYbFQGTgKq+Nm5fiY/aS34sMSTmtrHdQig2gmIyvr0azTQDMONthKRqi8CAlVg20CgsfwPFO7WOriF/jXAZVLdQc5O1gcCcr7wgc5dRpiPHoCWq1tJQ9r/nzliSCRZE9bgM7r1KoEKypFDGpXDn9yyXfORScmWKwMQFvGEPIXodu52+DXd4blyYaDdlURXpVLWPItZVfPi4+vV8Ux0V6KH1dmwq0r5YtV6DAEEFpbACotUVai1YZt+c+4p+CUaZleGGQ7nxJ+AQH8f8GmZ7/R0gyJwyRNhbwwqfdip5yblQE/U6RJ+PiAD+1j3Fw30yjD8gTFOH8KuxHpMvOAwtBJAOHIelrtoMjT9/5hr+6Hbr51qB7vGW6T8bfAAAA//8DAFBLAwQUAAYACAAAACEAtVUwI/UAAABMAgAACwAIAl9yZWxzLy5yZWxzIKIEAiigAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIySz07DMAzG70i8Q+T76m5ICKGlu0xIuyFUHsAk7h+1jaMkQPf2hAOCSmPb0fbnzz9b3u7maVQfHGIvTsO6KEGxM2J712p4rZ9WD6BiImdpFMcajhxhV93ebF94pJSbYtf7qLKLixq6lPwjYjQdTxQL8exypZEwUcphaNGTGahl3JTlPYa/HlAtPNXBaggHeweqPvo8+bK3NE1veC/mfWKXToxAnhM7y3blQ2YLqc/bqJpCy0mDFfOc0xHJ+yJjA54m2lxP9P+2OHEiS4nQSODzPN+Kc0Dr64Eun2ip+L3OPOKnhOFNZPhhwcUPVF8AAAD//wMAUEsDBBQABgAIAAAAIQCBPpSX9AAAALoCAAAaAAgBeGwvX3JlbHMvd29ya2Jvb2sueG1sLnJlbHMgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsks9KxDAQxu+C7xDmbtOuIiKb7kWEvWp9gJBMm7JtEjLjn769oaLbhWW99BL4Zsj3/TKZ7e5rHMQHJuqDV1AVJQj0JtjedwremuebBxDE2ls9BI8KJiTY1ddX2xccNOdL5PpIIrt4UuCY46OUZByOmooQ0edOG9KoOcvUyajNQXcoN2V5L9PSA+oTT7G3CtLe3oJoppiT//cObdsbfArmfUTPZyIk8TTkB4hGpw5ZwY8uMiPI8/GbNeM5jwWP6bOU81ldYqjWZPgM6UAOkY8cfyWSc+cizN2aMOR0QvvKKa/b8luW5d/JyJONq78BAAD//wMAUEsDBBQABgAIAAAAIQBU0RyZYgEAADwCAAAPAAAAeGwvd29ya2Jvb2sueG1sjJHNbsIwEITvlfoOlu+QEAiliAT1V+VSVaKFs4k3xMKxLdtpoE/fdSJoe+vJnl3n253JYnmsJfkE64RWGR0NY0pAFZoLtc/ox/vzYEaJ80xxJrWCjJ7A0WV+fbVotT3stD4QBCiX0cp7M48iV1RQMzfUBhR2Sm1r5lHafeSMBcZdBeBrGSVxPI1qJhTtCXP7H4YuS1HAoy6aGpTvIRYk87i+q4RxNF+UQsKmd0SYMa+sxr2PkhLJnH/iwgPPaIpSt/CnYBtz3wiJ3dtxPKZRfjH5Zgn6Kg4PujY4ayek8CeMixIOJWukf0fX56FYTyZJMg2AkNBGQOt+WEGS41YortuMDkYpRn66yMkMZds1t4L7CmE3SXKpvYDYVx4XnKVp4Ee/BnTJ4qDuJKqzvQ5p45pdbYXOEkrsXODFrvioI5w/K5gsgk88wsM4NLWEtfgCYqHM6F3//vzj828AAAD//wMAUEsDBBQABgAIAAAAIQAXOldi+gAAAOwBAAAUAAAAeGwvc2hhcmVkU3RyaW5ncy54bWyMkdFKwzAUhu8F3+Fw7re03VAZaYabTvDGFdwDhPbYFpuk5qRF394MBkKr4OX5zvfzkxO5/TQdjOS5dTbHdJkgkC1d1do6x9PrYXGHwEHbSnfOUo5fxLhV11eSOUDMWs6xCaHfCMFlQ0bz0vVk4+bNeaNDHH0tuPekK26IgulEliQ3wujWIpRusCH23iIMtv0YaH8BGSrJrZJBPZ5eDlIEJcV5vrDjnKWFyArYxaL3qZ4VYlVM4aoQ6xl88m7o4X6sp/ZRl7QAphDIT3d790vifJwN9zGWY3w9kx8J1XMGAJP8H+b632Y6N9XDLk1+ekT8LfUNAAD//wMAUEsDBBQABgAIAAAAIQD7YqVtlAYAAKcbAAATAAAAeGwvdGhlbWUvdGhlbWUxLnhtbOxZT2/bNhS/D9h3IHRvbSe2Gwd1itixm61NG8Ruhx5pmZZYU6JA0kl9G9rjgAHDumGXAbvtMGwr0AK7dJ8mW4etA/oV9khKshjLS9IGG9bVh0Qif3z/3+MjdfXag4ihQyIk5XHbq12ueojEPh/TOGh7d4b9SxsekgrHY8x4TNrenEjv2tb7713FmyokEUGwPpabuO2FSiWblYr0YRjLyzwhMcxNuIiwglcRVMYCHwHdiFXWqtVmJcI09lCMIyB7ezKhPkFDTdLbyoj3GLzGSuoBn4mBJk2cFQY7ntY0Qs5llwl0iFnbAz5jfjQkD5SHGJYKJtpe1fy8ytbVCt5MFzG1Ym1hXd/80nXpgvF0zfAUwShnWuvXW1d2cvoGwNQyrtfrdXu1nJ4BYN8HTa0sRZr1/katk9EsgOzjMu1utVGtu/gC/fUlmVudTqfRSmWxRA3IPtaX8BvVZn17zcEbkMU3lvD1zna323TwBmTxzSV8/0qrWXfxBhQyGk+X0Nqh/X5KPYdMONsthW8AfKOawhcoiIY8ujSLCY/VqliL8H0u+gDQQIYVjZGaJ2SCfYjiLo5GgmLNAG8SXJixQ75cGtK8kPQFTVTb+zDBkBELeq+ef//q+VP06vmT44fPjh/+dPzo0fHDHy0tZ+EujoPiwpfffvbn1x+jP55+8/LxF+V4WcT/+sMnv/z8eTkQMmgh0Ysvn/z27MmLrz79/bvHJfBtgUdF+JBGRKJb5Agd8Ah0M4ZxJScjcb4VwxBTZwUOgXYJ6Z4KHeCtOWZluA5xjXdXQPEoA16f3XdkHYRipmgJ5xth5AD3OGcdLkoNcEPzKlh4OIuDcuZiVsQdYHxYxruLY8e1vVkCVTMLSsf23ZA4Yu4zHCsckJgopOf4lJAS7e5R6th1j/qCSz5R6B5FHUxLTTKkIyeQFot2aQR+mZfpDK52bLN3F3U4K9N6hxy6SEgIzEqEHxLmmPE6nikclZEc4ogVDX4Tq7BMyMFc+EVcTyrwdEAYR70xkbJszW0B+hacfgNDvSp1+x6bRy5SKDoto3kTc15E7vBpN8RRUoYd0DgsYj+QUwhRjPa5KoPvcTdD9Dv4Accr3X2XEsfdpxeCOzRwRFoEiJ6ZiRJfXifcid/BnE0wMVUGSrpTqSMa/13ZZhTqtuXwrmy3vW3YxMqSZ/dEsV6F+w+W6B08i/cJZMXyFvWuQr+r0N5bX6FX5fLF1+VFKYYqrRsS22ubzjta2XhPKGMDNWfkpjS9t4QNaNyHQb3OHDpJfhBLQnjUmQwMHFwgsFmDBFcfURUOQpxA317zNJFApqQDiRIu4bxohktpazz0/sqeNhv6HGIrh8Rqj4/t8Loezo4bORkjVWDOtBmjdU3grMzWr6REQbfXYVbTQp2ZW82IZoqiwy1XWZvYnMvB5LlqMJhbEzobBP0QWLkJx37NGs47mJGxtrv1UeYW44WLdJEM8ZikPtJ6L/uoZpyUxcqSIloPGwz67HiK1QrcWprsG3A7i5OK7Oor2GXeexMvZRG88BJQO5mOLC4mJ4vRUdtrNdYaHvJx0vYmcFSGxygBr0vdTGIWwH2Tr4QN+1OT2WT5wputTDE3CWpw+2HtvqSwUwcSIdUOlqENDTOVhgCLNScr/1oDzHpRCpRUo7NJsb4BwfCvSQF2dF1LJhPiq6KzCyPadvY1LaV8pogYhOMjNGIzcYDB/TpUQZ8xlXDjYSqCfoHrOW1tM+UW5zTpipdiBmfHMUtCnJZbnaJZJlu4KUi5DOatIB7oViq7Ue78qpiUvyBVimH8P1NF7ydwBbE+1h7w4XZYYKQzpe1xoUIOVSgJqd8X0DiY2gHRAle8MA1BBXfU5r8gh/q/zTlLw6Q1nCTVAQ2QoLAfqVAQsg9lyUTfKcRq6d5lSbKUkImogrgysWKPyCFhQ10Dm3pv91AIoW6qSVoGDO5k/LnvaQaNAt3kFPPNqWT53mtz4J/ufGwyg1JuHTYNTWb/XMS8PVjsqna9WZ7tvUVF9MSizapnWQHMCltBK0371xThnFutrVhLGq81MuHAi8saw2DeECVwkYT0H9j/qPCZ/eChN9QhP4DaiuD7hSYGYQNRfck2HkgXSDs4gsbJDtpg0qSsadPWSVst26wvuNPN+Z4wtpbsLP4+p7Hz5sxl5+TiRRo7tbBjazu20tTg2ZMpCkOT7CBjHGO+lBU/ZvHRfXD0Dnw2mDElTTDBpyqBoYcemDyA5LcczdKtvwAAAP//AwBQSwMEFAAGAAgAAAAhAB6J1+hYAwAAZgsAAA0AAAB4bC9zdHlsZXMueG1sxFbbjpswEH2v1H9AvGdhc2EhAqqFxFKltqq0W6mvDpisVWNH4GyTVv33zkBuux22rfpQI4XxDOfMxePY8ZtdrZxH0bTS6MS9vvJdR+jClFKvE/fTPRuFrtNarkuujBaJuxet+yZ9/Spu7V6JuwchrAMUuk3cB2s3c89riwdR8/bKbIQGS2WamluYNmuv3TSCly2CauWNfT/wai612zPM6+JPSGrefNluRoWpN9zKlVTS7jsu16mL+du1Ng1fKQh1dz3lxZG7m/xCX8uiMa2p7BXQeaaqZCF+jTLyIg+Y0rgy2rZOYbbaQq2AGj3Mv2jzVTM0obL/Ko1XXhq335xHrkDruzDTvBb9/LaRXKHKQ8bDqwWkVOrEP0YqUKQx5GlFoxlMnIN8v99AhhqWpKfpvvvN1+uG76/HswuA1zmEWE1TQgucMzuq0liJykKgjVw/4NuaDfyujLVQrzQuJV8bzRWIXk9yEiCdQih1h23yuXrCvascva1Zbd+WiQsNh0U4ipDIQez5+gnyX7L13P9M6+yqp/zAeBH2k6BP7h1cx8T9gH2toAcOFM5qK5WV+ilhlz5wlrtzCbpmsNijvfVYdqhEKSq+Vfb+ZEzcs/xelHJbj09ffZSPxnYUiXuW3+FKXQe4yoVRpgHnUpdiJ8r8MG3Wq050QID6HwYCnltYN2gLwiiL7zM2ZEEbhUE3NAb1NAb1Q5ah2BBBY9APHcH/zyfE0MhaszyPpiFVUUSEJCYEPc2GehozybNlMKX8LLpBWdAPzZZlS385oTATGFFEWXw/gkFZoijPh/IZigCrQ2PgSArynPKTw/jb3sFuG/LzkoX2gzHTlpd2yUs1oNl8fyhT1NMYtNB1Y0t2O7ulKoorR7MxhjYKAw0fDWHQRmEmkyCgMdDvS3r/DHcV5kmvKUZNWzLYWhMytgAGHXUU4EPlM2X5zQ25G3H3BCSGLZa3s4xiwz1HR80m41l/XXh+LkQR1pRim4XhQB9MF/B0//7es/PIO55TYmfftXAdgrezbWTifl9mN9Fiycaj0M/C0XQiZqNoli1Gs2meLRYs8sd+/uPiivcPF7zuIprGcKWbtwqugc3hcD4cyXdnXeJeTPrjFkvhQdj9b5eE154uyOlPAAAA//8DAFBLAwQUAAYACAAAACEAfqsGhJcDAABaDQAAGAAAAHhsL3dvcmtzaGVldHMvc2hlZXQxLnhtbJRXTZOiMBC9b9X+B4r7QiLhI5Y6pY4zu4et2tqvO2JUaoBQkJnZ+ffbpJVRDA56Et7rznvdgQ6Tu395Zr2Iqk5lMbWpQ2xLFIncpMVuav/5/fAlsq1axcUmzmQhpvabqO272edPk1dZPdV7IZQFGYp6au+VKseuWyd7kce1I0tRALKVVR4ruKx2bl1WIt7ooDxzR4QEbh6nhY0ZxtWQHHK7TRNxL5PnXBQKk1QiixXor/dpWR+z5cmQdHlcPT2XXxKZl5BinWapetNJbStPxt92hazidQa+/1EWJ8fc+uIifZ4mlazlVjmQzkWhl565y13INJtsUnDQlN2qxHZqz+n4kfq2O5voAv1NxWt98t9S8fqXyESixAb6ZFtN/ddSPjXEb3CLNKHuReyDrv+PytqIbfycqZ/y9atId3sFSUZO6IOlxtl483Yv6gRKCpmcUSvjPlbxbFLJVwu6A6vWZdz0mo7DnsDZJGmoC+BCqhp8vszIxH0BbckBW55i9BxbnWKjc+zhFPPOscdTjLWYC8pb+aPh8lcjLTxw/LDNpX09IMAdGpwDjwhQCGmBs9W94avPgdsW7z0dFtY7KCMBiwJCfBISTinp1GqJNOZwxnzm+V7EKCeMvJdGZ7tHWqdBq+MaF+4RMLhHoN89u8E9cFv3nSovGG4osBWGzPOYx4OABZRHbdG1r+WRFwaceoTyIKKhR70ub4U8Q58RMDhFoN8pPFFDH5I5cFunnY228LVT7oBLf+RHJCL61ynIEmmBA9VggQcWkdZ5PFZH2kVHj8tc7GcE+n0GN/gEbuuz06lFoH0yh/gcPNIRMxpYIq27UfGuoX0IGNqHQL8teK8Nbh9wW1v8fAMuQm3Lc7yARpyQ8NC+zutuibSuLbxrsIWAwRYC/baaKT7w1T0HbmuLdqQtIu2LOtxnkcc567yeloh3olZ412AIAYOhwzoQ0hb27HXKbzAE3HdDnQ6suDZkkIaAQRoC/bWmcIYaXOyG3C8O0GaAGtQdEIO8A3JFH8zK4fpOByvtFo/SXn2ImPQhckXfDYN6ToF8pX69cxzimsqa9H00yekto7whX9HXO2khrk/fR7OW3jJsG/IVfb3zEeL69H00IeGUO3j/rYDc9wT0jq5DjGGH4fkYD7VlvBPf42qXFrWViS0UgejzcIWnY7xQstTn3rVUSub67x6+YQQccYkDPrZSquNFc/5uv4pm/wEAAP//AwBQSwMEFAAGAAgAAAAhAB7BSAIjAQAA6QEAABEACAFkb2NQcm9wcy9jb3JlLnhtbCCiBAEooAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGyRX0/DIBTF3038Dg3vLf2jjZK2S9TsySUmq5nxjcBdRyyUANr120u7rs7oI5zD7557KFZH2QZfYKzoVImSKEYBKNZxoZoSvdbr8A4F1lHFadspKNEAFq2q66uCacI6Ay+m02CcABt4krKE6RIdnNMEY8sOIKmNvEN5cd8ZSZ0/mgZryj5oAziN4xxLcJRTR/EIDPVCRDOSswWpP007ATjD0IIE5SxOogT/eB0Yaf99MCkXTincoP1Oc9xLNmcncXEfrViMfd9HfTbF8PkT/LZ53k6rhkKNXTFA1dhPS63b+Cr3AvjDUG13Bf57W3A25SJydgZ+FDkFO0u77PGpXqMqjZPbML4J47xOcpLekzR7L/DZdQZU05jfn1N9AwAA//8DAFBLAwQUAAYACAAAACEAPAIw/oEBAAD+AgAAEAAIAWRvY1Byb3BzL2FwcC54bWwgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACckkFv2zAMhe8D+h8M3RM5bTAMgayiaFr00GIBknZnVaZjIYpkiKyR7NePtpHF2XrqjeR7ePpESd0e9j5rIaGLoRCzaS4yCDaWLmwL8bp5nPwQGZIJpfExQCGOgOJWX31TqxQbSOQAM44IWIiaqFlIibaGvcEpy4GVKqa9IW7TVsaqchaW0X7sIZC8zvPvEg4EoYRy0vwNFEPioqWvhpbRdnz4tjk2DKzVXdN4Zw3xLfWLsylirCh7OFjwSo5FxXRrsB/J0VHnSo5btbbGwz0H68p4BCXPA/UEplvayriEWrW0aMFSTBm637y2a5G9G4QOpxCtSc4EYqzONjR97RukpH/FtMMagFBJNgzDvhx7x7Wb61lv4OLS2AUMICxcIm4cecCf1cok+oR4NibuGQbeAWfd8Q1njvn6K/NJ/2Q/u7DD12YTl4bgtLvLoVrXJkHJ6z7p54F64rUl34Xc1yZsoTx5/he6l34bvrOezaf5Tc6POJopef64+g8AAAD//wMAUEsBAi0AFAAGAAgAAAAhAGLunWhhAQAAkAQAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAtVUwI/UAAABMAgAACwAAAAAAAAAAAAAAAACaAwAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAgT6Ul/QAAAC6AgAAGgAAAAAAAAAAAAAAAADABgAAeGwvX3JlbHMvd29ya2Jvb2sueG1sLnJlbHNQSwECLQAUAAYACAAAACEAVNEcmWIBAAA8AgAADwAAAAAAAAAAAAAAAAD0CAAAeGwvd29ya2Jvb2sueG1sUEsBAi0AFAAGAAgAAAAhABc6V2L6AAAA7AEAABQAAAAAAAAAAAAAAAAAgwoAAHhsL3NoYXJlZFN0cmluZ3MueG1sUEsBAi0AFAAGAAgAAAAhAPtipW2UBgAApxsAABMAAAAAAAAAAAAAAAAArwsAAHhsL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAHonX6FgDAABmCwAADQAAAAAAAAAAAAAAAAB0EgAAeGwvc3R5bGVzLnhtbFBLAQItABQABgAIAAAAIQB+qwaElwMAAFoNAAAYAAAAAAAAAAAAAAAAAPcVAAB4bC93b3Jrc2hlZXRzL3NoZWV0MS54bWxQSwECLQAUAAYACAAAACEAHsFIAiMBAADpAQAAEQAAAAAAAAAAAAAAAADEGQAAZG9jUHJvcHMvY29yZS54bWxQSwECLQAUAAYACAAAACEAPAIw/oEBAAD+AgAAEAAAAAAAAAAAAAAAAAAeHAAAZG9jUHJvcHMvYXBwLnhtbFBLBQYAAAAACgAKAIACAADVHgAAAAA=";

		private static string embeddedPackagePart1Data = "UEsDBBQABgAIAAAAIQBi7p1oYQEAAJAEAAATAAgCW0NvbnRlbnRfVHlwZXNdLnhtbCCiBAIooAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACslE1PwzAMhu9I/IcqV9Rm44AQWrcDH0eYxPgBoXHXaGkSxd7Y/j1u9iGEyqaJXRq1sd/3iWtnNFm3NltBRONdKYbFQGTgKq+Nm5fiY/aS34sMSTmtrHdQig2gmIyvr0azTQDMONthKRqi8CAlVg20CgsfwPFO7WOriF/jXAZVLdQc5O1gcCcr7wgc5dRpiPHoCWq1tJQ9r/nzliSCRZE9bgM7r1KoEKypFDGpXDn9yyXfORScmWKwMQFvGEPIXodu52+DXd4blyYaDdlURXpVLWPItZVfPi4+vV8Ux0V6KH1dmwq0r5YtV6DAEEFpbACotUVai1YZt+c+4p+CUaZleGGQ7nxJ+AQH8f8GmZ7/R0gyJwyRNhbwwqfdip5yblQE/U6RJ+PiAD+1j3Fw30yjD8gTFOH8KuxHpMvOAwtBJAOHIelrtoMjT9/5hr+6Hbr51qB7vGW6T8bfAAAA//8DAFBLAwQUAAYACAAAACEAtVUwI/UAAABMAgAACwAIAl9yZWxzLy5yZWxzIKIEAiigAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIySz07DMAzG70i8Q+T76m5ICKGlu0xIuyFUHsAk7h+1jaMkQPf2hAOCSmPb0fbnzz9b3u7maVQfHGIvTsO6KEGxM2J712p4rZ9WD6BiImdpFMcajhxhV93ebF94pJSbYtf7qLKLixq6lPwjYjQdTxQL8exypZEwUcphaNGTGahl3JTlPYa/HlAtPNXBaggHeweqPvo8+bK3NE1veC/mfWKXToxAnhM7y3blQ2YLqc/bqJpCy0mDFfOc0xHJ+yJjA54m2lxP9P+2OHEiS4nQSODzPN+Kc0Dr64Eun2ip+L3OPOKnhOFNZPhhwcUPVF8AAAD//wMAUEsDBBQABgAIAAAAIQCBPpSX9AAAALoCAAAaAAgBeGwvX3JlbHMvd29ya2Jvb2sueG1sLnJlbHMgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsks9KxDAQxu+C7xDmbtOuIiKb7kWEvWp9gJBMm7JtEjLjn769oaLbhWW99BL4Zsj3/TKZ7e5rHMQHJuqDV1AVJQj0JtjedwremuebBxDE2ls9BI8KJiTY1ddX2xccNOdL5PpIIrt4UuCY46OUZByOmooQ0edOG9KoOcvUyajNQXcoN2V5L9PSA+oTT7G3CtLe3oJoppiT//cObdsbfArmfUTPZyIk8TTkB4hGpw5ZwY8uMiPI8/GbNeM5jwWP6bOU81ldYqjWZPgM6UAOkY8cfyWSc+cizN2aMOR0QvvKKa/b8luW5d/JyJONq78BAAD//wMAUEsDBBQABgAIAAAAIQDZF3YfYwEAAD8CAAAPAAAAeGwvd29ya2Jvb2sueG1sjFHLbsIwELxX6j9Yvpc8IBEgEtSn2ktVCQpnE2+IhWNHttNAv74bR+nj1pN3Zu3ZnfFqfa4l+QBjhVYZjSYhJaAKzYU6ZvR9+3Qzp8Q6pjiTWkFGL2DpOr++WnXanA5anwgKKJvRyrlmGQS2qKBmdqIbUNgptamZQ2iOgW0MMG4rAFfLIA7DNKiZUHRQWJr/aOiyFAU86KKtQblBxIBkDte3lWgszVelkLAbHBHWNK+sxr3PkhLJrHvkwgHPaIJQd/CHMG1z1wqJ3cU0nNIg/zb5Zgj6Kk73um5w1kFI4S4YFyUcStZKt0XX41Dk41kcp71An9BOQGd/tHpIznuhuO4yOptj4pcRpQg639kL7ipUmkcLXHXgnkEcK4dklKRJrx78kve54hh/EuVNb/qscUnPvaAvrM1SYGFeeOQVxmcFk0XvEg9/cZaksb+hJWzEJxADZUZvh0fj3+dfAAAA//8DAFBLAwQUAAYACAAAACEA0IywC3wAAACBAAAAFAAAAHhsL3NoYXJlZFN0cmluZ3MueG1sDMtBCsIwEEDRveAdwuxtogsRadqdJ9ADDM3YBJJJyAyitzfLz+PP67dk86EuqbKH8+TAEG81JN49vJ6P0w2MKHLAXJk8/EhgXY6HWUTNeFk8RNV2t1a2SAVlqo14yLv2gjqy71ZaJwwSibRke3HuagsmBrv8AQAA//8DAFBLAwQUAAYACAAAACEA+2KlbZQGAACnGwAAEwAAAHhsL3RoZW1lL3RoZW1lMS54bWzsWU9v2zYUvw/YdyB0b20nthsHdYrYsZutTRvEboceaZmWWFOiQNJJfRva44ABw7phlwG77TBsK9ACu3SfJluHrQP6FfZISrIYy0vSBhvW1YdEIn98/9/jI3X12oOIoUMiJOVx26tdrnqIxD4f0zhoe3eG/UsbHpIKx2PMeEza3pxI79rW++9dxZsqJBFBsD6Wm7jthUolm5WK9GEYy8s8ITHMTbiIsIJXEVTGAh8B3YhV1qrVZiXCNPZQjCMge3syoT5BQ03S28qI9xi8xkrqAZ+JgSZNnBUGO57WNELOZZcJdIhZ2wM+Y340JA+UhxiWCibaXtX8vMrW1QreTBcxtWJtYV3f/NJ16YLxdM3wFMEoZ1rr11tXdnL6BsDUMq7X63V7tZyeAWDfB02tLEWa9f5GrZPRLIDs4zLtbrVRrbv4Av31JZlbnU6n0UplsUQNyD7Wl/Ab1WZ9e83BG5DFN5bw9c52t9t08AZk8c0lfP9Kq1l38QYUMhpPl9Daof1+Sj2HTDjbLYVvAHyjmsIXKIiGPLo0iwmP1apYi/B9LvoA0ECGFY2Rmidkgn2I4i6ORoJizQBvElyYsUO+XBrSvJD0BU1U2/swwZARC3qvnn//6vlT9Or5k+OHz44f/nT86NHxwx8tLWfhLo6D4sKX337259cfoz+efvPy8RfleFnE//rDJ7/8/Hk5EDJoIdGLL5/89uzJi68+/f27xyXwbYFHRfiQRkSiW+QIHfAIdDOGcSUnI3G+FcMQU2cFDoF2CemeCh3grTlmZbgOcY13V0DxKANen913ZB2EYqZoCecbYeQA9zhnHS5KDXBD8ypYeDiLg3LmYlbEHWB8WMa7i2PHtb1ZAlUzC0rH9t2QOGLuMxwrHJCYKKTn+JSQEu3uUerYdY/6gks+UegeRR1MS00ypCMnkBaLdmkEfpmX6Qyudmyzdxd1OCvTeoccukhICMxKhB8S5pjxOp4pHJWRHOKIFQ1+E6uwTMjBXPhFXE8q8HRAGEe9MZGybM1tAfoWnH4DQ70qdfsem0cuUig6LaN5E3NeRO7waTfEUVKGHdA4LGI/kFMIUYz2uSqD73E3Q/Q7+AHHK919lxLH3acXgjs0cERaBIiemYkSX14n3InfwZxNMDFVBkq6U6kjGv9d2WYU6rbl8K5st71t2MTKkmf3RLFehfsPlugdPIv3CWTF8hb1rkK/q9DeW1+hV+XyxdflRSmGKq0bEttrm847Wtl4TyhjAzVn5KY0vbeEDWjch0G9zhw6SX4QS0J41JkMDBxcILBZgwRXH1EVDkKcQN9e8zSRQKakA4kSLuG8aIZLaWs89P7KnjYb+hxiK4fEao+P7fC6Hs6OGzkZI1VgzrQZo3VN4KzM1q+kREG312FW00KdmVvNiGaKosMtV1mb2JzLweS5ajCYWxM6GwT9EFi5Ccd+zRrOO5iRsba79VHmFuOFi3SRDPGYpD7Sei/7qGaclMXKkiJaDxsM+ux4itUK3Fqa7BtwO4uTiuzqK9hl3nsTL2URvPASUDuZjiwuJieL0VHbazXWGh7ycdL2JnBUhscoAa9L3UxiFsB9k6+EDftTk9lk+cKbrUwxNwlqcPth7b6ksFMHEiHVDpahDQ0zlYYAizUnK/9aA8x6UQqUVKOzSbG+AcHwr0kBdnRdSyYT4quiswsj2nb2NS2lfKaIGITjIzRiM3GAwf06VEGfMZVw42Eqgn6B6zltbTPlFuc06YqXYgZnxzFLQpyWW52iWSZbuClIuQzmrSAe6FYqu1Hu/KqYlL8gVYph/D9TRe8ncAWxPtYe8OF2WGCkM6XtcaFCDlUoCanfF9A4mNoB0QJXvDANQQV31Oa/IIf6v805S8OkNZwk1QENkKCwH6lQELIPZclE3ynEauneZUmylJCJqIK4MrFij8ghYUNdA5t6b/dQCKFuqklaBgzuZPy572kGjQLd5BTzzalk+d5rc+Cf7nxsMoNSbh02DU1m/1zEvD1Y7Kp2vVme7b1FRfTEos2qZ1kBzApbQStN+9cU4Zxbra1YSxqvNTLhwIvLGsNg3hAlcJGE9B/Y/6jwmf3goTfUIT+A2org+4UmBmEDUX3JNh5IF0g7OILGyQ7aYNKkrGnT1klbLdusL7jTzfmeMLaW7Cz+Pqex8+bMZefk4kUaO7WwY2s7ttLU4NmTKQpDk+wgYxxjvpQVP2bx0X1w9A58NpgxJU0wwacqgaGHHpg8gOS3HM3Srb8AAAD//wMAUEsDBBQABgAIAAAAIQCX4vVIWwIAADoFAAANAAAAeGwvc3R5bGVzLnhtbKRUTY+bMBC9V+p/sHwnJjTZJhGwUpKNtNK2Wimp1KsDhljrD2SbNLTqf+8YCGG1h7baC54Zj5/fPM8Q31+kQGdmLNcqwdNJiBFTmc65KhP87bALFhhZR1VOhVYswQ2z+D79+CG2rhFsf2LMIYBQNsEn56oVITY7MUntRFdMwU6hjaQOXFMSWxlGc+sPSUGiMLwjknKFO4SVzP4FRFLzUldBpmVFHT9ywV3TYmEks9VjqbShRwFUL9MZza7YrfMGXvLMaKsLNwE4oouCZ+wtyyVZEkBK40IrZ1Gma+VAK4D2N6xelP6hdn7LB7usNLY/0ZkKiEwxSeNMC22QA2WAWBtRVLIuY0MFPxru0woquWi6cOQDrZh9nuRQmg8Sz6NfLBziQgysIk8AAmkM6jhm1A4c1NuHpoLrFTxkB9Pm/SW7NLSZRvPRAdJemMZHbXJonJse11AaC1Y4IGp4efKr0xV8j9o5UDmNc05LragAk3QggwHlZEyIvW+u78Ur7EuBVC130j3mCYY29SJcTSikNzu8zvH4Y7QOewTrxfp/WHQpBvx3nEa0qkSzbkXse6JlC/xGIrySYCgG+e5J8Fc/WwL6sCeEjjUXjquB3q18wMwvN0FD/57Oz0kr9XAL6JqzgtbCHYbNBN/sLyzntYyGrGd+1q6FSPDNfvLvPr3zd7CLe7LQrLCi2vAE/3pYf15uH3ZRsAjXi2D2ic2D5Xy9DeazzXq73S3DKNz8Ho3tO4a2/bnAE09nKytgtE1fbF/i/hZL8Mjp6LcdD7Shia5FEDv89NI/AAAA//8DAFBLAwQUAAYACAAAACEAwNfLChgCAACIBQAAGAAAAHhsL3dvcmtzaGVldHMvc2hlZXQxLnhtbJRUTW/bMAy9D9h/EHSv5WTt2hpJii5GsR4GDN3XWZFpW4hlGpKyJP9+lJ0EtjNg3sUQn8j3SJrU4ulgKvYbrNNYL/ksijmDWmGm62LJf3x/uXngzHlZZ7LCGpb8CI4/rd6/W+zRbl0J4Bkx1G7JS++bRAinSjDSRdhATTc5WiM9mbYQrrEgszbIVGIexx+FkbrmHUNip3BgnmsFKaqdgdp3JBYq6Sl/V+rGndmMmkJnpN3umhuFpiGKja60P7aknBmVvBY1WrmpqO7D7FaqM3drXNEbrSw6zH1EdKJL9LrmR/EoiGm1yDRVENrOLORL/jxL0gcuVou2Pz817F3vzLzcfIMKlIeMfhNnof0bxG1wfCUoDqHiKvalbf9XyzLI5a7yb7j/DLooPZHcUTmhqiQ7puAUtZNoovldYFJYkTx9mdFhLqgd8tAJ68yXdCJI7ZxH8+sEnMK6gPkp4JYyPd3TZF0HiE6ozTuVXq4WFveMJoHoXSPDXM0SIvl7opRh8H0OziEkJB6AT2NgPQbSHiBI8qJLiU/XDc4D3TGwHgNpDxjofvgf3eA80B0D6zGQ9oCBLvV2er3BeaA7BtZjIO0BA10avum6wXmgOwbWYyDtAQPd8Jz9c64uq9SNZCML+CJtoWvHKsjbNbnnzHZ7FEd09tiE5bkn2Q162oqzVdKLBzSScUR/IEf0ZyNoXN7Q1R8AAAD//wMAUEsDBBQABgAIAAAAIQCYI+trOwEAAFUCAAARAAgBZG9jUHJvcHMvY29yZS54bWwgogQBKKAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACUkl9LwzAUxd8Fv0PJe5u0myKh7UBlTw6ETRTfQnK3BZs/JNFu39603Wplvvh47zn53XMvKRcH1SRf4Lw0ukJ5RlACmhsh9a5CL5tleocSH5gWrDEaKnQEjxb19VXJLeXGwbMzFlyQ4JNI0p5yW6F9CJZi7PkeFPNZdOgobo1TLMTS7bBl/IPtABeE3GIFgQkWGO6AqR2J6IQUfETaT9f0AMExNKBAB4/zLMc/3gBO+T8f9MrEqWQ42rjTKe6ULfggju6Dl6OxbdusnfUxYv4cv62e1v2qqdTdrTiguhSccgcsGFevX0s8KbvTNcyHVbzyVoK4P/aOy25k9JEHEIgkhqBD5LPyOnt43CxRXZD8JiXztJhvCkJnhBbFezf01/su1NBQp9H/Ic7JhHgG1CW++Aj1NwAAAP//AwBQSwMEFAAGAAgAAAAhADT/4W2MAQAAFAMAABAACAFkb2NQcm9wcy9hcHAueG1sIKIEASigAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnJJBT+MwEIXvSPyHyHfqFBBClWOECitWYkWltHA2zqSxcGzLM0Tt/nqcRN2mu3viNjPv6fnz2OJu19qsg4jGu4LNZznLwGlfGbct2Gb94+KWZUjKVcp6BwXbA7I7eX4mVtEHiGQAsxThsGANUVhwjrqBVuEsyS4ptY+totTGLfd1bTQ8eP3ZgiN+mec3HHYEroLqIvwJZGPioqPvhlZe93z4ut6HBCzFfQjWaEXplvKX0dGjryl73Gmwgk9FkehK0J/R0F7mgk9bUWplYZmCZa0sguDHgXgC1S9tpUxEKTpadKDJxwzN77S2S5a9K4Qep2CdikY5Sli9bWyG2gakKN98/MAGgFDwZBiHQzn1TmtzLeeDIRWnxj5gBEnCKeLakAV8qVcq0n+I51PigWHkHXHKnm88c8o3XDmd9Ff20rdBub0s738KfmjEs3EfuAlr/6AIDhs9HYqyURGq9AgH/TgQT2mZ0fYhy0a5LVQHz79C//6v4yeX8+tZfpWnp53MBD9+Z/kFAAD//wMAUEsBAi0AFAAGAAgAAAAhAGLunWhhAQAAkAQAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAtVUwI/UAAABMAgAACwAAAAAAAAAAAAAAAACaAwAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEAgT6Ul/QAAAC6AgAAGgAAAAAAAAAAAAAAAADABgAAeGwvX3JlbHMvd29ya2Jvb2sueG1sLnJlbHNQSwECLQAUAAYACAAAACEA2Rd2H2MBAAA/AgAADwAAAAAAAAAAAAAAAAD0CAAAeGwvd29ya2Jvb2sueG1sUEsBAi0AFAAGAAgAAAAhANCMsAt8AAAAgQAAABQAAAAAAAAAAAAAAAAAhAoAAHhsL3NoYXJlZFN0cmluZ3MueG1sUEsBAi0AFAAGAAgAAAAhAPtipW2UBgAApxsAABMAAAAAAAAAAAAAAAAAMgsAAHhsL3RoZW1lL3RoZW1lMS54bWxQSwECLQAUAAYACAAAACEAl+L1SFsCAAA6BQAADQAAAAAAAAAAAAAAAAD3EQAAeGwvc3R5bGVzLnhtbFBLAQItABQABgAIAAAAIQDA18sKGAIAAIgFAAAYAAAAAAAAAAAAAAAAAH0UAAB4bC93b3Jrc2hlZXRzL3NoZWV0MS54bWxQSwECLQAUAAYACAAAACEAmCPrazsBAABVAgAAEQAAAAAAAAAAAAAAAADLFgAAZG9jUHJvcHMvY29yZS54bWxQSwECLQAUAAYACAAAACEANP/hbYwBAAAUAwAAEAAAAAAAAAAAAAAAAAA9GQAAZG9jUHJvcHMvYXBwLnhtbFBLBQYAAAAACgAKAIACAAD/GwAAAAA=";



		private static System.IO.Stream GetBinaryDataStream(string base64String)
			{
			return new System.IO.MemoryStream(System.Convert.FromBase64String(base64String));
			}

		private static void GetBinaryData(EmbeddedPackagePart embeddedPackagePart1)
			{

			System.IO.Stream str = GetBinaryDataStream(embeddedPackagePart1Data);

			using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
				{
				CopyStream(str, ms);
				using (SpreadsheetDocument spreadsheetDoc = SpreadsheetDocument.Open(ms, true))
					{
					// Update data in spreadsheet
					// Find first worksheet
					ss.Sheet ws = (ss.Sheet)spreadsheetDoc.WorkbookPart.Workbook.Sheets.FirstOrDefault();
					string sheetId = ws.Id;

					WorksheetPart wsp = (WorksheetPart)spreadsheetDoc.WorkbookPart.Parts.Where(pt => pt.RelationshipId == sheetId).FirstOrDefault().OpenXmlPart;
					ss.SheetData sd = wsp.Worksheet.Elements<ss.SheetData>().FirstOrDefault();
					ss.Row row = sd.Elements<ss.Row>().FirstOrDefault();
					if (row != null)
						{
						ss.CellValue cell1value = row.Elements<ss.Cell>().ElementAt(0).Elements<ss.CellValue>().FirstOrDefault();

						if (row.Elements<ss.Cell>().Where(c => c.CellReference.Value == "A5").Count() > 0)
							{
							cell1value.Text = "10".ToString();
							}
						else
							{
							ss.Cell refcell = null;
							foreach (ss.Cell cell in row.Elements<ss.Cell>())
								{
								if (String.Compare(cell.CellReference.Value, "A5", true) > 0)
									{
									refcell = cell;
									break;
									}

								}
							ss.Cell newCell = new ss.Cell() {CellReference = "A5"};
							row.InsertBefore(newCell, refcell);

							}


						//ss.CellValue cell2value = row.Elements<ss.Cell>().ElementAt(1).Elements<ss.CellValue>().FirstOrDefault();
						//cell2value.Text = "20".ToString();
						//ss.CellValue cell3value = row.Elements<ss.Cell>().ElementAt(2).Elements<ss.CellValue>().FirstOrDefault();
						//cell3value.Text = "30".ToString();
						}
					}
				//embeddedPackagePart1.FeedData(ms);
				using (System.IO.Stream s = embeddedPackagePart1.GetStream())
					ms.WriteTo(s);
				}

			}

		public static void CopyStream(System.IO.Stream input, System.IO.Stream output)
			{
			byte[] buffer = new byte[32768];
			while (true)
				{
				int read = input.Read(buffer, 0, buffer.Length);
				if (read <= 0)
					return;
				output.Write(buffer, 0, read);
				}
			}
    


		#endregion

	}


	
	
}


namespace Sa.PresTest
	{
	public class Test
		{

		public static void ChartTest2()
			{
			using (PresentationDocument doc = PresentationDocument.Open(@"C:\Development\Date1.pptx", true))
				{
				PresentationPart pr = doc.PresentationPart;

				// Get the first slide, using the GetFirstSlide method.
				SlidePart slidePart1 = GetFirstSlide(doc);
				ChartPart chartPart1 = slidePart1.ChartParts.FirstOrDefault();
				//chartPart1.ChartSpace.ChartPart.Parts();
				foreach (OpenXmlElement x in chartPart1.ChartSpace.ChildElements)
					{
					if (x.GetType() == typeof(DocumentFormat.OpenXml.Drawing.Charts.Chart))
						{
						C.Chart c = (C.Chart)x;
						C.PlotArea p = c.PlotArea;
						C.CategoryAxis ca;
						C.DateAxis dateAxis1 = new C.DateAxis(); 
						foreach (OpenXmlElement y in p.ChildElements)
							if (y.GetType() == typeof(C.CategoryAxis))
								{
								ca = (C.CategoryAxis)y;

		//						C.AxisId dateaxisid = dateAxis1.AxisId;
								C.AxisId caaxisid = ca.AxisId;
								C.Scaling scale = ca.Scaling;
								C.Delete del = ca.Delete;
								C.AxisPosition ap = ca.AxisPosition;
								C.NumberingFormat nf = ca.NumberingFormat;
									nf.SourceLinked = false;
									nf.FormatCode = "mm/dd/yy;@";
								C.MajorTickMark mt = ca.MajorTickMark;
								C.MinorTickMark mit = ca.MinorTickMark;
								C.TickLabelPosition tl = ca.TickLabelPosition;
								C.ChartShapeProperties csp = ca.ChartShapeProperties;
								C.TextProperties tp = ca.TextProperties;
									A.BodyProperties bp = tp.BodyProperties;
									bp.Rotation = -5400000;
									bp.Vertical = A.TextVerticalValues.Horizontal;
									foreach (OpenXmlElement e in tp.ChildElements)
										{
										if (e.GetType() == typeof(A.Paragraph))
											{
											A.Paragraph para = (A.Paragraph)e;
											A.ParagraphProperties paraprop = para.ParagraphProperties;
											//A.DefaultRunProperties dfr = paraprop.d
											foreach (OpenXmlElement f in paraprop.ChildElements)
												{
												if (f.GetType() == typeof(A.DefaultRunProperties))
													{
													A.DefaultRunProperties drp = (A.DefaultRunProperties)f;
													drp.FontSize = 1400;
													}
												}
											}
										}
									//A.Paragraph apara = tp.
								C.CrossingAxis cax = ca.CrossingAxis;

								C.Crosses crosses1 = new C.Crosses() { Val = C.CrossesValues.AutoZero };
								C.AutoLabeled autoLabeled1 = new C.AutoLabeled() { Val = false };
								C.LabelOffset labelOffset1 = new C.LabelOffset() { Val = (UInt16Value)100U };
								C.BaseTimeUnit baseTimeUnit1 = new C.BaseTimeUnit() { Val = C.TimeUnitValues.Days };

								C.MajorUnit majorUnit1 = new C.MajorUnit() { Val = 1D };
								C.MajorTimeUnit majorTimeUnit1 = new C.MajorTimeUnit() { Val = C.TimeUnitValues.Months };

								dateAxis1.AppendChild(caaxisid.CloneNode(true));
								dateAxis1.AppendChild(scale.CloneNode(true));
								dateAxis1.AppendChild(del.CloneNode(true));
								dateAxis1.AppendChild(ap.CloneNode(true));
								dateAxis1.AppendChild(nf.CloneNode(true));
								dateAxis1.AppendChild(mt.CloneNode(true));
								dateAxis1.AppendChild(mit.CloneNode(true));
								dateAxis1.AppendChild(tl.CloneNode(true));
								dateAxis1.AppendChild(csp.CloneNode(true));
								dateAxis1.AppendChild(tp.CloneNode(true));
								dateAxis1.AppendChild(cax.CloneNode(true));
								dateAxis1.Append(crosses1);
								dateAxis1.Append(autoLabeled1);
								dateAxis1.Append(labelOffset1);
								dateAxis1.Append(baseTimeUnit1);
								dateAxis1.Append(majorUnit1);
								dateAxis1.Append(majorTimeUnit1);

								ca.Remove();
								}

						p.Append(dateAxis1);

						}
					}
				pr.Presentation.Save();

				
				}
			}


		// Get the slide part of the first slide in the presentation document.
		public static SlidePart GetFirstSlide(PresentationDocument presentationDocument)
			{
			// Get relationship ID of the first slide
			PresentationPart part = presentationDocument.PresentationPart;
			SlideId slideId = part.Presentation.SlideIdList.GetFirstChild<SlideId>();
			string relId = slideId.RelationshipId;

			// Get the slide part by the relationship ID.
			SlidePart slidePart = (SlidePart)part.GetPartById(relId);

			return slidePart;
			}

		}

	}