﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SQL = System.Data.SqlClient;
using System.Data;
using System.Drawing;
using Sa.PresBuilder.AsposeConnector;
using Aspose.Slides.Charts;

namespace Sa.PresBuilder
{

	using ce = CustomExtensions.ExtentionMethods;

	public class PresBuilder
	{
		//this class is the final version of PresBuilder
		//use this as the class you call to build a presentation
		//it will connect to the AsposeConnector, which then connects to the Aspose library and builds the final output

		#region Variables

			#region Public

				public class CurrentState
				{
					public String status;
					public Int32 workingSlideNumber;
					public Int32 totalSlides;
            public string errMsg;
				}

				public String Option1 = "";
				public String Option2 = "";
				public String Option3 = "";
				public String Option4 = "";
				public String Option5 = "";
        public String Option8 = "";
				public String CurrencyToUse = "USD";
				public Boolean Metric = false;
				public String templateToUse;
				public String filenameToSaveAs;
				public String server = "";
				public String database = "";
				public String username = "";
				public String password = "";
                public String slideid = "";
        public String slidegroupid = "";

        public String curParmID = string.Empty;
        public String lastParmID = string.Empty;
        public String firstParmID = string.Empty;
        public System.Data.DataTable parmTable_All;

        #endregion

        #region Private

        Color defaultColor = Color.FromArgb(41, 41, 41);

				Int32 slideNumber = 0;
				Int32 slideTableRow = 0;
				Int32 slidesRemaining;

				//S.DataTable slideColorsTable = new S.DataTable("SlideColors");
				System.Data.DataTable slideAnnotationsTable = new System.Data.DataTable("SlideAnnotations");
				//S.DataTable slideLineFormatsTable = new S.DataTable("SlideLineFormats");
				//S.DataTable slideTableFormatsTable = new S.DataTable("SlideTableFormats");
				String masterTemplate = Properties.Settings.Default.MasterTemplate;
				AsposeConnect pres;
				AsposeConnect othertemplate;
				SQL.SqlConnection conn;
        System.Data.DataTable slideGroupTable;
				System.Data.DataTable slidesTable;
				System.Data.DataTable slideChartsTable;
				System.Data.DataTable slideDataTable;
				CurrentState state;
				System.ComponentModel.BackgroundWorker bgWorker;

			#endregion

		#endregion

		#region Public Functions

			public void CreatePresFromTemplate()
			{
				//this version of the CreatePresFromTemplate is for the Console version of the app
				//the Console version doesn't handle the BackgroundWorker like the Form version does
				//so, here we create one, and pass it through to the other entry point, it is just so that method has the right items called
				//nothing else is done with it, it just gets thrown away at the end
				System.ComponentModel.BackgroundWorker worker = new System.ComponentModel.BackgroundWorker();
				worker.WorkerReportsProgress = true;
				System.ComponentModel.DoWorkEventArgs e = new System.ComponentModel.DoWorkEventArgs(this);

				CreatePresFromTemplate(worker, e);
			}

			public void CreatePresFromTemplate(System.ComponentModel.BackgroundWorker worker, System.ComponentModel.DoWorkEventArgs e)
			{
				if (templateToUse == "" || filenameToSaveAs == "")//gotta have a template and a save as
				{
					return;
				}

				//Instantiate the License class
				Aspose.Slides.License license = new Aspose.Slides.License();
				license.SetLicense("Aspose.Slides.lic");

				//create a pres
				pres = new AsposeConnect(masterTemplate, true);
				pres.Option1 = Option1;
				pres.Option2 = Option2;
				pres.Option3 = Option3;
				pres.Option4 = Option4;
				pres.Option5 = Option5;
            pres.Option8 = Option8;
            pres.CurrencyToUse = CurrencyToUse;
				pres.Metric = Metric;

				//get a connection to the database
				conn = new SQL.SqlConnection("Server=" + server + ";Database=" + database + ";Trusted_Connection=True; ");
				conn.Open();

				//get some standard data from the database
				LoadStandardTables();


            if (!String.IsNullOrEmpty(slideid))
            {            //Generate one slide by slide ID, no matter this slide is active or inactive
                slidesTable = LoadDataTable("SELECT s.* FROM Slides s WHERE s.SlideID = " + slideid, "Slides");
            }
            else
            {			//loading the data tables, if All - "1", using Slides table, else using slides table and slidegroup table where slidegroup table contains the used slide ids for that slidegroup
                if (slidegroupid == "1")
                {
                    slidesTable = LoadDataTable("SELECT s.* FROM Slides s WHERE Active = 1 AND SortOrder IS NOT NULL ORDER BY ISNULL(SortOrder,99999)", "Slides");
                }
                else
                {
                    updateGroupSortOrderWhenNull(slidegroupid);
                    //slidesTable = LoadDataTable("SELECT s.* FROM Slides s  join SlideGroup g on s.SlideID = g.SlideID WHERE s.Active = 1 AND s.SortOrder IS NOT NULL AND g.GroupNoID = " + slidegroupid + " ORDER BY ISNULL(SortOrder,99999)", "Slides");
                    slidesTable = LoadDataTable("SELECT s.SlideID, s.Name, s.Title, s.SubTitle, case when g.SGSortOrder is null then s.SortOrder else g.SGSortOrder end as SortOrder, s.Active, s.Multi, s.SlideLayout, s.Comments, s.CircaYear, s.HSID FROM Slides s  join SlideGroup g on s.SlideID = g.SlideID WHERE s.Active = 1 AND s.SortOrder IS NOT NULL AND g.GroupNoID = " + slidegroupid + " ORDER BY ISNULL(g.SGSortOrder,99999)", "Slides");
                }
            }

            slideChartsTable = LoadDataTable("SELECT * FROM SlideCharts WHERE SlideID IN (SELECT SlideID FROM Slides WHERE Active = 1) ORDER BY SlideChartID", "SlideCharts");
				slideDataTable = LoadDataTable("SELECT * FROM SlideData WHERE SlideChartID IN (SELECT SlideChartID FROM SlideCharts WHERE SlideID IN (SELECT SlideID FROM Slides WHERE Active = 1)) ORDER BY SlideDataID", "SlideData");

				//create a state worker to report progress
				state = new CurrentState();
				state.totalSlides = slidesTable.Rows.Count;
				state.workingSlideNumber = slideTableRow;
				state.status = "";
				worker.ReportProgress(0, state);
				bgWorker = worker;

            //loop through each slide
            try
            {
                SlideLoop();
            }
            catch (Exception ex)
            {
                conn.Close();
                state.status = state.errMsg;
                bgWorker.ReportProgress(0, state);
                throw new Exception(ex.Message);
            }
            conn.Close();

            state.status = "Saving";
            bgWorker.ReportProgress(0, state);
            pres.Save(filenameToSaveAs);
        }

        #endregion

        #region Private Functions

        private void LoadStandardTables()
			{
				//Load some standard tables from the database
				slideAnnotationsTable = LoadDataTable("SELECT * FROM vwSlideAnnotations ORDER BY SlideID", "slideAnnotationsTable");

				pres.slideColorsTable = LoadDataTable("SELECT * FROM vwSlideColors ORDER BY SlideColorGroup, ColorOrder", "slideColorsTable");
				pres.slideLineFormatsTable = LoadDataTable("SELECT * FROM vwSlideLineFormats ORDER BY SlideLineFormatID", "slideLineFormatsTable");
				pres.slideTableFormatsTable = LoadDataTable("SELECT * FROM SlideTableFormats", "slideTableFormatsTable");
			}

			public System.Data.DataTable LoadDataTable(String SQLCommand, String tableName)
			{
				//given the name of a table and a SQL string, fill the table from the database
				SQL.SqlCommand wkgCommand = new SQL.SqlCommand(SQLCommand, conn);
				System.Data.DataTable wkgTable = new System.Data.DataTable(tableName);
				SQL.SqlDataAdapter dapSlides = new SQL.SqlDataAdapter(wkgCommand);
                dapSlides.SelectCommand.CommandTimeout = 0; 
				wkgTable.Clear();
				dapSlides.Fill(wkgTable);

				return wkgTable;
			}

        private void updateGroupSortOrderWhenNull(string groupID)
        {

            //SQL.SqlConnection conn;
            //conn = new SQL.SqlConnection(ConnectionString);
            string sqlString = "update SlideGroup set SlideGroup.SGSortOrder = slides.sortorder from slidegroup join slides on slides.SlideID = SlideGroup.SlideID WHERE slides.Active = 1 And SlideGroup.SGSortOrder IS NULL AND slides.SortOrder IS NOT NULL AND SlideGroup.GroupNoID = " + groupID;
            try
            {
                conn.Open();
                SQL.SqlCommand wkgCommand = new SQL.SqlCommand(sqlString, conn);
                wkgCommand.ExecuteNonQuery();
                //MessageBox.Show("Screen parameters deleted", "PresBuilder", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                conn.Close();
            }
        }
        private void SlideLoop()
        {
            //loop through every slide, processing as appropriate
            foreach (DataRow slideRow in slidesTable.Rows)
            {
                slideTableRow++;//this is a counter of which slide we are on, which shows the user progress
                state.errMsg = "SlideID=" + slideRow["SlideID"].ToString() + "|"; //get the information if error occures

                generateSlide(slideRow);

                state.workingSlideNumber = slideTableRow;
                bgWorker.ReportProgress(0, state);
            }
        }

        private void generateSlide(DataRow slideRow)
        {
                pres.currentSlideData.Tables.Clear();

                //multi slides are slides where the user provides a block of data, and we split it into a slide for each section, based on the first column
                //for example if you want individual slides for each unit, provide all the data at once and you'll get each unit individually output on a slide
                //note that for multi slides, a 1 means you want the slide to be multi with the first column used as the subtitle on the page, while a 2 means
                //you want the slide to be multi, but leave the existing subtitle alone
                slidesRemaining = 1;

                Int32 multiSlides = Utilities.CheckForNull(slideRow["Multi"], 0);

                if (multiSlides > 0)
                {
                    slidesRemaining = 99;//really high number that we'll never reach, this will be reset the first time we pass through a multi slide
                }

                for (Int32 slideCount = 0; slideCount < slidesRemaining; slideCount++)
                {

                    if (slideRow["Title"].ToString() == "Template") //if the title of the slide is "Template" we get a slide from the template and put it into the presentation
                    {
                        //if we haven't already loaded the template, load it now (this saves a little time for presentations that don't use the template, they never have to load it)
                        if (othertemplate == null)
                        {
                            othertemplate = new AsposeConnect(templateToUse);
                        }
                        pres.AddSlideFromAnotherPresentation(othertemplate, slideRow["SubTitle"].ToString());//here subtitle is the name of the slide to add
                    }

                    else
                    {
                        pres.AddSlide(slideRow); //add a blank slide with some header info
                    }

                    slideNumber++;

                    DataView chartView = new DataView();
                    chartView.Table = slideChartsTable;
                    chartView.RowFilter = "SlideID = " + slideRow["SlideID"].ToString();

                    System.Data.DataTable currentCharts = chartView.ToTable();

                    foreach (DataRow chartRow in currentCharts.Rows)
                    {
                        pres.FixYAxisTitlePosition();
                        pres.AddChart(chartRow); //matching data, add a chart to the slide

                        DataView dataView = new DataView();
                        dataView.Table = slideDataTable;
                        dataView.RowFilter = "SlideChartID = " + chartRow["SlideChartID"].ToString();

                        System.Data.DataTable currentData = dataView.ToTable();
                    if (currentData.Rows.Count > 0)
                    {
                        foreach (DataRow dataRow in currentData.Rows) //find any matching data rows so we can populate their data
                        {
                            WorkWithData(dataRow, slideCount, chartRow);//matching data, add data to the chart
                        }
                        pres.FormatChartAxes(chartRow);
                    }
                    else
                        continue;
                    }

                    //and a little adjustment for the legend at the bottom of the slide
                    pres.FixLegendPosition();
                    pres.FixYAxisTitlePosition();

                    //and look for annotations:
                    DataView annotationView = new DataView(slideAnnotationsTable);
                    annotationView.RowFilter = "SlideID = " + slideRow["SlideID"].ToString();

                    System.Data.DataTable currentAnnotations = annotationView.ToTable();

                //add a data row to show the slide id and slide order
                DataRow newAnnotationRow = currentAnnotations.NewRow();
                newAnnotationRow["SlideID"]= slideRow["SlideID"];
                newAnnotationRow["AnnotationText"] = "SID: " + String.Format("{0:0000}", slideRow["SlideID"]) + ", " + String.Format("{0:0000}", slideRow["SortOrder"]);
                newAnnotationRow["Left"] = 510;
                newAnnotationRow["Top"] = 520;
                newAnnotationRow["Width"] = 80;
                newAnnotationRow["Height"] = 10;
                newAnnotationRow["FontHeight"] = 5;
                newAnnotationRow["Wrap"] = 0;
                newAnnotationRow["Alignment"] = 2;
                newAnnotationRow["Color"] = "000000";
                newAnnotationRow["BackColor"] = 0;
                currentAnnotations.Rows.Add(newAnnotationRow);

                foreach (DataRow annotationRow in currentAnnotations.Rows)
                    {
                        pres.AddAnnotation(annotationRow);
                    }
                }
        }
        private void WorkWithData(DataRow dataRow, Int32 slideCount, DataRow chartRow)
		{
            //try
            //{
                //slidecount is the slide we're working on, which will be >0 only for multi slides (and is used to parse the correct data)

                String thisDataRowNum = dataRow["SlideDataID"].ToString();

                System.Data.DataTable queryDataTable = new System.Data.DataTable();
                Boolean dataFound = false;

                foreach (System.Data.DataTable dt in pres.currentSlideData.Tables)
                {
                    if (dt.TableName == thisDataRowNum)
                    {
                        queryDataTable = dt.Copy();
                        dataFound = true;
                    }
                }

                if (dataFound == false)
                {
                    //get the data table, which we cache into a dataset so that we're not pulling the same query again and again for multis
                    String sqlString = MakeDataSQLString(dataRow);

                    //store the necessary information when error occurs
                    if (state.errMsg.Length > 0)
                        state.errMsg = state.errMsg.Substring(0, state.errMsg.IndexOf("|") + 1) + " function call is: " + sqlString;

                    queryDataTable = LoadDataTable(sqlString, "QueryData");
                    queryDataTable.TableName = thisDataRowNum;
                    pres.currentSlideData.Tables.Add(queryDataTable);

                    foreach (System.Data.DataTable dt in pres.currentSlideData.Tables)
                    {
                        if (dt.TableName == thisDataRowNum)
                        {
                            queryDataTable = dt.Copy();

                            if (queryDataTable.Columns.Contains("SortOrder1"))
                            {
                                System.Data.DataView queryDataView = new System.Data.DataView();
                                queryDataView = queryDataTable.DefaultView;
                                queryDataView.Sort = "SortOrder1 ASC, SortOrder2 DESC";
                                //queryDataTable.Clear();
                                queryDataTable = queryDataView.ToTable();
                                queryDataTable.Columns.Remove("SortOrder1");
                                queryDataTable.Columns.Remove("SortOrder2");
                            }
                        }
                    }
                }

                Int32 MultiData = Utilities.CheckForNull(dataRow["Multi"], 0);

                if (MultiData > 0)
                {
                    //break out the appropriate multi data
                    Tuple<System.Data.DataTable, String> tup = GetMultiData(queryDataTable, slideCount, dataRow);
                    queryDataTable = tup.Item1;
                    String slideToMake = tup.Item2;

                    if (MultiData == 1)//if it is 1, we want to use the unit name for the subtitle of the slide
                    {
                        if (slideToMake != "")
                        {
                            pres.ResetSubTitle(slideToMake);
                        }
                    }
                }

                pres.AddDataToChart(dataRow, chartRow, queryDataTable);
            //}
            //catch (Exception e)
            //{
            //    state.errMsg = state.errMsg + e.Source + e.Message;
            //}
				
	}

			private Tuple<System.Data.DataTable, String> GetMultiData(System.Data.DataTable queryDataTable, Int32 slideCount, DataRow dataRow)
			{

				//get the distinct values in the first column
				DataView view = new DataView(queryDataTable);
				String colname = queryDataTable.Columns[0].ColumnName;
				System.Data.DataTable distinctRows = view.ToTable(true, colname);

                string slideToMake = "";

                //if (distinctRows.Rows.Count == 0 || slideCount >= distinctRows.Rows.Count)
                //{
                //    return Tuple.Create(queryDataTable, slideToMake);
                //}

                //commented out the below condition - LW. 
                //Already know it is the multi slides. 
                //With the condition, the remaining slide only be set in the initial queryDataTable in the SlideData table. 
                //if the other queryDataTable in SlideData table has different row count, still use the first queryDataTable count which will cause errors
             
                //if (slidesRemaining == 99) //we haven't reset the value yet, so change it here
                //{
                    slidesRemaining = distinctRows.Rows.Count;
				//}


				if (distinctRows.Rows.Count > 0)
				{
					//TODO: next row assumes that the distinctRows always returns the same order, so uses slideCount to get the latest it needs. Is this correct?
					slideToMake = distinctRows.Rows[slideCount].ItemArray[0].ToString();//the unit name

					switch (dataRow["DataType"].ToString())
					{

						case "DistPoints":
						case "ScatLine":
						case "LineMarker":
						case "Bar":
						case "Line":
						case "Bar100":
						case "BarCluster":
						case "BarHoriz":
						case "Area":
						case "Scatter":
						case "Table":
							queryDataTable = Utilities.SplitMultiData(queryDataTable, slideToMake);
							break;
						default:
							break;

						//TODO: Multis for Pie?
					}

				}

				return Tuple.Create(queryDataTable, slideToMake);

			}

			private String fieldReplace(String input)
			{

				input = input.Replace("@CurrencyCode", CurrencyToUse);
				input = input.Replace("@Option1", Option1);
				input = input.Replace("@Option2", Option2);
				input = input.Replace("@Option3", Option3);
				input = input.Replace("@Option4", Option4);
				input = input.Replace("@Option5", Option5);
            input = input.Replace("@Option8", Option8);

            if (Metric == true)
				{
					input = input.Replace("@Btu", "MJ");
					input = input.Replace("@MBtu", "GJ");
				}
				else
				{
					input = input.Replace("@Btu", "Btu");
					input = input.Replace("@MBtu", "MBtu");
				}

				return input;
			}

			private String MakeDataSQLString(DataRow dataRow)
			{

				if (dataRow["DataSource"].ToString().Trim() == "")
				{
					throw new ArgumentException("DataSource cannot be blank or Null");
				}

				//get the data
				String sqlString = "SELECT * FROM " + dataRow["DataSource"].ToString() + "(";
				if (dataRow["Filter1"] != DBNull.Value) { sqlString += "'" + dataRow["Filter1"].ToString() + "'"; }//we assume that filter1 is populated first
				if (dataRow["Filter2"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter2"].ToString() + "'"; }
				if (dataRow["Filter3"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter3"].ToString() + "'"; }
				if (dataRow["Filter4"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter4"].ToString() + "'"; }
				if (dataRow["Filter5"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter5"].ToString() + "'"; }
				if (dataRow["Filter6"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter6"].ToString() + "'"; }
				if (dataRow["Filter7"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter7"].ToString() + "'"; }
            if (dataRow["Filter8"] != DBNull.Value) { sqlString += ",'" + dataRow["Filter8"].ToString() + "'"; }

            sqlString += ")";

				sqlString = fieldReplace(sqlString);
				if (Metric == true)
				{
					sqlString = sqlString.Replace("'@Metric'", "1");
				}
				else
				{
					sqlString = sqlString.Replace("'@Metric'", "0");
				}


				return sqlString;
			}

		#endregion

	}
}
